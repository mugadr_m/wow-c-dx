package ch.cromon.wow.ui

import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px


class Styles : Stylesheet() {
    companion object {
        val header by cssclass()
        val viewWrapper by cssclass()
        val vp5 by cssclass()
    }

    init {
        viewWrapper {
            padding = box(10.px)
            spacing = 10.px
        }

        vp5 {
            padding = box(0.px, 0.px, 5.px, 0.px)
        }

        label {
            fontFamily = "Segoe UI"

            and(header) {
                fontSize = 30.px
                fontWeight = FontWeight.BOLD
            }
        }

        textArea {
            fontFamily = "Consolas"
        }
    }
}