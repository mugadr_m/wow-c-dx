package ch.cromon.wow.ui.controller

import ch.cromon.wow.io.VirtualFileSystem
import ch.cromon.wow.steps.BuildOrder
import ch.cromon.wow.steps.BuildStep
import ch.cromon.wow.steps.ExecutionContext
import ch.cromon.wow.utils.ShellExecutor
import ch.cromon.wow.vs.VsShellExecutor
import javafx.collections.FXCollections
import org.reflections.Reflections
import tornadofx.Controller
import tornadofx.chooseDirectory
import tornadofx.runLater


class MainController : Controller() {
    val buildStepList = FXCollections.observableArrayList<String>()

    fun browseForVSFolder() =
            chooseDirectory {
                title = "Choose Visual Studio"
            }

    fun browseForBuildFolder() =
            chooseDirectory {
                title = "Choose Build Folder"
            }

    fun validateVSPath(path: String): Boolean {
        val ret = VsShellExecutor(path).evaluate("echo !VSCMD_VER!")
        if (ret.first != 0) {
            return false
        }

        val output = ret.second
        return output.split("\r\n").any {
            it.startsWith("15.")
        }
    }

    fun build(path: String, buildTarget: String, textCallback: (String) -> Unit) {
        val shell = VsShellExecutor(path)
        val fileSystem = VirtualFileSystem(buildTarget)
        fileSystem.makeDirectory("share")
        fileSystem.makeDirectory("bin")

        val reflection = Reflections(BuildStep::class.java.`package`.name)
        val buildSteps = reflection.getSubTypesOf(BuildStep::class.java)
                .filter { it.isAnnotationPresent(BuildOrder::class.java) }
                .sortedBy {
                    it.getAnnotation(BuildOrder::class.java).order
                }.map {
                    it.newInstance()
                }

        buildSteps.mapTo(buildStepList) {
            it.name
        }

        val context = ExecutionContext(fileSystem, shell, ShellExecutor(), null)
        buildSteps.forEach {
            if (!it.execute(context, textCallback)) {
                return
            }

            runLater {
                buildStepList.removeAt(0)
            }
        }
    }
}