package ch.cromon.wow.ui

import ch.cromon.wow.ui.controller.MainController
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import tornadofx.*


class MainView : View("WoW Edit Build Tool") {
    private val controller: MainController by inject()

    private lateinit var vsFolderTextField: TextField
    private lateinit var buildStepOutputField: TextArea

    private lateinit var buildFolderTextField: TextField

    private val folderInputDisabledProperty = SimpleBooleanProperty(false)
    private val isVsPathSelected = SimpleBooleanProperty(false)

    private val buildFolderInputDisabledProperty = SimpleBooleanProperty(false)
    private val buildFolderSelected = SimpleBooleanProperty(false)

    override val root = vbox {
        addClass(Styles.viewWrapper)

        prefWidth = 1024.0
        prefHeight = 800.0

        label {
            addClass(Styles.header)
            text = "Welcome to WoW Edit Build Tool"
        }

        vbox {
            visibleProperty().bind(isVsPathSelected.not())
            managedProperty().bind(isVsPathSelected.not())

            label {
                text = "Step 1: Select location of Visual Studio 2017"
            }

            label {
                text = "For Example: C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community"
            }

            vbox {
                addClass(Styles.vp5)
            }

            hbox {
                vsFolderTextField = textfield {
                    prefWidth = 500.0
                    disableProperty().bind(folderInputDisabledProperty)
                }
                button {
                    text = "..."
                    disableProperty().bind(folderInputDisabledProperty)
                    action {
                        vsFolderTextField.text = controller.browseForVSFolder()?.absolutePath
                    }
                }
            }

            vbox {
                addClass(Styles.vp5)
            }

            button {
                text = "Validate"

                action {
                    folderInputDisabledProperty.set(true)
                    runAsync {
                        controller.validateVSPath(vsFolderTextField.text)
                    } ui {
                        isVsPathSelected.set(it)
                    }
                }
            }
        }
        vbox {
            visibleProperty().bind(isVsPathSelected)
            managedProperty().bind(isVsPathSelected)

            label {
                text = "Step 2: Start the build!"
            }

            hbox {
                buildFolderTextField = textfield {
                    prefWidth = 500.0
                    disableProperty().bind(buildFolderInputDisabledProperty)
                }
                button {
                    text = "..."
                    disableProperty().bind(buildFolderInputDisabledProperty)
                    action {
                        buildFolderTextField.text = controller.browseForBuildFolder()?.absolutePath
                        buildFolderSelected.set(buildFolderTextField.text?.isNotEmpty() != false)
                    }
                }
            }

            vbox {
                addClass(Styles.vp5)
            }

            button {
                text = "Build"
                disableProperty().bind(buildFolderSelected.not())

                action {
                    buildFolderInputDisabledProperty.set(true)
                    runAsync {
                        controller.build(vsFolderTextField.text,
                                buildFolderTextField.text, {
                            runLater {
                                buildStepOutputField.appendText(it)
                                val text = buildStepOutputField.text
                                if (text.length > 10000) {
                                    buildStepOutputField.text = text.substring(text.length - 5000, text.length)
                                }

                                buildStepOutputField.scrollTop = Double.MAX_VALUE
                            }
                        })
                    }
                }
            }

            hbox {
                fillWidthProperty().set(true)

                listview(controller.buildStepList) {
                    prefWidth = 300.0
                }

                buildStepOutputField = textarea {
                    prefWidth = 700.0
                    editableProperty().set(false)
                }
            }
        }
    }

}