package ch.cromon.wow.steps

import java.io.File

@BuildOrder(10)
class DirectoryLinkStep : BuildStep {
    override val name = "Library Folder Links"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.fileSystem.makeDirectory("share/wow-c-dx/third_party")
        return createFolderLink(context, "share/wow-c-dx/third_party/boost/lib/x64", "Release", "share/boost_1_67_0/lib64-msvc-14.1", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/boost", "include", "share/boost_1_67_0", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/cef/x64/Release", "lib", "share/cef_distribution/libs", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/cef/x64/Release", "include", "share/cef_distribution/include", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/curl/x64/Release", "lib", "share/curl/build.install/lib/Release", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/curl/x64/Release", "include", "share/curl/include/curl", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party", "rapidjson", "share/rapidjson", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party", "rapidxml", "share/rapidxml", textCallback) &&
                createFolderLink(context, "share/wow-c-dx/third_party/zlib/lib/x64", "Release", "share/zlib-1.2.11/build.install/Release", textCallback) &&
                handleZlibInclude(context, textCallback)
    }

    private fun createFolderLink(context: ExecutionContext, folder: String, from: String, to: String, textCallback: (String) -> Unit): Boolean {
        context.fileSystem.delete(File(folder, from).path)
        context.fileSystem.makeDirectory(folder)
        val homeDir = context.fileSystem.resolve(folder)
        val command = "mklink /D $from ${context.fileSystem.resolve(to)}"
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }

    private fun handleZlibInclude(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.fileSystem.makeDirectory("share/zlib-1.2.11/build.install/include")
        context.fileSystem.copyFile("share/zlib-1.2.11/build.install/zconf.h", "share/zlib-1.2.11/build.install/include/zconf.h")
        context.fileSystem.copyFile("share/zlib-1.2.11/zlib.h", "share/zlib-1.2.11/build.install/include/zlib.h")
        return createFolderLink(context, "share/wow-c-dx/third_party/zlib", "include", "share/zlib-1.2.11/build.install/include", textCallback)
    }
}