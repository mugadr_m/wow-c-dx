package ch.cromon.wow.steps

@BuildOrder(11)
class ProjectBulildStep : BuildStep {
    override val name = "Build Project"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        val homeDir = context.fileSystem.resolve("share/wow-c-dx")
        return context.vsShell.evaluateLive(homeDir, "msbuild wow-cef-dx.sln /p:WindowsTargetPlatformVersion=!WindowsSDKLibVersion! /p:PlatformToolset=v141 /p:Configuration=Release") {
            textCallback(it)
        } == 0
    }

}