package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString

@BuildOrder(0)
class GitDownloadStep : BuildStep {
    companion object {
        private const val GIT_URL = "https://bitbucket.org/mugadr_m/wow-c-dx/downloads/PortableGit.zip"
    }

    override val name = "Download GIT"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        val ret = context.vsShell.evaluate("git version")
        if (ret.first == 0) {
            context.gitPath = "git"
            textCallback("Git found in PATH${System.lineSeparator()}")
            return true
        }

        if (context.fileSystem.exists("share/PortableGit/bin/git.exe")) {
            context.gitPath = context.fileSystem.resolve("share/PortableGit/bin/git.exe")
            textCallback("Found git: ${context.gitPath}${System.lineSeparator()}")
            return true
        }

        try {
            textCallback("Downloading $GIT_URL${System.lineSeparator()}")
            HttpHelper.downloadFile(GIT_URL, "share/PortableGit.zip", context.fileSystem)

            textCallback("Extracting portable_git...${System.lineSeparator()}")
            ZipHelper.extractNonExisting("share/PortableGit.zip", "share", context.fileSystem) {
                textCallback("Unpacking: $it${System.lineSeparator()}")
            }

            context.gitPath = context.fileSystem.resolve("share/PortableGit/bin/git.exe")
        } catch (e: Exception) {
            textCallback("Error downloading GIT: ${e.writeToString()}${System.lineSeparator()}")
            return false
        }

        textCallback("Complete${System.lineSeparator()}")
        return true
    }

}