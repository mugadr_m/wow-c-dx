package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString

@BuildOrder(9)
class ZlibBuildStep : BuildStep {
    companion object {
        private const val DOWNLOAD_URL = "https://zlib.net/zlib1211.zip"
    }

    override val name = "Build ZLib"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        return downloadZlib(context, textCallback) && cmakeSetup(context, textCallback) && cmakeBuild(context, textCallback)
    }

    private fun downloadZlib(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        return try {
            textCallback("Downloading $DOWNLOAD_URL${System.lineSeparator()}")
            HttpHelper.downloadFile(DOWNLOAD_URL, "share/zlib1211.zip", context.fileSystem)
            ZipHelper.extractNonExisting("share/zlib1211.zip", "share", context.fileSystem)  {
                textCallback("Unpack: $it${System.lineSeparator()}")
            }
            true
        } catch (e: Exception) {
            textCallback("Error downloading zlib: ${e.writeToString()}")
            false
        }
    }

    private fun cmakeSetup(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.cmakePath ?: return false
        val vfs = context.fileSystem
        vfs.makeDirectory("share/zlib-1.2.11/build.install")
        val homeDir = vfs.resolve("share/zlib-1.2.11/build.install")
        val command = "${context.cmakePath} -DCMAKE_CONFIGURATION_TYPES=Release -DCMAKE_BUILD_TYPE=Release -G \"Visual Studio 15 2017 Win64\" .."
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }

    private fun cmakeBuild(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.cmakePath ?: return false
        val vfs = context.fileSystem

        val homeDir = vfs.resolve("share/zlib-1.2.11/build.install")
        val command = "msbuild zlib.sln /p:PlatformToolset=v141"
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }

}