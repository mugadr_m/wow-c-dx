package ch.cromon.wow.steps

import ch.cromon.wow.io.VirtualFileSystem
import ch.cromon.wow.utils.toFile
import ch.cromon.wow.vs.VsShellExecutor

@BuildOrder(2)
class BoostBuildStep : BuildStep {
    override val name = "Build Boost 1.67.0"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.fileSystem.makeDirectory("share/boost_1_67_0")
        val baseDir = "share/boost_1_67_0"
        val baseDirAbs = context.fileSystem.resolve(baseDir)
        textCallback("Setting up boost binaries. Setup will not have any output, do not be confused if there is no output for a while now!")
        context.vsShell.evaluateLive(context.fileSystem.resolve("share"), "boost_1_67_0.exe /VERYSILENT /SP- /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /DIR=\"$baseDirAbs\"") {
            textCallback(it + System.lineSeparator())
        }

        return true
    }

    private fun build(fileSystem: VirtualFileSystem, shell: VsShellExecutor, textCallback: (String) -> Unit): Boolean {
        val b2 = fileSystem.resolve("share/boost_1_67_0/b2.exe")
        val homeDir = fileSystem.resolve("share/boost_1_67_0")

        val prefix = fileSystem.resolve("share/boost_1_67_0/build.install")
        val file = prefix.toFile()
        file.mkdirs()
        file.setReadable(true, false)
        file.setWritable(true, false)
        file.setExecutable(true, false)
        val params = arrayOf(
                "threading=multi",
                "variant=release",
                "link=static",
                "runtime-link=shared",
                "address-model=64"
        ).joinToString(" ")

        val retCode = shell.evaluateLive(homeDir, "$b2 --prefix=\"$prefix\" $params install") {
            textCallback(it)
        }

        return retCode == 0
    }

    private fun bootstrap(fileSystem: VirtualFileSystem, shell: VsShellExecutor, textCallback: (String) -> Unit): Boolean {
        if(fileSystem.exists("share/boost_1_67_0/b2.exe")) {
            return true
        }

        val bootstrap = fileSystem.resolve("share/boost_1_67_0/bootstrap.bat")
        val retCode = shell.evaluateLive(fileSystem.resolve("share/boost_1_67_0"), "$bootstrap msvc") {
            textCallback(it)
        }

        return retCode == 0
    }

}