package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString

@BuildOrder(3)
class CefDownloadStep : BuildStep {
    companion object {
        private const val CEF_DOWNLOAD_URL = "https://bitbucket.org/mugadr_m/wow-c-dx/downloads/cef_distribution.zip"
    }

    override val name: String = "Download CEF"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        try {
            textCallback("Downloading $CEF_DOWNLOAD_URL...${System.lineSeparator()}")
            HttpHelper.downloadFile(CEF_DOWNLOAD_URL, "share/cef_distribution.zip", context.fileSystem)

            textCallback("Extracting cef_distribution...${System.lineSeparator()}")
            ZipHelper.extractNonExisting("share/cef_distribution.zip", "share", context.fileSystem)  {
                textCallback("Unpack: $it${System.lineSeparator()}")
            }
        } catch (e: Exception) {
            textCallback("Error downloading CEF: ${e.writeToString()}${System.lineSeparator()}")
            return false
        }

        textCallback("Complete${System.lineSeparator()}")
        return true
    }

}