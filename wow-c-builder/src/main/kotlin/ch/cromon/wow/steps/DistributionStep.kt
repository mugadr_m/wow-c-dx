package ch.cromon.wow.steps

import java.io.File

@BuildOrder(12)
class DistributionStep : BuildStep {
    override val name = "Create Distribution"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        textCallback("Copying binaries${System.lineSeparator()}")
        context.fileSystem.makeDirectory("bin/x64/Release")
        context.fileSystem.copyDirectory("share/wow-c-dx/bin/x64/Release", "bin/x64/Release")
        context.fileSystem.copyDirectory("share/cef_distribution/bin", "bin/x64/Release")
        textCallback("Linking UI${System.lineSeparator()}")
        if(!createFolderLink(context, "bin/x64/Release", "UI", "share/wow-c-dx/UI", textCallback)) {
            return false
        }

        textCallback("Linking Definitions${System.lineSeparator()}")
        if(!createFolderLink(context, "bin/x64/Release", "Definitions", "share/wow-c-dx/Definitions", textCallback)) {
            return false
        }

        return true
    }

    private fun createFolderLink(context: ExecutionContext, folder: String, from: String, to: String, textCallback: (String) -> Unit): Boolean {
        context.fileSystem.delete(File(folder, from).path)
        context.fileSystem.makeDirectory(folder)
        val homeDir = context.fileSystem.resolve(folder)
        val command = "mklink /D $from ${context.fileSystem.resolve(to)}"
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }
}