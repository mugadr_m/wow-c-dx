package ch.cromon.wow.steps

@BuildOrder(4)
class ProjectCloneStep : BuildStep {
    companion object {
        private const val PROJECT_URL = "https://bitbucket.org/mugadr_m/wow-c-dx.git"
    }

    override val name = "Clone Project"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        val gitPath = context.gitPath ?: return false
        if (gitPath.isBlank()) {
            return false
        }

        if(isInvalidDirectory(context)) {
            context.fileSystem.delete("share/wow-c-dx")
        }

        val (command, homeDir) = if (!repositoryExists(context)) {
            "$gitPath clone $PROJECT_URL ${context.fileSystem.resolve("share/wow-c-dx")}" to context.fileSystem.resolve("share")
        } else {
            "$gitPath pull origin master" to context.fileSystem.resolve("share/wow-c-dx")
        }

        val retVal = context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        }

        return retVal == 0
    }

    private fun repositoryExists(context: ExecutionContext) = context.fileSystem.exists("share/wow-c-dx/.git")

    private fun isInvalidDirectory(context: ExecutionContext) = context.fileSystem.exists("share/wow-c-dx") && !context.fileSystem.exists("share/wow-c-dx/.git")
}