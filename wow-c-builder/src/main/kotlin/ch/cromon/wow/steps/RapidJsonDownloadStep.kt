package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString


@BuildOrder(5)
class RapidJsonDownloadStep : BuildStep {
    companion object {
        private const val DOWNLOAD_URL = "https://bitbucket.org/mugadr_m/wow-c-dx/downloads/rapidjson.zip"
    }

    override val name = "Download Rapid JSON"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        textCallback("Downloading $DOWNLOAD_URL${System.lineSeparator()}")
        try {
            HttpHelper.downloadFile(DOWNLOAD_URL, "share/rapidjson.zip", context.fileSystem)
            ZipHelper.extractNonExisting("share/rapidjson.zip", "share", context.fileSystem)  {
                textCallback("Unpack: $it${System.lineSeparator()}")
            }
        } catch (e: Exception) {
            textCallback("Error downloading rapidjson: ${e.writeToString()}")
            return false
        }

        return true
    }
}