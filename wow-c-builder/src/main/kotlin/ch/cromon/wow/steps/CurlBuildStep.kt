package ch.cromon.wow.steps

@BuildOrder(8)
class CurlBuildStep : BuildStep {
    companion object {
        private const val REPO_URL = "https://github.com/curl/curl.git"
    }

    override val name = "Build Curl"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        if(!cloneOrUpdateCurl(context, textCallback)) {
            return false
        }

        return cmakeSetup(context, textCallback) && cmakeBuild(context, textCallback)
    }

    private fun cloneOrUpdateCurl(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.gitPath ?: return false

        if(isInvalidDirectory(context)) {
            context.fileSystem.delete("share/curl")
        }

        val vfs = context.fileSystem
        val hasRepo = vfs.exists("share/curl/.git")
        val (homeDir, command) = if(hasRepo) {
            vfs.resolve("share/curl") to "${context.gitPath} pull origin master"
        } else {
            vfs.resolve("share") to "${context.gitPath} clone $REPO_URL ${context.fileSystem.resolve("share/curl")}"
        }

        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }

    private fun isInvalidDirectory(context: ExecutionContext) = context.fileSystem.exists("share/curl") && !context.fileSystem.exists("share/curl/.git")

    private fun cmakeSetup(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.cmakePath ?: return false
        val vfs = context.fileSystem
        vfs.makeDirectory("share/curl/build.install")
        val homeDir = vfs.resolve("share/curl/build.install")
        val command = "${context.cmakePath} -DCMAKE_CONFIGURATION_TYPES=Release -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=Release -G \"Visual Studio 15 2017 Win64\" .."
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }

    private fun cmakeBuild(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        context.cmakePath ?: return false
        val vfs = context.fileSystem

        val homeDir = vfs.resolve("share/curl/build.install")
        val command = "msbuild CURL.sln /p:PlatformToolset=v141"
        return context.vsShell.evaluateLive(homeDir, command) {
            textCallback(it)
        } == 0
    }
}