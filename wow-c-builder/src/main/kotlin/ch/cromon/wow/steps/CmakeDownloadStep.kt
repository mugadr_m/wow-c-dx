package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString

@BuildOrder(7)
class CmakeDownloadStep : BuildStep {
    companion object {
        private const val DOWNLOAD_URL = "https://cmake.org/files/v3.12/cmake-3.12.0-win64-x64.zip"
    }

    override val name = "Setup CMake"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        val ret = context.vsShell.evaluate("cmake --version")
        if(ret.first == 0) {
            textCallback(ret.second + System.lineSeparator())
            context.cmakePath = "cmake"
            return true
        }

        try {
            textCallback("Downloading $DOWNLOAD_URL${System.lineSeparator()}")
            HttpHelper.downloadFile(DOWNLOAD_URL, "share/cmake-3.12.0-win64-x64.zip", context.fileSystem)
            ZipHelper.extractNonExisting("share/cmake-3.12.0-win64-x64.zip", "share", context.fileSystem)  {
                textCallback("Unpack: $it${System.lineSeparator()}")
            }
            context.cmakePath = context.fileSystem.resolve("share/cmake-3.12.0-win64-x64/bin/cmake.exe")
        } catch (e: Exception) {
            textCallback("Error setting up CMake: ${e.writeToString()}")
            return false
        }

        return true
    }

}