package ch.cromon.wow.steps

import ch.cromon.wow.io.VirtualFileSystem
import ch.cromon.wow.utils.ShellExecutor
import ch.cromon.wow.vs.VsShellExecutor


data class ExecutionContext(
        val fileSystem: VirtualFileSystem,
        val vsShell: VsShellExecutor,
        val shell: ShellExecutor,
        var gitPath: String? = null,
        var cmakePath: String? = null
)