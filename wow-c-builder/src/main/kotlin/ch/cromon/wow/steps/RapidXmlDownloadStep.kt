package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.ZipHelper
import ch.cromon.wow.utils.writeToString


@BuildOrder(6)
class RapidXmlDownloadStep : BuildStep {
    companion object {
        private const val DOWNLOAD_URL = "https://bitbucket.org/mugadr_m/wow-c-dx/downloads/rapidxml.zip"
    }

    override val name = "Download Rapid XML"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        textCallback("Downloading $DOWNLOAD_URL${System.lineSeparator()}")
        try {
            HttpHelper.downloadFile(DOWNLOAD_URL, "share/rapidxml.zip", context.fileSystem)
            ZipHelper.extractNonExisting("share/rapidxml.zip", "share", context.fileSystem)  {
                textCallback("Unpack: $it${System.lineSeparator()}")
            }
        } catch (e: Exception) {
            textCallback("Error downloading rapidxml: ${e.writeToString()}")
            return false
        }

        return true
    }
}