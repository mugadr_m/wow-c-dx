package ch.cromon.wow.steps

import ch.cromon.wow.utils.HttpHelper
import ch.cromon.wow.utils.writeToString

@BuildOrder(1)
class BoostDownloadStep : BuildStep {
    companion object {
        private const val BOOST_URL = "https://kent.dl.sourceforge.net/project/boost/boost-binaries/1.67.0/boost_1_67_0-msvc-14.1-64.exe"
    }

    override val name = "Download Boost 1.67"

    override fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean {
        try {
            textCallback("Downloading $BOOST_URL...${System.lineSeparator()}")
            HttpHelper.downloadFile(BOOST_URL, "share/boost_1_67_0.exe", context.fileSystem)
        } catch (e: Exception) {
            textCallback("Error downloading Boost: ${e.writeToString()}${System.lineSeparator()}")
            return false
        }

        textCallback("Complete${System.lineSeparator()}")
        return true
    }

}