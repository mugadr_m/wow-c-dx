package ch.cromon.wow.steps


interface BuildStep {
    val name: String
    fun execute(context: ExecutionContext, textCallback: (String) -> Unit): Boolean
}