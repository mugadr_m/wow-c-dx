package ch.cromon.wow.utils

import ch.cromon.wow.io.VirtualFileSystem
import net.lingala.zip4j.core.ZipFile
import org.apache.commons.io.IOUtils
import java.io.File


object ZipHelper {
    fun extractZip(inputFiles: Collection<String>, outputFolder: String, vfs: VirtualFileSystem) {
        val zipFile = ZipFile(vfs.resolve(inputFiles.first()))
        zipFile.extractAll(vfs.resolve(outputFolder))
        inputFiles.forEach(vfs::delete)
    }

    fun extractNonExisting(inputFile: String, outputFolder: String, vfs: VirtualFileSystem, fileCallback: ((String) -> Unit)? = null) {
        val basePath = File(outputFolder)
        java.util.zip.ZipFile(vfs.resolve(inputFile)).use { zip ->
            zip.entries().toList().forEach { entry ->
                if (!entry.isDirectory) {
                    fileCallback?.invoke(entry.name)
                    val curPath = File(basePath, entry.name)
                    if (!vfs.exists(curPath.path)) {
                        vfs.openWrite(curPath.path).use { output ->
                            zip.getInputStream(entry).use {
                                IOUtils.copy(it, output)
                            }
                        }
                    }
                }
            }
        }

        vfs.delete(inputFile)
    }
}