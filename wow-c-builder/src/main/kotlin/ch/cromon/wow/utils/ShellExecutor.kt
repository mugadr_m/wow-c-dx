package ch.cromon.wow.utils

import org.apache.commons.io.IOUtils
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets


class ShellExecutor {
    fun evaluate(vararg commands: String): Pair<Int, String> {
        val process = ProcessBuilder("cmd.exe", "/V", "/C", "\"call ${commands.joinToString(" && ")}\"")
                .redirectErrorStream(true)
                .start()

        process.waitFor()
        val exitCode = process.exitValue()
        val output = IOUtils.toString(process.inputStream, StandardCharsets.ISO_8859_1)

        return exitCode to output
    }

    fun evaluateLive(directory: String, vararg commands: String, outputCallback: (String) -> Unit): Int {
        val process = ProcessBuilder("cmd.exe",  "/V", "/C", "\"call ${commands.joinToString(" && ")}\"")
                .directory(File(directory))
                .redirectErrorStream(true)
                .start()

        var isRunning = true
        val readerThread = process.inputStream.let {
            val reader = BufferedReader(InputStreamReader(it, StandardCharsets.ISO_8859_1))
            Thread {
                while(isRunning) {
                    if(it.available() == 0) {
                        Thread.sleep(1)
                        continue
                    }

                    outputCallback(reader.readLine() + System.lineSeparator())
                }
            }
        }

        readerThread.start()

        process.waitFor()
        isRunning = false
        readerThread.join()

        return process.exitValue()
    }
}