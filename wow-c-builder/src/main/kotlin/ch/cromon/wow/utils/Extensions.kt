package ch.cromon.wow.utils

import java.io.File
import java.io.PrintWriter
import java.io.StringWriter


fun String.toFile(): File = File(this)

fun Throwable.writeToString(): String {
    val writer = StringWriter()
    this.printStackTrace(PrintWriter(writer))
    return writer.buffer.toString()
}