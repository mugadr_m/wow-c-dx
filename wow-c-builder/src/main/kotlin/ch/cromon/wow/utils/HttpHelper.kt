package ch.cromon.wow.utils

import ch.cromon.wow.io.VirtualFileSystem
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.io.IOUtils
import java.io.IOException


object HttpHelper {
    fun downloadFile(url: String, target: String, fileSystem: VirtualFileSystem, errorCallback: ((Exception) -> Unit)? = null) {
        val client = OkHttpClient.Builder().followRedirects(true).build()

        try {
            val request = Request.Builder().url(url).build()
            val call = client.newCall(request)
            val response = call.execute()
            if(!response.isSuccessful) {
                throw IOException("Unable to download file $url because: ${response.message()}")
            }

            val body = response.body() ?: throw IOException("No response received for $url")

            fileSystem.openWrite(target).use { targetStrm ->
                body.byteStream().use {
                    IOUtils.copy(it, targetStrm)
                }
            }
        } catch (e: Exception) {
            if (errorCallback != null) {
                errorCallback(e)
            } else {
                throw e
            }
        }
    }
}