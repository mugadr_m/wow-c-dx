package ch.cromon.wow.io

import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


class VirtualFileSystem(basePath: String) {
    private val baseFile = File(basePath)

    init {
        baseFile.mkdirs()
        baseFile.setReadable(true, false)
        baseFile.setWritable(true, false)
        baseFile.setExecutable(true, false)
    }

    fun makeDirectory(path: String) {
        val file = File(baseFile, path)
        file.mkdirs()
        file.setReadable(true, false)
        file.setWritable(true, false)
        file.setExecutable(true, false)
    }

    fun copyFile(from: String, to: String) {
        val fromFile = File(baseFile, from)
        val toFile = File(baseFile, to)

        FileInputStream(fromFile).use {input ->
            FileOutputStream(toFile).use {
                IOUtils.copy(input, it)
            }
        }
    }

    fun copyDirectory(from: String, to: String) {
        val fromFile = File(baseFile, from)
        val toFile = File(baseFile, to)
        FileUtils.copyDirectory(fromFile, toFile)
    }

    fun resolve(path: String): String = File(baseFile, path).absolutePath

    fun exists(path: String) = File(baseFile, path).exists()

    fun openRead(path: String) = FileInputStream(File(baseFile, path))

    fun openWrite(path: String): FileOutputStream {
        val finalFile = File(baseFile, path)
        val parent = finalFile.parentFile
        parent.mkdirs()
        parent.setReadable(true, false)
        parent.setWritable(true, false)
        parent.setExecutable(true, false)
        return FileOutputStream(finalFile)
    }

    fun delete(path: String) {
        val file = File(baseFile, path)
        if(file.isDirectory) {
            FileUtils.deleteDirectory(file)
        } else {
            File(baseFile, path).delete()
        }
    }
}