package ch.cromon.wow

import ch.cromon.wow.ui.MainView
import ch.cromon.wow.ui.Styles
import javafx.application.Application
import tornadofx.App

class WoWCBuilder : App(MainView::class, Styles::class)

fun main(vararg args: String) {
    Application.launch(WoWCBuilder::class.java, *args)
}