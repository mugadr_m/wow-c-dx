cmake_minimum_required(VERSION 3.12)
project(wow_c_dx)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
message(${CMAKE_MODULE_PATH})

set(CMAKE_CXX_STANDARD 14)

add_subdirectory(web-subprocess)
add_subdirectory(wow-cef-dx)
