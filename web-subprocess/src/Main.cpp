#include "stdafx.h"
#include "WebApplication.h"

int main(int argc, const char* argv[]) {
	CefMainArgs mainArgs;
	mainArgs.instance = GetModuleHandle(nullptr);

	const auto webApplication = CefRefPtr<WebApplication>(new WebApplication());
	CefExecuteProcess(mainArgs, webApplication, nullptr);
}
