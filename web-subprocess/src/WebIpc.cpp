#include "stdafx.h"
#include "WebIpc.h"
#include <mutex>

const std::shared_ptr<WebIpc> IPC = std::make_shared<WebIpc>();

void WebIpc::initialize(uint32_t port) {
	static std::once_flag flag;
    std::call_once(flag, [=]() {
		WSADATA wsaData{};
		const auto ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (ret != 0) {
			throw std::exception("Error initializing Windows Sockets");
		}
	});

    if(mSocket != INVALID_SOCKET) {
		closesocket(mSocket);
    }

	mSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(mSocket == INVALID_SOCKET) {
		LOG(ERROR) << "Unable to create IPC socket: " << WSAGetLastError();
		throw std::exception();
    }

	SOCKADDR_IN sin;
	memset(&sin, 0, sizeof sin);
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	sin.sin_port = htons(port);

	const auto rc = connect(mSocket, reinterpret_cast<const sockaddr*>(&sin), sizeof sin);
    if(rc == SOCKET_ERROR) {
		LOG(ERROR) << "Unable to connect IPC socket: " << WSAGetLastError();
		throw std::exception();
    }

	LOG(INFO) << "IPC socket connected";
}

void WebIpc::onFocusedNodeChanged(bool hasNode) const {
    char buffer[] = {
        0x05, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        hasNode ? 1 : 0
	};

    const auto opcode = static_cast<uint32_t>(IpcOpcode::FOCUS_NODE_CHANGE);
	memcpy(&buffer + 4, &opcode, sizeof opcode);
	send(mSocket, buffer, sizeof buffer, 0);
}
