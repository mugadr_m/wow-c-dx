#include "stdafx.h"
#include "WebApplication.h"
#include "WebIpc.h"

void WebApplication::OnBeforeCommandLineProcessing(const CefString& process_type,
    CefRefPtr<CefCommandLine> command_line) {
    if(process_type.ToString() != "renderer") {
		return;
    }

    const auto port = std::stoi(command_line->GetSwitchValue("web-ipc-port").ToString());
	IPC->initialize(port);
}

void WebApplication::OnWebKitInitialized() {
	CefMessageRouterConfig config;
	config.js_query_function = "queryBrowser";
	mRouter = CefMessageRouterRendererSide::Create(config);
}

void WebApplication::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	CefRefPtr<CefV8Context> context) {
	mRouter->OnContextCreated(browser, frame, context);
}

void WebApplication::OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	CefRefPtr<CefV8Context> context) {
	mRouter->OnContextReleased(browser, frame, context);
}

bool WebApplication::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
	CefRefPtr<CefProcessMessage> message) {
	return mRouter->OnProcessMessageReceived(browser, source_process, message);
}

void WebApplication::OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
    CefRefPtr<CefDOMNode> node) {
	IPC->onFocusedNodeChanged(node != nullptr);
}
