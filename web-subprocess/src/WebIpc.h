#pragma once

enum class IpcOpcode : uint32_t {
	FOCUS_NODE_CHANGE
};

class WebIpc {
	SOCKET mSocket = INVALID_SOCKET;

public:
	void initialize(uint32_t port);

	void onFocusedNodeChanged(bool hasNode) const;
};

extern const std::shared_ptr<WebIpc> IPC;