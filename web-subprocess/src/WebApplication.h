#pragma once

#include <cef_app.h>
#include <wrapper/cef_message_router.h>

class WebApplication : public CefApp, public CefRenderProcessHandler {
	IMPLEMENT_REFCOUNTING(WebApplication);

	CefRefPtr<CefMessageRouterRendererSide> mRouter;

public:
    void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line) override;
	void OnWebKitInitialized() override;
	void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
	override;
	void OnContextReleased(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
	override;
	bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process,
		CefRefPtr<CefProcessMessage> message) override;

    void OnFocusedNodeChanged(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, 
		CefRefPtr<CefDOMNode> node) override;

	CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override { return this; }
};