$(document).ready(() => {
    let mapList = [];

    eventManager.submitTask('MapListQueryEvent', {}).then((result) => {
        mapList = result.maps;
        const rootElem = $('.map-list');
        for (let i = 0; i < result.maps.length; ++i) {
            const map = result.maps[i];
            const liElem = $('<li><img/><span class="map-name"></span></li>');
            liElem.find('.map-name').text(map.mapName);
            liElem.find('img').attr('src', map.loadingScreen + '?width=150&height=150');
            liElem[0].mapElement = map;
            liElem[0].searchMapName = map.mapName.toLowerCase();
            rootElem.append(liElem);
        }

        rootElem.find('li').click((e) => {
            const liElem = $(e.target).closest('li');
            const map = liElem[0].mapElement;
            console.log(`Selected map: ${map.mapName}`);
            localStorage.setItem("selectedMap", JSON.stringify(map));
            window.location = "map-entry-select.html";
        });
    });

    $('#mapListFilterInput').keydown(() => {
        setTimeout(updateVisibleMaps, 10);
    });

    const allowElements = $('#allowNormal, #allowInstance, #allowRaid, #allowBattleground, #allowArena');
    const allowAll = $('#allowAll');

    allowAll.change(() => {
        const isChecked = allowAll.prop('checked');
        if(isChecked) {
            allowElements.prop('disabled', true);
        } else {
            allowElements.prop('disabled', null);
        }

        updateVisibleMaps();
    });

    allowElements.change(updateVisibleMaps);

    function updateVisibleMaps() {
        const filterText = $('#mapListFilterInput').val().toLowerCase();
        const listElements = $('.map-list li');
        const allowAllMaps = $('#allowAll').prop('checked');
        const allowNormal = $('#allowNormal').prop('checked');
        const allowInstance = $('#allowInstance').prop('checked');
        const allowRaid = $('#allowRaid').prop('checked');
        const allowPvp = $('#allowBattleground').prop('checked');
        const allowArena = $('#allowArena').prop('checked');

        async.each(listElements, function(item, callback) {
            const elem = $(item);
            const instanceType = elem[0].mapElement.instanceType;
            let typeVisible = false;
            if(!allowAllMaps) {
                switch(instanceType) {
                    case 0: {
                        typeVisible = allowNormal;
                        break;
                    }

                    case 1: {
                        typeVisible = allowInstance;
                        break;
                    }

                    case 2: {
                        typeVisible = allowRaid;
                        break;
                    }

                    case 3: {
                        typeVisible = allowPvp;
                        break;
                    }

                    case 4: {
                        typeVisible = allowArena;
                        break;
                    }
                }
            } else {
                typeVisible = true;
            }

            const isVisible = typeVisible && (filterText.length === 0 || elem[0].searchMapName.indexOf(filterText) >= 0);
            if(!isVisible) {
                elem.addClass('element-hidden');
            } else {
                elem.removeClass('element-hidden');
            }
            callback();
        });
    }
});