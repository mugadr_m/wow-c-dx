$(document).ready(() => {
    $('.split-pane > .collapse-pane > .header > a').click((e) => {
        const collapsePane = $(e.target).closest('.collapse-pane');
        collapsePane.toggleClass('collapsed');
    });

    $('.split-pane > .collapse-pane > .commands > .command').click((e) => {
        const elem = $(e.target).closest('.command');
        const eventBinding = elem.attr('data-click');
        if (!eventBinding) {
            return;
        }

        const commandParent = elem.closest(".commands");
        const controller = commandParent.attr('data-binding');
        if (!controller) {
            return;
        }

        window[controller][eventBinding]();
    })
});

(function () {
    let ctrlDown = false;
    let shiftDown = false;
    let rDown = false;

    $(window).keydown((event) => {
        switch (event.keyCode) {
            case 82: {
                rDown = true;
                break;
            }

            case 16: {
                shiftDown = true;
                break;
            }

            case 17: {
                ctrlDown = true;
                break;
            }
        }

        if (ctrlDown && shiftDown && rDown) {
            location.reload();
        }
    });

    $(window).keyup((event) => {
        switch (event.keyCode) {
            case 82: {
                rDown = false;
                break;
            }

            case 16: {
                shiftDown = false;
                break;
            }

            case 17: {
                ctrlDown = false;
                break;
            }
        }
    });
})();
