$(document).ready(() => {
    const mapSize = 64 * (533.0 + 1.0 / 3.0);
    const tileSize = 64 * 256;
    const halfMapSize = 32 * (533.0 + 1.0 / 3.0);

    const selectedMap = JSON.parse(localStorage.getItem("selectedMap"));
    const map = new L.Map("map-entry-select", {crs: L.CRS.Simple, minZoom: 3, maxZoom: 6});
    L.tileLayer('map-entry://localhost/' + selectedMap.mapId + "/{z}/{x}/{y}", {
        minZoom: 3,
        maxZoom: 6
    }).addTo(map);
    const sw = map.unproject([0, tileSize], map.getMaxZoom());
    const ne = map.unproject([tileSize, 0], map.getMaxZoom());

    const key = "lastMapEntry" + selectedMap.mapId;
    console.log("Reading:", key);
    let lastEntry = JSON.parse(localStorage.getItem(key));
    if (!lastEntry) {
        lastEntry = map.unproject([tileSize / 2, tileSize / 2], map.getMaxZoom())
    }

    map.setMaxBounds(new L.LatLngBounds(sw, ne));
    map.setView(lastEntry, 6);

    map.on('mousemove', (e) => {
        let relPos = map.project(e.latlng, map.getMaxZoom());
        relPos.x /= tileSize;
        relPos.y /= tileSize;
        const offsetX = relPos.x;
        const offsetY = relPos.y;
        relPos.x *= mapSize;
        relPos.y *= mapSize;

        const adtX = Math.floor(offsetX * 64);
        const adtY = Math.floor(offsetY * 64);
        $('#adtLabelX').text(adtX);
        $('#adtLabelY').text(adtY);
        $('#positionX').text(relPos.x.toFixed(3));
        $('#positionY').text(relPos.y.toFixed(3));
    });

    map.on('click', (e) => {
        let relPos = map.project(e.latlng, map.getMaxZoom());
        relPos.x /= tileSize;
        relPos.y /= tileSize;
        relPos.x *= mapSize;
        relPos.y *= mapSize;

        let popup = L.popup();
        popup
            .setLatLng(e.latlng)
            .setContent(`<h4>Position: ${relPos.x.toFixed(2)} / ${relPos.y.toFixed(2)}</h4><button class='btn btn-info' 
onclick="onEnterWorld(${relPos.x}, ${relPos.y}, ${e.latlng.lat}, ${e.latlng.lng})">Enter World!</button>`)
            .openOn(map);
    });

    window.onEnterWorld = function (x, y, lat, lng) {
        console.log("Entering world at:", x, y);
        eventManager.submitTask("EnterWorldEvent", {
            mapName: selectedMap.mapDirectory,
            mapId: selectedMap.mapId,
            x: x,
            y: y
        }).then(() => {
            const key = "lastMapEntry" + selectedMap.mapId;
            const value = JSON.stringify(new L.LatLng(lat, lng));
            console.log("Storing:", key, "->", value);
            localStorage.setItem(key, value);
            window.location = 'world.html';
        });
    };

    eventManager.submitTask('GetMapPoiEvent', {mapId: selectedMap.mapId}).then((result) => {
        for (let i = 0; i < result.pois.length; ++i) {
            const poi = result.pois[i];
            let x = poi.x;
            let y = poi.y;

            x = ((-x / halfMapSize) + 1) / 2.0;
            y = ((-y / halfMapSize) + 1) / 2.0;

            L.marker(map.unproject([y * tileSize, x * tileSize], map.getMaxZoom()), {}).addTo(map).bindPopup(poi.name);
        }
    });

    window.mainMap = map;

    $('#backButton').click(() => {
        window.location = "map-select.html"
    })
});