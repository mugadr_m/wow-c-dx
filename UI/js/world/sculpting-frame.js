$(document).ready(() => {
    const frame = $('#terrain-editing-frame');

    frame.find('.slider-item-row .form-min-value').keypress((e) => {
        if (e.which !== 13) {
            return;
        }

        const elem = $(e.target);
        let minValue = parseInt(elem.text());
        if (isNaN(minValue)) {
            return false;
        }

        const maxElem = elem.siblings('.form-max-value');
        if (!maxElem) {
            return;
        }

        const maxValue = parseInt(maxElem.text());
        if (isNaN(maxValue)) {
            return;
        }

        if (minValue >= maxValue) {
            minValue = maxValue - 1;
            elem.text(minValue);
        }
    });

    frame.find('.slider-item-row .form-max-value').keypress((e) => {
        if (e.which !== 13) {
            return;
        }

        const elem = $(e.target);
        let maxValue = parseInt(elem.text());
        if (isNaN(maxValue)) {
            return false;
        }

        const minElem = elem.siblings('.form-min-value');
        if (!minElem) {
            return;
        }

        const minValue = parseInt(minElem.text());
        if (isNaN(minValue)) {
            return;
        }

        if (maxValue <= minValue) {
            maxValue = minValue + 1;
            elem.text(maxValue);
        }
    });

    frame.find('.slider-item-row .form-max-value, .slider-item-row .form-min-value').keypress((e) => {
        if (e.which !== 13) {
            return;
        }

        const parent = $(e.target).closest('.slider-item-row');
        const minElem = parent.find('.form-min-value');
        const maxElem = parent.find('.form-max-value');
        if (!minElem || !maxElem) {
            return;
        }

        const minValue = parseInt(minElem.text());
        const maxValue = parseInt(maxElem.text());
        if (isNaN(minValue) || isNaN(maxValue)) {
            return;
        }

        const slider = parent.find('input.slider');
        slider.attr('min', minValue);
        slider.attr('max', maxValue);
    });

    frame.find('.editable').click((e) => {
        const elem = $(e.target);
        elem.attr('contenteditable', true);
        elem.focus();
    }).keypress((e) => {
        const elem = $(e.target);
        if (e.which === 13) {
            elem.attr('contenteditable', false);
            return false;
        }

        return e.which >= 0x30 && e.which <= 0x39;
    }).on('focus', (e) => {
        const sliderRow = $(e.target).closest('.slider-item-row');
        const key = sliderRow.attr('data-property-name');
        if (!key) {
            return;
        }

        localStorage.setItem(key + '.visited', JSON.stringify(true));
        sliderRow.find('sup.form-help-icon').css({
            display: 'none'
        });
    });

    frame.find('.slider-item-row').each((i, v) => {
        const row = $(v);
        const key = row.attr('data-property-name') + '.visited';
        const value = localStorage.getItem(key);
        if (!value || !JSON.parse(value)) {
            return;
        }

        row.find('sup.form-help-icon').css({
            display: 'none'
        });
    });

    $('.slider-item-row input.slider').on('input', (e) => {
        const elem = $(e.target);
        const prop = elem.closest('.slider-item-row');
        if (!prop) {
            return;
        }

        eventManager.submitTask('SetPropertyEvent', { key: prop.attr('data-property-name'), value: parseFloat(elem.val()) + '' })
    });

    $('#terrain-editing--show-vertices').change((e) => {
       const elem = $(e.target);
       
    });
});