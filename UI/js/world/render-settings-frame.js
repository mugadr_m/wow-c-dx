$(document).ready(() => {
    const specular = $('#render-settings--specular-lighting');
    const texScales = $('#render-settings--texture-scaling');
    const heights = $('#render-settings--height-textures');
    const specularIntensity = $('#render-settings--specular-intensity');

    function updateValues() {
        eventManager.submitTask('RenderSettingsChangedEvent', {
            specular: specular.prop('checked') ? 1.0 : 0.0,
            textureScales: texScales.prop('checked') ? 1.0 : 0.0,
            heightTextures: heights.prop('checked') ? 1.0 : 0.0,
            specularIntensity: parseFloat(specularIntensity.val())
        })
    }

    specular.change(updateValues);
    texScales.change(updateValues);
    heights.change(updateValues);
    specularIntensity.on('input', updateValues);
});