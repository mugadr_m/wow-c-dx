$(document).ready(() => {
    const expandElement = $('#left-bar--expand-arrow-click');
    const leftBar = $('#left-bar');

    expandElement.click(() => {
        expandElement.find('span.fa').toggleClass('fa-angle-right').toggleClass('fa-angle-left');
        leftBar.toggleClass('expanded');
    });

    $('.icon-row[data-frame-id]').click((e) => {
        const frame = $(e.target).closest('.icon-row').attr('data-frame-id');
        if(!frame) {
            return;
        }

        const frameElement = $(frame);
        new Frame(frameElement).toggleVisibility();
    });
});