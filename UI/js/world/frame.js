$(document).ready(() => {
    const frames = $('.frame');
    frames.each((i, v) => {
        $(v).css({
            'z-index': 9999 - i
        })
    });

    frames.mousedown((e) => {
        const frame = $(e.target).closest('.frame');
        bringToFront(frame);
    });

    $('.frame-header').mousedown((e) => {
        const target = $(e.target);
        if(target.hasClass('close-icon')) {
            return;
        }

        const frame = $(e.target).closest('.frame');
        const offset = frame.offset();
        const dx = e.pageX - offset.left;
        const dy = e.pageY - offset.top;

        frame[0].clickData = {
            x: dx,
            y: dy
        };

        frame.addClass('is-clicked');
    });

    $(document).mouseup(() => {
        $('.frame').removeClass('is-clicked');
    });

    $(document).mousemove(function(e) {
        $('.frame.is-clicked').each(function() {
            const x = e.pageX - this.clickData.x;
            const y = e.pageY - this.clickData.y;

            $(this).offset({top: y, left: x});
        });
    });

    $('.frame-header > .close-icon').click((e) => {
        const frame = $(e.target).closest('.frame');
        new Frame(frame).hide();
    });

    function bringToFront(frame) {
        const oldIndex = parseInt(frame.css('z-index'));
        frame.css({
            'z-index': 9999
        });

        frames.each((i, v) => {
            if(v === frame[0]) {
                return;
            }

            const index = parseInt($(v).css('z-index'));
            if(index > oldIndex) {
                $(v).css({
                    'z-index': (index - 1)
                })
            }
        })
    }

    window.Frame = class Frame {
        constructor(element) {
            this.element = element;
        }

        show() {
            this.element.removeClass('hidden');
        }

        hide() {
            this.element.addClass('hidden');
        }

        toggleVisibility() {
            this.element.toggleClass('hidden');
            if(!this.element.hasClass('hidden')) {
                bringToFront(this.element);
            }
        }
    };
});