$(document).ready(() => {
    function onUpdatePosition() {
        eventManager.submitTask('GetPositionInfoEvent', {}).then((result) => {
            $('#footer--position-x-label').text(result.position.x.toFixed(3));
            $('#footer--position-y-label').text(result.position.y.toFixed(3));
            $('#footer--position-z-label').text(result.position.z.toFixed(3));
            $('#footer--position-map-name-label').text(result.map.name);
            $('#footer--position-map-id-label').text(result.map.id);
            $('#footer--position-zone-name-label').text(result.area.name);
            $('#footer--position-zone-id-label').text(result.area.id);

            if(!result.intersection.terrain.hasHit) {
                $('#footer--position-mouse-hover').text('<no intersection>');
            } else {
                const hit = result.intersection.terrain;
                $('#footer--position-mouse-hover').text(`(${hit.x.toFixed(3)}/${hit.y.toFixed(3)}/${hit.z.toFixed(3)})`);
            }
        });
    }

    setInterval(onUpdatePosition, 30);
});