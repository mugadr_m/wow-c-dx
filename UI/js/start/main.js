class DataSourceBarController {
    hideAllPages() {
        const pages = $('#contentArea > div');
        pages.addClass('element-hidden');
    };

    openCDN() {
        this.hideAllPages();
        $('#contentArea .content-configuration.cdn').removeClass('element-hidden');
    }

    openCASCHost() {
        this.hideAllPages();
        $('#contentArea .content-configuration.casc-host').removeClass('element-hidden');
    }

    openLocal() {
        this.hideAllPages();
        $('#contentArea .content-configuration.local').removeClass('element-hidden');
    }

    openExtracted() {
        this.hideAllPages();
        $('#contentArea .content-configuration.extracted').removeClass('element-hidden');
    }
}

$(document).ready(() => {
    window.DataSourceBar = new DataSourceBarController();

    onInitializeCDN();
});

