(function (global) {
    function onBuildSelected(e) {
        const selectedItem = $(e.target).closest('li');
        const build = selectedItem[0].buildInfo;
        const locale = selectedItem[0].localeFlag;
        eventManager.submitTask('OpenFromCdnEvent', {buildInfo: build, locale: locale});
    }

    function onLoadBuilds(gameType, locale) {
        $('.build-load-indicator').css({
            display: 'block'
        });

        $('#loadBuildsButton').prop('disabled', true);

        eventManager.submitTask('CdnGetBuildVersionsEvent', {gameType: gameType}).then((response) => {
            $('.build-load-indicator').css({
                display: 'none'
            });

            const buildView = $('#build-list-view');
            const buildItemView = buildView.find('.build-list');
            buildItemView.empty();
            const itemTemplate = buildView.find('.item-template li');
            for (let i = 0; i < response.builds.length; ++i) {
                const build = response.builds[i];
                const item = itemTemplate.clone();
                item.find('.region-flag .flag-icon').addClass('flag-icon-' + build.regionName);
                item.find('.version-name').text(build.versionName);
                item.find('.build-config-key').text("Config: " + build.buildConfigKey);
                item.find('.cdn-config-key').text("CDN: " + build.cdnConfigKey);
                item[0].buildInfo = build;
                item[0].localeFlag = locale;
                buildItemView.append(item);
                item.click(onBuildSelected);
            }

            $('#loadBuildsButton').prop('disabled', null);
        })
    };

    global.onInitializeCDN = function () {
        setInterval(() => {
            const loadIndicator = $('.build-load-indicator');
            let curText = loadIndicator.text();
            const idx = curText.indexOf('.');
            let numDots = idx >= 0 ? (curText.length - idx) : 0;
            numDots = (numDots + 1) % 4;
            loadIndicator.text((idx >= 0 ? curText.substr(0, idx) : curText) + '.'.repeat(numDots));
        }, 500);

        $('#loadBuildsButton').click(() => {
            onLoadBuilds($('#gameTypeSelect').val(), parseInt($('#localeSelect').val()));
        });
    }
})(window);