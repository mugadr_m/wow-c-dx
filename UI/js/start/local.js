$(document).ready(() => {
    const openFolderButton = $('#openLocalFolderButton');
    const parent = openFolderButton.closest('.local');

    openFolderButton.click(() => {
        openFolderButton.prop('disabled', true);
        eventManager.submitTask('OpenLocalFolderEvent', {}).then(() => {
        });
    });

    eventManager.registerEvent('OpenLocalFolderResultEvent', (result) => {
        eventManager.submitTask('GetLocalBuildInfoEvent', { path: result.path }).then((result) => {
            openFolderButton.prop('disabled', null);
            const container = parent.find('.local-info-container .local-build-list-view');
            const itemTemplate = parent.find('.local-info-container > .item-template li');
            container.empty();

            for(let i = 0; i < result.builds.length; ++i) {
                const build = result.builds[i];
                const item = itemTemplate.clone();
                item.find('div.version-name').text(build.version);
                item.find('.region-flag .flag-icon').addClass('flag-icon-' + build.branch);
                item.find('.last-used').text("Last used: " + new Date(build.lastUsed).toLocaleString());
                item.find('.active').text('Active: ' + build.active);
                item.find('.build-key').text("Key: " + build.buildKey);
                container.append(item);
                item[0].buildKey = build.buildKey;
                item[0].path = result.path;
                item[0].buildId = build.buildId;

                item.click((e) => {
                    const item = $(e.target).closest('li');
                    const buildKey = item[0].buildKey;
                    const path = item[0].path;
                    const buildId = item[0].buildId;
                    console.log("Using build", buildKey);
                    eventManager.submitTask('OpenLocalBuildEvent', {
                        buildKey: buildKey,
                        path: path,
                        buildId: buildId
                    }).then(() => {});
                })
            }

            parent.find('.local-info-container').removeClass('element-hidden');
        });
    });
});