class EventManager {
    constructor() {
        this.eventHandlers = {};

        queryBrowser({
            request: 'EventHandlerInit',
            persistent: true,
            onSuccess: (data) => {
                const element = JSON.parse(data);
                const type = element.type;
                const body = element.body;
                const handlers = this.eventHandlers[type];
                if(handlers) {
                    handlers.forEach((handler) => handler(body));
                }
            }
        });
    }

    registerEvent(type, callback) {
        let handlers = this.eventHandlers[type];
        if(!handlers) {
            handlers = [];
        }
        handlers.push(callback);
        this.eventHandlers[type] = handlers;
    }

    submitTask(type, payload) {
        return new Promise((resolve, reject) => {
            queryBrowser({
                request: JSON.stringify({type: type, request: payload}),
                onSuccess: (data) => {
                    const response = JSON.parse(data);
                    if (response.type !== type) {
                        console.warn("Received non-matching response for event:", response.type, " vs ", type);
                    }

                    resolve(response.body);
                },
                onFailure: (errorCode, errorString) => {
                    reject(errorCode, errorString);
                }
            })
        });
    }
}

window.eventManager = new EventManager();