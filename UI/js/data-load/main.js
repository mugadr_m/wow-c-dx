$(document).ready(() => {
   eventManager.registerEvent('DataLoadProgressEvent', (event) => {
        $('#progressLabel').text(event.message);
   });

   eventManager.registerEvent('DataInitCompleteEvent', (event) => {
       window.location = "map-select.html";
   });
});