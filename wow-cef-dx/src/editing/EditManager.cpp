#include "stdafx.h"
#include "EditManager.h"
#include "utils/PropertyStore.h"

namespace wowc {
    namespace editing {

        EditManager::EditManager(scene::GlobalShaderBufferManager& bufferManager) : mBufferManager(bufferManager) {
        }

        void EditManager::postConstruct(const cdi::ApplicationContextPtr& ctx) {
			const auto propStore = ctx->produce<utils::PropertyStore>();

			mInnerRadiusBinding = propStore->bind<float>("terrain.editing.innerRadius");
			mOuterRadiusBinding = propStore->bind<float>("terrain.editing.outerRadius");
			mShowVerticesBinding = propStore->bind<bool>("terrain.editing.show-vertices");
        }

        void EditManager::onFrame(scene::Scene& scene) {
            if(mInnerRadiusBinding->hasChanged() || mOuterRadiusBinding->hasChanged()) {
				mBufferManager.updateBrushRadius(mInnerRadiusBinding->getValue(), mOuterRadiusBinding->getValue());
            }

			scene.setShowTerrainVertices(mShowVerticesBinding->getValue());
        }
    }
}
