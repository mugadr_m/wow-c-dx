#pragma once

#include "scene/GlobalShaderBufferManager.h"
#include "utils/PropertyStore.h"
#include "scene/Scene.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
    namespace editing {
        class EditManager {
			scene::GlobalShaderBufferManager& mBufferManager;

			utils::PropertyBindingPtr<float> mInnerRadiusBinding;
			utils::PropertyBindingPtr<float> mOuterRadiusBinding;
			utils::PropertyBindingPtr<bool> mShowVerticesBinding;

        public:
			explicit EditManager(scene::GlobalShaderBufferManager& bufferManager);

			void postConstruct(const cdi::ApplicationContextPtr& ctx);

			void onFrame(scene::Scene& scene);
        };

		SHARED_PTR(EditManager);
    }
}