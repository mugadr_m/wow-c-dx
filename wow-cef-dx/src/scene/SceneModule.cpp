#include "stdafx.h"
#include "SceneModule.h"
#include "cdi/ApplicationContext.h"
#include "WorldFrame.h"
#include "terrain/TerrainMaterialManager.h"
#include "sky/SkySphere.h"
#include "liquid/LiquidManager.h"
#include "m2/M2MaterialManager.h"
#include "m2/M2ModelManager.h"
#include "m2/M2AnimationManager.h"

namespace wowc {
    namespace scene {

        void SceneModule::initialize() {
			const auto context = cdi::ApplicationContext::instance();
			context->registerSingleton<sky::SkySphere>();

			context->registerSingleton<terrain::TerrainMaterialManager>();
			context->registerSingleton<liquid::LiquidManager>();

			context->registerSingleton<m2::M2AnimationManager>();
			context->registerSingleton<m2::M2ModelManager>();
			context->registerSingleton<m2::M2MaterialManager>();

			context->registerSingleton<WorldFrame>();
        }

        void SceneModule::destroy() {
			const auto context = cdi::ApplicationContext::instance();
			context->produce<WorldFrame>()->shutdown();
        }

        void SceneModule::initUiEvents() {
			const auto context = cdi::ApplicationContext::instance();
			context->produce<WorldFrame>()->attachUiEvents(context, context->produce<ui::events::EventManager>());

			terrain::MapManager::attachUiEvents(context);
        }
    }
}
