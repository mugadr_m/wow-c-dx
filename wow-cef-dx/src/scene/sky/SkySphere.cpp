#include "stdafx.h"
#include "SkySphere.h"
#include "utils/Constants.h"
#include "res/resource1.h"
#include "io/files/sky/SkyColorType.h"
#include "math/Vector3.h"

namespace wowc {
    namespace scene {
        namespace sky {

            SkySphere::SkySphere() : mSkyColorBuffer(360) {
            }

            void SkySphere::onFrame() {
                if(!mIsValid) {
					return;
                }

                if(mIsDirty) {
					mSkyTexture->updateArgb8(1, 360, mSkyColorBuffer);
					mIsDirty = false;
                }

				mMesh->setTexture(gx::ShaderType::PIXEL, 0, mSkyTexture);
				mMesh->setSampler(gx::ShaderType::PIXEL, 0, mSampler);
				mMesh->beginFrame();
				mMesh->draw();
            }

            void SkySphere::initialize(const gx::GxContextPtr& gxContext) {
				mMesh = gxContext->createMesh();
				mVertexBuffer = gxContext->createVertexBuffer(false);
				mIndexBuffer = gxContext->createIndexBuffer(gx::IndexType::UINT32, false);
				mSkyTexture = gxContext->createTexture();

				mMesh->setVertexBuffer(mVertexBuffer);
				mMesh->setIndexBuffer(mIndexBuffer);

				auto blendState = gxContext->createBlendState();
				blendState->setBlendEnabled(false);
				mMesh->setBlendState(blendState);

				mSampler = gxContext->createSampler();
				mSampler->setTextureFilter(D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT);
				mSampler->setTextureAddressU(gx::TextureAddressMode::CLAMP);
				mSampler->setTextureAddressV(gx::TextureAddressMode::CLAMP);

				mMesh->addElement("POSITION", 0, 3);
				mMesh->addElement("TEXCOORD", 0, 2);

				const auto program = gxContext->createProgram();
				program->loadFromResources(IDR_SKY_BOX_SHADER_VERTEX, IDR_SKY_BOX_SHADER_PIXEL);
				mMesh->setProgram(program);

				mMesh->finalize();
            }

#pragma pack(push, 1)
			struct Vertex {
				float x, y, z;
				float tx, ty;

				Vertex(float x, float y, float z, float tx, float ty) :
					x(x), y(y), z(z), tx(tx), ty(ty) {

				}
			};
#pragma pack(pop)

			// ReSharper disable CppInconsistentNaming
            void SkySphere::updateRadius(float radius) {
				const auto rings = 100u;
				const auto sectors = 100u;

				const auto R = 1.0f / (rings - 1);
				const auto S = 1.0f / (sectors - 1);

				std::vector<Vertex> vertexData;
				vertexData.reserve(rings * sectors);
				std::vector<uint32_t> indexData(rings * sectors * 6);
				auto counter = 0u;

                for(auto r = 0u; r < rings; ++r) {
                    for(auto s = 0u; s < sectors; ++s) {
						using utils::PI;
						using std::sinf;
						using std::cosf;

						const auto z = sinf(-PI / 2.0f + PI * r * R);
						const auto x = cosf(2 * PI * s * S) * sinf(PI * r * R);
						const auto y = sinf(2 * PI * s * S) * sinf(PI * r * R);

						vertexData.emplace_back(
							x * radius, y * radius, z * radius,
							s * S, r * R
						);

                        if(r < (rings - 1) && s < (sectors - 1)) {
							indexData[counter++] = r * sectors + s;
							indexData[counter++] = (r + 1) * sectors + s + 1;
							indexData[counter++] = r * sectors + s + 1;
							indexData[counter++] = r * sectors + s;
							indexData[counter++] = (r + 1) * sectors + s;
							indexData[counter++] = (r + 1) * sectors + s + 1;
                        }
                    }
                }

				mVertexBuffer->updateData(vertexData);
				mIndexBuffer->updateData(indexData);
				mMesh->setIndexCount(static_cast<uint32_t>(indexData.size()));
				mIsValid = true;
            }

            void SkySphere::updateColors(const std::vector<uint32_t>& colors) {
				if (colors.size() <= static_cast<std::size_t>(io::files::sky::SkyColorType::FOG_COLOR)) {
					return;
				}

				const auto color0 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::SKY_COLOR0)]);
				const auto color1 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::SKY_COLOR1)]);
				const auto color2 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::SKY_COLOR2)]);
				const auto color3 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::SKY_COLOR3)]);
				const auto color4 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::SKY_COLOR4)]);
				const auto color5 = math::Vector3::fromXrgb(colors[static_cast<uint32_t>(io::files::sky::SkyColorType::FOG_COLOR)]);

				const auto fogColor = color5.toXrgb();
                for(auto i = 0u; i < 160u; ++i) {
					mSkyColorBuffer[i] = fogColor;
                }

                for(auto i = 160u; i < 180u; ++i) {
					const auto sat = (i - 160.0f) / 20.0f;
					mSkyColorBuffer[i] = (color4 * sat + color5 * (1 - sat)).toXrgb();
                }

                for(auto i = 180u; i < 190u; ++i) {
					const auto sat = (i - 180.0f) / 10.0f;
					mSkyColorBuffer[i] = (color3 * sat + color4 * (1 - sat)).toXrgb();
                }

                for(auto i = 190u; i < 210u; ++i) {
					const auto sat = (i - 190.0f) / 20.0f;
					mSkyColorBuffer[i] = (color2 * sat + color3 * (1 - sat)).toXrgb();
                }

                for(auto i = 210u; i < 240u; ++i) {
					const auto sat = (i - 210.0f) / 30.0f;
					mSkyColorBuffer[i] = (color1 * sat + color2 * (1 - sat)).toXrgb();
                }

                for(auto i = 240u; i < 360u; ++i) {
					const auto sat = (i - 240.0f) / 120.0f;
					mSkyColorBuffer[i] = (color0 * sat + color1 * (1 - sat)).toXrgb();
                }

				mIsDirty = true;
            }
        }
    }
}
