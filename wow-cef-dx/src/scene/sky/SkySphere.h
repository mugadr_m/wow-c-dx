#pragma once

#include <gx/Mesh.h>
#include "gx/GxContext.h"

namespace wowc {
    namespace scene {
        namespace sky {
            class SkySphere {
				gx::MeshPtr mMesh;
				gx::VertexBufferPtr mVertexBuffer;
				gx::IndexBufferPtr mIndexBuffer;
				gx::SamplerPtr mSampler;

				gx::TexturePtr mSkyTexture;

				std::vector<uint32_t> mSkyColorBuffer;
				bool mIsDirty = false;

				bool mIsValid = false;

            public:
				SkySphere();

				void onFrame();

				void initialize(const gx::GxContextPtr& gxContext);

				void updateRadius(float radius);

				void updateColors(const std::vector<uint32_t>& colors);
            };

			SHARED_PTR(SkySphere);
        }
    }
}
