#pragma once

#include "math/Matrix4.h"
#include "math/Vector3.h"
#include "io/input/Mouse.h"
#include "io/input/Keyboard.h"

namespace wowc {
    namespace scene {
        class Camera {
            std::chrono::steady_clock::time_point mLastUpdate;
            math::Vector2 mLastMousePosition;

            math::Matrix4 mMatView;
            math::Matrix4 mMatProjection;

            float mAspect = 1.0f;
            float mFovY = 60.0f;
            float mZNear = 1.0f;
            float mZFar = 2000.0f;

            math::Vector3 mPosition = math::Vector3();
            math::Vector3 mTarget = math::Vector3(1, 0, 0);
            math::Vector3 mUp = math::Vector3(0, 0, 1);

            void updateProjection();
            void updateView();

            std::vector<std::function<void(bool, const math::Matrix4&)>> mCallbacks;
            std::vector<std::function<void(const math::Vector3&)>> mPositionCallbacks;

            void handleRotation(float yaw, float pitch);
            void handleMovement(float forward, float side, float up);

			void onPositionChanged();

        public:
            Camera();

            void addCallback(const std::function<void(bool, const math::Matrix4&)>& callback);
			void addPositionCallback(const std::function<void(const math::Vector3&)>& callback);

            void setPosition(const math::Vector3& position);

            void onUpdate(bool isUiInputFocus, const io::input::MousePtr& mouse, const io::input::KeyboardPtr& keyboard);

            void setAspect(float aspect);

            void reloadMatrices();

            const math::Matrix4& getViewMatrix() const { return mMatView; }
            const math::Matrix4& getProjectionMatrix() const { return mMatProjection; }
        };

        SHARED_PTR(Camera);
    }
}
