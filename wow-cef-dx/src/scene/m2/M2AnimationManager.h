#pragma once
#include "M2Animator.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2AnimationManager {
				static const constexpr uint32_t THREAD_COUNT = 6;

                struct AnimationThreadContext {
					std::mutex lock;
					std::condition_variable notifier;
					std::vector<M2AnimatorPtr> animators;
                };

				std::array<AnimationThreadContext, THREAD_COUNT> mContexts;
				std::array<std::thread, THREAD_COUNT> mThreads;

				bool mIsRunning = false;

				void animationThread(uint32_t index);
            public:
				void postConstruct(const cdi::ApplicationContextPtr& context);

				void insertAnimator(const M2AnimatorPtr& animator);
				void removeAnimator(const M2AnimatorPtr& animator);

				void shutdown();
            };

			SHARED_PTR(M2AnimationManager);
        }
    }
}
