#include "stdafx.h"
#include "M2ModelManager.h"
#include "math/Matrix4.h"
#include "utils/String.h"

namespace wowc {
	namespace scene {
		namespace m2 {

		    void M2ModelManager::processUnloadQueue() {
				while (!mUnloadQueue.empty()) {
					const auto elem = mUnloadQueue.front();
					mUnloadQueue.pop_front();
					const auto itr = std::find(mActiveModels.begin(), mActiveModels.end(), elem);
					if (itr != mActiveModels.end()) {
						mActiveModels.erase(itr);
					}
				}
		    }

		    void M2ModelManager::processLoadQueue() {
                while(!mLoadQueue.empty()) {
					const auto elem = mLoadQueue.front();
					mLoadQueue.pop_front();
					mActiveModels.emplace_back(elem);
                }
		    }

		    void M2ModelManager::addInstance(const M2ModelInstancePtr& instance, uint32_t uuid, const math::Vector3& position, float scale,
		        const math::Vector3& rotation) const {
				const auto translation = math::Matrix4::translate(position);
				const auto scaling = math::Matrix4::scale(scale, scale, scale);
				const auto reverse = math::Matrix4::scale(-1, 1, 1);
				const auto matRot = math::Matrix4::rotateAngles(math::Vector3(rotation.z(), rotation.x(), rotation.y() + 90));

				const auto instanceMatrix = matRot * reverse * scaling * translation;

				instance->addInstance(uuid, scale, position, instanceMatrix);
		    }

		    void M2ModelManager::postConstruct(const cdi::ApplicationContextPtr& context) {
				mFileFactory = context->produce<io::files::m2::M2FileFactory>();
		    }

		    void M2ModelManager::addModel(uint32_t uuid, const std::string& fileName, const math::Vector3& position, float scale, const math::Vector3& rotation) {
				static const std::hash<std::string> STRING_HASH;
				auto hash = STRING_HASH(utils::String::toUpper(fileName));
				auto itr = mModels.find(hash);
                if(itr == mModels.end()) {
					auto modelInstance = std::make_shared<M2ModelInstance>(mFileFactory->loadModel(fileName));
					mLoadQueue.emplace_back(modelInstance);
					itr = mModels.insert(std::make_pair<>(hash, modelInstance)).first;
                }

				addInstance(itr->second, uuid, position, scale, rotation);
		    }

		    void M2ModelManager::addModel(uint32_t uuid, uint32_t fileDataId, const math::Vector3& position, float scale, const math::Vector3& rotation) {
				auto itr = mFileDataModels.find(fileDataId);
				if (itr == mFileDataModels.end()) {
					auto modelInstance = std::make_shared<M2ModelInstance>(mFileFactory->loadModel(fileDataId));
					mLoadQueue.emplace_back(modelInstance);
					itr = mFileDataModels.insert(std::make_pair<>(fileDataId, modelInstance)).first;
				}

				addInstance(itr->second, uuid, position, scale, rotation);
		    }

		    void M2ModelManager::removeModel(uint32_t uuid, const std::string& fileName) {
				static const std::hash<std::string> STRING_HASH;
				const auto hash = STRING_HASH(utils::String::toUpper(fileName));
				const auto itr = mModels.find(hash);
				if (itr == mModels.end()) {
					return;
				}

				const auto remove = itr->second->removeInstance(uuid);
                if(!remove) {
					return;
                }

				mUnloadQueue.emplace_back(itr->second);
				mModels.erase(itr);
		    }

		    void M2ModelManager::removeModel(uint32_t uuid, uint32_t fileDataId) {
				const auto itr = mFileDataModels.find(fileDataId);
				if (itr == mFileDataModels.end()) {
					return;
				}

				const auto remove = itr->second->removeInstance(uuid);
				if (!remove) {
					return;
				}

				mUnloadQueue.emplace_back(itr->second);
				mFileDataModels.erase(itr);
		    }

		    void M2ModelManager::onFrame(Scene& scene) {
				processUnloadQueue();
				processLoadQueue();

                for(const auto& elem : mActiveModels) {
					elem->onFrame(scene);
                }
		    }
		}
	}
}
