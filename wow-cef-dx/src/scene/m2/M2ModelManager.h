#pragma once
#include "cdi/ApplicationContext.h"
#include "io/files/m2/M2FileFactory.h"
#include "M2ModelInstance.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2ModelManager {
				io::files::m2::M2FileFactoryPtr mFileFactory;
				std::map<std::size_t, M2ModelInstancePtr> mModels;
				std::map<uint32_t, M2ModelInstancePtr> mFileDataModels;

				std::deque<M2ModelInstancePtr> mLoadQueue;
				std::deque<M2ModelInstancePtr> mUnloadQueue;

				std::list<M2ModelInstancePtr> mActiveModels;

				void processUnloadQueue();
				void processLoadQueue();

				void addInstance(const M2ModelInstancePtr& instance, uint32_t uuid, const math::Vector3& position, float scale, const math::Vector3& rotation) const;

            public:
				void postConstruct(const cdi::ApplicationContextPtr& context);

				void addModel(uint32_t uuid, const std::string& fileName, const math::Vector3& position, float scale, const math::Vector3& rotation);
				void addModel(uint32_t uuid, uint32_t fileDataId, const math::Vector3& position, float scale, const math::Vector3& rotation);

				void removeModel(uint32_t uuid, const std::string& fileName);
				void removeModel(uint32_t uuid, uint32_t fileDataId);

				void onFrame(Scene& scene);
            };

			SHARED_PTR(M2ModelManager);
        }
    }
}
