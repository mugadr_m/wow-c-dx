#pragma once

#include "gx/Mesh.h"
#include "gx/GxContext.h"
#include "gx/Program.h"

namespace wowc {
    namespace math {
        class Matrix4;
    }
}

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2Material {
                gx::MeshPtr mMesh;
                gx::ProgramPtr mProgram;
                gx::ConstantBufferPtr mBufferData;

            public:
                void initialize(const gx::GxContextPtr& context);

                void setupModel(const gx::VertexBufferPtr& vertexBuffer,
                                const gx::VertexBufferPtr& instanceBuffer,
                                const gx::IndexBufferPtr& indexBuffer,
                                uint32_t instanceCount,
                                const gx::ConstantBufferPtr& boneAnims) const;
                void renderBatch(uint32_t startIndex, uint32_t numIndices) const;
            };

            SHARED_PTR(M2Material);
        }
    }
}
