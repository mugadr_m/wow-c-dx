#include "stdafx.h"
#include "M2MaterialManager.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            void M2MaterialManager::onInitialize(const gx::GxContextPtr& context) {
				mMaterial = std::make_shared<M2Material>();
				mMaterial->initialize(context);
            }

            M2MaterialPtr M2MaterialManager::getMaterial() {
				return mMaterial;
            }
        }
    }
}