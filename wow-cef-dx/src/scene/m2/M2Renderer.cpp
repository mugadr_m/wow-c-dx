#include "stdafx.h"
#include "M2Renderer.h"
#include "M2MaterialManager.h"

namespace wowc {
    namespace scene {
        namespace m2 {

            void M2Renderer::syncLoad(Scene& scene) {
                const auto& context = scene.getContext();

                if(mFileData->getVertices().empty() || mFileData->getIndices().empty() || mFileData->getPasses().empty()) {
					return;
                }

                mVertexBuffer = context->createVertexBuffer(true);
				mInstanceBuffer = context->createVertexBuffer(false);
                mIndexBuffer = context->createIndexBuffer(gx::IndexType::UINT16, true);

                mVertexBuffer->updateData(mFileData->getVertices());
                mIndexBuffer->updateData(mFileData->getIndices());

				mBoneAnimBuffer = context->createConstantBufferFromObject(mBoneBuffer);

                mIsSyncLoaded = true;
            }

            void M2Renderer::asyncLoad(io::files::m2::chunked::ChunkedM2FilePtr file) {
				const auto ctx = cdi::ApplicationContext::instance();
                mFileData = std::move(file);
                mMaterial = ctx->produce<M2MaterialManager>()->getMaterial();
				mAnimationManager = ctx->produce<M2AnimationManager>();
				mAnimator = std::make_shared<M2Animator>(mFileData);
                if(!mAnimator->setAnimation(AnimationType::STAND)) {
					mAnimator->setAnimationByIndex(0);
                }

				mAnimationManager->insertAnimator(mAnimator);

                for(auto i = 0u; i < 256; ++i) {
					mBoneBuffer[i] = math::Matrix4();
                }

                mIsAsyncLoaded = true;
            }

            void M2Renderer::onFrame(Scene& scene) {
                if (!mIsAsyncLoaded) {
                    return;
                }

                if (!mIsSyncLoaded) {
                    syncLoad(scene);
                    return;
                }

                if(mInstanceData.empty()) {
					return;
                }

                if(mFileData->getVertices().empty() || mFileData->getIndices().empty() || mFileData->getPasses().empty()) {
					return;
                }

                if(mInstancesChanged) {
					mInstancesChanged = false;
					mInstanceBuffer->updateData(mInstanceData);
                }

                if(mAnimator->getBoneMatrices(mBoneBuffer) || !mHasMatricesPushed) {
					mHasMatricesPushed = true;
					mBoneAnimBuffer->commit(true);
                }

				mMaterial->setupModel(mVertexBuffer, mInstanceBuffer, mIndexBuffer, mInstanceCount, mBoneAnimBuffer);
                for(const auto& pass : mFileData->getPasses()) {
					mMaterial->renderBatch(pass.startIndex, pass.indexCount);
                }
            }

            void M2Renderer::updateVisibleInstances(const std::vector<math::Matrix4>& transforms) {
				mInstanceCount = static_cast<uint32_t>(transforms.size());
                if(mInstanceCount == 0) {
					return;
                }

				mInstanceData = transforms;
				mInstancesChanged = true;
            }

            void M2Renderer::asyncUnload() {
                if(!mIsAsyncLoaded) {
					return;
                }

				mIsAsyncLoaded = false;
				mAnimationManager->removeAnimator(mAnimator);
				mAnimator = nullptr;
            }
        }
    }
}
