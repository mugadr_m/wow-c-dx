#include "stdafx.h"
#include "M2ModelInstance.h"

namespace wowc {
    namespace scene {
        namespace m2 {

            bool M2ModelInstance::handleLoadQueue() {
				std::lock_guard<std::mutex> l(mLoadLock);

                auto hasChanged = false;
                while(!mLoadQueue.empty()) {
					const auto elem = mLoadQueue.front();
					mLoadQueue.pop_front();
					mActiveInstances.emplace_back(elem);
					hasChanged = true;
                }

				return hasChanged;
            }

            bool M2ModelInstance::handleUnloadQueue() {
				std::lock_guard<std::mutex> l(mUnloadLock);

				auto hasChanged = false;
                while(!mUnloadQueue.empty()) {
					const auto elem = mUnloadQueue.front();
					mUnloadQueue.pop_front();
					const auto itr = std::find(mActiveInstances.begin(), mActiveInstances.end(), elem);
                    if(itr != mActiveInstances.end()) {
						mActiveInstances.erase(itr);
						hasChanged = true;
                    }
                }

				return hasChanged;
            }

            bool M2ModelInstance::isVisible(const Scene& scene, const ElementHolder& instance) const {
				auto radius = mModelRadius * instance.scale;
				radius *= radius;
				const auto distance = (scene.getCameraPosition() - instance.position).lengthSquared();
				const auto factor = radius / distance;
				if(factor < 0.00005f) {
					return false;
				}

				return scene.isVisible(instance.boundingBox);
            }

            void M2ModelInstance::addInstance(uint32_t uuid, float scale, math::Vector3 position, const math::Matrix4& instanceMatrix) {
				const auto itr = mInstanceReferences.find(uuid);
                if(itr != mInstanceReferences.end()) {
					++itr->second;
					return;
                }

				mInstanceReferences.emplace(uuid, 1);
				mInstances.emplace(uuid, instanceMatrix);
				std::lock_guard<std::mutex> l(mLoadLock);
				mLoadQueue.emplace_back(uuid, instanceMatrix, scale, position, mModelBox.transformed(instanceMatrix));
            }

            bool M2ModelInstance::removeInstance(uint32_t uuid) {
				const auto itr = mInstanceReferences.find(uuid);
                if(itr != mInstanceReferences.end()) {
					if(--itr->second <= 0) {
						mInstanceReferences.erase(itr);
						mInstances.erase(mInstances.find(uuid));
						{
							std::lock_guard<std::mutex> l(mUnloadLock);
							mUnloadQueue.emplace_back(uuid);
						}
					} else {
						return false;
					}
                }

				const auto isEmpty = mInstanceReferences.empty();

				return isEmpty;
            }

            void M2ModelInstance::onFrame(Scene& scene) {
				auto hasChanged = handleUnloadQueue();
				hasChanged |= handleLoadQueue();

                if(hasChanged || scene.hasViewChanged()) {
					std::vector<math::Matrix4> transforms;
					transforms.reserve(mActiveInstances.size());
                    auto hasChangedInstance = false;
                    for(auto& instance : mActiveInstances) {
						if (isVisible(scene, instance)) {
							transforms.emplace_back(instance.transform);
                            if(!instance.isVisible) {
								instance.isVisible = true;
								hasChangedInstance = true;
                            }
						} else {
						    if(instance.isVisible) {
								instance.isVisible = false;
								hasChangedInstance = true;
						    }
						}
                    }

					if (hasChanged || hasChangedInstance) {
						mRenderer->updateVisibleInstances(transforms);
					}
                }

				mRenderer->onFrame(scene);
            }
        }
    }
}