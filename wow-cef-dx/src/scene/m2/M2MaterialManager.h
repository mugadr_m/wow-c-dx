#pragma once

#include "M2Material.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2MaterialManager {
				M2MaterialPtr mMaterial;

            public:
				void onInitialize(const gx::GxContextPtr& context);

				M2MaterialPtr getMaterial();
            };

			SHARED_PTR(M2MaterialManager);
        }
    }
}
