#pragma once

#include "io/files/m2/chunked/ChunkedM2File.h"
#include "scene/Scene.h"

#include "gx/VertexBuffer.h"
#include "gx/IndexBuffer.h"
#include "M2Material.h"
#include "M2Animator.h"
#include "M2AnimationManager.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2Renderer {
				io::files::m2::chunked::ChunkedM2FilePtr mFileData;

				bool mIsAsyncLoaded = false;
				bool mIsSyncLoaded = false;

				uint32_t mInstanceCount = 0;
				std::vector<math::Matrix4> mInstanceData;
				bool mInstancesChanged = false;

				std::array<math::Matrix4, 256> mBoneBuffer;
				bool mHasMatricesPushed = false;

				gx::VertexBufferPtr mVertexBuffer;
				gx::VertexBufferPtr mInstanceBuffer;

				gx::IndexBufferPtr mIndexBuffer;
				gx::ConstantBufferPtr mBoneAnimBuffer;

				M2MaterialPtr mMaterial;
				M2AnimatorPtr mAnimator;
				M2AnimationManagerPtr mAnimationManager;

				void syncLoad(Scene& scene);

            public:
				void asyncLoad(io::files::m2::chunked::ChunkedM2FilePtr file);

				void onFrame(Scene& scene);

				void updateVisibleInstances(const std::vector<math::Matrix4>& transforms);

				void asyncUnload();
            };

			SHARED_PTR(M2Renderer);
        }
    }
}
