#pragma once

#include "io/files/m2/animation/M2AnimationBone.h"
#include "io/files/m2/chunked/ChunkedM2File.h"
#include "math/Matrix4.h"
#include <array>
#include "AnimationType.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2Animator {
				static utils::Logger<M2Animator> mLogger;
				static math::Matrix4 mIdentity;

				std::function<const math::Matrix4&(uint32_t, uint64_t)> mGetBoneCallback;

				std::vector<io::files::m2::animation::M2AnimationBone> mBones;

				std::array<math::Matrix4, 256> mBoneMatrices{};
				std::array<bool, 256> mMatrixLoaded{};
				std::array<bool, 256> mIsMatrixCalculating{};

				std::vector<io::files::m2::M2Sequence> mAnimations;
				std::vector<int16_t> mAnimationLookup;
				io::files::m2::M2Sequence mAnimation{};
				uint32_t mAnimationId = 0;
				bool mHasAnimation = false;

				uint32_t mAnimatorContext = 0;

				bool mIsAnimUpdating = false;
				bool mIsAnimReading = false;
				bool mIsReadPending = false;

				const math::Matrix4& getMatrix(uint32_t boneIndex, uint64_t time);

            public:
				explicit M2Animator(const io::files::m2::chunked::ChunkedM2FilePtr& file);

				void updateBones(uint64_t runTime);

                void setAnimatorContext(uint32_t contextIndex) {
					mAnimatorContext = contextIndex;
                }

                uint32_t getAnimatorContext() const {
					return mAnimatorContext;
                }

				bool getBoneMatrices(std::array<math::Matrix4, 256>& outArray);

				void setAnimationByIndex(uint32_t index);
				bool setAnimation(uint32_t type);
                bool setAnimation(AnimationType animationType) {
					return setAnimation(static_cast<uint32_t>(animationType));
                }
            };

			SHARED_PTR(M2Animator);
        }
    }
}
