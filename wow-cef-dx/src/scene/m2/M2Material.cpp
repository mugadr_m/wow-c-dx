#include "stdafx.h"
#include "M2Material.h"
#include "res/resource1.h"
#include "math/Matrix4.h"

namespace wowc {
    namespace scene {
        namespace m2 {

            void M2Material::initialize(const gx::GxContextPtr& context) {
                mMesh = context->createMesh();
                mProgram = context->createProgram();
                mProgram->loadFromResources(IDR_M2_SHADER_VERTEX, IDR_M2_SHADER_PIXEL);

                mMesh->setProgram(mProgram);

                mMesh->addElement("POSITION", 0, 3);
                mMesh->addElement("BLENDWEIGHT", 0, 4, gx::DataType::BYTE, true);
                mMesh->addElement("BLENDINDEX", 0, 4, gx::DataType::BYTE, false);
                mMesh->addElement("NORMAL", 0, 3);
                mMesh->addElement("TEXCOORD", 0, 2);
                mMesh->addElement("TEXCOORD", 1, 2);

				mMesh->addElement("TEXCOORD", 2, 4, gx::DataType::FLOAT, false, 1, true);
				mMesh->addElement("TEXCOORD", 3, 4, gx::DataType::FLOAT, false, 1, true);
				mMesh->addElement("TEXCOORD", 4, 4, gx::DataType::FLOAT, false, 1, true);
				mMesh->addElement("TEXCOORD", 5, 4, gx::DataType::FLOAT, false, 1, true);

                mMesh->finalize();
            }

            void M2Material::setupModel(const gx::VertexBufferPtr& vertexBuffer,
                                        const gx::VertexBufferPtr& instanceBuffer,
                                        const gx::IndexBufferPtr& indexBuffer,
                                        uint32_t instanceCount,
                                        const gx::ConstantBufferPtr& boneAnims) const {

                mMesh->setVertexBuffer(vertexBuffer);
                mMesh->setInstanceBuffer(instanceBuffer);
                mMesh->setIndexBuffer(indexBuffer);

				mMesh->setInstanceCount(instanceCount);

                boneAnims->bind(gx::ShaderType::VERTEX, 4);

                mMesh->beginFrame();
            }

            void M2Material::renderBatch(uint32_t startIndex, uint32_t numIndices) const {
                mMesh->setStartIndex(startIndex);
                mMesh->setIndexCount(numIndices);
                mMesh->draw();
            }
        }
    }
}
