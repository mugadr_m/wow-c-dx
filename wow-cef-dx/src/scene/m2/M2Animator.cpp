#include "stdafx.h"
#include "M2Animator.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            using io::files::m2::chunked::ChunkedM2FilePtr;

            utils::Logger<M2Animator> M2Animator::mLogger;
            math::Matrix4 M2Animator::mIdentity;


            const math::Matrix4& M2Animator::getMatrix(uint32_t boneIndex, uint64_t time) {
                if (boneIndex >= 256) {
                    mLogger.warn("Invalid bone index: ", boneIndex, " >= 256");
                    return mIdentity;
                }

                if (mMatrixLoaded[boneIndex]) {
                    return mBoneMatrices[boneIndex];
                }

                if (mIsMatrixCalculating[boneIndex]) {
                    mLogger.warn("Invalid bone hierarchy. Cyclic dependency. Bone ", boneIndex,
                                 " is already calculating");
                    return mIdentity;
                }

                mIsMatrixCalculating[boneIndex] = true;
                mBoneMatrices[boneIndex] = mBones[boneIndex].getBoneTransform(time, mAnimationId, mAnimation.duration, mGetBoneCallback);
                mIsMatrixCalculating[boneIndex] = false;
                mMatrixLoaded[boneIndex] = true;
                return mBoneMatrices[boneIndex];
            }

            M2Animator::M2Animator(const ChunkedM2FilePtr& file) : mBones(file->getBones()),
                                                                   mAnimations(file->getAnimations()),
                                                                   mAnimationLookup(file->getAnimationLookup()) {
                mGetBoneCallback = std::bind(&M2Animator::getMatrix, this, std::placeholders::_1,
                                             std::placeholders::_2);
            }

            void M2Animator::updateBones(uint64_t runTime) {
                if (mIsAnimReading || mIsReadPending || !mHasAnimation) {
                    return;
                }

                const auto boneCount = std::min(mBones.size(), mBoneMatrices.size());
                memset(mMatrixLoaded.data(), 0, mMatrixLoaded.size());
                memset(mIsMatrixCalculating.data(), 0, mIsMatrixCalculating.size());

                mIsAnimUpdating = true;
                for (auto i = 0u; i < boneCount; ++i) {
                    if(mMatrixLoaded[i]) {
						continue;
                    }

                    mBoneMatrices[i] = getMatrix(i, runTime);
                }
                mIsReadPending = true;
                mIsAnimUpdating = false;
            }

            bool M2Animator::getBoneMatrices(std::array<math::Matrix4, 256>& outArray) {
                if (mIsAnimUpdating) {
                    return false;
                }

                if (!mIsReadPending) {
                    return false;
                }

                mIsAnimReading = true;
                memcpy(outArray.data(), mBoneMatrices.data(), outArray.size() * sizeof(math::Matrix4));
                mIsReadPending = false;
                mIsAnimReading = false;

                return true;
            }

            void M2Animator::setAnimationByIndex(uint32_t index) {
                if(index >= mAnimations.size()) {
					return;
                }

				mAnimation = mAnimations[index];
				mAnimationId = index;
				mHasAnimation = true;
            }

            bool M2Animator::setAnimation(uint32_t type) {
                if(type >= mAnimationLookup.size()) {
					return false;
                }

				auto i = type & mAnimationLookup.size();
                for(auto stride = 1u; true; ++stride) {
                    if(mAnimationLookup[i] < 0) {
						return false;
                    }

                    if(mAnimations[mAnimationLookup[i]].id == type) {
						mAnimationId = mAnimationLookup[type];
						mAnimation = mAnimations[mAnimationId];
						mHasAnimation = true;

						return true;
                    }

					i = (i + stride * stride) % mAnimationLookup.size();
                }
            }
        }
    }
}
