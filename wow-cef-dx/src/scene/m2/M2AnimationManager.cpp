#include "stdafx.h"
#include "M2AnimationManager.h"
#include <random>

namespace wowc {
    namespace scene {
        namespace m2 {

            void M2AnimationManager::animationThread(uint32_t index) {
				static const std::chrono::milliseconds SLEEP_DELAY(40);
				static const auto START_POINT = std::chrono::steady_clock::now();

				std::mt19937 random;
                
				const auto start = START_POINT - std::chrono::milliseconds(std::uniform_int_distribution<uint32_t>(0, 5000)(random));

				auto& context = mContexts[index];

                while(mIsRunning) {
					{
						if (context.animators.empty()) {
							std::unique_lock<std::mutex> l(context.lock);
							context.notifier.wait(l, [this, &context]() {
								return !mIsRunning || !context.animators.empty();
							});

                            if(!mIsRunning) {
								break;
                            }

							continue;
						}

						const auto runTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
						{
							std::lock_guard<std::mutex> l(context.lock);
                            for(const auto& animator : context.animators) {
								animator->updateBones(runTime);
                            }
						}

						std::this_thread::sleep_for(SLEEP_DELAY);
					}
                }
            }

            void M2AnimationManager::postConstruct(const cdi::ApplicationContextPtr& /*context*/) {
				mIsRunning = true;

                for(auto i = 0u; i < THREAD_COUNT; ++i) {
					mThreads[i] = std::thread(std::bind(&M2AnimationManager::animationThread, this, i));
                }
            }

            void M2AnimationManager::insertAnimator(const M2AnimatorPtr& animator) {
				auto minIndex = 0u;
                for(auto i = 0u; i < THREAD_COUNT; ++i) {
                    if(mContexts[i].animators.size() < mContexts[minIndex].animators.size()) {
						minIndex = i;
                    }
                }

				auto& context = mContexts[minIndex];
				{
					std::unique_lock<std::mutex> l(context.lock);
					context.animators.emplace_back(animator);
					animator->setAnimatorContext(minIndex);
				}
				context.notifier.notify_one();
            }

            void M2AnimationManager::removeAnimator(const M2AnimatorPtr& animator) {
				const auto contextIndex = animator->getAnimatorContext();
                if(contextIndex >= mContexts.size()) {
					return;
                }

				auto& context = mContexts[contextIndex];
				std::lock_guard<std::mutex> l(context.lock);
				const auto itr = std::find(context.animators.begin(), context.animators.end(), animator);
                if(itr != context.animators.end()) {
					context.animators.erase(itr);
                }
            }

            void M2AnimationManager::shutdown() {
				mIsRunning = false;
                for(auto i = 0u; i < THREAD_COUNT; ++i) {
					mContexts[i].notifier.notify_all();
                    if(mThreads[i].joinable()) {
						mThreads[i].join();
                    }
                }
            }
        }
    }
}