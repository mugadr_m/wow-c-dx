#pragma once

#include "io/files/m2/chunked/ChunkedM2File.h"
#include "scene/GlobalShaderBufferManager.h"
#include <atomic>
#include "M2Renderer.h"
#include "math/BoundingBox.h"

namespace wowc {
    namespace scene {
        namespace m2 {
            class M2ModelInstance {
                struct ElementHolder {
                    uint32_t uuid;
                    math::Matrix4 transform;
                    float scale;
                    math::Vector3 position;
                    math::BoundingBox boundingBox;
					bool isVisible = false;

                    explicit ElementHolder(uint32_t uuid, math::Matrix4 transform, float scale, math::Vector3 position,
                                           math::BoundingBox boundingBox)
                        : uuid(uuid),
                          transform(std::move(transform)),
                          scale(scale),
                          position(position),
                          boundingBox(std::move(boundingBox)) {
                    }

                    explicit ElementHolder(uint32_t uuid) : uuid(uuid), scale{} {

                    }

                    bool operator ==(const ElementHolder& other) const {
                        return other.uuid == uuid;
                    }

                    bool operator !=(const ElementHolder& other) const {
                        return other.uuid != uuid;
                    }
                };

                M2RendererPtr mRenderer;
                std::map<uint32_t, math::Matrix4> mInstances;
                std::map<uint32_t, std::atomic_int32_t> mInstanceReferences;

                std::deque<ElementHolder> mLoadQueue;
                std::deque<ElementHolder> mUnloadQueue;

                std::deque<ElementHolder> mActiveInstances;
                std::mutex mLoadLock;
                std::mutex mUnloadLock;

                float mModelRadius = 0.0f;
				math::BoundingBox mModelBox;

				bool mIsElementLoaded = false;
				std::function<void(const M2RendererPtr&)> mAsyncLoadFunction;

                bool handleLoadQueue();
                bool handleUnloadQueue();

                bool isVisible(const Scene& scene, const ElementHolder& instance) const;

            public:
                explicit M2ModelInstance(const io::files::m2::chunked::ChunkedM2FilePtr& modelFile) {
                    mModelRadius = modelFile->getModelRadius();
					mModelBox = modelFile->getBoundingBox();
                    mRenderer = std::make_shared<M2Renderer>();
                    mRenderer->asyncLoad(modelFile);
                }

                void addInstance(uint32_t uuid, float scale, math::Vector3 position,
                                 const math::Matrix4& instanceMatrix);


                /**
				 * \brief Remove a reference for the given UUID
				 * \param uuid UUID to dereference
				 * \return true if the last instance of this holder was removed, false if at least one is still present
				 */
                bool removeInstance(uint32_t uuid);

                void onFrame(Scene& scene);
            };

            SHARED_PTR(M2ModelInstance);
        }
    }
}
