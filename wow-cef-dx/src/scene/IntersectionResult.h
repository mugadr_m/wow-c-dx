#pragma once

#include "math/Vector3.h"
#include "terrain/MapTileRender.h"

namespace wowc {
    namespace scene {
        class IntersectionResult {
        protected:
			math::Vector3 mPosition;
			bool mHasHit = false;
			float mDistance = std::numeric_limits<float>::max();

        public:

            const math::Vector3& getPosition() const {
                return mPosition;
            }

            void setPosition(const math::Vector3& position) {
                mPosition = position;
            }

            bool isHasHit() const {
                return mHasHit;
            }

            void setHasHit(const bool hasHit) {
                mHasHit = hasHit;
            }

            float getDistance() const {
                return mDistance;
            }

            void setDistance(const float distance) {
                mDistance = distance;
            }
        };

        class TerrainIntersectionResult : public IntersectionResult {
			terrain::MapTileRenderPtr mSelectedTile;
			terrain::MapChunkRenderPtr mSelectedChunk;

        public:
            TerrainIntersectionResult() {
				mHasHit = false;
            }

            const terrain::MapTileRenderPtr& getSelectedTile() const {
                return mSelectedTile;
            }

            void setSelectedTile(const terrain::MapTileRenderPtr& selectedTile) {
                mSelectedTile = selectedTile;
            }

            const terrain::MapChunkRenderPtr& getSelectedChunk() const {
                return mSelectedChunk;
            }

            void setSelectedChunk(const terrain::MapChunkRenderPtr& selectedChunk) {
                mSelectedChunk = selectedChunk;
            }
        };
    }
}
