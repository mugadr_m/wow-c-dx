#pragma once
#include "math/Frustum.h"
#include "GlobalShaderBufferManager.h"

namespace wowc {
    namespace scene {
        class Scene {
			const math::Frustum& mFrustum;
			gx::GxContextPtr mContext;
			GlobalShaderBufferManager& mBufferManager;
			uint64_t mRunTime = 0;
			bool mViewChanged = false;
			math::Vector3 mCameraPosition;
			bool mShowTerrainVertices = false;

        public:
            explicit Scene(const math::Frustum& frustum, GlobalShaderBufferManager& bufferManager) : mFrustum(frustum), mBufferManager(bufferManager) {
                
            }

			GlobalShaderBufferManager& getBufferManager() const { return mBufferManager; }

            void setRunTime(uint64_t runTime) {
				mRunTime = runTime;
            }

            uint64_t getRunTime() const {
				return mRunTime;
            }

            void setContext(const gx::GxContextPtr& context) {
				mContext = context;
            }

            const gx::GxContextPtr& getContext() const {
				return mContext;
            }

            bool isVisible(const math::BoundingBox& bbox) const {
				return mFrustum.isVisible(bbox);
            }

            bool hasViewChanged() const {
				return mViewChanged;
            }

            void setViewChanged(bool viewChanged) {
				mViewChanged = viewChanged;
            }

            const math::Vector3& getCameraPosition() const {
                return mCameraPosition;
            }

            void setCameraPosition(const math::Vector3& cameraPosition) {
                mCameraPosition = cameraPosition;
            }

            bool showTerrainVertices() const {
				return mShowTerrainVertices;
            }

            void setShowTerrainVertices(bool showTerrainVertices) {
				mShowTerrainVertices = showTerrainVertices;
            }
        };
    }
}
