#include "stdafx.h"
#include "Camera.h"

namespace wowc {
    namespace scene {

        void Camera::updateProjection() {
            mMatProjection = math::Matrix4::perspective(mFovY, mAspect, mZNear, mZFar);
            std::for_each(mCallbacks.begin(), mCallbacks.end(), [&](const auto& callback) {
                callback(false, mMatProjection);
            });
        }

        void Camera::updateView() {
            mMatView = math::Matrix4::lookAt(mPosition, mTarget, mUp);
            std::for_each(mCallbacks.begin(), mCallbacks.end(), [&](const auto& callback) {
                callback(true, mMatView);
            });
        }

        void Camera::handleRotation(float yaw, float pitch) {
			auto direction = (mTarget - mPosition).normalized();
			const auto right = mUp.cross(direction);

			const auto yawMat = math::Matrix4::rotate(math::Vector3(0, 0, 1), yaw);
			const auto pitchMat = math::Matrix4::rotate(right.normalized(), pitch);
			const auto matrix = pitchMat * yawMat;

            direction = matrix * direction;
            mTarget = mPosition + direction;
            mUp = matrix * mUp;
            updateView();
        }

        void Camera::handleMovement(float forward, float side, float up) {
            const auto direction = (mTarget - mPosition).normalized();
			const auto right = mUp.cross(direction).normalized();
            mPosition += direction * forward + right * side + math::Vector3::UNIT_Z * up;
            mTarget = mPosition + direction;
			onPositionChanged();
			updateView();
        }

        void Camera::onPositionChanged() {
            for(const auto& callback : mPositionCallbacks) {
				callback(mPosition);
            }
        }

        Camera::Camera() {
            updateProjection();
            updateView();
        }

        void Camera::addCallback(const std::function<void(bool, const math::Matrix4&)>& callback) {
            mCallbacks.push_back(callback);
        }

        void Camera::addPositionCallback(const std::function<void(const math::Vector3&)>& callback) {
			mPositionCallbacks.push_back(callback);
        }

        void Camera::setPosition(const math::Vector3& position) {
            const auto direction = (mTarget - mPosition).normalized();
            mPosition = position;
            mTarget = mPosition + direction;
			onPositionChanged();
            updateView();
        }

        void Camera::onUpdate(bool isUiInputFocus, const io::input::MousePtr& mouse, const io::input::KeyboardPtr& keyboard) {
            const auto now = std::chrono::steady_clock::now();
			const auto diffTime = std::chrono::duration_cast<std::chrono::microseconds>(now - mLastUpdate).count() / 1000.0f;
            if (diffTime < 16) {
                return;
            }

            mLastUpdate = now;

			if (!isUiInputFocus) {
				auto forward = 0.0f;
				auto side = 0.0f;
				auto up = 0.0f;

				if (keyboard->isKeyDown('W')) {
					forward += diffTime * 0.1f;
				}
				if (keyboard->isKeyDown('S')) {
					forward -= diffTime * 0.1f;
				}
				if (keyboard->isKeyDown('A')) {
					side -= diffTime * 0.1f;
				}
				if (keyboard->isKeyDown('D')) {
					side += diffTime * 0.1f;
				}
				if (keyboard->isKeyDown('Q')) {
					up += diffTime * 0.1f;
				}
				if (keyboard->isKeyDown('E')) {
					up -= diffTime * 0.1f;
				}

				if (std::abs(forward) >= 0.0001f || std::abs(side) >= 0.0001f || std::abs(up) >= 0.0001f) {
					handleMovement(forward, side, up);
				}
			}

            if (mouse->isRightPressed()) {
                const auto diff = mouse->getLastPosition() - mLastMousePosition;

                const auto pitch = diff.y() * (diffTime / 80.0f);
                const auto yaw = -diff.x() * (diffTime / 80.0f);
                handleRotation(yaw, pitch);
            }

            mLastMousePosition = mouse->getLastPosition();
        }

        void Camera::setAspect(const float aspect) {
            mAspect = aspect;
            updateProjection();
        }

        void Camera::reloadMatrices() {
            updateView();
            updateProjection();
        }
    }
}
