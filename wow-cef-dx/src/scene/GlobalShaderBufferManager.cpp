#include "stdafx.h"
#include "GlobalShaderBufferManager.h"

namespace wowc {
    namespace scene {

        void GlobalShaderBufferManager::updateViewMatrix(const math::Matrix4& matrix) {
			mSceneConstantData.viewMatrix = matrix;
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateProjectionMatrix(const math::Matrix4& matrix) {
			mSceneConstantData.projectionMatrix = matrix;
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateCameraPosition(const math::Vector3& cameraPosition) {
			mSceneConstantData.cameraPosition.set(cameraPosition.x(), cameraPosition.y(), cameraPosition.z(), 1.0f);
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateMousePosition(const math::Vector3& mousePosition) {
			mSceneConstantData.mousePosition.x(mousePosition.x());
			mSceneConstantData.mousePosition.y(mousePosition.y());
			mSceneConstantData.mousePosition.z(mousePosition.z());
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateBrushRadius(const float& inner, const float& outer) {
			mSceneConstantData.brushParams.x(inner);
			mSceneConstantData.brushParams.y(outer);
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateBrushTime(const float& time) {
			mSceneConstantData.brushParams.z(time);
			mIsSceneDataDirty = true;
        }

        void GlobalShaderBufferManager::updateLightColors(uint32_t ambient, uint32_t diffuse) {
			mLightConstantData.ambientColor = math::Vector4::fromXrgb(ambient);
			mLightConstantData.diffuseColor = math::Vector4::fromXrgb(diffuse);
			mIsLightColorDirty = true;
        }

        void GlobalShaderBufferManager::updateWaterColors(uint32_t oceanLight, uint32_t oceanDark, uint32_t riverLight,
            uint32_t riverDark) {
			mLightConstantData.oceanColorLight = math::Vector4::fromXrgb(oceanLight);
			mLightConstantData.oceanColorDark = math::Vector4::fromXrgb(oceanDark);
			mLightConstantData.riverColorLight = math::Vector4::fromXrgb(riverLight);
            mLightConstantData.riverColorDark = math::Vector4::fromXrgb(riverDark);
			mIsLightColorDirty = true;
        }

        void GlobalShaderBufferManager::updateWaterAlpha(float oceanNear, float oceanFar, float riverNear,
            float riverFar) {
			mLightConstantData.liquidAlpha.set(riverNear, riverFar, oceanNear, oceanFar);
			mIsLightColorDirty = true;
        }

        void GlobalShaderBufferManager::updateSpecularOverride(float specular) {
			mRenderSettingsData.specularOverride = std::min(std::max(0.0f, specular), 1.0f);
			mIsRenderDataDirty = true;
        }

        void GlobalShaderBufferManager::updateSpecularIntensity(float intensity) {
			mRenderSettingsData.specularIntensity = intensity;
			mIsRenderDataDirty = true;
        }

        void GlobalShaderBufferManager::initialize(const gx::GxContextPtr& context) {
			mBuffer = context->createConstantBuffer(sizeof mSceneConstantData);
			mLightBuffer = context->createConstantBuffer(sizeof mLightConstantData);
			mRenderSettingsBuffer = context->createConstantBuffer(sizeof mRenderSettingsData);

			const auto fltMax = std::numeric_limits<float>::max();

			updateSpecularOverride(1.0f);
			updateSpecularIntensity(20.0f);
			updateBrushRadius(100.0f, 150.0f);
			updateMousePosition({ fltMax, fltMax, fltMax });
			updateBrushTime(0.0f);
        }

        void GlobalShaderBufferManager::onUpdate() {
            if(mIsSceneDataDirty) {
				mIsSceneDataDirty = false;
				mBuffer->updateData(&mSceneConstantData, 0, sizeof mSceneConstantData);
				mBuffer->commit();
            }

			if(mIsLightColorDirty) {
				mIsLightColorDirty = false;
				mLightBuffer->updateData(&mLightConstantData, 0, sizeof mLightConstantData);
				mLightBuffer->commit();
			}

            if(mIsRenderDataDirty) {
				mIsRenderDataDirty = false;
				mRenderSettingsBuffer->updateData(&mRenderSettingsData, 0, sizeof mRenderSettingsData);
				mRenderSettingsBuffer->commit();
            }
        }

        void GlobalShaderBufferManager::apply() const {
			mBuffer->bind(gx::ShaderType::PIXEL, 0);
			mBuffer->bind(gx::ShaderType::VERTEX, 0);

			mLightBuffer->bind(gx::ShaderType::PIXEL, 1);
			mLightBuffer->bind(gx::ShaderType::VERTEX, 1);

			mRenderSettingsBuffer->bind(gx::ShaderType::VERTEX, 2);
			mRenderSettingsBuffer->bind(gx::ShaderType::PIXEL, 2);
        }
    }
}