#pragma once
#include "math/Matrix4.h"
#include "gx/ConstantBuffer.h"
#include "gx/GxContext.h"
#include "math/Vector4.h"
#include "math/Vector3.h"
#include "math/Vector2.h"

namespace wowc {
    namespace scene {
#pragma pack(push, 1)
		struct SceneConstantData {
			math::Matrix4 viewMatrix;
			math::Matrix4 projectionMatrix;
			math::Vector4 cameraPosition;
			math::Vector4 mousePosition;
			math::Vector4 brushParams;
		};

        struct LightConstantData {
			math::Vector4 ambientColor;
			math::Vector4 diffuseColor;
			math::Vector4 oceanColorLight;
			math::Vector4 oceanColorDark;
			math::Vector4 riverColorLight;
			math::Vector4 riverColorDark;
			math::Vector4 liquidAlpha;
        };

        struct RenderSettingsData {
			float specularOverride;
			float specularIntensity;
			float padding;
			float padding2;
        };

#pragma pack(pop)

		class GlobalShaderBufferManager {
			SceneConstantData mSceneConstantData{};
			LightConstantData mLightConstantData{};
			RenderSettingsData mRenderSettingsData{};

			bool mIsSceneDataDirty = false;
			gx::ConstantBufferPtr mBuffer;

			bool mIsLightColorDirty = false;
			gx::ConstantBufferPtr mLightBuffer;

			bool mIsRenderDataDirty = false;
			gx::ConstantBufferPtr mRenderSettingsBuffer;

		public:
			void updateViewMatrix(const math::Matrix4& matrix);
			void updateProjectionMatrix(const math::Matrix4& matrix);
			void updateCameraPosition(const math::Vector3& cameraPosition);
			void updateMousePosition(const math::Vector3& mousePosition);
			void updateBrushRadius(const float& inner, const float& outer);
			void updateBrushTime(const float& time);

			void updateLightColors(uint32_t ambient, uint32_t diffuse);
			void updateWaterColors(uint32_t oceanLight, uint32_t oceanDark, uint32_t riverLight, uint32_t riverDark);
			void updateWaterAlpha(float oceanNear, float oceanFar, float riverNear, float riverFar);

			void updateSpecularOverride(float specular);
			void updateSpecularIntensity(float intensity);

			void initialize(const gx::GxContextPtr& context);
			void onUpdate();

			void apply() const;
        };
    }
}
