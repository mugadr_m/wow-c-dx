#include "stdafx.h"
#include "RenderSettingsChangedEvent.h"

namespace wowc {
    namespace scene {

        RenderSettingsChangedEvent RenderSettingsChangedEvent::fromJson(const rapidjson::Document& document) {
			const auto specularItr = document.FindMember("specular");
			const auto textureScaleItr = document.FindMember("textureScales");
			const auto heightTextureItr = document.FindMember("heightTextures");
			const auto specularIntensityItr = document.FindMember("specularIntensity");

			const auto specularFactor = specularItr != document.MemberEnd() ? specularItr->value.GetFloat() : 1.0f;
			const auto textureScales = textureScaleItr != document.MemberEnd() ? textureScaleItr->value.GetFloat() : 1.0f;
			const auto heightTextures = heightTextureItr != document.MemberEnd() ? heightTextureItr->value.GetFloat() : 1.0f;
			const auto specularIntensity = specularIntensityItr != document.MemberEnd() ? specularIntensityItr->value.GetFloat() : 20.0f;

            return {
                specularFactor,
                textureScales,
                heightTextures,
                specularIntensity
			};
        }
    }
}