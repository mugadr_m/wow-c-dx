#pragma once

#include "io/files/blp/TextureManager.h"
#include "io/files/liquid/LiquidTypeBuffer.h"
#include "scene/Scene.h"
#include "gx/VertexBuffer.h"
#include "gx/IndexBuffer.h"
#include "LiquidMaterial.h"

namespace wowc {
    namespace scene {
        namespace liquid {
            class LiquidRenderPass {
				io::files::liquid::LiquidTypeBufferPtr mBuffer;

				std::vector<gx::TexturePtr> mTextures;
				gx::VertexBufferPtr mVertexBuffer;
				gx::IndexBufferPtr mIndexBuffer;

				LiquidData mLiquidData{};

				math::BoundingBox mBoundingBox;
				math::Matrix4 mTransform;

            public:
				explicit LiquidRenderPass(math::Matrix4 transform, io::files::liquid::LiquidTypeBufferPtr liquidTypeBuffer);
				void onFrame(Scene& scene);
				void syncLoad(Scene& scene, io::files::blp::TextureManagerPtr& textureManager);

				void asyncUnload();
				void syncUnload();
            };

			SHARED_PTR(LiquidRenderPass);
        }
    }
}
