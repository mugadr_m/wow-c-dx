#include "stdafx.h"
#include "LiquidMaterial.h"
#include "res/resource1.h"

namespace wowc {
    namespace scene {
        namespace liquid {
			LiquidMaterial liquidMaterial;

            void LiquidMaterial::onFrame(const gx::VertexBufferPtr& vertexBuffer, const gx::IndexBufferPtr& indexBuffer,
                uint32_t numIndices, const LiquidData& liquidData, const gx::TexturePtr& texture) const {
				mMesh->setTexture(gx::ShaderType::PIXEL, 0, texture);
				mMesh->setSampler(gx::ShaderType::PIXEL, 0, mSampler);

				mMesh->setBlendState(mBlendState);

				mConstantData->updateData(&liquidData, 0, sizeof liquidData);
				mConstantData->commit();

				mConstantData->bind(gx::ShaderType::VERTEX, 3);
				mConstantData->bind(gx::ShaderType::PIXEL, 3);

				mMesh->setVertexBuffer(vertexBuffer);
				mMesh->setIndexBuffer(indexBuffer);

				mMesh->setIndexCount(numIndices);

				mMesh->beginFrame();
				mMesh->draw();
            }

            void LiquidMaterial::initialize(const gx::GxContextPtr& context) {
				mSampler = context->createSampler();
				mSampler->setTextureFilter(D3D11_FILTER_MIN_MAG_MIP_LINEAR);
				mSampler->setTextureAddressU(gx::TextureAddressMode::WRAP);
				mSampler->setTextureAddressV(gx::TextureAddressMode::WRAP);

				mBlendState = context->createBlendState();
				mBlendState->setBlendEnabled(true);
				mBlendState->setSourceBlend(D3D11_BLEND_SRC_ALPHA);
				mBlendState->setDestinationBlend(D3D11_BLEND_INV_SRC_ALPHA);
				mBlendState->setSourceBlendAlpha(D3D11_BLEND_ONE);
				mBlendState->setDestinationBlendAlpha(D3D11_BLEND_ZERO);

				mConstantData = context->createConstantBuffer(sizeof(LiquidData));

				mMesh = context->createMesh();

				mProgram = context->createProgram();
				mProgram->loadFromResources(IDR_LIQUID_SHADER_VERTEX, IDR_LIQUID_SHADER_PIXEL);

				mMesh->setProgram(mProgram);

				mMesh->addElement("POSITION", 0, 3);
				mMesh->addElement("TEXCOORD", 0, 2);
				mMesh->addElement("TEXCOORD", 1, 1, gx::DataType::BYTE, true);

				mMesh->finalize();
            }
        }
    }
}
