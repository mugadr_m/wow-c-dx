#include "stdafx.h"
#include "LiquidManager.h"

namespace wowc {
    namespace scene {
        namespace liquid {

            void LiquidManager::onFrame(Scene& scene) {
				updateTileList();

                for(const auto& tile : mLiquidRenderers) {
					tile->onFrame(scene);
                }
            }

            void LiquidManager::updateTileList() {
				while (!mUnloadList.empty()) {
					const auto tile = mUnloadList.front();
					mUnloadList.pop_front();
					const auto itr = std::find(mLiquidRenderers.begin(), mLiquidRenderers.end(), tile);
					if (itr != mLiquidRenderers.end()) {
						mLiquidRenderers.erase(itr);
					}

					tile->syncUnload();
				}

				while (!mLoadList.empty()) {
					const auto tile = mLoadList.front();
					mLoadList.pop_front();
					mLiquidRenderers.emplace_back(tile);
				}
            }

            void LiquidManager::loadTile(const MapLiquidRenderPtr& tile) {
				mLoadList.emplace_back(tile);
            }

            void LiquidManager::unloadTile(const MapLiquidRenderPtr& tile) {
				tile->asyncUnload();
				mUnloadList.emplace_back(tile);
            }
        }
    }
}