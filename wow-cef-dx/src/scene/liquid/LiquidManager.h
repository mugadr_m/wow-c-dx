#pragma once
#include "MapLiquidRender.h"

namespace wowc {
    namespace scene {
        namespace liquid {
            class LiquidManager {
				std::deque<MapLiquidRenderPtr> mLiquidRenderers;

				std::deque<MapLiquidRenderPtr> mLoadList;
				std::deque<MapLiquidRenderPtr> mUnloadList;

				void updateTileList();

            public:
				void loadTile(const MapLiquidRenderPtr& tile);
				void unloadTile(const MapLiquidRenderPtr& tile);

				void onFrame(Scene& scene);
            };

			SHARED_PTR(LiquidManager);
        }
    }
}
