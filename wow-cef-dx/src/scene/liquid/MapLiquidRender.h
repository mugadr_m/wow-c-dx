#pragma once

#include "math/Matrix4.h"
#include "io/files/blp/TextureManager.h"
#include "LiquidRenderPass.h"
#include "scene/Scene.h"

namespace wowc {
    namespace scene {
        namespace liquid {
            class MapLiquidRender {
				math::Matrix4 mTransform;

				bool mIsSyncLoaded = false;
				bool mIsAsyncLoaded = false;
				bool mIsEmpty = true;

				io::files::blp::TextureManagerPtr mTextureManager;

				std::vector<LiquidRenderPassPtr> mRenderPasses;

				void syncLoad(Scene& scene);

            public:
				MapLiquidRender(uint32_t indexX, uint32_t indexY, io::files::blp::TextureManagerPtr textureManager);

				void onFrame(Scene& scene);

				void asyncLoad(const io::files::liquid::TileLiquidManagerPtr& liquidManager);

				void asyncUnload();
				void syncUnload();
            };

			SHARED_PTR(MapLiquidRender);
        }
    }
}
