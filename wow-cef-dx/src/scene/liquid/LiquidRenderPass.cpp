#include "stdafx.h"
#include "LiquidRenderPass.h"
#include "LiquidMaterial.h"

namespace wowc {
    namespace scene {
        namespace liquid {

            LiquidRenderPass::LiquidRenderPass(math::Matrix4 transform,
                                               io::files::liquid::LiquidTypeBufferPtr liquidTypeBuffer) :
                mBuffer(std::move(liquidTypeBuffer)),
                mTransform(std::move(transform)) {
                mLiquidData.liquidInfo.set(
                    mBuffer->isOcean() ? 1.0f : 0.0f,
                    mBuffer->isOcean() ? 0.0f : 1.0f,
                    0.0f,
                    0.0f
                );

				mLiquidData.transformMatrix = transform;

                mBoundingBox = mBuffer->getBoundingBox();
				mBoundingBox.transform(transform);
            }

            void LiquidRenderPass::onFrame(Scene& scene) {
                if (mTextures.empty() || !scene.isVisible(mBoundingBox)) {
                    return;
                }

                const auto textureIndex = static_cast<uint64_t>(scene.getRunTime() / 60.0f) % mTextures.size();
                liquidMaterial.onFrame(mVertexBuffer, mIndexBuffer, mBuffer->getIndexCount(), mLiquidData,
                                       mTextures[static_cast<std::size_t>(textureIndex)]);
            }

            void LiquidRenderPass::syncLoad(Scene& scene, io::files::blp::TextureManagerPtr& textureManager) {
                const auto& context = scene.getContext();

                mVertexBuffer = context->createVertexBuffer(true);
                mIndexBuffer = context->createIndexBuffer(gx::IndexType::UINT32, true);

                mVertexBuffer->updateData(mBuffer->getVertices());
                mIndexBuffer->updateData(mBuffer->getIndices());

                for (const auto& texture : mBuffer->getTextureNames()) {
                    mTextures.emplace_back(textureManager->getTexture(texture));
                }
            }

            void LiquidRenderPass::asyncUnload() {
				const auto textureManager = cdi::ApplicationContext::instance()->produce<io::files::blp::TextureManager>();

                for(auto& texture : mTextures) {
					textureManager->releaseTexture(texture);
                }

				mTextures.clear();
            }

            void LiquidRenderPass::syncUnload() {
				mVertexBuffer = nullptr;
				mIndexBuffer = nullptr;
            }
        }
    }
}
