#include "stdafx.h"
#include "MapLiquidRender.h"
#include "utils/Constants.h"

namespace wowc {
    namespace scene {
        namespace liquid {

            void MapLiquidRender::syncLoad(Scene& scene) {
                for(auto& pass : mRenderPasses) {
					pass->syncLoad(scene, mTextureManager);
                }
            }

            MapLiquidRender::MapLiquidRender(uint32_t indexX, uint32_t indexY,
                                             io::files::blp::TextureManagerPtr textureManager) :
                mTextureManager(std::move(textureManager)) {

				mTransform = math::Matrix4::translate(indexX * utils::TILE_SIZE, indexY * utils::TILE_SIZE, 0.0f);

            }

            void MapLiquidRender::onFrame(Scene& scene) {
                if(!mIsAsyncLoaded || mIsEmpty) {
					return;
                }

                if(!mIsSyncLoaded) {
					syncLoad(scene);
					mIsSyncLoaded = true;
                }

                for(auto& pass : mRenderPasses) {
					pass->onFrame(scene);
                }
            }

            void MapLiquidRender::asyncLoad(const io::files::liquid::TileLiquidManagerPtr& liquidManager) {
                if(!liquidManager->hasAnyLiquid()) {
					mIsEmpty = true;
					mIsAsyncLoaded = true;
					return;
                }

				mIsEmpty = false;

				const auto& buffers = liquidManager->getBuffers();
				mRenderPasses.reserve(buffers.size());

                for(const auto& pair : buffers) {
					mRenderPasses.emplace_back(std::make_shared<LiquidRenderPass>(mTransform, pair.second));
                }

				mIsAsyncLoaded = true;
            }

            void MapLiquidRender::asyncUnload() {
                for(auto& pass : mRenderPasses) {
					pass->asyncUnload();
                }
            }

            void MapLiquidRender::syncUnload() {
				for (auto& pass : mRenderPasses) {
					pass->syncUnload();
				}
            }
        }
    }
}
