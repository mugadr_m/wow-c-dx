#pragma once
#include "gx/VertexBuffer.h"
#include "gx/IndexBuffer.h"
#include "gx/Texture.h"
#include "math/Matrix4.h"
#include "gx/GxContext.h"
#include "gx/Mesh.h"
#include "gx/Sampler.h"
#include "math/Vector4.h"

namespace wowc {
    namespace scene {
        namespace liquid {

#pragma pack(push, 1)
			struct LiquidData {
				math::Matrix4 transformMatrix;
				math::Vector4 liquidInfo;
			};
#pragma pack(pop)

            class LiquidMaterial {
				gx::MeshPtr mMesh;
				gx::ProgramPtr mProgram;

				gx::BlendStatePtr mBlendState;
				gx::SamplerPtr mSampler;
				gx::ConstantBufferPtr mConstantData;

            public:
				void onFrame(const gx::VertexBufferPtr& vertexBuffer, const gx::IndexBufferPtr& indexBuffer, uint32_t numIndices,
					const LiquidData& liquidData, const gx::TexturePtr& texture) const;

				void initialize(const gx::GxContextPtr& context);
            };

			extern LiquidMaterial liquidMaterial;
        }
    }
}
