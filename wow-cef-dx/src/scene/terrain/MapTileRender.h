#pragma once

#include "io/files/map/bfa/MapTile.h"
#include "gx/VertexBuffer.h"
#include "TerrainMaterial.h"
#include "MapChunkRender.h"
#include "MapTileVertex.h"
#include "scene/Scene.h"
#include "math/BoundingBox.h"
#include "scene/liquid/MapLiquidRender.h"
#include "math/Ray.h"

namespace wowc {
    namespace scene {
		class TerrainIntersectionResult;

        namespace terrain {
            class MapTileRender : public std::enable_shared_from_this<MapTileRender> {
                uint32_t mIndexX, mIndexY;
                
                gx::VertexBufferPtr mVertexBuffer;
				gx::VertexBufferPtr mGridVertexBuffer;

                std::vector<MapTileVertex> mVertices;
				std::vector<TileGridVertex> mGridVertices;

                std::vector<MapChunkRenderPtr> mMapChunks;
                std::vector<gx::TexturePtr> mTextures;
				std::vector<gx::TexturePtr> mHeightTextures;
				std::vector<gx::TexturePtr> mSpecularTextures;

                std::vector<float> mTextureScales;
				std::vector<float> mHeightScales;
				std::vector<float> mHeightOffsets;

				std::vector<float> mSpecularFactors;

                TerrainMaterialPtr mMaterial;

				liquid::MapLiquidRenderPtr mLiquidRender;

                math::BoundingBox mBoundingBox;

                bool mIsAsyncLoaded = false;
                bool mIsSyncLoaded = false;

				bool intersectChunk(const math::Ray& ray, uint32_t baseVertex, float& distance) const;

            public:
                MapTileRender(uint32_t ix, uint32_t iy);

                void asyncLoad(const io::files::map::bfa::MapTilePtr& tile);
                void syncLoad();

                uint32_t getIndexX() const {
                    return mIndexX;
                }

                uint32_t getIndexY() const {
                    return mIndexY;
                }

                void onFrame(Scene& scene);

                const gx::TexturePtr& getTexture(uint32_t textureIndex) const {
                    return mTextures[textureIndex];
                }

                const gx::TexturePtr& getHeightTexture(uint32_t textureIndex) const {
					return mHeightTextures[textureIndex];
                }

                const gx::TexturePtr& getSpecularTexture(uint32_t textureIndex) const {
					return mSpecularTextures[textureIndex];
                }

                float getTextureScale(uint32_t textureIndex) const {
					return mTextureScales[textureIndex];
                }

                float getHeightScale(uint32_t textureIndex) const {
					return mHeightScales[textureIndex];
                }

                float getHeightOffset(uint32_t textureIndex) const {
					return mHeightOffsets[textureIndex];
                }

				bool hasHeightTextures() const {
                    return !mHeightTextures.empty();
                }

                float getSpecularFactor(uint32_t textureIndex) const {
					return mSpecularFactors[textureIndex];
                }

				const MapChunkRenderPtr& getChunk(uint32_t index) const { return mMapChunks[index]; }

				void syncUnload();
				void asyncUnload();

				void handleIntersection(const math::Ray& ray, TerrainIntersectionResult& intersection) const;
            };

            SHARED_PTR(MapTileRender);
        }
    }
}
