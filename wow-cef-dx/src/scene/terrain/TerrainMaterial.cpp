#include "stdafx.h"
#include "TerrainMaterial.h"
#include "res/ResourceManager.h"
#include "res/resource1.h"

namespace wowc {
    namespace scene {
        namespace terrain {
			std::vector<uint16_t> TerrainMaterial::mIndexValues;

            void TerrainMaterial::loadIndexBuffer(const gx::GxContextPtr& gxContext) {
				if (mIndexValues.empty()) {
					mIndexValues.resize(768);

					for (auto y = 0u; y < 8; ++y) {
						for (auto x = 0u; x < 8; ++x) {
							const auto i = y * 8 * 12 + x * 12;
							mIndexValues[i + 0] = y * 17 + x;
							mIndexValues[i + 2] = y * 17 + x + 9;
							mIndexValues[i + 1] = y * 17 + x + 1;

							mIndexValues[i + 3] = y * 17 + x + 1;
							mIndexValues[i + 5] = y * 17 + x + 9;
							mIndexValues[i + 4] = y * 17 + x + 18;

							mIndexValues[i + 6] = y * 17 + x + 18;
							mIndexValues[i + 8] = y * 17 + x + 9;
							mIndexValues[i + 7] = y * 17 + x + 17;

							mIndexValues[i + 9] = y * 17 + x + 17;
							mIndexValues[i + 11] = y * 17 + x + 9;
							mIndexValues[i + 10] = y * 17 + x;
						}
					}
				}

                mIndexBuffer = gxContext->createIndexBuffer(gx::IndexType::UINT16, true);
                mIndexBuffer->updateData(mIndexValues);
            }

            void TerrainMaterial::initialize(const gx::GxContextPtr& gxContext) {
                const auto ctx = cdi::ApplicationContext::instance();
                const auto resourceManager = ctx->produce<res::ResourceManager>();

                mBlendState = gxContext->createBlendState();
                mBlendState->setBlendEnabled(false);

                const auto depthState = gxContext->createDepthState();
                depthState->setDepthEnabled(true);

                mMesh = gxContext->createMesh();

                mProgram = gxContext->createProgram();
                mProgram->loadFromResources(IDR_TERRAIN_SHADER_VERTEX, IDR_TERRAIN_SHADER_PIXEL);

                mMesh->setProgram(mProgram);

                mMesh->setIndexCount(256 * 3);
                mMesh->setBlendState(mBlendState);
                mMesh->setDepthState(depthState);

                loadIndexBuffer(gxContext);

                mMesh->setIndexBuffer(mIndexBuffer);
                mMesh->setIndexCount(768);

                mMesh->addElement("POSITION", 0, 3);
                mMesh->addElement("TEXCOORD", 0, 2);
                mMesh->addElement("TEXCOORD", 1, 2);
                mMesh->addElement("NORMAL", 0, 3);
                mMesh->addElement("COLOR", 0, 4, gx::DataType::BYTE, true);
                mMesh->addElement("COLOR", 1, 4, gx::DataType::BYTE, true);

                mMesh->finalize();

                mAlphaSampler = gxContext->createSampler();
                mAlphaSampler->setTextureAddressU(gx::TextureAddressMode::CLAMP);
                mAlphaSampler->setTextureAddressV(gx::TextureAddressMode::CLAMP);
                mAlphaSampler->setTextureFilter(D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT);

                mColorSampler = gxContext->createSampler();
                mColorSampler->setTextureAddressU(gx::TextureAddressMode::WRAP);
                mColorSampler->setTextureAddressV(gx::TextureAddressMode::WRAP);
                mColorSampler->setAnisotropicFilter();

                mScaleBuffer = gxContext->createConstantBuffer(16 * sizeof(float));
                float initialData[]{
                    1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
                };
                mScaleBuffer->updateData(initialData, 0, sizeof initialData);
            }

            void TerrainMaterial::startFrame(const gx::VertexBufferPtr& vertexBuffer) const {
                mScaleBuffer->bind(gx::ShaderType::PIXEL, 3);
                mMesh->setSampler(gx::ShaderType::PIXEL, 0, mAlphaSampler);
                mMesh->setSampler(gx::ShaderType::PIXEL, 1, mColorSampler);
                mMesh->setVertexBuffer(vertexBuffer);
                mMesh->beginFrame();
            }

            void TerrainMaterial::onFrame(uint32_t startVertex) const {
                mMesh->setStartVertex(startVertex);
                mMesh->draw();
            }

            void TerrainMaterial::setTextureScales(const std::vector<float>& scales,
                                                   const std::vector<float>& specularFactors,
                                                   const std::vector<float>& heightOffsets,
                                                   const std::vector<float>& heightScales) const {

                mScaleBuffer->updateData(scales.data(), 0, static_cast<uint32_t>(scales.size() * sizeof(float)));

                mScaleBuffer->updateData(heightOffsets.data(), static_cast<uint32_t>(4 * sizeof(float)),
                                         static_cast<uint32_t>(heightOffsets.size() * sizeof(float)));

                mScaleBuffer->updateData(heightScales.data(), static_cast<uint32_t>(8 * sizeof(float)),
                                         static_cast<uint32_t>(heightScales.size() * sizeof(float)));

                mScaleBuffer->updateData(specularFactors.data(), static_cast<uint32_t>(12 * sizeof(float)),
                                         static_cast<uint32_t>(4 * sizeof(float)));

                mScaleBuffer->commit();
            }

            void TerrainMaterial::setTextures(const gx::TexturePtr& alphaTexture,
                                              const std::vector<gx::TexturePtr>& textures,
                                              const std::vector<gx::TexturePtr>& specularTextures,
                                              const std::vector<gx::TexturePtr>& heightTextures) const {
                mMesh->setTexture(gx::ShaderType::PIXEL, 0, alphaTexture);
                mMesh->setTextures(gx::ShaderType::PIXEL, 1, textures);
                if (!heightTextures.empty()) {
                    mMesh->setTextures(gx::ShaderType::PIXEL, 5, heightTextures);
                }

                mMesh->setTextures(gx::ShaderType::PIXEL, 9, specularTextures);
            }
        }
    }
}
