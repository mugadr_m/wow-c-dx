#include "stdafx.h"
#include "TerrainGridMaterial.h"
#include "res/resource1.h"

namespace wowc {
    namespace scene {
        namespace terrain {
			const TerrainGridMaterialPtr GRID_MATERIAL{ std::make_shared<TerrainGridMaterial>() };

            void TerrainGridMaterial::loadIndices() const {
				std::vector<uint32_t> indices;
				indices.reserve(256 * 36 * 145);

                for(auto c = 0u; c < 256; ++c) {
					const auto baseVertex = c * 8 * 145;
                    for(auto v = 0u; v < 145; ++v) {
						auto bi = baseVertex + v * 8;
						const auto i0 = bi++;
						const auto i1 = bi++;
						const auto i2 = bi++;
						const auto i3 = bi++;
						const auto i4 = bi++;
						const auto i5 = bi++;
						const auto i6 = bi++;
						const auto i7 = bi;

#pragma region Quads
						indices.emplace_back(i0);
						indices.emplace_back(i1);
						indices.emplace_back(i2);

						indices.emplace_back(i0);
						indices.emplace_back(i2);
						indices.emplace_back(i3);

						indices.emplace_back(i7);
						indices.emplace_back(i6);
						indices.emplace_back(i5);

						indices.emplace_back(i7);
						indices.emplace_back(i5);
						indices.emplace_back(i4);

						indices.emplace_back(i5);
						indices.emplace_back(i6);
						indices.emplace_back(i2);

						indices.emplace_back(i5);
						indices.emplace_back(i2);
						indices.emplace_back(i1);

						indices.emplace_back(i4);
						indices.emplace_back(i5);
						indices.emplace_back(i1);

						indices.emplace_back(i4);
						indices.emplace_back(i1);
						indices.emplace_back(i0);

						indices.emplace_back(i7);
						indices.emplace_back(i4);
						indices.emplace_back(i0);

						indices.emplace_back(i7);
						indices.emplace_back(i0);
						indices.emplace_back(i3);

						indices.emplace_back(i6);
						indices.emplace_back(i7);
						indices.emplace_back(i3);

						indices.emplace_back(i6);
						indices.emplace_back(i3);
						indices.emplace_back(i2);
#pragma endregion 
                    }
                }

				mIndexBuffer->updateData(indices);
            }

            void TerrainGridMaterial::initialize(const gx::GxContextPtr& context) {
				mMesh = context->createMesh();
				const auto program = context->createProgram();

				const auto blendState = context->createBlendState();
				blendState->setBlendEnabled(true);
				blendState->setAlphaBlendDefaults();
				mMesh->setBlendState(blendState);

				mIndexBuffer = context->createIndexBuffer(gx::IndexType::UINT32, true);
				loadIndices();

				mMesh->setIndexCount(256 * 36 * 145);
				mMesh->setIndexBuffer(mIndexBuffer);

				program->loadFromResources(IDR_TERRAIN_GRID_SHADER_VERTEX, IDR_TERRAIN_GRID_SHADER_PIXEL);
				mMesh->setProgram(program);

				mMesh->addElement("POSITION", 0, 3);
				mMesh->addElement("COLOR", 0, 4, gx::DataType::BYTE, true);

				mMesh->finalize();
            }

            void TerrainGridMaterial::onFrame(const gx::VertexBufferPtr& tileBuffer) {
				mMesh->setVertexBuffer(tileBuffer);
				mMesh->beginFrame();
				mMesh->draw();
            }
        }
    }
}
