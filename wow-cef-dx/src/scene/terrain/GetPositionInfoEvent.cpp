#include "stdafx.h"
#include "GetPositionInfoEvent.h"

namespace wowc {
    namespace scene {
        namespace terrain {

            rapidjson::Document GetPositionInfoEvent::getResponseJson() const {
				rapidjson::Document document;
				auto& allocator = document.GetAllocator();

				auto& root = document.SetObject();
				rapidjson::Value positionObject(rapidjson::kObjectType);
				positionObject.AddMember("x", rapidjson::Value(mX), allocator);
				positionObject.AddMember("y", rapidjson::Value(mY), allocator);
				positionObject.AddMember("z", rapidjson::Value(mZ), allocator);

				root.AddMember("position", positionObject, allocator);

				rapidjson::Value mapObject(rapidjson::kObjectType);
				mapObject.AddMember("id", rapidjson::Value(mMapId), allocator);
				mapObject.AddMember("name", rapidjson::Value(mMapName.c_str(), allocator), allocator);

				root.AddMember("map", mapObject, allocator);

				rapidjson::Value areaObject(rapidjson::kObjectType);
				areaObject.AddMember("id", rapidjson::Value(mAreaId), allocator);
				areaObject.AddMember("name", rapidjson::Value(mAreaName.c_str(), allocator), allocator);

				root.AddMember("area", areaObject, allocator);

				rapidjson::Value intersectionObject(rapidjson::kObjectType);
				rapidjson::Value terrainObject(rapidjson::kObjectType);
				terrainObject.AddMember("hasHit", rapidjson::Value(mHasTerrainIntersection), allocator);
				terrainObject.AddMember("x", rapidjson::Value(mTerrainIntersection.x()), allocator);
				terrainObject.AddMember("y", rapidjson::Value(mTerrainIntersection.y()), allocator);
				terrainObject.AddMember("z", rapidjson::Value(mTerrainIntersection.z()), allocator);

				intersectionObject.AddMember("terrain", terrainObject, allocator);
				root.AddMember("intersection", intersectionObject, allocator);

				return document;

            }
        }
    }
}