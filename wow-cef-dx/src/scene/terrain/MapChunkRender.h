#pragma once

#include "io/files/map/bfa/MapChunk.h"
#include "TerrainMaterial.h"
#include "scene/Scene.h"
#include "gx/Texture.h"
#include "math/BoundingBox.h"

namespace wowc {
    namespace scene {
        namespace terrain {
			SHARED_FWD(MapTileRender);

            class MapChunkRender {
				bool mIsAsyncLoaded = false;
				bool mIsSyncLoaded = false;

				uint32_t mAreaId = 0;

				uint32_t mStartVertex = 0;

				math::BoundingBox mBoundingBox;

				std::vector<uint32_t> mAlphaData;
				gx::TexturePtr mAlphaTexture;
				
				std::vector<gx::TexturePtr> mTextures;
				std::vector<gx::TexturePtr> mHeightTextures; 
                std::vector<gx::TexturePtr> mSpecularTextures;

				std::vector<float> mTextureScales;
				std::vector<float> mHeightOffsets;
				std::vector<float> mHeightScales;

				std::vector<float> mSpecularFactors;

				TerrainMaterialPtr mMaterial;
            public:
				void asyncLoad(const MapTileRenderPtr& tile, const io::files::map::bfa::MapChunkPtr& chunk, uint32_t startVertex); 
                void syncLoad(const TerrainMaterialPtr& material);

				void onFrame(Scene& scene);

				const uint32_t& getAreaId() const { return mAreaId; }

				void syncUnload();
				void asyncUnload();

                bool isAsyncLoaded() const {
					return mIsAsyncLoaded;
                }

				const math::BoundingBox& getBoundingBox() const {
					return mBoundingBox;
                }

                uint32_t getStartVertex() const {
					return mStartVertex;
                }
            };

			SHARED_PTR(MapChunkRender);
        }
    }
}
