#include "stdafx.h"
#include "MapTileRender.h"
#include "cdi/ApplicationContext.h"
#include "gx/GxContext.h"
#include "TerrainMaterialManager.h"
#include "io/files/blp/TextureManager.h"
#include "scene/liquid/LiquidManager.h"
#include "scene/m2/M2ModelManager.h"
#include "scene/IntersectionResult.h"
#include "TerrainGridMaterial.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            bool MapTileRender::intersectChunk(const math::Ray& ray, uint32_t baseVertex, float& distance) const {
                auto minDist{std::numeric_limits<float>::max()};
                auto hasHit{false};

                const auto& dir = ray.getDirection();
                const auto& orig = ray.getPosition();

                const auto& indices = TerrainMaterial::getIndexValues();

                for (auto i = 0u; i < indices.size(); i += 3) {
                    const auto& v0 = mVertices[indices[i] + baseVertex];
                    const auto& v1 = mVertices[indices[i + 1] + baseVertex];
                    const auto& v2 = mVertices[indices[i + 2] + baseVertex];

                    const auto p0 = math::Vector3{v0.x, v0.y, v0.z};
                    const auto p1 = math::Vector3{v1.x, v1.y, v1.z};
                    const auto p2 = math::Vector3{v2.x, v2.y, v2.z};

                    const auto e1 = p1 - p0;
                    const auto e2 = p2 - p0;

                    const auto p = dir.cross(e2);
                    const auto det = e1.dot(p);

                    if (std::abs(det) < 1e-4) {
                        continue;
                    }

                    const auto invDet = 1.0f / det;
                    const auto tv = orig - p0;
                    const auto u = tv.dot(p) * invDet;
                    if (u < 0 || u > 1) {
                        continue;
                    }

                    const auto q = tv.cross(e1);
                    const auto v = dir.dot(q) * invDet;
                    if (v < 0 || (u + v) > 1) {
                        continue;
                    }

                    const auto t = e2.dot(q) * invDet;
                    if (t < 1e-4) {
                        continue;
                    }

                    hasHit = true;
                    if (t < minDist) {
                        minDist = t;
                    }
                }

                if (hasHit) {
                    distance = minDist;
                }

                return hasHit;
            }

            MapTileRender::MapTileRender(uint32_t ix, uint32_t iy) : mIndexX(ix), mIndexY(iy) {
            }

            void MapTileRender::asyncLoad(const io::files::map::bfa::MapTilePtr& tile) {
                const auto context = cdi::ApplicationContext::instance();
                const auto textureManager = context->produce<io::files::blp::TextureManager>();
                const auto liquidManager = context->produce<liquid::LiquidManager>();
                const auto m2Manager = context->produce<m2::M2ModelManager>();

                mVertices = tile->getVertices();
                mTextureScales = tile->getTextureScales();
                mHeightScales = tile->getHeightScales();
                mHeightOffsets = tile->getHeightOffsets();

                const auto textures = tile->getTextures();
                for (const auto& textureName : textures) {
                    mTextures.emplace_back(textureManager->getTexture(textureName));
                }

                const auto heightTextures = tile->getHeightTextures();
                for (const auto& heightTexture : heightTextures) {
                    mHeightTextures.emplace_back(textureManager->getTexture(heightTexture, true));
                }

                const auto specularTextures = tile->getSpecularTextures();
                for (const auto& specularTexture : specularTextures) {
                    const auto exists = textureManager->exists(specularTexture);
                    mSpecularTextures.emplace_back(textureManager->getTexture(specularTexture, true));
                    mSpecularFactors.emplace_back(exists ? 1.0f : 0.0f);
                }

                for (const auto& model : tile->getM2Models()) {
                    const math::Vector3 position(model.definition.x, model.definition.z, model.definition.y);
                    const math::Vector3 rotation(model.definition.rx, model.definition.ry, model.definition.rz);
                    const auto scale = model.definition.scale / 1024.0f;
                    const auto uuid = model.definition.uniqueId;

                    if (model.isFileDataId) {
                        m2Manager->addModel(uuid, model.fileDataId, position, scale, rotation);
                    } else {
                        m2Manager->addModel(uuid, model.fileName, position, scale, rotation);
                    }
                }

                mGridVertices.resize(145 * 8 * 256);
                const auto& gridVertices = tile->getGridVertices();
                std::transform(gridVertices.begin(), gridVertices.end(), mGridVertices.begin(),
                               [](const math::Vector3& v) -> TileGridVertex {
                                   return {
                                       v.x(), v.y(), v.z(),
                                       0xAAFFFFFF
                                   };
                               });

                mMapChunks.resize(256);

                const auto chunks = tile->getChunks();
                auto startVertex = 0u;
                std::transform(chunks.begin(), chunks.end(), mMapChunks.begin(),
                               [&](const io::files::map::bfa::MapChunkPtr& chunk) {
                                   if (chunk == nullptr) {
                                       return MapChunkRenderPtr(nullptr);
                                   }

                                   const auto cnk = std::make_shared<MapChunkRender>();
                                   cnk->asyncLoad(shared_from_this(), chunk, startVertex);
                                   startVertex += 145;
                                   return cnk;
                               });

                mBoundingBox.set(tile->getBboxMin(), tile->getBboxMax());

                if (tile->getLiquidManager()->hasAnyLiquid()) {
                    mLiquidRender = std::make_shared<liquid::MapLiquidRender>(mIndexX, mIndexY, textureManager);
                    mLiquidRender->asyncLoad(tile->getLiquidManager());
                    liquidManager->loadTile(mLiquidRender);
                }

                mIsAsyncLoaded = true;
            }

            void MapTileRender::syncLoad() {
                const auto context = cdi::ApplicationContext::instance();
                const auto gxContext = context->produce<gx::GxContext>();

                mMaterial = context->produce<TerrainMaterialManager>()->getMaterial();

                mVertexBuffer = gxContext->createVertexBuffer(true);
                mVertexBuffer->updateData(mVertices);

                mGridVertexBuffer = gxContext->createVertexBuffer(true);
                mGridVertexBuffer->updateData(mGridVertices);

                for (auto& chunk : mMapChunks) {
                    chunk->syncLoad(mMaterial);
                }

                mIsSyncLoaded = true;
            }

            void MapTileRender::onFrame(Scene& scene) {
                if (!mIsAsyncLoaded) {
                    return;
                }

                if (!mIsSyncLoaded) {
                    syncLoad();
                }

                if (!scene.isVisible(mBoundingBox)) {
                    return;
                }

                mMaterial->startFrame(mVertexBuffer);

                for (auto& chunk : mMapChunks) {
                    chunk->onFrame(scene);
                }

                if (scene.showTerrainVertices()) {
                    GRID_MATERIAL->onFrame(mGridVertexBuffer);
                }
            }

            void MapTileRender::syncUnload() {
                if (!mIsSyncLoaded) {
                    return;
                }

                mIsSyncLoaded = false;
                mVertexBuffer = nullptr;

                for (auto& chunk : mMapChunks) {
                    chunk->syncUnload();
                }

                const auto context = cdi::ApplicationContext::instance();
                const auto textureManager = context->produce<io::files::blp::TextureManager>();

                for (auto& texture : mTextures) {
                    textureManager->releaseTexture(texture);
                }

                for (auto& heightTexture : mHeightTextures) {
                    textureManager->releaseTexture(heightTexture);
                }

                mTextures.clear();
                mHeightTextures.clear();
            }

            void MapTileRender::asyncUnload() {
                const auto context = cdi::ApplicationContext::instance();

                mVertices.clear();
                mTextureScales.clear();
                mHeightOffsets.clear();

                for (auto& chunk : mMapChunks) {
                    chunk->asyncUnload();
                }

                if (mLiquidRender != nullptr) {
                    const auto liquidManager = context->produce<liquid::LiquidManager>();
                    liquidManager->unloadTile(mLiquidRender);
                }
            }

            void MapTileRender::handleIntersection(const math::Ray& ray,
                                                   TerrainIntersectionResult& intersection) const {
                if (!mIsAsyncLoaded) {
                    return;
                }

                auto baseDistance{0.0f};
                if (!ray.intersects(mBoundingBox, baseDistance)) {
                    return;
                }

                if (intersection.isHasHit() && baseDistance > intersection.getDistance()) {
                    return;
                }

                auto hasHit{false};
                auto closestHit{0.0f};

                for (const auto& chunk : mMapChunks) {
                    auto chunkDistance{0.0f};
                    if (!chunk->isAsyncLoaded() || !ray.intersects(chunk->getBoundingBox(), chunkDistance)) {
                        continue;
                    }

                    if (hasHit && closestHit < chunkDistance) {
                        continue;
                    }

                    if (!intersectChunk(ray, chunk->getStartVertex(), chunkDistance)) {
                        continue;
                    }

                    if (hasHit && closestHit < chunkDistance) {
                        continue;
                    }

                    hasHit = true;
                    closestHit = chunkDistance;
                }

                if (!hasHit) {
                    return;
                }

                if (intersection.isHasHit() && closestHit > intersection.getDistance()) {
                    return;
                }

                intersection.setDistance(closestHit);
                intersection.setHasHit(true);
            }
        }
    }
}
