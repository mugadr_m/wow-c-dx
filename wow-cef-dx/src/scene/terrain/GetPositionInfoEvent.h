#pragma once
#include <document.h>
#include "math/Vector3.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            class GetPositionInfoEvent {
				float mX = 0.0f;
				float mY = 0.0f;
				float mZ = 0.0f;

				std::string mMapName;
				uint32_t mMapId = 0;

				std::string mAreaName;
				uint32_t mAreaId = 0;

				bool mHasTerrainIntersection = false;
				math::Vector3 mTerrainIntersection{};

            public:
                float getX() const {
                    return mX;
                }

                void setX(const float x) {
                    mX = x;
                }

                float getY() const {
                    return mY;
                }

                void setY(const float y) {
                    mY = y;
                }

                float getZ() const {
                    return mZ;
                }

                void setZ(const float z) {
                    mZ = z;
                }

                void setMapName(std::string mapName) {
					mMapName = std::move(mapName);
                }

                const std::string& getMapName() const {
					return mMapName;
                }

                uint32_t getMapId() const {
                    return mMapId;
                }

                void setMapId(const uint32_t mapId) {
                    mMapId = mapId;
                }

                const std::string& getAreaName() const {
                    return mAreaName;
                }

                void setAreaName(const std::string& areaName) {
                    mAreaName = areaName;
                }

                uint32_t getAreaId() const {
                    return mAreaId;
                }

                void setAreaId(const uint32_t areaId) {
                    mAreaId = areaId;
                }

                bool isHasTerrainIntersection() const {
                    return mHasTerrainIntersection;
                }

                void setHasTerrainIntersection(const bool hasTerrainIntersection) {
                    mHasTerrainIntersection = hasTerrainIntersection;
                }

                const math::Vector3& getTerrainIntersection() const {
                    return mTerrainIntersection;
                }

                void setTerrainIntersection(const math::Vector3& terrainIntersection) {
                    mTerrainIntersection = terrainIntersection;
                }

				void toJson(rapidjson::Document& document) const {
				}

				rapidjson::Document getResponseJson() const;
				static GetPositionInfoEvent fromJson(const rapidjson::Document& document) { return {}; }
            };
        }
    }
}
