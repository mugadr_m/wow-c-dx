#pragma once

#include "TerrainMaterial.h"
#include "gx/GxContext.h"

namespace wowc {
    namespace scene {
        namespace terrain {
			class TerrainMaterialManager {
				TerrainMaterialPtr mMaterial;

			public:
				void initialize(const gx::GxContextPtr& context);

				const TerrainMaterialPtr& getMaterial() const { return mMaterial; }
			};

			SHARED_PTR(TerrainMaterialManager);
        }
    }
}
