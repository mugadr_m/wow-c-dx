#pragma once

namespace wowc {
    namespace scene {
        namespace terrain {

#pragma pack(push, 1)
			struct MapTileVertex {
				float x, y, z;
				float tx, ty;
				float ax, ay;
				float nx, ny, nz;
				uint32_t mccv;
				uint32_t mclv;
			};

            struct TileGridVertex {
				float x, y, z;
				uint32_t color;
            };
#pragma pack(pop)
        }
    }
}
