#pragma once

#include "gx/Mesh.h"
#include "ui/WebCore.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            class TerrainMaterial {
				static std::vector<uint16_t> mIndexValues;

                gx::MeshPtr mMesh;
                gx::ProgramPtr mProgram;
                gx::BlendStatePtr mBlendState;
                gx::SamplerPtr mAlphaSampler;
                gx::SamplerPtr mColorSampler;

                gx::ConstantBufferPtr mScaleBuffer;

                gx::IndexBufferPtr mIndexBuffer;

                void loadIndexBuffer(const gx::GxContextPtr& gxContext);

            public:
                void initialize(const gx::GxContextPtr& gxContext);

                void startFrame(const gx::VertexBufferPtr& vertexBuffer) const;

                void onFrame(uint32_t baseVertex) const;

                void setTextureScales(const std::vector<float>& scales,
                                      const std::vector<float>& specularFactors,
                                      const std::vector<float>& heightOffsets,
                                      const std::vector<float>& heightScales) const;

                void setTextures(const gx::TexturePtr& alphaTexture,
                                 const std::vector<gx::TexturePtr>& textures,
                                 const std::vector<gx::TexturePtr>& specularTextures,
                                 const std::vector<gx::TexturePtr>& heightTextures) const;

                static std::vector<uint16_t>& getIndexValues() {
					return mIndexValues;
                }
            };

            SHARED_PTR(TerrainMaterial);
        }
    }
}
