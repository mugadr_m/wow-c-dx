#include "stdafx.h"
#include "MapManager.h"
#include "utils/Constants.h"
#include <boost/thread/future.hpp>
#include "MapTileRender.h"
#include "GetPositionInfoEvent.h"
#include "io/files/storage/StorageManager.h"
#include "scene/WorldFrame.h"

namespace wowc {
    namespace scene {
        namespace terrain {

            uint32_t MapManager::getIndex(uint32_t ix, uint32_t iy) {
                return ix + iy * 64;
            }

            void MapManager::splitIndex(uint32_t index, uint32_t& ix, uint32_t& iy) {
                ix = index % 64;
                iy = index / 64;
            }

            void MapManager::handleTileIndexChanged() {
                const auto tx = static_cast<int32_t>(mPosition.x() / utils::TILE_SIZE);
                const auto ty = static_cast<int32_t>(mPosition.y() / utils::TILE_SIZE);

                std::list<uint32_t> outOfRangeTiles;
                findOutOfRangeTiles(tx, ty, outOfRangeTiles);
                onUnloadTiles(outOfRangeTiles);

                std::list<uint32_t> tilesToLoad;
                findTilesToLoad(tx, ty, tilesToLoad);
                onLoadTiles(tilesToLoad);
            }

            void MapManager::handleTileUpdate(const math::Vector3& position) {
                const auto tx = static_cast<int32_t>(position.x() / utils::TILE_SIZE);
                const auto ty = static_cast<int32_t>(position.y() / utils::TILE_SIZE);
                const auto tIndex = getIndex(tx, ty);
                if (tIndex == mCurrentIndex) {
                    return;
                }


                mCurrentIndex = tIndex;
                handleTileIndexChanged();
            }

            void MapManager::updateZoneInformation() {
                const auto tx = static_cast<int32_t>(mPosition.x() / utils::TILE_SIZE);
                const auto ty = static_cast<int32_t>(mPosition.y() / utils::TILE_SIZE);
                const auto cx = static_cast<int32_t>((mPosition.x() - tx * utils::TILE_SIZE) / utils::CHUNK_SIZE);
                const auto cy = static_cast<int32_t>((mPosition.y() - ty * utils::TILE_SIZE) / utils::CHUNK_SIZE);

                const auto tIndex = getIndex(tx, ty);
                const auto tileItr = mActiveTiles.find(tIndex);
                if (tileItr == mActiveTiles.end() || cx > 15 || cx < 0 || cy > 15 || cy < 0) {
                    mZoneId = 0;
                    mZoneName = "unknown";
                    return;
                }

                const auto areaId = tileItr->second->getChunk(cy * 16 + cx)->getAreaId();
                if (areaId == mZoneId) {
                    return;
                }

                mZoneId = areaId;
                try {
                    mZoneName = mMapDao->getAreaName(mZoneId);
                } catch (std::exception&) {
                    mZoneName = "Not Found";
                }
            }

            void MapManager::findOutOfRangeTiles(int32_t tileX, int32_t tileY, std::list<uint32_t>& outOfRangeList) {
                for (auto& pair : mActiveTiles) {
                    const auto& tile = pair.second;
                    const auto ctx = static_cast<int32_t>(tile->getIndexX());
                    const auto cty = static_cast<int32_t>(tile->getIndexY());

                    if (std::abs(tileX - ctx) > TILE_LOAD_RADIUS ||
                        std::abs(tileY - cty) > TILE_LOAD_RADIUS) {
                        outOfRangeList.emplace_back(getIndex(ctx, cty));
                    }
                }
            }

            void MapManager::onUnloadTiles(const std::list<uint32_t>& unloadIndices) {
                for (const auto& idx : unloadIndices) {
                    const auto itr = mActiveTiles.find(idx);
                    if (itr == mActiveTiles.end()) {
                        continue;
                    }

                    mUnloadTiles.emplace_back(itr->second);
                    mSyncUnloadTiles.emplace_back(itr->second);
                    mActiveTiles.erase(itr);
                }
            }

            void MapManager::findTilesToLoad(int32_t tileX, int32_t tileY, std::list<uint32_t>& loadList) {
                for (auto y = tileY - TILE_LOAD_RADIUS; y <= tileY + TILE_LOAD_RADIUS; ++y) {
                    for (auto x = tileX - TILE_LOAD_RADIUS; x <= tileX + TILE_LOAD_RADIUS; ++x) {
                        const auto index = getIndex(x, y);
                        if (mActiveTiles.find(index) != mActiveTiles.end()) {
                            continue;
                        }

                        loadList.emplace_back(index);
                    }
                }
            }

            void MapManager::onLoadTiles(const std::list<uint32_t>& loadTiles) {
                std::copy(loadTiles.begin(), loadTiles.end(), std::back_inserter(mLoadIndices));
            }

            void MapManager::loaderThreadProc() {
                while (!mIsShutdown) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    while (!mLoadIndices.empty()) {
                        const auto index = mLoadIndices.front();
                        mLoadIndices.pop_front();
                        uint32_t x, y;
                        splitIndex(index, x, y);
                        if (y < 0 || x < 0 || y >= 64 || x >= 64) {
                            continue;
                        }

                        const auto tile = mTileLoader->loadTile(mContinent, x, y);
                        if (tile == nullptr) {
                            continue;
                        }

                        const auto renderTile = std::make_shared<MapTileRender>(
                            static_cast<uint32_t>(x), static_cast<uint32_t>(y));
                        renderTile->asyncLoad(tile);
                        mPendingTiles.push_back(renderTile);
                    }

                    while (!mUnloadTiles.empty()) {
                        const auto tile = mUnloadTiles.front();
                        mUnloadTiles.pop_front();
                        tile->asyncUnload();
                    }
                }
            }

            void MapManager::onEnterWorld(uint32_t mapId, const std::string& continent, const math::Vector2& position) {
                mContinent = continent;

                const auto tx = static_cast<int32_t>(position.x() / utils::TILE_SIZE);
                const auto ty = static_cast<int32_t>(position.y() / utils::TILE_SIZE);

                mMapId = mapId;
                try {
                    mMapName = mMapDao->getMapName(mapId);
                } catch (std::exception&) {
                    mMapName = "Unknown Map";
                }

                mPosition = math::Vector3(position.x(), position.y(), 500.0f);

                mCurrentIndex = getIndex(tx, ty);

                mLoadFuture = async(boost::launch::async, [=]() {
                    mTileLoader->onEnterWorld(continent);
                    mSkyManager->onEnterWorld(mapId);

                    for (auto y = ty - TILE_LOAD_RADIUS; y <= ty + TILE_LOAD_RADIUS; ++y) {
                        for (auto x = tx - TILE_LOAD_RADIUS; x <= tx + TILE_LOAD_RADIUS; ++x) {
                            if (mIsShutdown) {
                                return;
                            }

                            if (y < 0 || x < 0 || y >= 64 || x >= 64) {
                                continue;
                            }

                            const auto tile = mTileLoader->loadTile(continent, x, y);
                            if (tile == nullptr) {
                                continue;
                            }

                            const auto renderTile = std::make_shared<MapTileRender>(
                                static_cast<uint32_t>(x), static_cast<uint32_t>(y));
                            renderTile->asyncLoad(tile);
                            mPendingTiles.push_back(renderTile);
                        }
                    }
                }).share();
            }

            void MapManager::onFrame(Scene& scene) {
                mSkyManager->onFrame(scene);

                while (!mPendingTiles.empty()) {
                    const auto tile = mPendingTiles.front();
                    mPendingTiles.pop_front();
                    const auto index = getIndex(tile->getIndexX(), tile->getIndexY());
                    mActiveTiles.emplace(index, tile);
                }

                while (!mSyncUnloadTiles.empty()) {
                    const auto tile = mSyncUnloadTiles.front();
                    mSyncUnloadTiles.pop_front();
                    tile->syncUnload();
                }

                for (auto& pair : mActiveTiles) {
                    pair.second->onFrame(scene);
                }
            }

            void MapManager::postConstruct(const cdi::ApplicationContextPtr& ctx) {
                mTileLoader = ctx->produce<io::files::map::MapTileLoader>();
                mSkyManager = ctx->produce<io::files::sky::SkyManager>();

                ctx->registerEvent<GetPositionInfoEvent>([=](GetPositionInfoEvent& event) {
					const auto& wf = ctx->produce<WorldFrame>();

                    event.setX(mPosition.x());
                    event.setY(mPosition.y());
                    event.setZ(mPosition.z());
                    event.setMapId(mMapId);
                    event.setMapName(mMapName);
                    event.setAreaId(mZoneId);
                    event.setAreaName(mZoneName);
					event.setHasTerrainIntersection(wf->getTerrainIntersection().isHasHit());
					event.setTerrainIntersection(wf->getTerrainIntersection().getPosition());
                });

                ctx->registerEvent<io::files::DataLoadCompleteEvent>([=](io::files::DataLoadCompleteEvent&) {
                    mMapDao = ctx->produce<io::files::storage::StorageManager>()->getMapStroageDao();
                });

                mLoadThread = std::thread(std::bind(&MapManager::loaderThreadProc, this));
            }

            void MapManager::preDestroy() {
                mIsShutdown = true;

                if (mLoadFuture.valid()) {
                    mLoadFuture.wait();
                }

                if (mLoadThread.joinable()) {
                    mLoadThread.join();
                }

                mSkyManager->onShutdown();
            }

            TerrainIntersectionResult MapManager::intersect(const math::Ray& ray) const {
                TerrainIntersectionResult result{};
                for(const auto& tilePair : mActiveTiles) {
					tilePair.second->handleIntersection(ray, result);
                }

                if(result.isHasHit()) {
					result.setPosition(ray.getPosition() + result.getDistance() * ray.getDirection());
                }

				return result;
            }

            void MapManager::attachUiEvents(const cdi::ApplicationContextPtr& ctx) {
                ctx->produce<ui::events::EventManager>()->registerEvent<GetPositionInfoEvent>();
            }

            void MapManager::onPositionChanged(const math::Vector3& position) {
                mPosition = position;
                mSkyManager->onPositionChanged(position);
                handleTileUpdate(position);
                updateZoneInformation();
            }
        }
    }
}
