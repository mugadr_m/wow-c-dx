#include "stdafx.h"
#include "TerrainMaterialManager.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            void TerrainMaterialManager::initialize(const gx::GxContextPtr& context) {
				mMaterial = std::make_shared<TerrainMaterial>();
				mMaterial->initialize(context);
            }
        }
    }
}
