#include "stdafx.h"
#include "MapChunkRender.h"
#include "MapTileRender.h"

namespace wowc {
    namespace scene {
        namespace terrain {

            void MapChunkRender::syncLoad(const TerrainMaterialPtr& material) {
                mMaterial = material;
                mIsSyncLoaded = true;
            }

            void MapChunkRender::asyncLoad(const MapTileRenderPtr& tile, const io::files::map::bfa::MapChunkPtr& chunk, uint32_t startVertex) {
				mAreaId = chunk->getAreaId();
                mStartVertex = startVertex;
                mAlphaData = chunk->getAlphaValues();

				mSpecularFactors.assign(4, 0.0f);

				auto index = 0u;
                for(const auto& texture : chunk->getTextures()) {
					mTextures.push_back(tile->getTexture(texture));
					mTextureScales.push_back(tile->getTextureScale(texture));
					if (tile->hasHeightTextures()) {
						mHeightTextures.push_back(tile->getHeightTexture(texture));
						mHeightScales.push_back(tile->getHeightScale(texture));
						mHeightOffsets.push_back(tile->getHeightOffset(texture));
					}

					mSpecularFactors[index++] = tile->getSpecularFactor(texture);
					mSpecularTextures.push_back(tile->getSpecularTexture(texture));
                }

				mBoundingBox.set(chunk->getBboxMin(), chunk->getBboxMax());

                mIsAsyncLoaded = true;
            }

            void MapChunkRender::onFrame(Scene& scene) {
                if (!mIsAsyncLoaded) {
                    return;
                }

                if (!mIsSyncLoaded) {
                    return;
                }

                if(!scene.isVisible(mBoundingBox)) {
					return;
                }

                if (mAlphaTexture == nullptr) {
					mAlphaTexture = scene.getContext()->createTexture();
					mAlphaTexture->updateArgb8(64, 64, mAlphaData);
                }

				mMaterial->setTextureScales(mTextureScales, mSpecularFactors, mHeightOffsets, mHeightScales);
				mMaterial->setTextures(mAlphaTexture, mTextures, mSpecularTextures, mHeightTextures);
                mMaterial->onFrame(mStartVertex);
            }

            void MapChunkRender::syncUnload() {
                if(!mIsSyncLoaded) {
					return;
                }

				mIsSyncLoaded = false;
				mAlphaTexture = nullptr;
				mTextures.clear();
				mHeightTextures.clear();
            }

            void MapChunkRender::asyncUnload() {
				mAlphaData.clear();
				mIsAsyncLoaded = false;
            }
        }
    }
}
