#pragma once

#include "math/Vector2.h"
#include "io/files/map/MapTileLoader.h"
#include <boost/thread/future.hpp>
#include "MapTileRender.h"
#include "scene/Scene.h"
#include "io/files/sky/SkyManager.h"
#include "io/files/storage/MapStorageDao.h"
#include "scene/IntersectionResult.h"
#include "math/Ray.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            class MapManager {
				static const int32_t TILE_LOAD_RADIUS = 3;

                io::files::map::MapTileLoaderPtr mTileLoader;
				io::files::sky::SkyManagerPtr mSkyManager;

				io::files::storage::MapStorageDaoPtr mMapDao;

				boost::shared_future<void> mLoadFuture;

				std::map<uint32_t, bool> mTileIndex;
				std::map<uint32_t, MapTileRenderPtr> mActiveTiles;
				std::deque<MapTileRenderPtr> mPendingTiles;
				uint32_t mCurrentIndex = 0;

				std::deque<MapTileRenderPtr> mUnloadTiles;
				std::deque<MapTileRenderPtr> mSyncUnloadTiles;
				std::deque<uint32_t> mLoadIndices;

				math::Vector3 mPosition;
				uint32_t mZoneId = -1;
				uint32_t mMapId = -1;
				std::string mZoneName;
				std::string mMapName;

				std::string mContinent;

				std::thread mLoadThread;
				bool mIsShutdown = false;

                static uint32_t getIndex(uint32_t ix, uint32_t iy);
				static void splitIndex(uint32_t index, uint32_t& ix, uint32_t& iy);

				void handleTileIndexChanged();
				void handleTileUpdate(const math::Vector3& position);
				void updateZoneInformation();

				void findOutOfRangeTiles(int32_t tileX, int32_t tileY, std::list<uint32_t>& outOfRangeList);
				void onUnloadTiles(const std::list<uint32_t>& unloadIndices);
				void findTilesToLoad(int32_t tileX, int32_t tileY, std::list<uint32_t>& loadList);
				void onLoadTiles(const std::list<uint32_t>& loadTiles);

				void loaderThreadProc();

            public:
				void onEnterWorld(uint32_t mapId, const std::string& continent, const math::Vector2& position);

				void onFrame(Scene& scene);

				void postConstruct(const cdi::ApplicationContextPtr& ctx);
				void preDestroy();
                TerrainIntersectionResult intersect(const math::Ray& ray) const;
                static void attachUiEvents(const cdi::ApplicationContextPtr& ctx);

				void onPositionChanged(const math::Vector3& position);
            };

			SHARED_PTR(MapManager);
        }
    }
}
