#pragma once

#include "gx/Mesh.h"
#include "gx/IndexBuffer.h"
#include "gx/BlendState.h"
#include "gx/GxContext.h"

namespace wowc {
    namespace scene {
        namespace terrain {
            class TerrainGridMaterial {
				gx::MeshPtr mMesh;
				gx::IndexBufferPtr mIndexBuffer;

				void loadIndices() const;

            public:
				void initialize(const gx::GxContextPtr& context);

				void onFrame(const gx::VertexBufferPtr& tileBuffer);
            };

			SHARED_PTR(TerrainGridMaterial);

			extern const TerrainGridMaterialPtr GRID_MATERIAL;
        }
    }
}