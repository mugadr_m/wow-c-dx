#pragma once

#include "cdi/ApplicationContext.h"
#include "Camera.h"
#include "gx/Window.h"
#include "gx/GxContext.h"
#include "GlobalShaderBufferManager.h"
#include "terrain/TerrainMaterialManager.h"
#include "EnterWorldEvent.h"
#include "terrain/MapManager.h"
#include "io/input/Keyboard.h"
#include "math/Frustum.h"
#include "Scene.h"
#include "sky/SkySphere.h"
#include "liquid/LiquidManager.h"
#include "RenderSettingsChangedEvent.h"
#include "m2/M2ModelManager.h"
#include "IntersectionResult.h"
#include "math/Ray.h"
#include "editing/EditManager.h"

namespace wowc {
    namespace ui {
		SHARED_FWD(WebCore);
    }

    namespace scene {

        class WorldFrame {
			std::chrono::steady_clock::time_point mStartTime;

			CameraPtr mCamera;
			GlobalShaderBufferManager mBufferManager;
			io::input::MousePtr mMouse;
			io::input::KeyboardPtr mKeyboard;

			math::Frustum mFrustum;
			Scene mScene;

			math::Vector2 mLastMousePosition;
			math::Vector2 mViewSize;
			TerrainIntersectionResult mTerrainIntersection;

            terrain::MapManagerPtr mMapManager;
			liquid::LiquidManagerPtr mLiquidManager;
			m2::M2ModelManagerPtr mM2ModelManager;

			terrain::TerrainMaterialManagerPtr mTerrainMaterialManager;

			editing::EditManagerPtr mEditManager;

			sky::SkySpherePtr mSkySphere;

            ui::WebCorePtr mWebCore;

			void handleEnterWorld(EnterWorldEvent& event);
			void handleRenderSettingsEvent(RenderSettingsChangedEvent& event);

			void onMatrixChanged(bool isView, const math::Matrix4& matrix);
			void onPositionChanged(const math::Vector3& position);

			void updateClickPosition();

            math::Ray buildClickRay() const;

			TerrainIntersectionResult intersectTerrain(const math::Ray& ray) const;
        public:
			WorldFrame();

			void onResize(const gx::WindowPtr& window);

			void onContextCreated(const gx::GxContextPtr& context);

			void postConstruct(const cdi::ApplicationContextPtr& ctx);
			void shutdown();

			void onFrame();

            void attachUiEvents(const cdi::ApplicationContextPtr& context, const ui::events::EventManagerPtr& eventManager);

            const TerrainIntersectionResult& getTerrainIntersection() const {
				return mTerrainIntersection;
            }

        };
    }
}
