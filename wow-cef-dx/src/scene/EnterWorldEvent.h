#pragma once
#include <document.h>

namespace wowc {
    namespace scene {
        class EnterWorldEvent {
            std::string mContinent;
            uint32_t mMapId;
            float mX, mY;

        public:
            EnterWorldEvent(std::string continent, uint32_t mapId, float x, float y)
                : mContinent(std::move(continent)),
                  mMapId(mapId),
                  mX(x),
                  mY(y) {
            }


            const std::string& getContinent() const {
                return mContinent;
            }

            uint32_t getMapId() const {
				return mMapId;
            }

            float getX() const {
                return mX;
            }

            float getY() const {
                return mY;
            }

            void toJson(rapidjson::Document& document) const {
            }

            rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
            static EnterWorldEvent fromJson(const rapidjson::Document& document);
        };
    }
}
