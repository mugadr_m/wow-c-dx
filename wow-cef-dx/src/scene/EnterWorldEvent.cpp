#include "stdafx.h"
#include "EnterWorldEvent.h"

namespace wowc {
    namespace scene {

        EnterWorldEvent EnterWorldEvent::fromJson(const rapidjson::Document& document) {
			const auto mapItr = document.FindMember("mapName");
			const auto xItr = document.FindMember("x");
			const auto yItr = document.FindMember("y");
			const auto mapIdItr = document.FindMember("mapId");

            if(mapItr == document.MemberEnd() || xItr == document.MemberEnd() || yItr == document.MemberEnd() || mapIdItr == document.MemberEnd()) {
				throw std::exception("Missing members for EnterWorldEvent");
            }

			return EnterWorldEvent(mapItr->value.GetString(), mapIdItr->value.GetInt(), xItr->value.GetFloat(), yItr->value.GetFloat());
        }
    }
}