#include "stdafx.h"
#include "WorldFrame.h"
#include "EnterWorldEvent.h"
#include "liquid/LiquidMaterial.h"
#include "m2/M2MaterialManager.h"
#include "m2/M2ModelManager.h"
#include "terrain/TerrainGridMaterial.h"
#include "ui/WebCore.h"

namespace wowc {
    namespace scene {
        void WorldFrame::handleEnterWorld(EnterWorldEvent& event) {
            mStartTime = std::chrono::steady_clock::now();

            mMapManager->onEnterWorld(event.getMapId(), event.getContinent(),
                                      math::Vector2(event.getX(), event.getY()));
            mCamera->setPosition(math::Vector3(event.getX(), event.getY(), 500.0f));
            mCamera->reloadMatrices();
        }

        void WorldFrame::handleRenderSettingsEvent(RenderSettingsChangedEvent& event) {
            mBufferManager.updateSpecularOverride(event.getSpecularTextures());
            mBufferManager.updateSpecularIntensity(event.getSpecularIntensity());
        }

        void WorldFrame::onMatrixChanged(bool isView, const math::Matrix4& matrix) {
            if (isView) {
                mBufferManager.updateViewMatrix(matrix);
                mScene.setViewChanged(true);
            } else {
                mBufferManager.updateProjectionMatrix(matrix);
            }

            mFrustum.updateFrustum(mCamera->getViewMatrix(), mCamera->getProjectionMatrix());
        }

        void WorldFrame::onPositionChanged(const math::Vector3& position) {
            mMapManager->onPositionChanged(position);
            mBufferManager.updateCameraPosition(position);
            mScene.setCameraPosition(position);
        }

        void WorldFrame::updateClickPosition() {
			const auto window = cdi::ApplicationContext::instance()->produce<gx::Window>();

            auto curPosition = mMouse->getLastPosition();
			window->screenToClient(curPosition);
            if (curPosition == mLastMousePosition && !mScene.hasViewChanged()) {
                mLastMousePosition = curPosition;
                return;
            }

            mLastMousePosition = curPosition;
            const auto clickRay = buildClickRay();

			mTerrainIntersection = intersectTerrain(clickRay);

            if(mTerrainIntersection.isHasHit()) {
				mBufferManager.updateMousePosition(mTerrainIntersection.getPosition());
            } else {
				static const auto MAX_FLOAT = std::numeric_limits<float>::max();
				mBufferManager.updateMousePosition({ MAX_FLOAT, MAX_FLOAT, MAX_FLOAT });
            }
        }

        math::Ray WorldFrame::buildClickRay() const {
            const auto invView = mCamera->getViewMatrix().inverted();
            const auto invProj = mCamera->getProjectionMatrix().inverted();

            const auto matrix = invProj * invView;
            const auto sx = 2 * mLastMousePosition.x() / mViewSize.x() - 1.0f;
            const auto sy = -(2 * mLastMousePosition.y() / mViewSize.y() - 1.0f);

            const math::Vector3 screenMin{sx, sy, 0.0f};
            const math::Vector3 screenMax{sx, sy, 1.0f};
            const auto nearPos = matrix * screenMin;
            const auto farPos = matrix * screenMax;

            const auto dir = (farPos - nearPos).normalized();
            return {nearPos, dir};
        }

        TerrainIntersectionResult WorldFrame::intersectTerrain(const math::Ray& ray) const {
            return mMapManager->intersect(ray);
        }

        WorldFrame::WorldFrame() : mScene(mFrustum, mBufferManager) {
            mStartTime = std::chrono::steady_clock::now();
        }

        void WorldFrame::onResize(const gx::WindowPtr& window) {
            uint32_t w = 0, h = 0;
            window->getSize(w, h);
            mCamera->setAspect(static_cast<float>(w) / h);
            mViewSize = math::Vector2{static_cast<float>(w), static_cast<float>(h)};
        }

        void WorldFrame::onContextCreated(const gx::GxContextPtr& context) {
            const auto ctx = cdi::ApplicationContext::instance();

            mBufferManager.initialize(context);
            mTerrainMaterialManager->initialize(context);
			terrain::GRID_MATERIAL->initialize(context);

            liquid::liquidMaterial.initialize(context);

            mScene.setContext(context);
            mSkySphere->initialize(context);

            mSkySphere->updateRadius(2000.0f);

            ctx->produce<m2::M2MaterialManager>()->onInitialize(context);

			mWebCore = ctx->produce<ui::WebCore>();
        }

        void WorldFrame::postConstruct(const cdi::ApplicationContextPtr& ctx) {
            mCamera = std::make_shared<Camera>();
            ctx->registerSingleton(mCamera);

            mCamera->addCallback(std::bind(&WorldFrame::onMatrixChanged, this, std::placeholders::_1,
                                           std::placeholders::_2));
            mCamera->addPositionCallback(std::bind(&WorldFrame::onPositionChanged, this, std::placeholders::_1));

            mMouse = ctx->produce<io::input::Mouse>();
            mKeyboard = ctx->produce<io::input::Keyboard>();

            mTerrainMaterialManager = ctx->produce<terrain::TerrainMaterialManager>();

            mMapManager = std::make_shared<terrain::MapManager>();
            ctx->registerSingleton(mMapManager);

            mSkySphere = ctx->produce<sky::SkySphere>();

            mLiquidManager = ctx->produce<liquid::LiquidManager>();

            mM2ModelManager = ctx->produce<m2::M2ModelManager>();

			mEditManager = std::make_shared<editing::EditManager>(mBufferManager);
			ctx->registerSingleton(mEditManager);
        }

        void WorldFrame::shutdown() {
            cdi::ApplicationContext::instance()->produce<m2::M2AnimationManager>()->shutdown();
        }

        void WorldFrame::onFrame() {
            const auto now = std::chrono::steady_clock::now();
            const auto runTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - mStartTime).count();

			mEditManager->onFrame(mScene);

            mScene.setRunTime(runTime);
			mBufferManager.updateBrushTime(runTime / 100.0f);

            mMouse->onUpdate();
            mKeyboard->onUpdate();

            mScene.setViewChanged(false);
			mCamera->onUpdate(mWebCore->hasInputFocus(), mMouse, mKeyboard);
            updateClickPosition();

            mBufferManager.onUpdate();
            mBufferManager.apply();

            mSkySphere->onFrame();
            mMapManager->onFrame(mScene);
            mLiquidManager->onFrame(mScene);
            mM2ModelManager->onFrame(mScene);
        }

        void WorldFrame::attachUiEvents(const cdi::ApplicationContextPtr& context,
                                        const ui::events::EventManagerPtr& eventManager) {
            eventManager->registerEvent<EnterWorldEvent>();
            context->registerEvent<EnterWorldEvent>(std::bind(&WorldFrame::handleEnterWorld, this,
                                                              std::placeholders::_1));

            eventManager->registerEvent<RenderSettingsChangedEvent>();
            context->registerEvent<RenderSettingsChangedEvent>(
                std::bind(&WorldFrame::handleRenderSettingsEvent, this, std::placeholders::_1));
        }
    }
}
