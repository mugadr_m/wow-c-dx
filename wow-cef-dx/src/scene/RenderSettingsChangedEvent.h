#pragma once
#include <document.h>

namespace wowc {
    namespace scene {
        class RenderSettingsChangedEvent {
            float mSpecularTextures = 0.0f;
            float mTextureScales = 0.0f;
            float mHeightTextures = 0.0f;
            float mSpecularIntensity = 20.0f;

        public:
            RenderSettingsChangedEvent(float specularTextures, float textureScales, float heightTextures,
                                       float specularIntensity)
                : mSpecularTextures(specularTextures),
                  mTextureScales(textureScales),
                  mHeightTextures(heightTextures),
                  mSpecularIntensity(specularIntensity) {
            }

            RenderSettingsChangedEvent() = default;

            float getSpecularTextures() const {
                return mSpecularTextures;
            }

            float getTextureScales() const {
                return mTextureScales;
            }

            float getHeightTextures() const {
                return mHeightTextures;
            }

            float getSpecularIntensity() const {
				return mSpecularIntensity;
            }

            void toJson(rapidjson::Document& document) const {
            }

            rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
            static RenderSettingsChangedEvent fromJson(const rapidjson::Document& document);
        };
    }
}
