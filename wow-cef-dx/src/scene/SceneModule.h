#pragma once

namespace wowc {
    namespace scene {
        class SceneModule {
        public:
			SceneModule() = delete;

			static void initialize();
			static void destroy();

			static void initUiEvents();
        };
    }
}