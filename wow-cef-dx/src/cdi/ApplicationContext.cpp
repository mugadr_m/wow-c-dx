#include "stdafx.h"
#include "ApplicationContext.h"

namespace wowc {
	namespace cdi {
		utils::Logger<ApplicationContext> ApplicationContext::mLogger;

		ApplicationContextPtr ApplicationContext::instance() {
			static auto instance = std::make_shared<ApplicationContext>();
			return instance;
		}
	}
}