#pragma once
#include "utils/Logger.h"

namespace wowc {
	namespace cdi {
		SHARED_FWD(ApplicationContext);

		class ApplicationContext : public std::enable_shared_from_this<ApplicationContext> {
			template <typename T>
			class HasPostConstruct {
				struct Small {
					char mDummy;
				};

				struct Big {
					char mDummy[2];
				};

				template <typename C>
				static Small test(decltype(&C::postConstruct));
				template <typename C>
				static Big test(...);

			public:
				static const bool VALUE = sizeof(test<T>(0)) == sizeof(Small);
			};

			template <typename T>
			class HasPreDestroy {
				struct Small {
					char mDummy;
				};

				struct Big {
					char mDummy[2];
				};

				template <typename C>
				static Small test(decltype(&C::preDestroy));
				template <typename C>
				static Big test(...);

			public:
				static const bool VALUE = sizeof(test<T>(0)) == sizeof(Small);
			};

			class ElementHolder {
			public:
				ElementHolder() = default;
				ElementHolder(const ElementHolder&) = default;
				ElementHolder(ElementHolder&&) = default;
				virtual ~ElementHolder() = default;

				ElementHolder& operator =(const ElementHolder&) = default;
				ElementHolder& operator =(ElementHolder&&) = default;
			};

			template <typename T>
			class GenericElementHolder : public ElementHolder {
			public:
				GenericElementHolder() = default;
				GenericElementHolder(const GenericElementHolder&) = default;
				GenericElementHolder(GenericElementHolder&&) = default;
				virtual ~GenericElementHolder() = default;

				GenericElementHolder& operator =(const GenericElementHolder&) = default;
				GenericElementHolder& operator =(GenericElementHolder&&) = default;

				virtual std::shared_ptr<T> produce() = 0;
				virtual bool isSingleton() const { return false; }
			};

			template <typename T>
			class SingletonHolder : public GenericElementHolder<T> {
				std::shared_ptr<T> mInstance;

			public:
				explicit SingletonHolder(const std::shared_ptr<T>& instance) : mInstance(instance) {
				}


				std::shared_ptr<T> produce() override { return mInstance; }

				bool isSingleton() const override { return true; }
			};

			template <typename T>
			class PrototypeHolder : public GenericElementHolder<T> {
			public:
				std::shared_ptr<T> produce() override { return std::make_shared<T>(); }
			};

			template <typename T>
			class CustomPrototypeHolder : public GenericElementHolder<T> {
				std::function<std::shared_ptr<T>()> mProducer;
			public:
				explicit CustomPrototypeHolder(const std::function<std::shared_ptr<T>()>& producer) : mProducer(producer) {
				}


				std::shared_ptr<T> produce() override { return mProducer(); }
			};

			class GenericInstanceHolder {
			public:
				virtual ~GenericInstanceHolder() = default;
				virtual void preDestroy() = 0;
			};

			template <typename T>
			class InstanceHolder : public GenericInstanceHolder {
				std::weak_ptr<T> mInstance;

				template <typename U>
				std::enable_if_t<HasPreDestroy<U>::VALUE, int> preDestroy(const std::shared_ptr<U>& instance) {
					instance->preDestroy();
					return 0;
				}

				template <typename U>
				typename std::enable_if<!HasPreDestroy<U>::VALUE, int>::type preDestroy(const std::shared_ptr<U>& /*instance*/) {
					return 0;
				}

			public:
				explicit InstanceHolder(const std::shared_ptr<T>& instance) : mInstance(instance) {

				}

				void preDestroy() override {
					auto elem = mInstance.lock();
					if (elem == nullptr) {
						return;
					}

					preDestroy(elem);
				}
			};

			class GenericEventHolder {
			public:
				virtual ~GenericEventHolder() {
				}
			};

			template <typename T>
			class EventHolder : public GenericEventHolder {
				std::function<void(T&)> mCallback;

			public:
				explicit EventHolder(const std::function<void (T&)> callback) : mCallback(callback) {

				}

				void onInvoke(T& eventValue) {
					mCallback(eventValue);
				}
			};

			static utils::Logger<ApplicationContext> mLogger;

			std::map<const std::type_info*, std::shared_ptr<ElementHolder>> mHolders;
			std::list<std::shared_ptr<GenericInstanceHolder>> mInstances;
			std::map<const std::type_info*, std::list<std::shared_ptr<GenericEventHolder>>> mEventHandlers;
			std::recursive_mutex mEventLock;

			template <typename T>
			std::enable_if_t<HasPostConstruct<T>::VALUE, void> postConstruct(const std::shared_ptr<T>& value) {
				value->postConstruct(shared_from_this());
			}

			template <typename T>
			std::enable_if_t<!HasPostConstruct<T>::VALUE, void> postConstruct(const std::shared_ptr<T>& /*value*/) {

			}

		public:
			static ApplicationContextPtr instance();

			void destroy() {
				for (const auto& instance : mInstances) {
					instance->preDestroy();
				}
				mHolders.clear();
			}

			template <typename T>
			void publishEvent(T& evnt) {
				auto& typeId = typeid(T);
				std::lock_guard<std::recursive_mutex> l(mEventLock);
				auto itr = mEventHandlers.find(&typeId);
				if (itr != mEventHandlers.end()) {
					std::list<std::shared_ptr<GenericEventHolder>> copyHolders(itr->second);
					for (auto& handler : copyHolders) {
						std::dynamic_pointer_cast<EventHolder<T>>(handler)->onInvoke(evnt);
					}
				}
			}

			template <typename T>
			void registerEvent(const std::function<void (T&)> callback) {
				auto& typeId = typeid(T);
				auto eventHandler = std::make_shared<EventHolder<T>>(callback);
				std::lock_guard<std::recursive_mutex> l(mEventLock);
				auto itr = mEventHandlers.find(&typeId);
				if (itr == mEventHandlers.end()) {
					mEventHandlers.emplace(&typeId, std::list<std::shared_ptr<GenericEventHolder>>{eventHandler});
				} else {
					itr->second.push_back(eventHandler);
				}
			}

			template <typename T>
			void registerSingleton() {
				registerSingleton(std::make_shared<T>());
			}

			template <typename T>
			void registerSingleton(const std::shared_ptr<T>& instance) {
				auto& typeId = typeid(T);
				auto itr = mHolders.find(&typeId);
				if (itr != mHolders.end()) {
					mLogger.error("Duplicate holder definition for ", typeId.name());
					throw std::exception("Duplicate dependency definition");
				}

				mHolders.emplace(&typeId, std::make_shared<SingletonHolder<T>>(instance));
				mInstances.emplace_back(std::make_shared<InstanceHolder<T>>(instance));
				postConstruct(instance);
			}

			template <typename T>
			void registerPrototype() {
				auto& typeId = typeid(T);
				auto itr = mHolders.find(&typeId);
				if (itr != mHolders.end()) {
					mLogger.error("Duplicate holder definition for ", typeId.name());
					throw std::exception("Duplicate dependency definition");
				}

				mHolders.emplace(&typeId, std::make_shared<PrototypeHolder<T>>());
			}

			template <typename T>
			void registerPrototype(const std::function<std::shared_ptr<T>()>& producer) {
				auto& typeId = typeid(T);
				auto itr = mHolders.find(&typeId);
				if (itr != mHolders.end()) {
					mLogger.error("Duplicate holder definition for ", typeId.name());
					throw std::exception("Duplicate dependency definition");
				}

				mHolders.emplace(&typeId, std::make_shared<CustomPrototypeHolder<T>>(producer));
			}

			template <typename T>
			std::shared_ptr<T> produce() {
				auto& typeId = typeid(T);
				auto itr = mHolders.find(&typeId);
				if (itr == mHolders.end()) {
					mLogger.error("Unsatisfied holder dependency for ", typeId.name());
					throw std::exception("Unable to produce dependency");
				}

				auto& holder = std::dynamic_pointer_cast<GenericElementHolder<T>>(itr->second);
				auto ret = holder->produce();
				if (!holder->isSingleton()) {
					mInstances.emplace_back(std::make_shared<InstanceHolder<T>>(ret));
					postConstruct(ret);
				}

				return ret;
			}
		};

		SHARED_PTR(ApplicationContext);
	}
}
