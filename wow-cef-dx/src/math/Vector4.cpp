#include "stdafx.h"
#include "Vector4.h"

namespace wowc {
    namespace math {

        Vector4::Vector4(float x, float y, float z, float w) : mVector(DirectX::XMVectorSet(x, y, z, w)) {
        }
    }
}