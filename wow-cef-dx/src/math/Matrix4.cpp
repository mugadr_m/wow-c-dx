#include "stdafx.h"
#include "Matrix4.h"
#include "Vector3.h"
#include "Quaternion.h"

namespace wowc {
    namespace math {

        Matrix4::Matrix4() : mMatrix(DirectX::XMMatrixIdentity()) {

        }

        Matrix4::Matrix4(DirectX::XMMATRIX matrix) : mMatrix(matrix) {

        }

        Matrix4::Matrix4(Matrix4&& other) noexcept : mMatrix(other.mMatrix) {
        }

        Matrix4 Matrix4::transposed() const {
            return Matrix4(XMMatrixTranspose(mMatrix));
        }

        Matrix4 Matrix4::inverted() const {
            return Matrix4(XMMatrixInverse(nullptr, mMatrix));
        }

        void Matrix4::getFloat(float* data) const {
            __declspec(align(16)) DirectX::XMFLOAT4X4 storage{};
            XMStoreFloat4x4(&storage, mMatrix);
            memcpy(data, storage.m, sizeof storage.m);
        }

        Vector3 Matrix4::operator*(const Vector3& other) const {
            return Vector3(XMVector3TransformCoord(other.mVector, mMatrix));
        }

        Matrix4 Matrix4::operator*(const Matrix4& other) const {
            return Matrix4(XMMatrixMultiply(mMatrix, other.mMatrix));
        }

        Matrix4& Matrix4::operator=(const Matrix4& other) = default;

        Matrix4 Matrix4::perspective(float fovY, float aspect, float zNear, float zFar) {
            return Matrix4(DirectX::XMMatrixPerspectiveFovLH(DirectX::XMConvertToRadians(fovY), aspect, zNear, zFar));
        }

        Matrix4 Matrix4::lookAt(const Vector3& position, const Vector3& target, const Vector3& up) {
            return Matrix4(DirectX::XMMatrixLookAtLH(position, target, up));
        }

        Matrix4 Matrix4::translate(const Vector3& position) {
            return Matrix4(DirectX::XMMatrixTranslationFromVector(position.mVector));
        }

        Matrix4 Matrix4::translate(float x, float y, float z) {
            return Matrix4(DirectX::XMMatrixTranslation(x, y, z));
        }

        Matrix4 Matrix4::scale(const Vector3& scale) {
            return Matrix4(DirectX::XMMatrixScalingFromVector(scale.mVector));
        }

        Matrix4 Matrix4::scale(float x, float y, float z) {
            return Matrix4(DirectX::XMMatrixScaling(x, y, z));
        }

        Matrix4 Matrix4::rotate(const Vector3& axis, float angle) {
            return Matrix4(DirectX::XMMatrixRotationAxis(axis.mVector, DirectX::XMConvertToRadians(angle)));
        }

        Matrix4 Matrix4::rotate(const Vector3& angles) {
            return Matrix4(DirectX::XMMatrixRotationRollPitchYawFromVector(angles.mVector));
        }

        Matrix4 Matrix4::rotate(float yaw, float pitch, float roll) {
            return Matrix4(DirectX::XMMatrixRotationRollPitchYaw(
                DirectX::XMConvertToRadians(roll),
                DirectX::XMConvertToRadians(pitch),
                DirectX::XMConvertToRadians(yaw)
            ));
        }

        Matrix4 Matrix4::rotateAngles(const Vector3& angles) {
            using namespace DirectX;
            return
                Matrix4(XMMatrixRotationX(XMConvertToRadians(angles.x()))) *
                Matrix4(XMMatrixRotationY(XMConvertToRadians(angles.y()))) *
                Matrix4(XMMatrixRotationZ(XMConvertToRadians(angles.z())));
        }

        Matrix4 Matrix4::rotate(const Quaternion& quaternion) {
			return Matrix4(DirectX::XMMatrixRotationQuaternion(quaternion.mValue));
        }
    }
}
