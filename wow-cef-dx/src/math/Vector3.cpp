#include "stdafx.h"
#include "Vector3.h"

namespace wowc {
    namespace math {
        const Vector3 Vector3::UNIT_Z{0.0f, 0.0f, 1.0f};
    }
}
