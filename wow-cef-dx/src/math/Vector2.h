#pragma once

#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Vector2 {
			DirectX::XMVECTOR mVector{};

        public:
            explicit Vector2(float x = 0.0f, float y = 0.0f);
			Vector2(const Vector2& other);
			explicit Vector2(DirectX::XMVECTOR vector);

            float x() const {
				return DirectX::XMVectorGetX(mVector);
            }

            float y() const {
				return DirectX::XMVectorGetY(mVector);
            }

            Vector2& x(const float value) {
				mVector = DirectX::XMVectorSetX(mVector, value);
				return *this;
            }

            Vector2& x(const int32_t value) {
				return x(static_cast<float>(value));
            }

			Vector2& y(const float value) {
				mVector = DirectX::XMVectorSetY(mVector, value);
				return *this;
            }

            Vector2& y(const int32_t value) {
				return y(static_cast<float>(value));
            }

            Vector2& set(const float x, const float y) {
				mVector = DirectX::XMVectorSet(x, y, 0.0f, 0.0f);
				return *this;
            }

            float length() const {
				return DirectX::XMVectorGetX(DirectX::XMVector2Length(mVector));
            }

            Vector2 normalized() const {
				return Vector2(DirectX::XMVector2Normalize(mVector));
            }

            Vector2& normalize() {
				mVector = DirectX::XMVector2Normalize(mVector);
				return *this;
            }

            Vector2 operator * (const float value) const {
				return Vector2(DirectX::XMVectorMultiply(mVector, DirectX::XMVectorReplicate(value)));
            }

            Vector2& operator *= (const float value) {
				mVector = DirectX::XMVectorMultiply(mVector, DirectX::XMVectorReplicate(value));
				return *this;
            }

			Vector2 operator / (const float value) const {
				return Vector2(DirectX::XMVectorDivide(mVector, DirectX::XMVectorReplicate(value)));
			}

			Vector2& operator /= (const float value) {
				mVector = DirectX::XMVectorDivide(mVector, DirectX::XMVectorReplicate(value));
				return *this;
			}

			Vector2 operator / (const Vector2& value) const {
				return Vector2(DirectX::XMVectorDivide(mVector, value.mVector));
			}

			Vector2& operator /= (const Vector2& value) {
				mVector = DirectX::XMVectorDivide(mVector, value.mVector);
				return *this;
			}

            Vector2 operator + (const Vector2& value) const {
				return Vector2(DirectX::XMVectorAdd(mVector, value.mVector));
            }

            Vector2& operator += (const Vector2& value) {
				mVector = DirectX::XMVectorAdd(mVector, value.mVector);
				return *this;
            }

			Vector2 operator - (const Vector2& value) const {
				return Vector2(DirectX::XMVectorSubtract(mVector, value.mVector));
			}

			Vector2& operator -= (const Vector2& value) {
				mVector = DirectX::XMVectorSubtract(mVector, value.mVector);
				return *this;
			}

            bool operator == (const Vector2& value) const {
				return x() == value.x() && y() == value.y();
            }

        };

        inline Vector2 operator * (const float value, const Vector2& v) {
			return v * value;
        }
    }
}
