#pragma once
#include "Vector3.h"

namespace wowc {
    namespace math {
        class BoundingBox;

        class Ray {
			Vector3 mPosition{};
			Vector3 mDirection{};

			Vector3 mInvDirection{};

        public:
			Ray() = default;
			Ray(const Vector3& position, const Vector3& direction);

			bool intersects(const BoundingBox& boundingBox, float& distance) const;

            const Vector3& getPosition() const {
				return mPosition;
            }

            const Vector3& getDirection() const {
				return mDirection;
            }
        };
    }
}
