#pragma once
#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Vector3;
		class Quaternion;

        class Matrix4 {
            __declspec(align(16)) DirectX::XMMATRIX mMatrix;

        public:
			Matrix4();
			explicit Matrix4(DirectX::XMMATRIX matrix);
			Matrix4(const Matrix4& other) = default;
			Matrix4(Matrix4&& other) noexcept;

			~Matrix4() = default;

			Matrix4 transposed() const;
			Matrix4 inverted() const;

			void getFloat(float* data) const;

			Vector3 operator * (const Vector3& other) const;
			Matrix4 operator * (const Matrix4& other) const;

			Matrix4& operator = (const Matrix4& other);

			static Matrix4 perspective(float fovY, float aspect, float zNear, float zFar);
			static Matrix4 lookAt(const Vector3& position, const Vector3& target, const Vector3& up);
			static Matrix4 translate(const Vector3& position);
			static Matrix4 translate(float x, float y, float z);
			static Matrix4 scale(const Vector3& scale);
			static Matrix4 scale(float x, float y, float z);
			static Matrix4 rotate(const Vector3& axis, float angle);
			static Matrix4 rotate(const Vector3& angles);
			static Matrix4 rotate(float yaw, float pitch, float roll);
			static Matrix4 rotateAngles(const Vector3& angles);
			static Matrix4 rotate(const Quaternion& quaternion);
        };
    }
}
