#include "stdafx.h"
#include "BoundingBox.h"
#include "math/Matrix4.h"
#include "math/Plane.h"

namespace wowc {
    namespace math {

        BoundingBox::BoundingBox(const Vector3& minPos, const Vector3& maxPos) : mMinPos(minPos), mMaxPos(maxPos) {
        }

        void BoundingBox::set(const Vector3& minPos, const Vector3& maxPos) {
			mMinPos = minPos;
			mMaxPos = maxPos;
        }

        void BoundingBox::getCorners(Vector3* corners) const {
			corners[0] = mMinPos;
			corners[1].set(mMinPos.x(), mMinPos.y(), mMaxPos.z());
			corners[2].set(mMinPos.x(), mMaxPos.y(), mMinPos.z());
			corners[3].set(mMinPos.x(), mMaxPos.y(), mMaxPos.z());
			corners[4].set(mMaxPos.x(), mMinPos.y(), mMinPos.z());
			corners[5].set(mMaxPos.x(), mMinPos.y(), mMaxPos.z());
			corners[6].set(mMaxPos.x(), mMaxPos.y(), mMinPos.z());
			corners[7] = mMaxPos;
        }

        void BoundingBox::transform(const Matrix4& matrix) {
			Vector3 corners[8];
			getCorners(corners);

			const auto fltMax = std::numeric_limits<float>::max();
			const auto fltMin = -fltMax;
			Vector3 minPos(fltMax, fltMax, fltMax);
			Vector3 maxPos(fltMin, fltMin, fltMin);

            for(auto& corner : corners) {
				corner = matrix * corner;
				minPos.takeMin(corner);
				maxPos.takeMax(corner);
            }

			mMinPos = minPos;
			mMaxPos = maxPos;
        }

        BoundingBox BoundingBox::transformed(const Matrix4& matrix) const {
			BoundingBox bbox{ mMinPos, mMaxPos };
			bbox.transform(matrix);
			return bbox;
        }

        bool BoundingBox::intersects(const Plane& plane) const {
			float data[4];
			plane.coeffs(data);
			const auto x = data[0] > 0 ? mMaxPos.x() : mMinPos.x();
			const auto y = data[1] > 0 ? mMaxPos.y() : mMinPos.y();
			const auto z = data[2] > 0 ? mMaxPos.z() : mMinPos.z();
			const auto dp = data[0] * x + data[1] * y + data[2] * z;
			return dp >= -data[3];
        }

    }
}
