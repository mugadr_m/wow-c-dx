#pragma once

#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Vector3 {
			friend class Matrix4;

			__declspec(align(16)) DirectX::XMVECTOR mVector;

        public:
            explicit Vector3(const DirectX::XMVECTOR v) : mVector(v) {
                
            }

            Vector3(float x = 0.0f, float y = 0.0f, float z = 0.0f) : mVector(DirectX::XMVectorSet(x, y, z, 0.0f)) {
                
            }

            float x() const {
				return DirectX::XMVectorGetX(mVector);
            }

			float y() const {
				return DirectX::XMVectorGetY(mVector);
			}

			float z() const {
				return DirectX::XMVectorGetZ(mVector);
			}

            Vector3& x(const float value) {
				mVector = DirectX::XMVectorSetX(mVector, value);
				return *this;
            }

			Vector3& y(const float value) {
				mVector = DirectX::XMVectorSetY(mVector, value);
				return *this;
			}

			Vector3& z(const float value) {
				mVector = DirectX::XMVectorSetZ(mVector, value);
				return *this;
			}

            void set(float x, float y, float z) {
				mVector = DirectX::XMVectorSet(x, y, z, 0.0f);
            }

            Vector3& normalize() {
				mVector = DirectX::XMVector3Normalize(mVector);
				return *this;
            }

            Vector3 normalized() const {
				return Vector3(DirectX::XMVector3Normalize(mVector));
            }

            Vector3& operator *= (const float& value) {
				mVector = DirectX::XMVectorMultiply(mVector, DirectX::XMVectorReplicate(value));
				return *this;
            }

            Vector3 operator * (const float& value) const {
				return Vector3(DirectX::XMVectorMultiply(mVector, DirectX::XMVectorReplicate(value)));
            }

            Vector3& operator *= (const Vector3& value) {
				mVector = DirectX::XMVectorMultiply(mVector, value.mVector);
				return *this;
            }

            Vector3 operator * (const Vector3& value) const {
				return Vector3(DirectX::XMVectorMultiply(mVector, value.mVector));
            }

			Vector3& operator += (const float& value) {
				mVector = DirectX::XMVectorAdd(mVector, DirectX::XMVectorReplicate(value));
				return *this;
			}

			Vector3 operator + (const float& value) const {
				return Vector3(DirectX::XMVectorAdd(mVector, DirectX::XMVectorReplicate(value)));
			}

			Vector3& operator += (const Vector3& value) {
				mVector = DirectX::XMVectorAdd(mVector, value.mVector);
				return *this;
			}

			Vector3 operator + (const Vector3& value) const {
				return Vector3(DirectX::XMVectorAdd(mVector, value.mVector));
			}

			Vector3& operator -= (const float& value) {
				mVector = DirectX::XMVectorSubtract(mVector, DirectX::XMVectorReplicate(value));
				return *this;
			}

			Vector3 operator - (const float& value) const {
				return Vector3(DirectX::XMVectorSubtract(mVector, DirectX::XMVectorReplicate(value)));
			}

			Vector3& operator -= (const Vector3& value) {
				mVector = DirectX::XMVectorSubtract(mVector, value.mVector);
				return *this;
			}

			Vector3 operator - (const Vector3& value) const {
				return Vector3(DirectX::XMVectorSubtract(mVector, value.mVector));
			}

			Vector3& operator /= (const float& value) {
				mVector = DirectX::XMVectorDivide(mVector, DirectX::XMVectorReplicate(value));
				return *this;
			}

			Vector3 operator / (const float& value) const {
				return Vector3(DirectX::XMVectorDivide(mVector, DirectX::XMVectorReplicate(value)));
			}

			Vector3& operator /= (const Vector3& value) {
				mVector = DirectX::XMVectorDivide(mVector, value.mVector);
				return *this;
			}

			Vector3 operator / (const Vector3& value) const {
				return Vector3(DirectX::XMVectorDivide(mVector, value.mVector));
			}

            Vector3 cross(const Vector3& value) const {
				return Vector3(DirectX::XMVector3Cross(mVector, value.mVector));
            }

            float dot(const Vector3& value) const {
				return DirectX::XMVectorGetX(DirectX::XMVector3Dot(mVector, value.mVector));
            }

            float length2D() const {
				return DirectX::XMVectorGetX(DirectX::XMVector2Length(mVector));
            }

            float length() const {
				return DirectX::XMVectorGetX(DirectX::XMVector3Length(mVector));
            }

            float estimatedLength() const {
				return DirectX::XMVectorGetX(DirectX::XMVector3LengthEst(mVector));
            }

            float lengthSquared() const {
				return DirectX::XMVectorGetX(DirectX::XMVector3LengthSq(mVector));
            }

            explicit operator const float* () const {
				return reinterpret_cast<const float*>(mVector.m128_f32);
            }

            explicit operator float* () {
				return reinterpret_cast<float*>(mVector.m128_f32);
            }

            operator DirectX::XMVECTOR() const {
				return mVector;
            }

            void takeMin(const Vector3& other) {
				mVector = DirectX::XMVectorMin(mVector, other.mVector);
            }

            void takeMax(const Vector3& other) {
				mVector = DirectX::XMVectorMax(mVector, other.mVector);
            }

            uint32_t toXrgb() const {
				const auto r = static_cast<uint8_t>(x() * 255);
				const auto g = static_cast<uint8_t>(y() * 255);
				const auto b = static_cast<uint8_t>(z() * 255);

				return r | (g << 8) | (b << 16) | 0xFF000000;
            }

            static Vector3 fromXrgb(uint32_t xrgb) {
				const auto r = xrgb & 0xFF;
				const auto g = (xrgb >> 8) & 0xFF;
				const auto b = (xrgb >> 16) & 0xFF;
				return {r / 255.0f, g / 255.0f, b / 255.0f};
            }

			static const Vector3 UNIT_Z;
        };

        inline Vector3 operator * (const float l, const Vector3& r) {
			return r * l;
        }
    }
}