#include "stdafx.h"
#include "Vector2.h"

namespace wowc {
    namespace math {

        Vector2::Vector2(float x, float y) : mVector(DirectX::XMVectorSet(x, y, 0.0f, 0.0f)) {
			
        }

        Vector2::Vector2(const Vector2& other) = default;

        Vector2::Vector2(DirectX::XMVECTOR vector) : mVector(vector) {

        }
    }
}