#pragma once
#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Vector4 {
			__declspec(align(16)) DirectX::XMVECTOR mVector{};

        public:
			explicit Vector4(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 0.0f);

            void set(float x, float y, float z, float w) {
				mVector = DirectX::XMVectorSet(x, y, z, w);
            }

            float x() const {
				return DirectX::XMVectorGetX(mVector);
            }

			float y() const {
				return DirectX::XMVectorGetY(mVector);
			}

			float z() const {
				return DirectX::XMVectorGetZ(mVector);
			}

			float w() const {
				return DirectX::XMVectorGetW(mVector);
			}

            Vector4& x(const float& v) {
				mVector = DirectX::XMVectorSetX(mVector, v);
				return *this;
            }

			Vector4& y(const float& v) {
				mVector = DirectX::XMVectorSetY(mVector, v);
				return *this;
			}

			Vector4& z(const float& v) {
				mVector = DirectX::XMVectorSetZ(mVector, v);
				return *this;
			}

			Vector4& w(const float& v) {
				mVector = DirectX::XMVectorSetW(mVector, v);
				return *this;
			}

            static Vector4 fromXrgb(uint32_t xrgb) {
				return Vector4(
					(xrgb & 0xFF) / 255.0f,
					((xrgb >> 8) & 0xFF) / 255.0f,
					((xrgb >> 16) & 0xFF) / 255.0f,
					1.0f
				);
            }

        };
    }
}
