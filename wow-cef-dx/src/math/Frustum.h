#pragma once

#include "Plane.h"

namespace wowc {
    namespace math {
        class BoundingBox;
        class Matrix4;

        class Frustum {
			Plane mPlanes[6];

        public:
			void updateFrustum(const Matrix4& matView, const Matrix4& matProjection);

			bool isVisible(const BoundingBox& bbox) const;
        };
    }
}