#pragma once

#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Quaternion {
			friend class Matrix4;

			__declspec(align(16)) DirectX::XMVECTOR mValue;

        public:
			Quaternion();
            explicit Quaternion(const DirectX::XMVECTOR& value);
			Quaternion(const Quaternion& other);
			Quaternion(Quaternion&& other) noexcept;
			Quaternion(float x, float y, float z, float w);

			~Quaternion() = default;

			Quaternion& operator = (const Quaternion& other);
			Quaternion& operator = (Quaternion&& other) noexcept;

            explicit operator DirectX::XMVECTOR() const {
				return mValue;
            }

			static Quaternion slerp(const Quaternion& q1, const Quaternion& q2, float factor);
        };
    }
}