#include "stdafx.h"
#include "Ray.h"
#include "BoundingBox.h"

namespace wowc {
    namespace math {
        Ray::Ray(const Vector3& position, const Vector3& direction)
            : mPosition(position), mDirection(direction),
              mInvDirection(1.0f / direction.x(), 1.0f / direction.y(), 1.0f / direction.z()) {

        }

        bool Ray::intersects(const BoundingBox& boundingBox, float& distance) const {
			const auto& b0 = boundingBox.getMin();
			const auto& b1 = boundingBox.getMax();

			auto txMin = ((mInvDirection.x() < 0 ? b1.x() : b0.x()) - mPosition.x()) * mInvDirection.x();
			auto txMax = ((mInvDirection.x() < 0 ? b0.x() : b1.x()) - mPosition.x()) * mInvDirection.x();

            const auto tyMin = ((mInvDirection.y() < 0 ? b1.y() : b0.y()) - mPosition.y()) * mInvDirection.y();
            const auto tyMax = ((mInvDirection.y() < 0 ? b0.y() : b1.y()) - mPosition.y()) * mInvDirection.y();

            if((txMin > tyMax) || (tyMin > txMax)) {
				return false;
            }

            if(tyMin > txMin) {
				txMin = tyMin;
            }
            if(tyMax < txMax) {
				txMax = tyMax;
            }

            const auto tzMin = ((mInvDirection.z() < 0 ? b1.z() : b0.z()) - mPosition.z()) * mInvDirection.z();
            const auto tzMax = ((mInvDirection.z() < 0 ? b0.z() : b1.z()) - mPosition.z()) * mInvDirection.z();

            if((txMin > tzMax) || (tzMin > txMax)) {
				return false;
            }

            if(tzMin > txMin) {
				txMin = tzMin;
            }

            if(tzMax < txMax) {
				txMax = tzMax;
            }

            if(txMin < 0 && txMax < 0) {
				return false;
            }

            if(txMin >= 0) {
				distance = txMin;
            } else {
				distance = txMax;
            }

			return true;
        }

    }
}
