#include "stdafx.h"
#include "Plane.h"
#include "Vector3.h"

namespace wowc {
    namespace math {

        Plane::Plane() : mPlane(DirectX::XMVectorReplicate(0.0f)) {
        }

        Plane::Plane(const Vector3& pos, float distance) : mPlane(DirectX::XMVectorSet(pos.x(), pos.y(), pos.z(), distance)) {
        }


        float Plane::dot(const Vector3& vector) const {
			return DirectX::XMVectorGetX(DirectX::XMPlaneDotCoord(mPlane, vector));
        }

        void Plane::normalize() {
			mPlane = DirectX::XMPlaneNormalize(mPlane);
        }

        void Plane::set(float a, float b, float c, float d) {
			mPlane = DirectX::XMVectorSet(a, b, c, d);
        }

        void Plane::coeffs(float* outData) const {
			XMStoreFloat4(reinterpret_cast<DirectX::XMFLOAT4*>(outData), mPlane);
        }
    }
}