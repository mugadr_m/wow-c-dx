#pragma once
#include <DirectXMath.h>

namespace wowc {
    namespace math {
        class Vector3;

        class Plane {
			DirectX::XMVECTOR mPlane;

        public:
			Plane();
			Plane(const Vector3& pos, float distance);

			float dot(const Vector3& vector) const;
			void normalize();

			void set(float a, float b, float c, float d);

			void coeffs(float* outData) const;
        };
    }
}
