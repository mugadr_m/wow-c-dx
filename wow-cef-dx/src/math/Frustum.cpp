#include "stdafx.h"
#include "Frustum.h"
#include "Matrix4.h"
#include "BoundingBox.h"

namespace wowc {
    namespace math {

        void Frustum::updateFrustum(const Matrix4& matView, const Matrix4& matProjection) {
			float mv[16];
			float mp[16];
			matView.getFloat(mv);
			matProjection.getFloat(mp);

			float clip[16];
			clip[0] = mv[0] * mp[0] + mv[1] * mp[4] + mv[2] * mp[8]
				+ mv[3] * mp[12];
			clip[1] = mv[0] * mp[1] + mv[1] * mp[5] + mv[2] * mp[9]
				+ mv[3] * mp[13];
			clip[2] = mv[0] * mp[2] + mv[1] * mp[6] + mv[2] * mp[10]
				+ mv[3] * mp[14];
			clip[3] = mv[0] * mp[3] + mv[1] * mp[7] + mv[2] * mp[11]
				+ mv[3] * mp[15];
			clip[4] = mv[4] * mp[0] + mv[5] * mp[4] + mv[6] * mp[8]
				+ mv[7] * mp[12];
			clip[5] = mv[4] * mp[1] + mv[5] * mp[5] + mv[6] * mp[9]
				+ mv[7] * mp[13];
			clip[6] = mv[4] * mp[2] + mv[5] * mp[6] + mv[6] * mp[10]
				+ mv[7] * mp[14];
			clip[7] = mv[4] * mp[3] + mv[5] * mp[7] + mv[6] * mp[11]
				+ mv[7] * mp[15];
			clip[8] = mv[8] * mp[0] + mv[9] * mp[4] + mv[10] * mp[8]
				+ mv[11] * mp[12];
			clip[9] = mv[8] * mp[1] + mv[9] * mp[5] + mv[10] * mp[9]
				+ mv[11] * mp[13];
			clip[10] = mv[8] * mp[2] + mv[9] * mp[6] + mv[10]
				* mp[10] + mv[11] * mp[14];
			clip[11] = mv[8] * mp[3] + mv[9] * mp[7] + mv[10]
				* mp[11] + mv[11] * mp[15];
			clip[12] = mv[12] * mp[0] + mv[13] * mp[4] + mv[14]
				* mp[8] + mv[15] * mp[12];
			clip[13] = mv[12] * mp[1] + mv[13] * mp[5] + mv[14]
				* mp[9] + mv[15] * mp[13];
			clip[14] = mv[12] * mp[2] + mv[13] * mp[6] + mv[14]
				* mp[10] + mv[15] * mp[14];
			clip[15] = mv[12] * mp[3] + mv[13] * mp[7] + mv[14]
				* mp[11] + mv[15] * mp[15];


			mPlanes[0].set(clip[3] - clip[0], clip[7] - clip[4], clip[11] - clip[8], clip[15] - clip[12]);
			mPlanes[1].set(clip[3] + clip[0], clip[7] + clip[4], clip[11] + clip[8], clip[15] + clip[12]);
			mPlanes[2].set(clip[3] + clip[1], clip[7] + clip[5], clip[11] + clip[9], clip[15] + clip[13]);
			mPlanes[3].set(clip[3] - clip[1], clip[7] - clip[5], clip[11] - clip[9], clip[15] - clip[13]);
			mPlanes[4].set(clip[3] - clip[2], clip[7] - clip[6], clip[11] - clip[10], clip[15] - clip[14]);
			mPlanes[5].set(clip[3] + clip[2], clip[7] + clip[6], clip[11] + clip[10], clip[15] + clip[14]);

            for (auto& plane : mPlanes) {
                plane.normalize();
            }
        }

        bool Frustum::isVisible(const BoundingBox& bbox) const {
            for(const auto& plane : mPlanes) {
                if(!bbox.intersects(plane)) {
					return false;
                }
            }

			return true;
        }
    }
}