#pragma once
#include "Vector3.h"

namespace wowc {
    namespace math {
        class Plane;

        class BoundingBox {
			Vector3 mMinPos;
			Vector3 mMaxPos;

        public:
			BoundingBox() = default;
			BoundingBox(const Vector3& minPos, const Vector3& maxPos);

			void set(const Vector3& minPos, const Vector3& maxPos);
			void getCorners(Vector3* corners) const;

            const Vector3& getMin() const {
				return mMinPos;
            }

            const Vector3& getMax() const {
				return mMaxPos;
            }

			void transform(const Matrix4& matrix);
			BoundingBox transformed(const Matrix4& matrix) const;

			bool intersects(const Plane& plane) const;
        };
    }
}
