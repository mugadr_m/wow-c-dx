#include "stdafx.h"
#include "Quaternion.h"

namespace wowc {
    namespace math {

        Quaternion::Quaternion() : mValue(DirectX::XMQuaternionIdentity()) {
        }

        Quaternion::Quaternion(const DirectX::XMVECTOR& value) : mValue(value) {
        }

        Quaternion::Quaternion(const Quaternion& other) : mValue(other.mValue) {
        }

        Quaternion::Quaternion(Quaternion&& other) noexcept : mValue(std::move(other.mValue)) {
        }

        Quaternion::Quaternion(float x, float y, float z, float w) : mValue(DirectX::XMVectorSet(x, y, z, w)) {
        }

        Quaternion& Quaternion::operator=(const Quaternion& other) {
			mValue = other.mValue;
			return *this;
        }

        Quaternion& Quaternion::operator=(Quaternion&& other) noexcept {
			mValue = std::move(other.mValue);
			return *this;
        }

        Quaternion Quaternion::slerp(const Quaternion& q1, const Quaternion& q2, float factor) {
			return Quaternion(DirectX::XMQuaternionSlerp(q1.mValue, q2.mValue, factor));
        }
    }
}