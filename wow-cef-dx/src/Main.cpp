#include "stdafx.h"
#include "gx/Window.h"
#include "gx/GxContext.h"
#include "utils/Platform.h"
#include "ui/WebCore.h"
#include "cdi/ApplicationContext.h"
#include "res/ResourceManager.h"
#include "utils/CommandLine.h"
#include "io/IoModule.h"
#include "io/files/storage/DefinitionManager.h"
#include "scene/SceneModule.h"
#include "scene/WorldFrame.h"
#include "utils/PropertyStore.h"

namespace wowc {
    int wowc(int argc, const char* argv[]) {
        auto ctx = cdi::ApplicationContext::instance();

        const auto commandLine = std::make_shared<utils::CommandLine>();
        ctx->registerSingleton(commandLine);
		ctx->registerSingleton<utils::PropertyStore>();

        io::IoModule::initialize();
		scene::SceneModule::initialize();

        auto platform = utils::Platform::getOrCreate();
        auto window = std::make_shared<gx::Window>();
        auto context = gx::GxContext::createContext();
        auto core = std::make_shared<ui::WebCore>();

        ctx->registerSingleton<res::ResourceManager>();

        ctx->registerSingleton(platform);
        platform->initialize();

        ctx->registerSingleton(window);
        window->initialize();

        ctx->registerSingleton(context);
        context->initialize(window);

        ctx->registerSingleton(core);
        core->initialize();

		auto worldFrame = ctx->produce<scene::WorldFrame>();
        worldFrame->onContextCreated(context);

        window->showWindow();
        while (window->runFrame()) {
            context->beginFrame();

            worldFrame->onFrame();
            core->onFrame();

            context->endFrame();
        }

		worldFrame->shutdown();
        core->shutdown();
        ctx->destroy();
        return 0;
    }
}

int main(int argc, const char* argv[]) {
    return wowc::wowc(argc, argv);
}
