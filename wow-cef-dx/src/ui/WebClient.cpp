#include "stdafx.h"
#include "WebClient.h"
#include "WebCore.h"

namespace wowc {
	namespace ui {
		utils::Logger<WebClient> WebClient::mLogger;

	    void WebClient::ContextMenuHandler::OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	        CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model) {
			model->Clear();
	    }

		bool WebClient::RequestHandler::OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
			CefRefPtr<CefRequest> request, bool user_gesture, bool is_redirect) {
			return mWebClient->onBeforeBrowse(browser, frame);
		}

		void WebClient::RequestHandler::OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) {
			mWebClient->onRenderProcessTerminated(browser);
		}

		bool WebClient::RenderHandler::GetRootScreenRect(const CefRefPtr<CefBrowser> browser, CefRect& rect) {
			return false;
		}

		bool WebClient::RenderHandler::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) {
			const auto wndHandle = mWebClient->getWebCore()->getWindow()->getHandle();
			const auto dpiScale = mWebClient->getWebCore()->getWindow()->getDpi();
			uint32_t width = 0, height = 0;
			mWebClient->getWebCore()->getWindow()->getSize(width, height);
			rect.x = 0;
			rect.y = 0;
			rect.width = static_cast<int>(width / dpiScale);
			rect.height = static_cast<int>(height / dpiScale);
			return true;
		}

		bool WebClient::RenderHandler::GetScreenPoint(CefRefPtr<CefBrowser> browser, const int viewX, const int viewY, int& screenX,
			int& screenY) {
			POINT pt{ viewX, viewY };
			ClientToScreen(mWebClient->getWebCore()->getWindow()->getHandle(), &pt);

			screenX = pt.x;
			screenY = pt.y;
			return true;
		}

		bool WebClient::RenderHandler::GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screenInfo) {
			CefRect viewRect;
			GetViewRect(browser, viewRect);
			const auto wndHandle = mWebClient->getWebCore()->getWindow()->getHandle();
			const auto dpiScale = mWebClient->getWebCore()->getWindow()->getDpi();
			screenInfo.device_scale_factor = std::min(std::max(dpiScale, 1.0f), 4.0f);
			screenInfo.rect = viewRect;
			screenInfo.available_rect = viewRect;
			return true;
		}

		void WebClient::RenderHandler::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type,
			const RectList& dirtyRects, const void* buffer, const int width, const int height) {
		}

	    void WebClient::RenderHandler::OnAcceleratedPaint(CefRefPtr<CefBrowser> browser, PaintElementType type,
	        const RectList& dirtyRects, void* shared_handle, uint64 sync_key) {
			mWebClient->onAcceleratedPaint(browser, type, dirtyRects, shared_handle, sync_key);
	    }

	    void WebClient::RenderHandler::OnCursorChange(CefRefPtr<CefBrowser> browser, const HCURSOR cursor, CursorType type,
			const CefCursorInfo& customCursorInfo) {
			mWebClient->getWebCore()->getWindow()->updateCursor(cursor);
		}

		void WebClient::RenderHandler::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) {
			mWebClient->getWebCore()->getPopupDrawer().onPopupVisibilityChanged(show);
		}

		void WebClient::RenderHandler::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) {
			mWebClient->getWebCore()->getPopupDrawer().onResizePopup(rect);
		}

		void WebClient::LifeSpanHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
			mWebClient->onLocalBrowserReady(browser);
		}

		void WebClient::LifeSpanHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser) {
			mWebClient->onBrowserClosed();
		}

		bool WebClient::LifeSpanHandler::DoClose(CefRefPtr<CefBrowser> browser) {
			return false;
		}

		bool WebClient::onBeforeBrowse(const CefRefPtr<CefBrowser>& browser, const CefRefPtr<CefFrame>& frame) const {
			mMessageRouter->OnBeforeBrowse(browser, frame);
			return false;
		}

		void WebClient::onRenderProcessTerminated(const CefRefPtr<CefBrowser> browser) const {
			mMessageRouter->OnRenderProcessTerminated(browser);
		}

		void WebClient::onPaint(CefRenderHandler::PaintElementType type, const CefRenderHandler::RectList& dirtyRects,
			const void* buffer, int width, int height) const {

		}

	    void WebClient::onAcceleratedPaint(CefRefPtr<CefBrowser> browser, CefRenderHandler::PaintElementType type,
	        const CefRenderHandler::RectList& dirtyRects, void* shared_handle, uint64 sync_key) {
			if(type == PET_VIEW) {
				mWebCore->getBitmapTarget().onAcceleratedPaint(shared_handle);
			} else {
				mWebCore->getPopupDrawer().onAcceleratedPaint(shared_handle);
			}
	    }

	    WebClient::WebClient(WebCore* webCore) : mWebCore(webCore) {
			mRenderHandler = new RenderHandler(this);
			mLifeSpanHandler = new LifeSpanHandler(this);
			mRequestHandler = new RequestHandler(this);
		}

		void WebClient::onLocalBrowserReady(const CefRefPtr<CefBrowser>& localBrowser) {
			auto settings = CefMessageRouterConfig();
			settings.js_query_function = "queryBrowser";
			mMessageRouter = CefMessageRouterBrowserSide::Create(settings);
			mMessageRouter->AddHandler(mMessageHandler.get(), true);
			mLocalBrowser = localBrowser;
			mLocalBrowser->GetMainFrame()->LoadURL("local://localhost/start.html");
			mLocalBrowser->GetHost()->SetFocus(true);
			mLocalBrowser->GetHost()->SendFocusEvent(true);
		}

		void WebClient::onBrowserClosed() {
			mMessageRouter->RemoveHandler(mMessageHandler.get());
			mMessageRouter->OnBeforeClose(mLocalBrowser);
			mMessageHandler = nullptr;
			mMessageRouter = nullptr;
			mLocalBrowser = nullptr;
			mWebCore->onBrowserClosed();
		}

		void WebClient::onResize(uint32_t width, uint32_t height) const {
			if(mLocalBrowser == nullptr) {
				return;
			}

			mLocalBrowser->GetHost()->WasResized();
			mLocalBrowser->GetHost()->NotifyMoveOrResizeStarted();
		}

		void WebClient::shutdown() {
			if(mMessageHandler != nullptr) {
				mMessageHandler->destroy();
			}

			if(mLocalBrowser == nullptr) {
				mWebCore->onBrowserClosed();
			} else {
				mLocalBrowser->StopLoad();
				mLocalBrowser->GetHost()->SetFocus(false);
				mLocalBrowser->GetHost()->CloseBrowser(true);
			}
		}

		void WebClient::postConstruct(const cdi::ApplicationContextPtr& context) {
			mMessageHandler->postConstruct(context);
		}

		bool WebClient::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message) {
			return mMessageRouter->OnProcessMessageReceived(browser, source_process, message);
		}

	    void WebClient::onFrame() {
            if(mLocalBrowser != nullptr) {
				mLocalBrowser->GetHost()->SendExternalBeginFrame(0, 0, 0);
            }
	    }
	}
}