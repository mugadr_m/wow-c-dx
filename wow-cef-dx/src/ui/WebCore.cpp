#include "stdafx.h"
#include "WebCore.h"
#include <wrapper/cef_closure_task.h>
#include <base/cef_bind.h>
#include "cdi/ApplicationContext.h"
#include "gx/IndexBuffer.h"
#include "gx/Program.h"
#include "gx/Mesh.h"
#include "res/resource.h"
#include "events/EventManager.h"
#include "io/IoModule.h"
#include "io/cef/MapEntryLeafletResourceHandlerFactory.h"
#include "scene/SceneModule.h"
#include "utils/PropertyStore.h"

namespace wowc {
	namespace ui {
		utils::Logger<WebCore> WebCore::mLogger;

		void WebCore::initGraphics() const {
			mProgram->loadFromResources(IDR_CEF_SHADER_VERTEX, IDR_CEF_SHADER_PIXEL);
			mMesh->setProgram(mProgram);
			mMesh->setIndexCount(6);
			mMesh->addElement("POSITION", 0, 2);
			mMesh->addElement("TEXCOORD", 0, 2);
			mMesh->finalize();

			mVertexBuffer->updateData(std::vector<float>{
				-1.0f, 1.0f, 0.0f, 0.0f,
				1.0f, 1.0f, 1.0f, 0.0f,
				1.0f, -1.0f, 1.0f, 1.0f,
				-1.0f, -1.0f, 0.0f, 1.0f
			});

			mIndexBuffer->updateData(std::vector<uint16>{
				0, 1, 2,
				0, 2, 3
			});

			mMesh->setVertexBuffer(mVertexBuffer);
			mMesh->setIndexBuffer(mIndexBuffer);
		}

		void WebCore::messageThreadHandler() {
			CefEnableHighDPISupport();

			mWebIpc.initialize(shared_from_this());

			CefMainArgs mainArgs;
			mainArgs.instance = GetModuleHandle(nullptr);

			if (CefExecuteProcess(mainArgs, mApplication, nullptr) >= 0) {
				mLogger.error("Main process in CEF was considered a sub process.");
				mIsLoadDone = true;
				mLoadResult = false;
				mLoadEvent.notify_all();
				return;
			}

			TCHAR curDir[2048]{};
			GetCurrentDirectory(2048, curDir);
			CefSettings settings;
			CefString(&settings.resources_dir_path) = curDir;
			settings.windowless_rendering_enabled = TRUE;
			TCHAR subProcessPath[2048]{};
			strcat_s(subProcessPath, sizeof subProcessPath, curDir);
			strcat_s(subProcessPath, sizeof subProcessPath, "\\web-subprocess.exe");
			TCHAR cachePath[2048]{};
			strcat_s(cachePath, sizeof cachePath, curDir);
			strcat_s(cachePath, sizeof cachePath, "\\web_cache");
			CreateDirectory(cachePath, nullptr);
			CefString(&settings.browser_subprocess_path) = subProcessPath;
			settings.ignore_certificate_errors = TRUE;
			settings.single_process = FALSE;
			settings.no_sandbox = TRUE;
			settings.remote_debugging_port = 52231;
			settings.multi_threaded_message_loop = false;
			CefString(&settings.cache_path) = cachePath;

			const auto result = CefInitialize(mainArgs, settings, mApplication, nullptr);
			if (!result) {
				mLogger.error("Unable to initialize CEF");
				mIsLoadDone = true;
				mLoadResult = false;
			} else {
				mLogger.info("Successfully initialized CEF");
				mIsLoadDone = true;
				mLoadResult = true;
			}

			CefRegisterSchemeHandlerFactory(io::cef::LocalSchemeHandlerFactory::SCHEME_NAME, CefString(), mLocalScheme);
			CefRegisterSchemeHandlerFactory(io::cef::LoadingScreenHandlerFactory::SCHEME_NAME, CefString(),
			                                mLoadingScreenScheme);
			CefRegisterSchemeHandlerFactory(io::cef::MapEntryLeafletResourceHandlerFactory::SCHEME_NAME, CefString(),
			                                mMapEntryLeafletResourceHandler);

			mLoadEvent.notify_all();

			CefRunMessageLoop();

			mWebIpc.shutdown();

			CefClearSchemeHandlerFactories();
			mWebClient = nullptr;
			mLocalScheme = nullptr;
			mLoadingScreenScheme = nullptr;
			mMapEntryLeafletResourceHandler = nullptr;

			mLogger.info("Shutting down CEF");
			CefShutdown();

			mApplication = nullptr;
		}

		uint32_t WebCore::getModifiers() {
			static uint8_t keyboardBuffer[256];
			GetKeyboardState(keyboardBuffer);
			uint32 modifiers = 0;
			if ((keyboardBuffer[VK_LBUTTON] & 0x80) != 0) {
				modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
			}
			if ((keyboardBuffer[VK_RBUTTON] & 0x80) != 0) {
				modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;
			}
			if ((keyboardBuffer[VK_MBUTTON] & 0x80) != 0) {
				modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
			}
			if ((keyboardBuffer[VK_SHIFT] & 0x80) != 0) {
				modifiers |= EVENTFLAG_SHIFT_DOWN;
			}
			if ((keyboardBuffer[VK_MENU] & 0x80) != 0) {
				modifiers |= EVENTFLAG_ALT_DOWN;
			}
			if ((keyboardBuffer[VK_CONTROL] & 0x80) != 0) {
				modifiers |= EVENTFLAG_CONTROL_DOWN;
			}

			return modifiers;
		}

		cef_mouse_button_type_t WebCore::getMouseButton(const uint32_t messageType) {
			switch (messageType) {
			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_LBUTTONDBLCLK:
				return MBT_LEFT;

			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			case WM_RBUTTONDBLCLK:
				return MBT_RIGHT;

			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			case WM_MBUTTONDBLCLK:
				return MBT_MIDDLE;

			default:
				mLogger.error("Unrecognized message type: ", messageType);
				throw std::exception("Unrecognized message type");
			}
		}

		void WebCore::handleMouseMove(float x, float y) {
			const auto scaleFactor = mWindow->getDpi();
			x /= scaleFactor;
			y /= scaleFactor;

			const auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
			const auto currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
			const auto cancelPreviousClick =
				std::abs(mLastClickX - x) > GetSystemMetrics(SM_CXDOUBLECLK) / 2.0f ||
				std::abs(mLastClickY - y) > GetSystemMetrics(SM_CYDOUBLECLK) / 2.0f ||
				currentTime - mLastClickTime > GetDoubleClickTime();

			if (cancelPreviousClick) {
				mLastClickCount = 0;
				mLastClickX = 0;
				mLastClickY = 0;
				mLastClickTime = 0;
			}

			CefMouseEvent e;
			e.x = static_cast<int>(x);
			e.y = static_cast<int>(y);
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendMouseMoveEvent(e, false);
		}

		void WebCore::handleMouseDown(const uint32_t messageType, const LPARAM lParam) {
			const auto scaleFactor = mWindow->getDpi();
			const auto x = static_cast<int32_t>(GET_X_LPARAM(lParam) / scaleFactor);
			const auto y = static_cast<int32_t>(GET_Y_LPARAM(lParam) / scaleFactor);
			const auto button = getMouseButton(messageType);

			const auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
			const auto currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
			const auto cancelPreviousClick =
				std::abs(mLastClickX - x) > GetSystemMetrics(SM_CXDOUBLECLK) / 2 ||
				std::abs(mLastClickY - y) > GetSystemMetrics(SM_CYDOUBLECLK) / 2 ||
				currentTime - mLastClickTime > GetDoubleClickTime();

			if (cancelPreviousClick) {
				mLastClickCount = 0;
				mLastClickX = 0;
				mLastClickY = 0;
				mLastClickTime = 0;
			}

			if (!cancelPreviousClick && mLastClickButton == button) {
				++mLastClickCount;
			} else {
				mLastClickCount = 1;
				mLastClickX = x;
				mLastClickY = y;
			}

			mLastClickTime = currentTime;
			mLastClickButton = button;

			CefMouseEvent e;
			e.x = x;
			e.y = y;
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}

			browser->GetHost()->SendMouseClickEvent(e, button, false, mLastClickCount);
		}

		void WebCore::handleMouseUp(const uint32_t messageType, const LPARAM lParam) const {
			CefMouseEvent e;
			const auto scaleFactor = mWindow->getDpi();
			e.x = static_cast<int>(GET_X_LPARAM(lParam) / scaleFactor);
			e.y = static_cast<int>(GET_Y_LPARAM(lParam) / scaleFactor);
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendMouseClickEvent(e, getMouseButton(messageType), true, 1);
		}

		void WebCore::handleMouseLeave() const {
			CefMouseEvent e;
			e.x = 0;
			e.y = 0;
			e.modifiers = 0;
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendMouseMoveEvent(e, true);
		}

		void WebCore::handleMouseWheel(const WPARAM wParam, const LPARAM lParam) const {
			const auto scaleFactor = mWindow->getDpi();
			const auto delta = GET_WHEEL_DELTA_WPARAM(wParam);
			const auto x = static_cast<int32_t>(GET_X_LPARAM(lParam));
			const auto y = static_cast<int32_t>(GET_Y_LPARAM(lParam));

			POINT pt{x, y};
			ScreenToClient(mWindow->getHandle(), &pt);

			pt.x = static_cast<int32_t>(static_cast<float>(pt.x) / scaleFactor);
			pt.y = static_cast<int32_t>(static_cast<float>(pt.y) / scaleFactor);

			CefMouseEvent e;
			e.x = pt.x;
			e.y = pt.y;
			e.modifiers = getModifiers();

			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendMouseWheelEvent(e, 0, delta);
		}

		void WebCore::handleCharPress(const WPARAM wParam) const {
			const auto character = static_cast<wchar_t>(wParam);
			CefKeyEvent e;
			e.type = KEYEVENT_CHAR;
			e.native_key_code = character;
			e.windows_key_code = character;
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendKeyEvent(e);
		}

		void WebCore::handleKeyDown(const WPARAM wParam, const LPARAM lParam) const {
			CefKeyEvent e;
			e.type = KEYEVENT_KEYDOWN;
			e.native_key_code = static_cast<int32_t>(lParam);
			e.windows_key_code = static_cast<int32_t>(wParam);
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendKeyEvent(e);
		}

		void WebCore::handleKeyUp(const WPARAM wParam, const LPARAM lParam) const {
			CefKeyEvent e;
			e.type = KEYEVENT_KEYUP;
			e.native_key_code = static_cast<int32_t>(lParam);
			e.windows_key_code = static_cast<int32_t>(wParam);
			e.modifiers = getModifiers();
			auto browser = mWebClient->getLocalBrowser();
			if (browser == nullptr) {
				return;
			}
			browser->GetHost()->SendKeyEvent(e);
		}

		void WebCore::handleWindowMove() {
			const auto scaleFactor = mWindow->getDpi();
			mPopupDrawer.setDpiScale(scaleFactor);
		}

		void WebCore::onApplicationReady() const {
			CefWindowInfo windowInfo;
			windowInfo.SetAsWindowless(mWindow->getHandle());
			windowInfo.external_begin_frame_enabled = true;
			windowInfo.windowless_rendering_enabled = true;
			windowInfo.shared_texture_enabled = true;
			windowInfo.shared_texture_sync_key = static_cast<uint64_t>(-1);

			CefBrowserSettings browserSettings;
			browserSettings.background_color = 0;
			browserSettings.windowless_frame_rate = 10000;

			CefBrowserHost::CreateBrowser(windowInfo, mWebClient, "", browserSettings, nullptr);
		}

		void WebCore::onBrowserClosed() const {
			CefPostTask(TID_UI, base::Bind(CefQuitMessageLoop));
		}

		void WebCore::postConstruct(const cdi::ApplicationContextPtr& ctx) {
			ctx->registerSingleton<events::EventManager>();
			mApplication = CefRefPtr<WebApplication>(new WebApplication(this));
			mWebClient = CefRefPtr<WebClient>(new WebClient(this));
			mLocalScheme = CefRefPtr<io::cef::LocalSchemeHandlerFactory>(new io::cef::LocalSchemeHandlerFactory());
			mLoadingScreenScheme = CefRefPtr<io::cef::LoadingScreenHandlerFactory>(new io::cef::LoadingScreenHandlerFactory());
			mMapEntryLeafletResourceHandler = CefRefPtr<io::cef::MapEntryLeafletResourceHandlerFactory>(new io::cef::MapEntryLeafletResourceHandlerFactory());

			mWebClient->postConstruct(ctx);

			mWindow = ctx->produce<gx::Window>();
			mLocalScheme->postConstruct(ctx);

			const auto context = ctx->produce<gx::GxContext>();
			mMesh = context->createMesh();
			mTexture = context->createTexture();
			mSampler = context->createSampler();
			mVertexBuffer = context->createVertexBuffer(true);
			mIndexBuffer = context->createIndexBuffer(gx::IndexType::UINT16, true);
			mProgram = context->createProgram();

			const auto blendState = context->createBlendState();
			mMesh->setBlendState(blendState);

			const auto depthState = context->createDepthState();
			mMesh->setDepthState(depthState);

			mPopupDrawer.setTexture(context->createTexture());

			mLoadingScreenScheme->postConstruct(ctx);
			mMapEntryLeafletResourceHandler->postConstruct(ctx);

			mPopupDrawer.setContext(context);
			mBitmapTarget.setContext(context);

			io::IoModule::initUiEvents();
			scene::SceneModule::initUiEvents();
			utils::PropertyStore::initUiEvents();
		}

		void WebCore::initialize() {
			initGraphics();
			handleWindowMove();

			mMessageThread = std::thread(std::bind(&WebCore::messageThreadHandler, this));
			{
				std::unique_lock<std::mutex> l(mLoadEventLock);
				mLoadEvent.wait(l, [&] { return mIsLoadDone; });
			}

			if (!mLoadResult) {
				throw std::exception("Failed to initialize CEF");
			}
		}

		void WebCore::shutdown() {
			mWebClient->shutdown();

			if (mIsLoadDone && mMessageThread.joinable()) {
				mMessageThread.join();
			}
		}

		void WebCore::onFrame() {
			mWebClient->onFrame();
			mBitmapTarget.onUpdate();

			const auto texture = mBitmapTarget.getSharedTexture();
			if (texture != nullptr) {
				mMesh->setVertexBuffer(mVertexBuffer);
				mMesh->beginFrame();
				mMesh->setTexture(gx::ShaderType::PIXEL, 0, texture);
				mMesh->setSampler(gx::ShaderType::PIXEL, 0, mSampler);
				mMesh->draw();
			}

			mPopupDrawer.updateData();

			if (mPopupDrawer.isVisible()) {
				mMesh->setVertexBuffer(mPopupDrawer.getVertexBuffer());
				mMesh->beginFrame();
				mMesh->setTexture(gx::ShaderType::PIXEL, 0, mPopupDrawer.getTexture());
				mMesh->draw();
			}
		}

		void WebCore::onResize() {
			handleWindowMove();
			uint32_t width = 0, height = 0;
			mWindow->getSize(width, height);
			mPopupDrawer.onWidowResize(width, height);
			if (mWebClient != nullptr) {
				mWebClient->onResize(width, height);
			}

		}

	    void WebCore::onBeforeChildProcess(CefRefPtr<CefCommandLine> commandLine) const {
			commandLine->AppendSwitchWithValue("web-ipc-port", std::to_string(mWebIpc.getIpcPort()));
	    }


	}
}
