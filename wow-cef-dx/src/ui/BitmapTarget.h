#pragma once

#include "gx/Texture.h"
#include <cef_render_handler.h>
#include "gx/GxContext.h"

namespace wowc {
	namespace ui {
		class BitmapTarget {
			gx::TexturePtr mTexture;
			void* mSharedTexture = nullptr;
			bool mHasSharedTextureChanged = false;
			gx::GxContextPtr mContext;

		public:
			BitmapTarget() = default;
			~BitmapTarget() = default;

			BitmapTarget(const BitmapTarget&) = delete;
			BitmapTarget(BitmapTarget&&) = delete;

			void setContext(const gx::GxContextPtr& context);

			gx::TexturePtr getSharedTexture() const { return mTexture; }

			void operator = (const BitmapTarget&) = delete;
			void operator = (BitmapTarget&&) = delete;

			void onUpdate();

			void onAcceleratedPaint(void* sharedTexture);
		};
	}
}
