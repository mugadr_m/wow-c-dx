#pragma once

#include <cef_client.h>
#include "utils/Logger.h"
#include "UiMessageHandler.h"

namespace wowc {
	namespace ui {
		class WebCore;
		class WebClient;

		class WebClient final : public CefClient {
            class ContextMenuHandler : public CefContextMenuHandler {
				IMPLEMENT_REFCOUNTING(ContextMenuHandler);

            public:

                void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
                    CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model) override;
            };

			class RequestHandler : public CefRequestHandler {
				IMPLEMENT_REFCOUNTING(RequestHandler);

				WebClient* mWebClient;

			public:
				explicit RequestHandler(WebClient* webClient) : mWebClient(webClient) {
					
				}

				bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefRequest> request,
					bool user_gesture, bool is_redirect) override;
				void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) override;

			};

			class RenderHandler : public CefRenderHandler {
			IMPLEMENT_REFCOUNTING(RenderHandler);

				WebClient* mWebClient;

			public:
				explicit RenderHandler(WebClient* webClient) : mWebClient(webClient) {
				}

				RenderHandler(const RenderHandler&) = delete;
				RenderHandler(RenderHandler&&) = delete;
				~RenderHandler() = default;

				void operator =(const RenderHandler&) = delete;
				void operator =(RenderHandler&&) = delete;

				bool GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
				bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override;
				bool GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY) override;
				bool GetScreenInfo(CefRefPtr<CefBrowser> browser, CefScreenInfo& screenInfo) override;
				void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer,
				             int width, int height) override;

			    void OnAcceleratedPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects,
			        void* shared_handle, uint64 sync_key) override;

				void OnCursorChange(CefRefPtr<CefBrowser> browser, HCURSOR cursor, CursorType type,
				                    const CefCursorInfo& customCursorInfo) override;


				void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) override;
				void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) override;
			};

			class LifeSpanHandler : public CefLifeSpanHandler {
			IMPLEMENT_REFCOUNTING(LifeSpanHandler);

				WebClient* mWebClient;

			public:
				explicit LifeSpanHandler(WebClient* webClient) : mWebClient(webClient) {
				}


				void OnAfterCreated(CefRefPtr<CefBrowser> browser) override;

				void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;

				bool DoClose(CefRefPtr<CefBrowser> browser) override;
			};

		IMPLEMENT_REFCOUNTING(WebClient);

		static utils::Logger<WebClient> mLogger;

			WebCore* mWebCore;
			CefRefPtr<RenderHandler> mRenderHandler;
			CefRefPtr<LifeSpanHandler> mLifeSpanHandler;
			CefRefPtr<CefRequestHandler> mRequestHandler;
			CefRefPtr<ContextMenuHandler> mContextMenuHandler = new ContextMenuHandler();
			CefRefPtr<CefBrowser> mLocalBrowser;
			CefRefPtr<CefMessageRouterBrowserSide> mMessageRouter;
			std::unique_ptr<UiMessageHandler> mMessageHandler = std::make_unique<UiMessageHandler>();

			WebCore* getWebCore() const { return mWebCore; }

			bool onBeforeBrowse(const CefRefPtr<CefBrowser>& browser, const CefRefPtr<CefFrame>& frame) const;
			void onRenderProcessTerminated(CefRefPtr<CefBrowser> browser) const;

			void onPaint(CefRenderHandler::PaintElementType type, const CefRenderHandler::RectList& dirtyRects, const void* buffer,
				int width, int height) const;

			void onAcceleratedPaint(CefRefPtr<CefBrowser> browser, CefRenderHandler::PaintElementType type, const CefRenderHandler::RectList& dirtyRects,
				void* shared_handle, uint64 sync_key);

		public:
			explicit WebClient(WebCore* webCore);

			void onLocalBrowserReady(const CefRefPtr<CefBrowser>& localBrowser);
			void onBrowserClosed();

			void onResize(uint32_t width, uint32_t height) const;

			const CefRefPtr<CefBrowser>& getLocalBrowser() const { return mLocalBrowser; }

			void shutdown();

			void postConstruct(const cdi::ApplicationContextPtr& context);

			CefRefPtr<CefRenderHandler> GetRenderHandler() override { return mRenderHandler; }
			CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override { return mLifeSpanHandler; }
			CefRefPtr<CefRequestHandler> GetRequestHandler() override { return mRequestHandler;  }
			CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() override { return mContextMenuHandler; }

			bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
				CefProcessId source_process,
				CefRefPtr<CefProcessMessage> message) override;

			void onFrame();
		};
	}
}
