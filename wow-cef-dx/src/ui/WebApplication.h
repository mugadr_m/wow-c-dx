#pragma once

#include <cef_app.h>

namespace wowc {
	namespace ui {
		class WebCore;

		class WebApplication : public CefApp {
			class RenderProcessHandler : public CefRenderProcessHandler {
				IMPLEMENT_REFCOUNTING(RenderProcessHandler);

				WebApplication* mWebApplication;
			public:
				explicit RenderProcessHandler(WebApplication* webApplication);

				void OnUncaughtException(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context,
					CefRefPtr<CefV8Exception> exception, CefRefPtr<CefV8StackTrace> stackTrace) override;
			};

			class BrowserProcessHandler : public CefBrowserProcessHandler {
				IMPLEMENT_REFCOUNTING(BrowserProcessHandler);

				WebApplication* mWebApplication;

			public:
				explicit BrowserProcessHandler(WebApplication* webApplication) { mWebApplication = webApplication; }
				void OnContextInitialized() override;

			    void OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line) override;
			};

			IMPLEMENT_REFCOUNTING(WebApplication);

			CefRefPtr<RenderProcessHandler> mRenderProcess;
			CefRefPtr<BrowserProcessHandler> mBrowserProcess;

			WebCore* mWebCore = nullptr;

			void onContextInitialized() const;

		public:
			explicit WebApplication(WebCore* core);
			WebApplication(const WebApplication&) = delete;
			WebApplication(WebApplication&&) = delete;
			~WebApplication() = default;

			void operator = (const WebApplication&) = delete;
			void operator = (WebApplication&&) = delete;

			void shutdown();

			CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override { return mRenderProcess; }
			CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override { return mBrowserProcess; }

		};
	}
}