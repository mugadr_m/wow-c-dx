#include "stdafx.h"
#include "UiMessageHandler.h"
#include "events/EventManager.h"

namespace wowc {
	namespace ui {
		utils::Logger<UiMessageHandler> UiMessageHandler::mLogger;

		UiMessageHandler::UiMessageHandler() {
			mApplicationContext = cdi::ApplicationContext::instance();
		}

		bool UiMessageHandler::OnQuery(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int64 query_id,
			const CefString& request, bool persistent, CefRefPtr<Callback> callback) {
			if(request == "EventHandlerInit") {
				if(!persistent) {
					mLogger.error("Attempted to initialize event handler with non persistent callback");
					return false;
				}

				mEventManager->onInitEventHandler(callback);
				return true;
			}

			if(persistent) {
				mLogger.warn("Only 'EventHandlerInit' can be a persistent callback. Event is considered non persistent.");
			}

			try {
				mEventManager->onJsEvent(request, callback);
			} catch (std::exception& ex) {
				mLogger.error("Error in js event: ", ex.what());
				callback->Failure(0xBADBEEF, ex.what());
			}

			return true;
		}

		void UiMessageHandler::postConstruct(const cdi::ApplicationContextPtr& context) {
			mApplicationContext = context;
			mEventManager = context->produce<events::EventManager>();
		}

		void UiMessageHandler::destroy() {
			mEventManager->preDestroy();
		}
	}
}