#pragma once
#include "cdi/ApplicationContext.h"
#include <internal/cef_ptr.h>
#include <wrapper/cef_message_router.h>
#include <rapidjson.h>
#include <error/en.h>
#include <document.h>
#include <utility>

namespace wowc {
	namespace ui {
		namespace events {
			class EventHandlerInstance {
				std::function<rapidjson::Document (rapidjson::Document&)> mHandler;
			public:
				explicit EventHandlerInstance(std::function<rapidjson::Document (rapidjson::Document&)> callback) : mHandler(
					std::move(callback)) {

				}

				rapidjson::Document execute(rapidjson::Document& value) const {
					return mHandler(value);
				}
			};

			class EventManager {
				static utils::Logger<EventManager> mLogger;

				CefRefPtr<CefMessageRouterBrowserSide::Callback> mEventCallback;
				std::map<std::string, std::shared_ptr<EventHandlerInstance>> mEventHandlers;
				cdi::ApplicationContextPtr mApplicationContext;

				static std::string getEventName(const std::type_info& typeInfo);
				static rapidjson::Document parseJson(const std::string& content);
				static std::string toString(const rapidjson::Document& document);

				static void validateEventObject(const rapidjson::Document& eventObject, const std::string& payload);
				static rapidjson::Document buildResponseObject(rapidjson::Document& response, const std::string& type);

				void dispatchApplicationEvent(const std::string& eventName, rapidjson::Document& event) const;

			public:
				void preDestroy();
				void postConstruct(const cdi::ApplicationContextPtr& context);

				void onInitEventHandler(const CefRefPtr<CefMessageRouterBrowserSide::Callback>& callback);

				template <typename T>
				void registerEvent() {
					auto eventName = getEventName(typeid(T));
					auto itr = mEventHandlers.find(eventName);
					if (itr != mEventHandlers.end()) {
						mLogger.error("Attempted to register event of type ", eventName, " multiple times");
						throw std::exception("Event already registered");
					}

					mEventHandlers.emplace(
						eventName,
						std::make_shared<EventHandlerInstance>([this](rapidjson::Document& value) {
							auto payload = T::fromJson(value);
							mApplicationContext->publishEvent(payload);
							return payload.getResponseJson();
						})
					);
				}

				void onJsEvent(const std::string& request, const CefRefPtr<CefMessageRouterBrowserSide::Callback>& callback);

				template <typename T>
				void onApplicationEvent(const T& evnt) {
					const auto eventName = getEventName(typeid(T));
					auto itr = mEventHandlers.find(eventName);
					if (itr == mEventHandlers.end()) {
						mLogger.warn("Event ", typeid(T).name(), " is not a registered event. It is not invoked");
						return;
					}

					rapidjson::Document eventDocument;
					evnt.toJson(eventDocument);
					dispatchApplicationEvent(eventName, eventDocument);
				}
			};

			SHARED_PTR(EventManager);
		}
	}
}
