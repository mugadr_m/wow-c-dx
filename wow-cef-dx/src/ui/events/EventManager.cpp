#include "stdafx.h"
#include "EventManager.h"
#include <wrapper/cef_message_router.h>
#include <../rapidjson/stringbuffer.h>
#include <../rapidjson/writer.h>

namespace wowc {
	namespace ui {
		namespace events {
			utils::Logger<EventManager> EventManager::mLogger;

			std::string EventManager::getEventName(const std::type_info& typeInfo) {
				std::string fullName = typeInfo.name();
				const auto lastDot = fullName.find_last_of(':');
				if(lastDot == std::string::npos) {
					return fullName;
				}

				return fullName.substr(lastDot + 1);
			}

			rapidjson::Document EventManager::parseJson(const std::string& content) {
				auto document = rapidjson::Document();
				document.Parse(content.c_str());
				if(document.HasParseError()) {
					mLogger.error("Error parsing json: ", GetParseError_En(document.GetParseError()), " @ ", document.GetErrorOffset());
					throw std::exception("Error parsing json");
				}
				return document;
			}

			std::string EventManager::toString(const rapidjson::Document& document) {
				rapidjson::StringBuffer stringBuffer;
				rapidjson::Writer<rapidjson::StringBuffer> writer(stringBuffer);
				if(!document.Accept(writer)) {
					return std::string();
				}

				const auto length = stringBuffer.GetLength();
				const auto strPointer = stringBuffer.GetString();
				return std::string(strPointer, strPointer + length);
			}

			void EventManager::dispatchApplicationEvent(const std::string& eventName, rapidjson::Document& event) const {
				rapidjson::Document document;
				auto& allocator = document.GetAllocator();

				rapidjson::Value nameValue(rapidjson::kStringType);
				nameValue.SetString(eventName.c_str(), allocator);

				document.SetObject();
				document.AddMember("type", nameValue, allocator);
				if (!event.IsNull()) {
					document.AddMember("body", event.GetObject(), allocator);
				}

				auto response = toString(document);
				if(response.empty()) {
					mLogger.error("Unable to write object to JSON");
					return;
				}

				if(mEventCallback != nullptr) {
					mEventCallback->Success(response);
				}

			}

			void EventManager::validateEventObject(const rapidjson::Document& eventObject, const std::string& payload) {
				if (!eventObject.IsObject()) {
					mLogger.error("Got JS event hat is not an object: ", payload);
					throw std::exception("Invalid JS event");
				}

				if (!eventObject.HasMember("type")) {
					mLogger.error("Got JS event without a 'type' field: ", payload);
					throw std::exception("Invalid JS event");
				}

				if (!eventObject.HasMember("request")) {
					mLogger.error("Get JS event without a 'request' field: ", payload);
					throw std::exception("Invalid JS event");
				}
			}

			rapidjson::Document EventManager::buildResponseObject(rapidjson::Document& response, const std::string& type) {
                if(response.IsNull()) {
					response.SetObject();
                }

				rapidjson::Document ret;
				auto& allocator = ret.GetAllocator();
				ret.SetObject();
				rapidjson::Value typeValue(rapidjson::kStringType);
				typeValue.SetString(type.c_str(), allocator);
				ret.AddMember("type", typeValue, allocator);
				ret.AddMember("body", response.GetObject(), allocator);
				return ret;
			}

			void EventManager::preDestroy() {
				mEventCallback = nullptr;
			}

			void EventManager::postConstruct(const cdi::ApplicationContextPtr& context) {
				mApplicationContext = context;
			}

			void EventManager::onInitEventHandler(const CefRefPtr<CefMessageRouterBrowserSide::Callback>& callback) {
				mEventCallback = callback;
			}

			void EventManager::onJsEvent(const std::string& request, const CefRefPtr<CefMessageRouterBrowserSide::Callback>& callback) {
				auto document = parseJson(request);
				validateEventObject(document, request);

				auto typeName = document.GetObject().FindMember("type");
				auto& value = typeName->value;
				if(!value.IsString()) {
					mLogger.error("Got JS event with a 'type' field that is not a string: ", request);
					throw std::exception("Invalid JS event");
				}

				const std::string requestType = value.GetString();
				const auto itr = mEventHandlers.find(requestType);
				if(itr == mEventHandlers.end()) {
					mLogger.error("No event handler found for JS type ", requestType);
					throw std::exception("Invalid JS event");
				}

				auto requestContent = document.GetObject().FindMember("request");
				auto& requestValue = requestContent->value;
				rapidjson::StringBuffer stringBuffer;
				rapidjson::Writer<rapidjson::StringBuffer> writer(stringBuffer);
				if (!requestValue.Accept(writer)) {
					mLogger.error("Unable to write request object to JSON");
					throw std::exception("Invalid JS event");
				}

				auto payload = parseJson(stringBuffer.GetString());
				auto responsePayload = itr->second->execute(payload);

				const auto responseObject = buildResponseObject(responsePayload, requestType);
				auto responseString = toString(responseObject);
				if(responseString.empty()) {
					mLogger.warn("Unable to create response string from json object");
					return;
				}

				if(callback != nullptr) {
					callback->Success(responseString);
				}
			}
		}
	}
}
