#include "stdafx.h"
#include "PopUpDrawer.h"

namespace wowc {
    namespace ui {
        void PopUpDrawer::updateVertices() {
#pragma pack(push, 1)
            struct Vertex {
                float x, y;
                float tx, ty;
            };
#pragma pack(pop)

            const auto flw = std::max(1.0f, static_cast<float>(mImageWidth));
            const auto flh = std::max(1.0f, static_cast<float>(mImageHeight));

            const auto minX = (mPositionX / flw) * 2 - 1;
            const auto maxX = ((mPositionX + mWidth) / flw) * 2 - 1;
            const auto minY = 1 - (mPositionY / flh) * 2;
            const auto maxY = 1 - ((mPositionY + mHeight) / flh) * 2;


            Vertex vertices[]{
                {minX, minY, 0.0f, 0.0f},
                {maxX, minY, 1.0f, 0.0f},
                {maxX, maxY, 1.0f, 1.0f},
                {minX, maxY, 0.0f, 1.0f}
            };

            memcpy(mVertices.data(), vertices, sizeof vertices);
            mVerticesChanged = true;
        }

        PopUpDrawer::PopUpDrawer() {
            mVertices.resize(16);
        }

        void PopUpDrawer::onResizePopup(const CefRect& rect) {
            mPositionX = static_cast<int32>(rect.x * mDpiScale);
            mPositionY = static_cast<int32>(rect.y * mDpiScale);
            mWidth = static_cast<int32>(rect.width * mDpiScale);
            mHeight = static_cast<int32>(rect.height * mDpiScale);
            updateVertices();
        }

        void PopUpDrawer::onPopupVisibilityChanged(bool isVisible) {
            mIsVisible = isVisible;
        }

        void PopUpDrawer::setContext(const gx::GxContextPtr& context) {
            mContext = context;
            mVertexBuffer = mContext->createVertexBuffer(false);
            mVertexBuffer->updateData(mVertices);
        }

        void PopUpDrawer::updateData() {
            if (mHasSharedTextureChanged) {
                mHasSharedTextureChanged = false;
                mTexture = mContext->openSharedTexture(mSharedTexture);;
            }

            if (mVerticesChanged) {
                mVerticesChanged = false;
                mVertexBuffer->updateData(mVertices);
            }
        }

        void PopUpDrawer::onWidowResize(uint32_t width, uint32_t height) {
            mImageWidth = width;
            mImageHeight = height;
            updateVertices();
        }

        void PopUpDrawer::onAcceleratedPaint(void* sharedTexture) {
            if (sharedTexture == mSharedTexture) {
                return;
            }

            mSharedTexture = sharedTexture;
            mHasSharedTextureChanged = true;
        }
    }
}
