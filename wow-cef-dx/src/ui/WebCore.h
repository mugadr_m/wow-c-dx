#pragma once

#include <cef_client.h>
#include "utils/Logger.h"
#include "gx/Window.h"
#include "WebApplication.h"
#include "WebClient.h"
#include "cdi/ApplicationContext.h"
#include "io/cef/LocalSchemeHandlerFactory.h"
#include "BitmapTarget.h"
#include "gx/GxContext.h"
#include "PopUpDrawer.h"
#include "io/files/cef/LoadingScreenHandlerFactory.h"
#include "io/cef/MapEntryLeafletResourceHandlerFactory.h"
#include "WebIPC.h"

namespace wowc {
	namespace ui {
		class WebApplication;

		class WebCore : public std::enable_shared_from_this<WebCore> {
			static utils::Logger<WebCore> mLogger;

			gx::MeshPtr mMesh;
			gx::TexturePtr mTexture;
			gx::SamplerPtr mSampler;
			gx::ProgramPtr mProgram;
			gx::VertexBufferPtr mVertexBuffer;
			gx::IndexBufferPtr mIndexBuffer;

			WebIpc mWebIpc;

			CefRefPtr<WebApplication> mApplication;
			CefRefPtr<WebClient> mWebClient;
			CefRefPtr<io::cef::LocalSchemeHandlerFactory> mLocalScheme;
			CefRefPtr<io::cef::LoadingScreenHandlerFactory> mLoadingScreenScheme;
			CefRefPtr<io::cef::MapEntryLeafletResourceHandlerFactory> mMapEntryLeafletResourceHandler;

			gx::WindowPtr mWindow;

			std::thread mMessageThread;
			std::condition_variable mLoadEvent;
			bool mLoadResult = false;
			bool mIsLoadDone = false;
			std::mutex mLoadEventLock;

			uint32_t mLastClickCount = 0;
			uint64_t mLastClickTime = 0;
			int32_t mLastClickX = 0;
			int32_t mLastClickY = 0;
			cef_mouse_button_type_t mLastClickButton = MBT_LEFT;

			BitmapTarget mBitmapTarget;
			PopUpDrawer mPopupDrawer;

			bool mHasInputFocus = false;

			void initGraphics() const;

			void messageThreadHandler();

			static uint32_t getModifiers();
			static cef_mouse_button_type_t getMouseButton(uint32_t messageType);
		public:
			WebCore() = default;
			WebCore(const WebCore&) = delete;
			WebCore(WebCore&&) = delete;
			~WebCore() = default;

			void operator =(const WebCore&) = delete;
			void operator =(WebCore&&) = delete;

			void handleMouseMove(float x, float y);
			void handleMouseDown(uint32_t messageType, LPARAM lParam);
			void handleMouseUp(uint32_t messageType, LPARAM lParam) const;
			void handleMouseLeave() const;
			void handleMouseWheel(WPARAM wParam, LPARAM lParam) const;
			void handleCharPress(WPARAM wParam) const;
			void handleKeyDown(WPARAM wParam, LPARAM lParam) const;
			void handleKeyUp(WPARAM wParam, LPARAM lParam) const;
			void handleWindowMove();

			void onApplicationReady() const;
			void onBrowserClosed() const;

			void postConstruct(const cdi::ApplicationContextPtr& ctx);;

			void initialize();
			void shutdown();

			void onFrame();
			void onResize();

			void onFocusedNodeChanged(const bool hasNode) {
				mHasInputFocus = hasNode;
			}

            bool hasInputFocus() const {
				return mHasInputFocus;
			}

			void onBeforeChildProcess(CefRefPtr<CefCommandLine> commandLine) const;

			BitmapTarget& getBitmapTarget() { return mBitmapTarget; }
			PopUpDrawer& getPopupDrawer() { return mPopupDrawer; }

			const gx::WindowPtr& getWindow() const { return mWindow; }
		};
	}
}
