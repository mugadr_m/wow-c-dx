#include "stdafx.h"
#include "WebIPC.h"
#include <ws2tcpip.h>
#include "io/BinaryReader.h"
#include "WebCore.h"

namespace wowc {
    namespace ui {
        utils::Logger<WebIpc> WebIpc::mLogger;

        namespace detail {
            class WsaInitializer {
                utils::Logger<WsaInitializer> mLogger;

            public:
                WsaInitializer() {
                    WSADATA wsaData{};
                    const auto ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
                    if (ret != 0) {
                        mLogger.error("WSAStartup failed: ", WSAGetLastError());
                        throw std::exception("Error initializing Windows Sockets");
                    }

                    mLogger.info("WSA loaded with: ", wsaData.szDescription, " @ ", wsaData.szSystemStatus);
                }
            };
        }


        void WebIpc::handleIpcThread() {
            mIsRunning = true;
            auto client{INVALID_SOCKET};

            while (mIsRunning) {
                FD_SET acceptSet{};
                FD_ZERO(&acceptSet);
                FD_SET(mSocket, &acceptSet);
                timeval timeout{1, 0};
                const auto ret = select(1, &acceptSet, nullptr, nullptr, &timeout);
                if (ret == 0) {
                    continue;
                }

                if (ret < 0) {
                    return;
                }

                if (FD_ISSET(mSocket, &acceptSet)) {
                    SOCKADDR_IN sin;
                    memset(&sin, 0, sizeof sin);
                    auto addrLen = static_cast<int>(sizeof sin);
                    client = accept(mSocket, reinterpret_cast<sockaddr*>(&sin), &addrLen);
                    if (client == INVALID_SOCKET) {
                        mLogger.error("Unable to accept client socket: ", WSAGetLastError());
                        return;
                    }
                    break;
                }
            }

            onClientConnected(client);
        }

        void WebIpc::onClientConnected(SOCKET client) const {
            SOCKADDR_IN sin;
            memset(&sin, 0, sizeof sin);
            auto nameLen = static_cast<int>(sizeof sin);
            if (getsockname(client, reinterpret_cast<sockaddr*>(&sin), &nameLen) != SOCKET_ERROR) {
                char addrName[1024]{0};
                if (inet_ntop(sin.sin_family, &sin.sin_addr, addrName, sizeof addrName) != nullptr) {
                    mLogger.info("Accepted client from ", addrName, ":", sin.sin_port);
                } else {
                    mLogger.info("Accepted client on port ", sin.sin_port);
                }
            }


            std::vector<uint8_t> currentBuffer;
            while (mIsRunning) {
                FD_SET readSet{};
                FD_ZERO(&readSet);
                FD_SET(client, &readSet);
                timeval timeout{1, 0};
                const auto ret = select(1, &readSet, nullptr, nullptr, &timeout);
                if (ret == 0) {
                    continue;
                }

                if (ret < 0) {
                    return;
                }

                if (!FD_ISSET(client, &readSet)) {
                    continue;
                }

                uint8_t chunk[1024]{0};
                const auto rc = recv(client, reinterpret_cast<char*>(chunk), 1024, 0);
                if (rc == 0) {
                    continue;
                }

                if (rc < 0) {
                    return;
                }

                currentBuffer.insert(currentBuffer.end(), chunk, chunk + rc);
                onDataReceived(currentBuffer);
            }
        }

        void WebIpc::onDataReceived(std::vector<uint8_t>& buffer) const {
            while (buffer.size() >= 4) {
                const auto packetSize = *reinterpret_cast<uint32_t*>(buffer.data());
                if (packetSize + 4 > buffer.size()) {
                    break;
                }

                std::vector<uint8_t> packet(packetSize);
                memcpy(packet.data(), buffer.data() + sizeof(uint32_t), packetSize);
                onPacket(packet);
				buffer.erase(buffer.begin(), buffer.begin() + 4 + packetSize);
            }
        }

        void WebIpc::onPacket(std::vector<uint8_t>& packet) const {
            const auto core = mWebCoreWeak.lock();
            if (core == nullptr) {
                return;
            }

            io::BinaryReader reader(packet);

            const auto opcode = reader.read<IpcOpcode>();
            switch (opcode) {
            case IpcOpcode::FOCUS_NODE_CHANGE: {
                core->onFocusedNodeChanged(reader.read<bool>());
                break;
            }
            }
        }

        void WebIpc::initialize(const std::shared_ptr<WebCore>& core) {
            // ReSharper disable once CppDeclaratorNeverUsed
            static detail::WsaInitializer initializer;

            mWebCoreWeak = core;

            mSocket = socket(AF_INET, SOCK_STREAM, 0);
            if (mSocket == INVALID_SOCKET) {
                mLogger.error("Unable to create IPC socket: ", WSAGetLastError());
                throw std::exception("WebCore IPC error: ", WSAGetLastError());
            }

            SOCKADDR_IN sockAddr;
            memset(&sockAddr, 0, sizeof sockAddr);
            sockAddr.sin_family = PF_INET;
            sockAddr.sin_port = 0; // generate dynamic port binding
            sockAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

            auto rc = bind(mSocket, reinterpret_cast<const sockaddr*>(&sockAddr), sizeof sockAddr);
            if (rc == SOCKET_ERROR) {
                mLogger.error("Unable to bind IPC socket: ", WSAGetLastError());
                throw std::exception("WebCore IPC error");
            }

            rc = listen(mSocket, 1);
            if (rc == SOCKET_ERROR) {
                mLogger.error("Unable to listen on IPC socket: ", WSAGetLastError());
                throw std::exception("WebCore IPC error");
            }

            auto nameLen = static_cast<int>(sizeof sockAddr);
            if (getsockname(mSocket, reinterpret_cast<sockaddr*>(&sockAddr), &nameLen) == SOCKET_ERROR) {
                mLogger.error("Unable to fetch binding port: ", WSAGetLastError());
                throw std::exception("ebCore IPC error");
            }

            mLogger.info("Bound IPC socket to port: ", htons(sockAddr.sin_port));
            mIpcPort = static_cast<uint32_t>(htons(sockAddr.sin_port));

            mIpcThread = std::thread(std::bind(&WebIpc::handleIpcThread, this));
        }

        void WebIpc::shutdown() {
            mIsRunning = false;
            closesocket(mSocket);
            if (mIpcThread.joinable()) {
                mIpcThread.join();
            }
        }
    }
}
