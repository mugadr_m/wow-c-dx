#pragma once

#include <cef_v8.h>

namespace wowc {
	namespace ui {
		namespace cef {
			template <typename T>
			class is_shared_ptr : public std::false_type {
			};

			template <typename T>
			class is_shared_ptr<std::shared_ptr<T>> : public std::true_type {
			public:
				typedef T type;
			};

			template <typename T>
			class is_container : public std::false_type {
			};

			template <typename T>
			class is_container<std::list<T>> : public std::true_type {
			public:
				typedef T type;
			};

			template <typename T>
			class is_container<std::vector<T>> : public std::true_type {
			public:
				typedef T type;
			};

			template <typename T>
			constexpr bool is_container_v = is_container<T>::value;

			template <typename T>
			using is_container_t = typename is_container<T>::type;

			class ObjectRegistry;

			class RegisterableObject {
			public:
				virtual ~RegisterableObject() = default;

				/**
				 * \brief   Called by the registration process which attempts to visit this object
				 *			so that it can perform its initialization
				 * \param registry ObjectRegistry that knows about all the available class mappings
				 * \param obj Object where this type of object should be registered to
				 */
				virtual void visit(ObjectRegistry* registry, CefRefPtr<CefV8Value> obj) {
				}
			};

			class Function : public CefV8Handler, public RegisterableObject {
			IMPLEMENT_REFCOUNTING(Function);

				class GenericOverload {
				protected:
					ObjectRegistry* mRegistry = nullptr;

				public:
					GenericOverload() = default;
					GenericOverload(const GenericOverload&) = delete;
					GenericOverload(GenericOverload&&) = delete;
					virtual ~GenericOverload() = default;

					void operator =(const GenericOverload&) = delete;
					void operator =(GenericOverload&&) = delete;

					virtual void onInvocation(const CefV8ValueList& arguments, CefString& exception,
					                          CefRefPtr<CefV8Value>& retValue) = 0;
					virtual int32 getScore(const CefV8ValueList& arguments) = 0;

					void setRegistry(ObjectRegistry* registry) { mRegistry = registry; }
				};

				template <typename Ret, typename... Args>
				class Overload : public GenericOverload {
					std::function<Ret(Args ...)> mCallback;

					template <uint32 index, typename T, typename... Rem>
					void getScore(int32& curScore, const CefV8ValueList& arguments);

					template <uint32 index>
					void getScore(int32& curScore, const CefV8ValueList& arguments) {

					}

					template <uint32 curArg, typename R, typename... Rem, typename... Cur>
					void chainInvocation(const CefV8ValueList& arguments, CefString& exception, CefRefPtr<CefV8Value>& retValue,
					                     const Cur&... curArgs);

					template <uint32 curArg, typename... Rem>
					void chainInvocation(const CefV8ValueList& arguments, CefString& exception, CefRefPtr<CefV8Value>& retValue,
					                     const Args&... args) {
						callFunction<Ret>(args..., retValue, exception);
					}

					template <typename T>
					std::enable_if_t<std::is_same_v<T, void>, void> callFunction(
						const Args&... args, CefRefPtr<CefV8Value>& retValue, CefString& exception) {
						mCallback(args...);
					}

					template <typename T>
					std::enable_if_t<!std::is_same_v<T, void>, void> callFunction(
						const Args&... args, CefRefPtr<CefV8Value>& retValue, CefString& exception);

				public:
					explicit Overload(const std::function<Ret(Args ...)>& callback) : mCallback(callback) {

					}

					int32 getScore(const CefV8ValueList& arguments) override {
						if (arguments.size() != sizeof...(Args)) {
							return 0;
						}

						auto retScore = 100;
						getScore<0, Args...>(retScore, arguments);
						return retScore;
					}

					void onInvocation(const CefV8ValueList& arguments, CefString& exception, CefRefPtr<CefV8Value>& retValue)
					override {
						chainInvocation<0, Args...>(arguments, exception, retValue);
					}
				};

				template <typename T>
				class FunctionalTraits : public FunctionalTraits<decltype(&T::operator())> {
				};

				template <typename ClassType, typename ReturnType, typename... Args>
				class FunctionalTraits<ReturnType(ClassType::*)(Args ...) const> {
				public:
					typedef std::function<ReturnType(Args ...)> CallbackType;
					typedef Overload<ReturnType, Args...> OverloadType;
				};

				template <typename T>
				class is_functor {
					struct Small {
						char mDummy;
					};

					struct Big {
						char mDummy[2];
					};

					template <typename C>
					static Small test(decltype(&C::operator()));
					template <typename C>
					static Big test(...);

				public:
					static const bool VALUE = sizeof(test<T>(0)) == sizeof(Small);
				};

				std::string mName;
				std::list<std::shared_ptr<GenericOverload>> mOverloads;

			public:
				explicit Function(std::string name);

				template <typename T>
				explicit Function(const std::string& name, const T& function) : Function(name) {
					def(function);
				}

				template <typename T>
				std::enable_if_t<is_functor<T>::VALUE, Function&> def(const T& function) {
					using FunctionType = typename FunctionalTraits<T>::CallbackType;
					using OverloadType = typename FunctionalTraits<T>::OverloadType;

					const auto funcInstance = FunctionType(function);
					mOverloads.push_back(std::make_shared<OverloadType>(funcInstance));
					return *this;
				}


				bool Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments,
				             CefRefPtr<CefV8Value>& retval, CefString& exception) override;

				void visit(ObjectRegistry* registry, CefRefPtr<CefV8Value> obj) override;
			};

			class Module {
				ObjectRegistry* mRegistry;
				CefRefPtr<CefV8Context> mContext;

			public:
				Module(ObjectRegistry* registry, const CefRefPtr<CefV8Context>& context);

				void operator [](RegisterableObject&& object) const;

			};

			class ObjectRegistry {
			public:
				Module createModule(const CefRefPtr<CefV8Context>& context);
			};

			class ArgumentMatcher {
			public:
				template <typename T>
				static std::enable_if_t<std::is_integral_v<T>, void> matchArgument(const CefRefPtr<CefV8Value>& value, uint32 numArguments,
				                                                            int32& score) {
					if (value->IsUInt() || value->IsInt()) {
						return;
					}
					if (value->IsDouble()) {
						score -= 100 / numArguments;
						return;
					}

					score = 0;
				}

				template <typename T>
				static std::enable_if_t<std::is_floating_point_v<T>, void> matchArgument(const CefRefPtr<CefV8Value>& value,
				                                                                  uint32 numArguments, int32& score) {
					if (value->IsDouble()) {
						return;
					}

					if (value->IsUInt() || value->IsInt()) {
						score -= 100 / numArguments;
						return;
					}

					score = 0;
				}

				template <typename T>
				static std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<T>>, std::string>, void> matchArgument(
					const CefRefPtr<CefV8Value>& value, uint32 numArguments, int32& score) {
					if(value->IsString()) {
						return;
					}

					score -= 100 / numArguments;
				}
			};

			class ValueWrap {
			public:
				ValueWrap() = delete;

				/**************************************************
				*        WRAPPING
				**************************************************/
#pragma region WRAP

				/*
				 * INT -> CefV8Value
				 */
				template <typename T>
				static std::enable_if_t<std::is_integral_v<T> && !std::is_unsigned_v<T>, CefRefPtr<CefV8Value>> wrap(
					ObjectRegistry* registry, const T& value) {
					return CefV8Value::CreateInt(value);
				}

				/*
				 * UINT -> CefV8Value
				 */
				template <typename T>
				static std::enable_if_t<std::is_integral_v<T> && std::is_unsigned_v<T>, CefRefPtr<CefV8Value>> wrap(
					ObjectRegistry* registry, const T& value) {
					return CefV8Value::CreateUInt(value);
				}

				/*
				 * FLOAT -> CefV8Value
				 */
				template <typename T>
				static std::enable_if_t<std::is_floating_point_v<T>, CefRefPtr<CefV8Value>> wrap(
					ObjectRegistry* registry, const T& value) {
					return CefV8Value::CreateDouble(value);
				}

				/*
				 * std::string -> CefV8Value
				 */
				template <typename T>
				static std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<T>>, std::string>, CefRefPtr<CefV8Value>>
				wrap(ObjectRegistry* registry, const T& value) {
					return CefV8Value::CreateString(value);
				}

				/*
				 * container -> CefV8Value
				 */
				template <typename T>
				static std::enable_if_t<is_container_v<T>, CefRefPtr<CefV8Value>> wrap(ObjectRegistry* registry, const T& value) {
					auto ret = CefV8Value::CreateArray(value.size());
					auto itr = std::cbegin(value);
					auto end = std::cend(value);
					auto index = 0;
					std::for_each(itr, end, [&index, &ret, registry](const auto& val) {
						ret->SetValue(index++, ValueWrap::wrap(registry, val));
					});

					return ret;
				}
#pragma endregion

#pragma region UNWRAP
				/**************************************************
				 *        UNWRAP
				 **************************************************/
				template <typename T>
				static std::enable_if_t<std::is_integral_v<T> || std::is_floating_point_v<T>, T> unwrap(
					ObjectRegistry* registry, CefString& exception, bool& hasException, const CefRefPtr<CefV8Value>& value) {
					if (value->IsInt() || value->IsUInt()) {
						return static_cast<T>(value->GetIntValue());
					}
					if (value->IsDouble()) {
						return static_cast<T>(value->GetDoubleValue());
					}

					std::stringstream strm;
					strm << "Cannot convert value to " << typeid(T).name();
					exception = strm.str();
					hasException = true;
					return T();
				}

				template <typename T>
				static std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<T>>, std::string>, T> unwrap(
					ObjectRegistry* registry, CefString& exception, bool& hasException, const CefRefPtr<CefV8Value>& value) {
					if (value->IsNull()) {
						return "null";
					}

					return value->GetStringValue();
				}
#pragma endregion
			};

			template <typename Ret, typename... Args>
			template <uint32 curArg, typename R, typename... Rem, typename... Cur>
			void Function::Overload<Ret, Args...>::chainInvocation(const CefV8ValueList& arguments, CefString& exception,
			                                                       CefRefPtr<CefV8Value>& retValue,
			                                                       const Cur&... curArgs) {
				auto hasException = false;
				const auto& currentArg = arguments[curArg];
				const auto& unwrapped = ValueWrap::unwrap<R>(mRegistry, exception, hasException, currentArg);
				if (hasException) {
					return;
				}

				chainInvocation<curArg + 1, Rem...>(arguments, exception, retValue, curArgs..., unwrapped);
			}

			template <typename Ret, typename... Args>
			template <typename T>
			std::enable_if_t<!std::is_same_v<T, void>, void> Function::Overload<Ret, Args...>::callFunction(
				const Args&... args, CefRefPtr<CefV8Value>& retValue, CefString& exception) {
				const auto& retVal = mCallback(args...);
				retValue = ValueWrap::wrap(mRegistry, retVal);
			}

			template <typename Ret, typename ... Args>
			template <uint32 index, typename T, typename ... Rem>
			void Function::Overload<Ret, Args...>::getScore(int32& curScore, const CefV8ValueList& arguments) {
				const auto& arg = arguments[index];
				ArgumentMatcher::matchArgument<T>(arg, arguments.size(), curScore);
				if(curScore <= 0) {
					return;
				}

				getScore<index + 1, Rem...>(curScore, arguments);
			}

		}
	}
}
