#include "stdafx.h"
#include "ObjectRegistry.h"
#include <utility>

namespace wowc {
	namespace ui {
		namespace cef {

			void Module::operator[](RegisterableObject&& object) const {
				object.visit(mRegistry, mContext->GetGlobal());
			}

			Module ObjectRegistry::createModule(const CefRefPtr<CefV8Context>& context) {
				return Module(this, context);
			}

			Function::Function(std::string name) : mName(std::move(name)) {
			}

			bool Function::Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments,
				CefRefPtr<CefV8Value>& retval, CefString& exception) {
				std::shared_ptr<GenericOverload> bestMatchingOverload = nullptr;
				auto currentScore = 0;
				for(auto& overload : mOverloads) {
					const auto score = overload->getScore(arguments);
					if(score <= 0) {
						continue;
					}

					if(bestMatchingOverload == nullptr || score > currentScore) {
						bestMatchingOverload = overload;
						currentScore = score;
						if(score == 100) {
							break;
						}
					}
				}

				if(bestMatchingOverload == nullptr) {
					exception = "No matching overload found";
					return true;
				}

				bestMatchingOverload->onInvocation(arguments, exception, retval);
				return true;
			}

			void Function::visit(ObjectRegistry* registry, CefRefPtr<CefV8Value> obj) {
				auto functionHandler = CefRefPtr<Function>(new Function(mName));
				functionHandler->mOverloads = mOverloads;

				obj->SetValue(mName, CefV8Value::CreateFunction(mName, functionHandler), V8_PROPERTY_ATTRIBUTE_READONLY);
			}

			Module::Module(ObjectRegistry* registry, const CefRefPtr<CefV8Context>& context) : mRegistry(registry), mContext(context) {
			}
		}
	}
}