#pragma once

#include "gx/Texture.h"
#include "gx/GxContext.h"
#include "gx/VertexBuffer.h"
#include <cef_render_handler.h>

namespace wowc {
	namespace ui {
		class PopUpDrawer {
			int32_t mPositionX = 0, mPositionY = 0;
			int32_t mWidth = 0, mHeight = 0;
			uint32_t mImageWidth = 0, mImageHeight = 0;

			float mDpiScale = 1.0f;

			bool mIsVisible = false;

			std::mutex mBufferLock;
			std::vector<uint8_t> mBuffer;

			gx::TexturePtr mTexture;
			void* mSharedTexture = nullptr;
			bool mHasSharedTextureChanged = false;
			gx::GxContextPtr mContext = nullptr;

			gx::VertexBufferPtr mVertexBuffer;
			std::vector<float> mVertices;
			bool mVerticesChanged = false;

			void updateVertices();

		public:
			PopUpDrawer();

			const gx::TexturePtr& getTexture() const { return mTexture; }
			void setTexture(const gx::TexturePtr& texture) { mTexture = texture; }

			const gx::VertexBufferPtr& getVertexBuffer() const { return mVertexBuffer; }

			bool isVisible() const { return mIsVisible; }

			void onResizePopup(const CefRect& rect);
			void onPopupVisibilityChanged(bool isVisible);

			void setDpiScale(const float scale) { mDpiScale = scale; }

			void setContext(const gx::GxContextPtr& context);

			void updateData();

			void onWidowResize(uint32_t width, uint32_t height);

			void onAcceleratedPaint(void* sharedTexture);
		};
	}
}
