#pragma once
#include <cef_base.h>
#include <wrapper/cef_message_router.h>
#include "cdi/ApplicationContext.h"
#include "events/EventManager.h"

namespace wowc {
	namespace ui {
		class UiMessageHandler : public CefMessageRouterBrowserSide::Handler {
			static utils::Logger<UiMessageHandler> mLogger;

			cdi::ApplicationContextPtr mApplicationContext;
			events::EventManagerPtr mEventManager;

		public:
			UiMessageHandler();

			bool OnQuery(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int64 query_id, const CefString& request,
				bool persistent, CefRefPtr<Callback> callback) override;

			void postConstruct(const cdi::ApplicationContextPtr& context);
			void destroy();
		};
	}
}
