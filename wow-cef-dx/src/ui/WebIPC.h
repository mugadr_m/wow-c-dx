#pragma once
#include <winsock2.h>
#include "utils/Logger.h"

namespace wowc {
    namespace ui {
		class WebCore;

        enum class IpcOpcode : uint32_t {
            FOCUS_NODE_CHANGE
        };

        class WebIpc {
            static utils::Logger<WebIpc> mLogger;
            SOCKET mSocket = INVALID_SOCKET;

			bool mIsRunning = false;
			std::thread mIpcThread;

			uint32_t mIpcPort = 0;

			std::weak_ptr<WebCore> mWebCoreWeak;

			void handleIpcThread();

			void onClientConnected(SOCKET client) const;
			void onDataReceived(std::vector<uint8_t>& buffer) const;
			void onPacket(std::vector<uint8_t>& packet) const;

        public:
            void initialize(const std::shared_ptr<WebCore>& core);

			void shutdown();

			uint32_t getIpcPort() const {
				return mIpcPort;
			}
        };
    }
}
