#include "stdafx.h"
#include "BitmapTarget.h"

namespace wowc {
	namespace ui {

	    void BitmapTarget::setContext(const gx::GxContextPtr& context) {
			mContext = context;
	    }

		void BitmapTarget::onUpdate() {
            if(!mHasSharedTextureChanged) {
				return;
            }

			mTexture = mContext->openSharedTexture(mSharedTexture);
			mHasSharedTextureChanged = false;
		}

	    void BitmapTarget::onAcceleratedPaint(void* sharedTexture) {
			mSharedTexture = sharedTexture;
			mHasSharedTextureChanged = true;
	    }
	}
}