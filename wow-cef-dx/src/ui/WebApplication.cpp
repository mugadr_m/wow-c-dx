#include "stdafx.h"
#include "WebApplication.h"
#include "WebCore.h"

namespace wowc {
	namespace ui {
		WebApplication::RenderProcessHandler::
		RenderProcessHandler(WebApplication* webApplication) : mWebApplication(webApplication) {

		}

		void WebApplication::RenderProcessHandler::OnUncaughtException(CefRefPtr<CefBrowser> browser,
		                                                               CefRefPtr<CefFrame> frame,
		                                                               CefRefPtr<CefV8Context> context,
		                                                               CefRefPtr<CefV8Exception> exception,
		                                                               CefRefPtr<CefV8StackTrace> stackTrace) {
			std::cout << "OnException" << std::endl;
		}

		void WebApplication::BrowserProcessHandler::OnContextInitialized() {
			mWebApplication->onContextInitialized();
		}

	    void WebApplication::BrowserProcessHandler::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line) {
			mWebApplication->mWebCore->onBeforeChildProcess(command_line);
	    }

	    void WebApplication::onContextInitialized() const {
			mWebCore->onApplicationReady();
		}

		WebApplication::WebApplication(WebCore* core) : mWebCore(core) {
			mRenderProcess = new RenderProcessHandler(this);
			mBrowserProcess = new BrowserProcessHandler(this);
		}

		void WebApplication::shutdown() {
			mRenderProcess = nullptr;
			mBrowserProcess = nullptr;
		}

	}
}
