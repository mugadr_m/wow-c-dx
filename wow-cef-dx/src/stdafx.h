#pragma once

#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <iomanip>
#include <sstream>

#include <vector>
#include <mutex>
#include <deque>
#include <list>
#include <map>

#include <windowsx.h>
#include <windows.h>
#include <d3d11.h>

#include <memory>
#include <algorithm>

#define SHARED_PTR(T) typedef std::shared_ptr<T> T##Ptr
#define SHARED_FWD(T) class T; SHARED_PTR(T)

#undef min
#undef max
#undef GetFirstChild
#undef GetNextSibling