#include "stdafx.h"
#include "Texture.h"

namespace wowc {
    namespace gx {
        utils::Logger<Texture> Texture::mLogger;
        CComPtr<ID3D11Texture2D> Texture::mDefaultTextureObject;
        CComPtr<ID3D11ShaderResourceView> Texture::mDefaultTextureView;
        CComPtr<ID3D11Texture2D> Texture::mDefaultBlankTextureObject;
        CComPtr<ID3D11ShaderResourceView> Texture::mDefaultBlankTextureView;

        void Texture::createTextureWithView(const uint32_t width, const uint32_t height,
                                            const std::vector<std::vector<uint8_t>>& data,
                                            const std::vector<uint32_t>& rowPitches,
                                            const DXGI_FORMAT format) {
            if (data.size() != rowPitches.size()) {
                mLogger.error("Attempted to pass ", data.size(), " layers but only got ", rowPitches.size(),
                              " row sizes");
                throw std::exception("Invalid texture data");
            }

            mTexture = nullptr;
            mView = nullptr;

            D3D11_TEXTURE2D_DESC textureDesc;
            textureDesc.ArraySize = 1;
            textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
            textureDesc.CPUAccessFlags = 0;
            textureDesc.Format = format;
            textureDesc.Height = height;
            textureDesc.Width = width;
            textureDesc.MipLevels = static_cast<UINT>(data.size());
            textureDesc.MiscFlags = 0;
            textureDesc.SampleDesc.Count = 1;
            textureDesc.SampleDesc.Quality = 0;
            textureDesc.Usage = D3D11_USAGE_DEFAULT;

            std::vector<D3D11_SUBRESOURCE_DATA> layerData(data.size());
            for (auto i = 0u; i < data.size(); ++i) {
                const auto& pixels = data[i];
                const auto& rowPitch = rowPitches[i];
                D3D11_SUBRESOURCE_DATA dataElement;
                dataElement.SysMemPitch = rowPitch;
                dataElement.SysMemSlicePitch = 0;
                dataElement.pSysMem = pixels.data();
                layerData[i] = dataElement;
            }

            auto result = mDevice->CreateTexture2D(&textureDesc, layerData.data(), &mTexture);
            if (FAILED(result)) {
                mLogger.error("Unable to create texture from data");
                throw std::exception("Unable to create texture");
            }

            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
            viewDesc.Format = textureDesc.Format;
            viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
            viewDesc.Texture2D.MipLevels = textureDesc.MipLevels;
            viewDesc.Texture2D.MostDetailedMip = 0;

            result = mDevice->CreateShaderResourceView(mTexture, &viewDesc, &mView);
            if (FAILED(result)) {
                mTexture = nullptr;
                mLogger.error("Error creating shader resource view");
                throw std::exception("Unable to create texture");
            }

            mWidth = width;
            mHeight = height;
            mFormat = format;
            mNumLayers = static_cast<uint32_t>(data.size());
        }

        Texture::Texture(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context, bool blankDefault) :
            mDevice(std::move(device)),
            mContext(std::move(context)) {

            if (!blankDefault) {
                mTexture = mDefaultTextureObject;
                mView = mDefaultTextureView;
            } else {
                mTexture = mDefaultBlankTextureObject;
                mView = mDefaultBlankTextureView;
            }
        }

        Texture::Texture(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context,
                         CComPtr<ID3D11Texture2D> texture, CComPtr<ID3D11ShaderResourceView> view) :
            mDevice(std::move(device)), mContext(std::move(context)), mTexture(std::move(texture)),
            mView(std::move(view)) {
        }

        void Texture::updateArgb8(const uint32_t width, const uint32_t height, const std::vector<uint8_t>& data) {
            if (mIsDefaultTexture || width != mWidth || height != mHeight || mNumLayers != 1 || mFormat !=
                DXGI_FORMAT_B8G8R8A8_UNORM) {
                createTextureWithView(width, height, {data}, {width * 4}, DXGI_FORMAT_B8G8R8A8_UNORM);
            } else {
                mContext->UpdateSubresource(mTexture, 0, nullptr, data.data(), width * 4, 0);
            }
        }

        void Texture::updateArgb8(const uint32_t width, const uint32_t height, const std::vector<uint32_t>& data) {
            std::vector<uint8_t> fullData(width * height * 4);
            memcpy(fullData.data(), data.data(), fullData.size());
            updateArgb8(width, height, fullData);
        }

        void Texture::updateFromMemory(const uint32_t width, const uint32_t height, const TextureDataFormat& format,
                                       const std::vector<uint8_t>& data) {
            DXGI_FORMAT actualFormat;
            uint32_t rowPitch;

            switch (format) {
            case TextureDataFormat::ARGB8:
                actualFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
                rowPitch = width * 4;
                break;

            case TextureDataFormat::R8:
                actualFormat = DXGI_FORMAT_R8_UNORM;
                rowPitch = width;
                break;

            case TextureDataFormat::BC1:
                actualFormat = DXGI_FORMAT_BC1_UNORM;
                rowPitch = ((width + 3) / 4) * 8;
                break;

            case TextureDataFormat::BC2:
                actualFormat = DXGI_FORMAT_BC2_UNORM;
                rowPitch = ((width + 3) / 4) * 16;
                break;

            case TextureDataFormat::BC3:
                actualFormat = DXGI_FORMAT_BC3_UNORM;
                rowPitch = ((width + 3) / 4) * 16;
                break;

            default:
                mLogger.error("Attempted to update texture with unknown memory format: ",
                              static_cast<uint32_t>(format));
                throw std::exception("Invalid texture format");
            }

            if (mIsDefaultTexture || width != mWidth || height != mHeight || mNumLayers != 1 || mFormat != actualFormat
            ) {
                createTextureWithView(width, height, {data}, {rowPitch}, actualFormat);
            } else {
                mContext->UpdateSubresource(mTexture, 0, nullptr, data.data(), rowPitch, 0);
            }
        }

        void Texture::loadFromBlp(uint32_t width, uint32_t height, const TextureDataFormat& format,
                                  const std::vector<uint32_t>& pitches, const std::vector<std::vector<uint8_t>>& data) {
            DXGI_FORMAT actualFormat;

            switch (format) {
            case TextureDataFormat::ARGB8:
                actualFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
                break;

            case TextureDataFormat::R8:
                actualFormat = DXGI_FORMAT_R8_UNORM;
                break;

            case TextureDataFormat::BC1:
                actualFormat = DXGI_FORMAT_BC1_UNORM;
                break;

            case TextureDataFormat::BC2:
                actualFormat = DXGI_FORMAT_BC2_UNORM;
                break;

            case TextureDataFormat::BC3:
                actualFormat = DXGI_FORMAT_BC3_UNORM;
                break;

            default:
                mLogger.error("Attempted to update texture with unknown memory format: ",
                              static_cast<uint32_t>(format));
                throw std::exception("Invalid texture format");
            }

            if (mIsDefaultTexture) {
                createTextureWithView(width, height, data, pitches, actualFormat);
            } else {
                for (auto i = 0u; i < data.size(); ++i) {
                    mContext->UpdateSubresource(mTexture, i, nullptr, data[i].data(), pitches[i], 0);
                }
            }
        }

        void Texture::initializeDefaultTexture(const CComPtr<ID3D11Device>& device) {
            D3D11_TEXTURE2D_DESC textureDesc;
            textureDesc.ArraySize = 1;
            textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
            textureDesc.CPUAccessFlags = 0;
            textureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
            textureDesc.Height = 2;
            textureDesc.Width = 2;
            textureDesc.MipLevels = 1;
            textureDesc.MiscFlags = 0;
            textureDesc.SampleDesc.Count = 1;
            textureDesc.SampleDesc.Quality = 0;
            textureDesc.Usage = D3D11_USAGE_DEFAULT;

            std::vector<uint32_t> defaultColors{
                0xFFFF0000, 0xFF00FF00,
                0xFF0000FF, 0xFFFF7F3F
            };

            D3D11_SUBRESOURCE_DATA defaultData;
            defaultData.SysMemPitch = 2 * sizeof(uint32_t);
            defaultData.pSysMem = defaultColors.data();
            defaultData.SysMemSlicePitch = 0;


            auto result = device->CreateTexture2D(&textureDesc, &defaultData, &mDefaultTextureObject);

            if (FAILED(result)) {
                mLogger.error("Error creating default texture object");
                throw std::exception("Unable to create default texture");
            }

            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
            viewDesc.Format = textureDesc.Format;
            viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
            viewDesc.Texture2D.MipLevels = 1;
            viewDesc.Texture2D.MostDetailedMip = 0;

            result = device->CreateShaderResourceView(mDefaultTextureObject, &viewDesc, &mDefaultTextureView);
            if (FAILED(result)) {
                mLogger.error("Error creating shader resource view for default texture");
                throw std::exception("Unable to create default texture");
            }

            textureDesc.Height = textureDesc.Width = 1;
            defaultColors = {0x00};
            defaultData.SysMemPitch = sizeof(uint32_t);
            defaultData.pSysMem = defaultColors.data();

            result = device->CreateTexture2D(&textureDesc, &defaultData, &mDefaultBlankTextureObject);
            if (FAILED(result)) {
                mLogger.error("Error creating default texture object");
                throw std::exception("Unable to create default texture");
            }

            result = device->CreateShaderResourceView(mDefaultBlankTextureObject, &viewDesc, &mDefaultBlankTextureView);
            if (FAILED(result)) {
                mLogger.error("Error creating shader resource view for default texture");
                throw std::exception("Unable to create default texture");
            }
        }
    }
}
