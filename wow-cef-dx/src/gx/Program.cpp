#include "stdafx.h"
#include "Program.h"
#include <utility>
#include "cdi/ApplicationContext.h"
#include "res/ResourceManager.h"
#include <d3dcompiler.h>

namespace wowc {
	namespace gx {
		utils::Logger<Program> Program::mLogger;
		CComPtr<ID3D11VertexShader> Program::mBoundVertexShader;
		CComPtr<ID3D11PixelShader> Program::mBoundPixelShader;

		void Program::compileVertexShader(const std::string& vertexShader, const std::string& entry) {
			CComPtr<ID3DBlob> dataBlob;
			CComPtr<ID3DBlob> errorBlob;
			auto result = D3DCompile2(vertexShader.data(), vertexShader.size(), nullptr,
			                          nullptr, nullptr, entry.c_str(), "vs_5_0",
#ifdef _DEBUG
			                          D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION
#else
									  D3DCOMPILE_OPTIMIZATION_LEVEL3
#endif
			                          , 0, 0, nullptr, 0, &dataBlob, &errorBlob);

			if(FAILED(result)) {
				const auto ptr = reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
				const std::string error(ptr, ptr + errorBlob->GetBufferSize());
				mLogger.error("Error compiling vertex shader: ", error);
				throw std::exception("Error compiling vertex shader");
			}

			mVertexShader = nullptr;
			result = mDevice->CreateVertexShader(dataBlob->GetBufferPointer(), dataBlob->GetBufferSize(), nullptr, &mVertexShader);
			if(FAILED(result)) {
				mLogger.error("Error creating vertex shader. HRESULT=", result);
				throw std::exception("Error creating vertex shader");
			}

			mVertexShaderBlob = dataBlob;
		}

		void Program::compilePixelShader(const std::string& pixelShader, const std::string& entry) {
			CComPtr<ID3DBlob> dataBlob;
			CComPtr<ID3DBlob> errorBlob;
			auto result = D3DCompile2(pixelShader.data(), pixelShader.size(), nullptr,
				nullptr, nullptr, entry.c_str(), "ps_5_0",
#ifdef SHADER_DEBUG
				D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION
#else
				D3DCOMPILE_OPTIMIZATION_LEVEL3
#endif
				, 0, 0, nullptr, 0, &dataBlob, &errorBlob);

			if (FAILED(result)) {
				const auto ptr = reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
				const std::string error(ptr, ptr + errorBlob->GetBufferSize());
				mLogger.error("Error compiling pixel shader: ", error);
				throw std::exception("Error compiling pixel shader");
			}

			mPixelShader = nullptr;
			result = mDevice->CreatePixelShader(dataBlob->GetBufferPointer(), dataBlob->GetBufferSize(), nullptr, &mPixelShader);
			if (FAILED(result)) {
				mLogger.error("Error creating pixel shader. HRESULT=", result);
				throw std::exception("Error creating pixel shader");
			}
		}

		Program::Program(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context) :
			mDevice(std::move(device)), mContext(std::move(context)) {
		}

		void Program::loadFromResources(const int32_t vertexShaderId, const int32_t pixelShaderId, const std::string& vertexMain, const std::string& pixelMain) {
			const auto resourceManager = cdi::ApplicationContext::instance()->produce<res::ResourceManager>();
			const auto vertexShader = resourceManager->getStringResource(vertexShaderId);
			const auto pixelShader = resourceManager->getStringResource(pixelShaderId);

			compileVertexShader(vertexShader, vertexMain);
			compilePixelShader(pixelShader, pixelMain);
		}

		void Program::bind() const {
			if(mVertexShader != mBoundVertexShader) {
				mBoundVertexShader = mVertexShader;
				mContext->VSSetShader(mVertexShader, nullptr, 0);
			}

			if(mPixelShader != mBoundPixelShader) {
				mBoundPixelShader = mPixelShader;
				mContext->PSSetShader(mPixelShader, nullptr, 0);
			}
		}
	}
}
