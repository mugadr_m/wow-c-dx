#pragma once

#include "Buffer.h"
#include <atlbase.h>
#include <utils/Logger.h>

namespace wowc {
	namespace gx {
		class VertexBuffer : public Buffer {
			static utils::Logger<VertexBuffer> mLogger;

			CComPtr<ID3D11Buffer> mBuffer;
			CComPtr<ID3D11DeviceContext> mContext;
			CComPtr<ID3D11Device> mDevice;

			uint32_t mSize = 0;
			bool mIsInitialized = false;
			bool mIsImmutable = false;

		public:
			VertexBuffer(bool immutable, CComPtr<ID3D11DeviceContext> context, CComPtr<ID3D11Device> device);

			void apply(uint32_t slot, const std::size_t& stride, const std::size_t& startOffset);
			void setData(const void* data, const std::size_t& size) override;
		};

		SHARED_PTR(VertexBuffer);
	}
}