#pragma once

#include "utils/Logger.h"
#include <atlbase.h>
#include <d3d11.h>

namespace wowc {
	namespace gx {
		enum class TextureDataFormat {
			R8,
			ARGB8,
			BC1,
			BC2,
			BC3,
            UNKNOWN
		};

		class Texture {
			static utils::Logger<Texture> mLogger;

			static CComPtr<ID3D11Texture2D> mDefaultTextureObject;
			static CComPtr<ID3D11ShaderResourceView> mDefaultTextureView;

			static CComPtr<ID3D11Texture2D> mDefaultBlankTextureObject;
			static CComPtr<ID3D11ShaderResourceView> mDefaultBlankTextureView;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11DeviceContext> mContext;

			CComPtr<ID3D11Texture2D> mTexture;
			CComPtr<ID3D11ShaderResourceView> mView;
			bool mIsDefaultTexture = true;

			uint32_t mWidth = 0;
			uint32_t mHeight = 0;
			DXGI_FORMAT mFormat = DXGI_FORMAT_UNKNOWN;
			uint32_t mNumLayers = 0;
			std::string mTag;

			void createTextureWithView(uint32_t width, uint32_t height,
				const std::vector<std::vector<uint8_t>>& data,
				const std::vector<uint32_t>& rowPitches,
				DXGI_FORMAT format);

		public:
			Texture(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context, bool blankDefault);
			Texture(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context, CComPtr<ID3D11Texture2D> texture,
				CComPtr<ID3D11ShaderResourceView> view);

			~Texture() = default;

			void updateArgb8(uint32_t width, uint32_t height, const std::vector<uint8_t>& data);
			void updateArgb8(uint32_t width, uint32_t height, const std::vector<uint32_t>& data);
			void updateFromMemory(uint32_t width, uint32_t height, const TextureDataFormat& format,
				const std::vector<uint8_t>& data);

			void setTag(const std::string& tag) { mTag = tag; }
			const std::string& getTag() const { return mTag; }

			void loadFromBlp(uint32_t width, uint32_t height, const TextureDataFormat& format, const std::vector<uint32_t>& pitches, const std::vector<std::vector<uint8_t>>& data);

			static void initializeDefaultTexture(const CComPtr<ID3D11Device>& device);

			CComPtr<ID3D11ShaderResourceView> getView() const { return mView; }

		};

		SHARED_PTR(Texture);
	}
}
