#pragma once

#include <d3d11.h>
#include <atlbase.h>
#ifdef _DEBUG
#include <dxgidebug.h>
#endif

#include "utils/Logger.h"
#include "GpuDispatcher.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace gx {
		SHARED_FWD(Window);
		SHARED_FWD(GxContext);
		SHARED_FWD(VertexBuffer);
		SHARED_FWD(IndexBuffer);
		SHARED_FWD(Texture);
		SHARED_FWD(Mesh);
		SHARED_FWD(Sampler);
		SHARED_FWD(Program);
		SHARED_FWD(BlendState);
		SHARED_FWD(ConstantBuffer);
		SHARED_FWD(DepthState);

		enum class IndexType;

		class GxContext : public std::enable_shared_from_this<GxContext> {
			static utils::Logger<GxContext> mLogger;

			HWND mWindow = nullptr;

#ifdef _DEBUG
			CComPtr<IDXGIInfoQueue> mDebug;
#endif

			CComPtr<ID3D11DeviceContext> mContext = nullptr;
			CComPtr<ID3D11Device> mDevice = nullptr;
			CComPtr<IDXGISwapChain> mSwapChain = nullptr;
			CComPtr<IDXGIFactory1> mFactory = nullptr;
			CComPtr<IDXGIAdapter1> mAdapter = nullptr;
			CComPtr<IDXGIOutput> mOutput = nullptr;

			CComPtr<ID3D11RenderTargetView> mRenderTarget = nullptr;
			CComPtr<ID3D11Texture2D> mDepthTexture = nullptr;
			CComPtr<ID3D11DepthStencilView> mDepthTarget = nullptr;

			CComPtr<ID3D11BlendState> mBlendState;
			CComPtr<ID3D11BlendState> mAlphaBlendState;
			CComPtr<ID3D11DepthStencilState> mDepthState;
			CComPtr<ID3D11RasterizerState> mRasterState;

			DXGI_MODE_DESC mActiveMode{};
			DXGI_SWAP_CHAIN_DESC mSwapChainDesc{};

			uint32_t mWidth = 0;
			uint32_t mHeight = 0;

			GpuDispatcherPtr mGpuDispatcher;

			void initD3D();
			void initMode();
			void initDevice();

			void initBlendState();
			void initDepthState();
			void initRasterizer();

			void printAdapter() const;
			void printOutput() const;

			void fillSwapChainInfo(DXGI_SWAP_CHAIN_DESC& swapChainDesc) const;

			void buildRenderTarget();
			void buildDepthTarget();

			ConstantBufferPtr internalCreateConstantBuffer(uint8_t* dataPointer, std::size_t size) const;

		public:
			void beginFrame() const;
			void endFrame() const;

			void postConstruct(const cdi::ApplicationContextPtr& context);

			VertexBufferPtr createVertexBuffer(bool immutable) const;
			IndexBufferPtr createIndexBuffer(const IndexType& indexType, bool immutable) const;
			TexturePtr createTexture(bool blankDefault = false) const;
			MeshPtr createMesh() const;
			SamplerPtr createSampler() const;
			ProgramPtr createProgram() const;
			BlendStatePtr createBlendState() const;
			ConstantBufferPtr createConstantBuffer(std::size_t size) const;

            template<typename T>
            ConstantBufferPtr createConstantBufferFromObject(T& dataObject) const {
				return internalCreateConstantBuffer(reinterpret_cast<uint8_t*>(&dataObject), sizeof(T));
            }

            template<typename T, std::size_t Size>
            ConstantBufferPtr createConstantBufferFromObject(std::array<T, Size>& dataObject) {
				return internalCreateConstantBuffer(reinterpret_cast<uint8_t*>(dataObject.data()), Size * sizeof(T));
            }

			DepthStatePtr createDepthState() const;

			TexturePtr openSharedTexture(void* sharedTexture);

			const GpuDispatcherPtr& getDispatcher() const { return mGpuDispatcher; }

			void initialize(const WindowPtr& window);
			void onResize(const WindowPtr& window);

			static GxContextPtr createContext();
		};
	}
}
