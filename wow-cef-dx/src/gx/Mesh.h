#pragma once

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Program.h"
#include "utils/Logger.h"
#include "ConstantBuffer.h"
#include <array>
#include "Texture.h"
#include "Sampler.h"
#include "BlendState.h"
#include "ShaderType.h"
#include "DepthState.h"

namespace wowc {
    namespace gx {
        enum class DataType {
            FLOAT,
            BYTE
        };

        class VertexElement {
            static utils::Logger<VertexElement> mLogger;

            std::string mSemantic;
            uint32_t mIndex = 0;
            uint32_t mComponents = 0;
            uint32_t mStream = 0;
            DataType mDataType = DataType::FLOAT;
            bool mIsNormalized = false;
            bool mIsPerInstance = false;

        public:
            VertexElement(std::string semantic,
                          uint32_t index,
                          uint32_t components,
                          DataType dataType = DataType::FLOAT,
                          bool isNormalized = false,
                          uint32_t stream = 0,
                          bool isPerInstance = false);


            std::string getSemantic() const {
                return mSemantic;
            }

            uint32_t getIndex() const {
                return mIndex;
            }

            uint32_t getComponents() const {
                return mComponents;
            }

            DataType getDataType() const {
                return mDataType;
            }

            bool isIsNormalized() const {
                return mIsNormalized;
            }

            DXGI_FORMAT getFormat() const;
            uint32_t getByteSize() const;

            uint32_t getStreamIndex() const {
                return mStream;
            }

            bool isPerInstance() const {
                return mIsPerInstance;
            }
        };

        class Mesh {
            static utils::Logger<Mesh> mLogger;
            static std::array<ID3D11ShaderResourceView*, 64> mResourceViewBuffer;

            CComPtr<ID3D11Device> mDevice;
            CComPtr<ID3D11DeviceContext> mContext;

            CComPtr<ID3D11InputLayout> mInputLayout;

            std::vector<VertexElement> mElements;
            uint32_t mIndexCount = 0;
			uint32_t mInstanceCount = 0;

            uint32_t mStartVertex = 0;
            uint32_t mStartIndex = 0;

            uint32_t mStride = 0;
			uint32_t mInstanceStride = 0;

            IndexBufferPtr mIndexBuffer;
            VertexBufferPtr mVertexBuffer;
            VertexBufferPtr mInstanceBuffer;
            ProgramPtr mProgram;
            BlendStatePtr mBlendState;
            DepthStatePtr mDepthState;

        public:
            Mesh(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);

            void addElement(const VertexElement& element) { mElements.push_back(element); }

            void addElement(const std::string& semantic, const uint32_t index, const uint32_t components,
                            const DataType dataType = DataType::FLOAT, const bool normalize = false,
                            uint32_t stream = 0, bool perInstance = false) {
                addElement(VertexElement(semantic, index, components, dataType, normalize, stream, perInstance));
            }

            void setProgram(const ProgramPtr& program);

            void setIndexCount(const uint32_t indexCount) {
                mIndexCount = indexCount;
            }

            void setStartVertex(const uint32_t startVertex) {
                mStartVertex = startVertex;
            }

            void setStartIndex(const uint32_t startIndex) {
                mStartIndex = startIndex;
            }

            void setInstanceCount(const uint32_t instances) {
				mInstanceCount = instances;
            }

            void setVertexBuffer(const VertexBufferPtr& vertexBuffer) { mVertexBuffer = vertexBuffer; }
			void setInstanceBuffer(const VertexBufferPtr& instanceBuffer) { mInstanceBuffer = instanceBuffer; }
            void setIndexBuffer(const IndexBufferPtr& indexBuffer) { mIndexBuffer = indexBuffer; }

            void finalize();
            void beginFrame() const;
            void updateBuffers() const;
            void draw() const;

            void setConstantBuffer(ShaderType type, uint32_t slot, const ConstantBufferPtr& constantBuffer) const;
            void setTextures(ShaderType type, uint32_t startSlot, const std::vector<TexturePtr>& textures) const;
            void setTexture(ShaderType type, uint32_t slot, const TexturePtr& texture) const;
            void setSampler(ShaderType type, uint32_t slot, const SamplerPtr& sampler) const;

            void setBlendState(const BlendStatePtr& blendState);
            void setDepthState(const DepthStatePtr& depthState);
        };

        SHARED_PTR(Mesh);
    }
}
