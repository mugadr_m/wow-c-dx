#pragma once

namespace wowc {
	namespace gx {
		class Buffer {
		public:
			virtual ~Buffer() = default;
			virtual void setData(const void* data, const std::size_t& numBytes) = 0;

			template<typename T>
			void updateData(const T& value) {
				setData(&value, sizeof(T));
			}

			template<typename T, uint32_t Size>
			void updateData(const T(& values)[Size]) {
				setData(values, Size * sizeof(T));
			}

			template<typename T>
			void updateData(const std::vector<T>& data) {
				setData(data.data(), data.size() * sizeof(T));
			}
		};
	}
}