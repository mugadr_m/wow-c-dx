#pragma once

#include <atlbase.h>
#include <d3d11.h>
#include "utils/Logger.h"

namespace wowc {
	namespace gx {
		class BlendState {
			static utils::Logger<BlendState> mLogger;

			D3D11_BLEND_DESC mDescription{};
			CComPtr<ID3D11BlendState> mBlendState;
			bool mIsDirty = false;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11DeviceContext> mContext;

			void createBlendState();

		public:
			BlendState(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);

			void setBlendEnabled(bool enabled);
			void setSourceBlend(D3D11_BLEND blendMode);
			void setSourceBlendAlpha(D3D11_BLEND blendMode);
			void setDestinationBlend(D3D11_BLEND blendMode);
			void setDestinationBlendAlpha(D3D11_BLEND blendMode);

			void setAlphaBlendDefaults();

			void apply();
		};

		SHARED_PTR(BlendState);
	}
}
