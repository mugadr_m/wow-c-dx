#include "stdafx.h"
#include "VertexBuffer.h"

namespace wowc {
	namespace gx {
		utils::Logger<VertexBuffer> VertexBuffer::mLogger;

		VertexBuffer::VertexBuffer(
			bool immutable,
			CComPtr<ID3D11DeviceContext> context,
			CComPtr<ID3D11Device> device
		) : mBuffer(nullptr), mContext(std::move(context)), mDevice(std::move(device)), mIsImmutable(immutable) {

		}

		void VertexBuffer::apply(uint32_t slot, const std::size_t& stride, const std::size_t& startOffset) {
			if (!mIsInitialized) {
				mLogger.error("Attempted to bind uninitialized vertex buffer");
				throw std::exception("Cannot bind uninitialized vertex buffer");
			}

			static UINT strides[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT]{};
			static UINT offsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT]{};

			strides[0] = static_cast<UINT>(stride);
			offsets[0] = static_cast<UINT>(startOffset);

			const auto ptr = mBuffer.p;

			mContext->IASetVertexBuffers(slot, 1, &ptr, strides, offsets);
		}

		void VertexBuffer::setData(const void* data, const std::size_t& size) {
			if (mIsImmutable && mIsInitialized) {
				mLogger.error("Attempted to update immutable vertex buffer that has already been updated before");
				throw std::exception("Cannot update immutable buffer after it has been created");
			}

			if (size == 0 || data == nullptr) {
				mLogger.warn("Attempted to update vertex buffer with NULL or 0 bytes");
				return;
			}


			if (size != mSize || !mIsInitialized) {
				mBuffer = nullptr;

				mSize = static_cast<uint32_t>(size);

				auto desc = D3D11_BUFFER_DESC{};
				desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
				desc.ByteWidth = mSize;
				desc.CPUAccessFlags = 0;
				desc.StructureByteStride = 0;
				desc.Usage = mIsImmutable ? D3D11_USAGE_IMMUTABLE : D3D11_USAGE_DEFAULT;

				D3D11_SUBRESOURCE_DATA initialData;
				initialData.pSysMem = data;
				initialData.SysMemPitch = static_cast<uint32_t>(size);
				initialData.SysMemSlicePitch = 0;
				const auto result = mDevice->CreateBuffer(&desc, &initialData, &mBuffer);
				if (FAILED(result)) {
					mLogger.error("Failed to create vertex buffer for size ", size);
					throw std::exception("Buffer creation failed");
				}
			}
			else {
				D3D11_BOX dataBox;
				dataBox.back = 1;
				dataBox.front = 0;
				dataBox.top = 0;
				dataBox.bottom = 1;
				dataBox.left = 0;
				dataBox.right = static_cast<uint32_t>(size);
				mContext->UpdateSubresource(mBuffer, 0, &dataBox, data, static_cast<uint32_t>(size), 0);
			}

			mIsInitialized = true;
		}
	}
}