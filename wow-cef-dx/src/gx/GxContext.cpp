#include "stdafx.h"
#include "GxContext.h"
#include "utils/String.h"
#include <dxgi.h>
#include "Texture.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Window.h"
#include "Mesh.h"
#include "Sampler.h"
#include "BlendState.h"
#include "ConstantBuffer.h"
#include "DepthState.h"
#include "ContextCreatedEvent.h"

namespace wowc {
	namespace gx {
		namespace detail {
#ifdef _DEBUG
			const GUID DXGI_DEBUG_ALL
				= { 0xe48ae283, 0xda80, 0x490b,{ 0x87, 0xe6, 0x43, 0xe9, 0xa9, 0xcf, 0xda, 0x8 } };
			typedef HRESULT(WINAPI *tDXGIGETDEBUGINTERFACE)(REFIID riid, void** ppDebug);
#endif
		}

		utils::Logger<GxContext> GxContext::mLogger;

		void GxContext::initD3D() {
			auto result = CreateDXGIFactory1(IID_PPV_ARGS(&mFactory));
			if (FAILED(result)) {
				mLogger.error("Error calling CreateDXGIFactory1");
				throw std::runtime_error("Unable to CreateDXGIFactory1");
			}

			result = mFactory->EnumAdapters1(0, &mAdapter);
			if (FAILED(result)) {
				mLogger.error("Error getting primary adapter");;
				throw std::runtime_error("Unable to EnumAdapter1");
			}

			result = mAdapter->EnumOutputs(0, &mOutput);
			if (FAILED(result)) {
				mLogger.error("Error getting primary output");
				throw std::runtime_error("Unable to EnumOutputs");
			}

			printAdapter();
			printOutput();

			initMode();
			initDevice();
		}

		void GxContext::initMode() {
			mActiveMode.RefreshRate.Numerator = 60;
			mActiveMode.RefreshRate.Denominator = 1;
			mActiveMode.Height = mHeight;
			mActiveMode.Width = mWidth;
			mActiveMode.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			mActiveMode.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			mActiveMode.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

			const auto result = mOutput->FindClosestMatchingMode(&mActiveMode, &mActiveMode, nullptr);
			if (FAILED(result)) {
				mLogger.error("Unable to find a matching display mode for ", mWidth, "x", mHeight, "@60HZ");
				throw std::runtime_error("Unable to find matching mode");
			}

			mActiveMode.Width = mWidth;
			mActiveMode.Height = mHeight;
		}

		void GxContext::initDevice() {
#ifdef _DEBUG
			const auto dll = LoadLibrary("dxgidebug.dll");
			const auto dxgiGetDebugInterface = reinterpret_cast<detail::tDXGIGETDEBUGINTERFACE>(GetProcAddress(
				dll, "DXGIGetDebugInterface"));
			dxgiGetDebugInterface(IID_PPV_ARGS(&mDebug));
#endif

			fillSwapChainInfo(mSwapChainDesc);

			D3D_FEATURE_LEVEL featureLevels[] = {
				D3D_FEATURE_LEVEL_11_1,
				D3D_FEATURE_LEVEL_11_0
			};

			D3D_FEATURE_LEVEL selectedLevel{};

			uint32_t flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#ifdef SHADER_DEBUG
			flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
			const auto result = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, flags, featureLevels,
				sizeof(featureLevels) / sizeof(D3D_FEATURE_LEVEL),
				D3D11_SDK_VERSION, &mSwapChainDesc, &mSwapChain, &mDevice,
				&selectedLevel, &mContext);

			if (FAILED(result)) {
#ifdef _DEBUG
				const auto numMessages = mDebug->GetNumStoredMessages(detail::DXGI_DEBUG_ALL);
				if (numMessages > 0) {
					std::vector<char> messageBuffer(0x1000);
					auto infoQueueMessage = reinterpret_cast<DXGI_INFO_QUEUE_MESSAGE*>(messageBuffer.data());
					infoQueueMessage->DescriptionByteLength = 0x1000 - sizeof(DXGI_INFO_QUEUE_MESSAGE);
					mDebug->GetMessage(detail::DXGI_DEBUG_ALL, 0, infoQueueMessage, &infoQueueMessage->DescriptionByteLength);
					std::stringstream strm;
					strm << "Error creating device: " << infoQueueMessage->pDescription;
					mLogger.error(strm.str());
					throw std::exception(strm.str().c_str());
				}
#endif
				mLogger.error("Unable to create Device and Swap Chain");
				throw std::runtime_error("Unknown exception creating D3D11 device");
			}

			buildRenderTarget();
			buildDepthTarget();

			D3D11_VIEWPORT viewPort;
			viewPort.Height = static_cast<float>(mHeight);
			viewPort.Width = static_cast<float>(mWidth);
			viewPort.MinDepth = 0.0f;
			viewPort.MaxDepth = 1.0f;
			viewPort.TopLeftX = 0.0f;
			viewPort.TopLeftY = 0.0f;
			mContext->RSSetViewports(1, &viewPort);

			const auto renderTargetPtr = mRenderTarget.p;
			mContext->OMSetRenderTargets(1, &renderTargetPtr, mDepthTarget);

			initBlendState();
			initDepthState();
			initRasterizer();

			Texture::initializeDefaultTexture(mDevice);

			ContextCreatedEvent event(shared_from_this());
			cdi::ApplicationContext::instance()->publishEvent(event);
		}

		void GxContext::initBlendState() {
			D3D11_BLEND_DESC blendDesc{};
			blendDesc.RenderTarget[0].BlendEnable = FALSE;
			blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;

			const auto result = mDevice->CreateBlendState(&blendDesc, &mBlendState);
			if(FAILED(result)) {
				mLogger.error("Unable to create blend state. HRESULT=", result);
				throw std::runtime_error("Unable to create blend state");
			}

			static float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
			mContext->OMSetBlendState(mBlendState, blendFactors, 0xFFFFFFFF);
		}

		void GxContext::initDepthState() {
			D3D11_DEPTH_STENCIL_DESC desc{};
			desc.DepthEnable = FALSE;
			desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
			desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
			desc.StencilEnable = FALSE;

			const auto result = mDevice->CreateDepthStencilState(&desc, &mDepthState);
			if (FAILED(result)) {
				mLogger.error("Unable to create depth state. HRESULT=", result);
				throw std::runtime_error("Unable to create depth state");
			}

			mContext->OMSetDepthStencilState(mDepthState, 1);
		}

		void GxContext::initRasterizer() {
			D3D11_RASTERIZER_DESC desc;
			desc.AntialiasedLineEnable = FALSE;
			desc.CullMode = D3D11_CULL_NONE;
			desc.DepthBias = 0;
			desc.DepthBiasClamp = 0.0f;
			desc.DepthClipEnable = TRUE;
			desc.FillMode = D3D11_FILL_SOLID;
			desc.FrontCounterClockwise = TRUE;
			desc.MultisampleEnable = FALSE;
			desc.ScissorEnable = FALSE;
			desc.SlopeScaledDepthBias = 0.0f;

			const auto result = mDevice->CreateRasterizerState(&desc, &mRasterState);
			if (FAILED(result)) {
				mLogger.error("Unable to create raster state. HRESULT=", result);
				throw std::runtime_error("Unable to create raster state");
			}

			mContext->RSSetState(mRasterState);
		}

		void GxContext::printAdapter() const {
			DXGI_ADAPTER_DESC1 description{};
			const auto result = mAdapter->GetDesc1(&description);
			if (FAILED(result)) {
				mLogger.error("Unable to get monitor description");
				return;
			}

			mLogger.info("Adapter name:  ", utils::String::toMultibyte(description.Description));
			mLogger.info("Video Memory:  ", description.DedicatedVideoMemory / (1024.0f * 1024.0f), "MB");
			mLogger.info("System Memory: ", description.DedicatedSystemMemory / (1024.0f * 1024.0f), "MB");
		}

		void GxContext::printOutput() const {
			DXGI_OUTPUT_DESC description{};
			const auto result = mOutput->GetDesc(&description);
			if (FAILED(result)) {
				mLogger.error("Unable to get output description");
				return;
			}

			mLogger.info("Device name:   ", utils::String::toMultibyte(description.DeviceName));
		}

		void GxContext::fillSwapChainInfo(DXGI_SWAP_CHAIN_DESC& swapChainDesc) const {
			swapChainDesc.BufferCount = 1;
			swapChainDesc.BufferDesc = mActiveMode;
			swapChainDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapChainDesc.Flags = 0;
			swapChainDesc.OutputWindow = mWindow;
			swapChainDesc.SampleDesc = DXGI_SAMPLE_DESC{ 1, 0 };
			swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			swapChainDesc.Windowed = TRUE;
		}

		void GxContext::buildRenderTarget() {
			CComPtr<ID3D11Texture2D> backBuffer{};
			auto result = mSwapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));
			if (FAILED(result)) {
				mLogger.error("Unable to fetch back buffer from swap chain");
				throw std::runtime_error("Error loading render target");
			}

			mRenderTarget = nullptr;

			D3D11_RENDER_TARGET_VIEW_DESC description;
			description.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			description.Format = mActiveMode.Format;
			description.Texture2D.MipSlice = 0;

			result = mDevice->CreateRenderTargetView(backBuffer, &description, &mRenderTarget);
			if (FAILED(result)) {
				mLogger.error("Unable to build render target view for back buffer");
				throw std::runtime_error("Error loading render target");
			}
		}

		void GxContext::buildDepthTarget() {
			mDepthTarget = nullptr;
			mDepthTexture = nullptr;

			D3D11_TEXTURE2D_DESC textureDescription;
			textureDescription.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			textureDescription.ArraySize = 1;
			textureDescription.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			textureDescription.CPUAccessFlags = 0;
			textureDescription.Height = mHeight;
			textureDescription.Width = mWidth;
			textureDescription.MipLevels = 1;
			textureDescription.MiscFlags = 0;
			textureDescription.SampleDesc = mSwapChainDesc.SampleDesc;
			textureDescription.Usage = D3D11_USAGE_DEFAULT;

			auto result = mDevice->CreateTexture2D(&textureDescription, nullptr, &mDepthTexture);
			if (FAILED(result)) {
				mLogger.error("Error creating depth texture");
				throw std::runtime_error("Unable to create depth target");
			}

			D3D11_DEPTH_STENCIL_VIEW_DESC viewDesc{};
			viewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			viewDesc.Format = textureDescription.Format;

			result = mDevice->CreateDepthStencilView(mDepthTexture, &viewDesc, &mDepthTarget);
			if (FAILED(result)) {
				mLogger.error("Error creating view on depth texture");
				throw std::runtime_error("Unable to create depth target");
			}
		}

	    ConstantBufferPtr GxContext::internalCreateConstantBuffer(uint8_t* dataPointer, std::size_t size) const {
			return std::make_shared<ConstantBuffer>(size, dataPointer, mDevice, mContext);
	    }

	    void GxContext::initialize(const WindowPtr& window) {
			mWindow = window->getHandle();
			window->getSize(mWidth, mHeight);
			initD3D();
		}

		void GxContext::onResize(const WindowPtr& window) {
			window->getSize(mWidth, mHeight);
			mContext->OMSetRenderTargets(0, nullptr, nullptr);

			mRenderTarget = nullptr;
			mDepthTarget = nullptr;

			const auto result = mSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
			if(FAILED(result)) {
				mLogger.error("Unable to resize swap chain. HRESULT=", result);
				throw std::runtime_error("Resize failed");
			}

			buildRenderTarget();
			buildDepthTarget();

			D3D11_VIEWPORT viewPort;
			viewPort.Height = static_cast<float>(mHeight);
			viewPort.Width = static_cast<float>(mWidth);
			viewPort.MinDepth = 0.0f;
			viewPort.MaxDepth = 1.0f;
			viewPort.TopLeftX = 0.0f;
			viewPort.TopLeftY = 0.0f;
			mContext->RSSetViewports(1, &viewPort);

			const auto renderTargetPtr = mRenderTarget.p;
			mContext->OMSetRenderTargets(1, &renderTargetPtr, mDepthTarget);
		}

		void GxContext::beginFrame() const {
			mGpuDispatcher->doPerFrameWork();

			static const float CLEAR_COLOR[] = { 0.0f, 0.0f, 0.0f, 1.0f };

			mContext->ClearDepthStencilView(mDepthTarget, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
			mContext->ClearRenderTargetView(mRenderTarget, CLEAR_COLOR);
		}

		void GxContext::endFrame() const {
			mSwapChain->Present(1, 0);
		}

	    void GxContext::postConstruct(const cdi::ApplicationContextPtr& context) {
			mGpuDispatcher = std::make_shared<GpuDispatcher>();
			context->registerSingleton(mGpuDispatcher);
	    }

	    VertexBufferPtr GxContext::createVertexBuffer(bool immutable) const {
			return std::make_shared<VertexBuffer>(immutable, mContext, mDevice);
		}

		IndexBufferPtr GxContext::createIndexBuffer(const IndexType& indexType, bool immutable) const {
			return std::make_shared<IndexBuffer>(immutable, indexType, mContext, mDevice);
		}

		TexturePtr GxContext::createTexture(bool blankDefault) const {
			return std::make_shared<Texture>(mDevice, mContext, blankDefault);
		}

		MeshPtr GxContext::createMesh() const {
			return std::make_shared<Mesh>(mDevice, mContext);
		}

		SamplerPtr GxContext::createSampler() const {
			return std::make_shared<Sampler>(mDevice);
		}

		ProgramPtr GxContext::createProgram() const {
			return std::make_shared<Program>(mDevice, mContext);
		}

		BlendStatePtr GxContext::createBlendState() const {
			return std::make_shared<BlendState>(mDevice, mContext);
		}

	    ConstantBufferPtr GxContext::createConstantBuffer(const std::size_t size) const {
			return std::make_shared<ConstantBuffer>(static_cast<uint32_t>(size), mDevice, mContext);
	    }

	    DepthStatePtr GxContext::createDepthState() const {
			return std::make_shared<DepthState>(mDevice, mContext);
	    }

	    TexturePtr GxContext::openSharedTexture(void* sharedTexture) {
			CComPtr<ID3D11Texture2D> texture;
			auto result = mDevice->OpenSharedResource(reinterpret_cast<HANDLE>(sharedTexture), IID_PPV_ARGS(&texture));
            if(FAILED(result)) {
				throw std::runtime_error("Unable to open shared texture");
            }

			D3D11_TEXTURE2D_DESC textureDesc{};
			texture->GetDesc(&textureDesc);

			CComPtr<ID3D11ShaderResourceView> resourceView;
			D3D11_SHADER_RESOURCE_VIEW_DESC desc;
			desc.Format = textureDesc.Format;
			desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			desc.Texture2D.MipLevels = textureDesc.MipLevels;
			desc.Texture2D.MostDetailedMip = 0;
		    result = mDevice->CreateShaderResourceView(texture, &desc, &resourceView);
			if (FAILED(result)) {
				throw std::runtime_error("Unable to open shared texture view");
			}

			return std::make_shared<gx::Texture>(mDevice, mContext, texture, resourceView);
	    }

	    GxContextPtr GxContext::createContext() {
			return std::make_shared<GxContext>();
		}
	}
}
