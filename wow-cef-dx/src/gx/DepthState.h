#pragma once

#include <atlbase.h>
#include <d3d11.h>

namespace wowc {
    namespace gx {
        class DepthState {
			CComPtr<ID3D11DepthStencilState> mDepthState;
			D3D11_DEPTH_STENCIL_DESC mDescription{};

			bool mIsChanged = true;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11DeviceContext> mContext;

        public:
			DepthState(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);
			void setDepthEnabled(bool enabled);

			void apply();
        };

		SHARED_PTR(DepthState);
    }
}