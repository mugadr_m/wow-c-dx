#pragma once

#include <atlbase.h>
#include <d3d11.h>
#include "utils/Logger.h"

namespace wowc {
	namespace gx {
		class Program {
			static utils::Logger<Program> mLogger;

			static CComPtr<ID3D11VertexShader> mBoundVertexShader;
			static CComPtr<ID3D11PixelShader> mBoundPixelShader;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11DeviceContext> mContext;
			CComPtr<ID3D11VertexShader> mVertexShader;
			CComPtr<ID3D11PixelShader> mPixelShader;
			CComPtr<ID3DBlob> mVertexShaderBlob;

			void compileVertexShader(const std::string& vertexShader, const std::string& entry);
			void compilePixelShader(const std::string& pixelShader, const std::string& entry);

		public:
			Program(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);

			void loadFromResources(int32_t vertexShaderId, int32_t pixelShaderId, const std::string& vertexMain = "main", const std::string& pixelMain = "main");

			void bind() const;

			CComPtr<ID3DBlob> getVertexShaderBlob() const { return mVertexShaderBlob; }
		};

		SHARED_PTR(Program);
	}
}
