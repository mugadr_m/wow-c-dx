#include "stdafx.h"
#include "Window.h"
#include <exception>
#include "GxContext.h"
#include "ui/WebCore.h"
#include "res/resource.h"
#include "scene/WorldFrame.h"
#include <ShellScalingApi.h>

namespace wowc {
	namespace gx {
		namespace detail {
			LRESULT WINAPI WndProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
				auto window = reinterpret_cast<Window*>(GetProp(hWindow, "WND_PROC_INSTANCE"));

				if(uMsg == WM_CREATE) {
					const auto createParams = reinterpret_cast<CREATESTRUCT*>(lParam);
					window = reinterpret_cast<Window*>(createParams->lpCreateParams);
					SetProp(hWindow, "WND_PROC_INSTANCE", reinterpret_cast<HANDLE>(window));
				}

				if(window != nullptr) {
					return window->onMessage(hWindow, uMsg, wParam, lParam);
				}
			
				return DefWindowProc(hWindow, uMsg, wParam, lParam);
			}
		}

		void Window::onResize() {
			auto gxContext = mApplicationContext->produce<GxContext>();
			auto webCore = mApplicationContext->produce<ui::WebCore>();
		    const auto worldFrame = mApplicationContext->produce<scene::WorldFrame>();
			gxContext->onResize(shared_from_this());
			webCore->onResize();

			worldFrame->onResize(shared_from_this());
		}

		void Window::handleMouseMessage(const uint32_t messageType, const WPARAM wParam, const LPARAM lParam) const {
			const auto core = mApplicationContext->produce<ui::WebCore>();

			switch(messageType) {
			case WM_MOUSEMOVE: {
				core->handleMouseMove(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam)));
				break;

			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
				core->handleMouseDown(messageType, lParam);
				break;

			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_MBUTTONUP:
				core->handleMouseUp(messageType, lParam);
				break;

			case WM_MOUSEWHEEL:
				core->handleMouseWheel(wParam, lParam);
				break;

			case WM_MOUSELEAVE:
				core->handleMouseLeave();
				break;

			default:
				break;
			}
			}
		}

		void Window::handleKeyboard(const uint32_t messageType, const WPARAM wParam, const LPARAM lParam) const {
			const auto core = mApplicationContext->produce<ui::WebCore>();

			switch(messageType) {
			case WM_CHAR:
				core->handleCharPress(wParam);
				break;

			case WM_KEYDOWN:
				core->handleKeyDown(wParam, lParam);
				break;

			case WM_KEYUP:
				core->handleKeyUp(wParam, lParam);
				break;

			default:
				break;
			}
		}

		Window::Window() : mHandle(nullptr) {
			
		}

		void Window::postConstruct(const cdi::ApplicationContextPtr& context) {
			mApplicationContext = context;
		}

		void Window::initialize() {
			WNDCLASSEX wce = { };
			wce.cbSize = sizeof(WNDCLASSEX);
			wce.hCursor = LoadCursor(nullptr, IDC_ARROW);
			wce.hIcon = LoadIcon(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_WINDOW_ICON));
			wce.hIconSm = LoadIcon(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_WINDOW_ICON));
			wce.hInstance = GetModuleHandle(nullptr);
			wce.hbrBackground = GetSysColorBrush(COLOR_WINDOW);
			wce.lpszClassName = "MainWindowClass";
			wce.lpszMenuName = nullptr;
			wce.style = CS_HREDRAW | CS_VREDRAW;
			wce.lpfnWndProc = detail::WndProc;

			const auto result = RegisterClassEx(&wce);
			if(result == 0) {
				throw std::exception("Unable to register class");
			}

			mHandle = CreateWindow("MainWindowClass", "MainWindow", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 2000, 1200, nullptr, nullptr, wce.hInstance, this);
			if(mHandle == nullptr) {
				throw std::exception("Unable to create window");
			}

			mIsRunning = true;
		}

		bool Window::runFrame() {
			MSG msg = { };
			while(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) != 0) {
				if(msg.message == WM_QUIT) {
					mIsRunning = false;
				}

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			return mIsRunning;
		}

		void Window::showWindow() const {
			ShowWindow(mHandle, SW_SHOW);
		}

		void Window::hideWindow() const {
			ShowWindow(mHandle, SW_HIDE);
		}

	    void Window::screenToClient(math::Vector2& screenPos) const {
			POINT pt{ static_cast<int32_t>(screenPos.x()), static_cast<int32_t>(screenPos.y()) };
			ScreenToClient(mHandle, &pt);
			screenPos.x(pt.x);
			screenPos.y(pt.y);
	    }

	    void Window::getSize(uint32_t& width, uint32_t& height) const {
			RECT rc{};
			GetClientRect(mHandle, &rc);
			width = static_cast<uint32_t>(rc.right - rc.left);
			height = static_cast<uint32_t>(rc.bottom - rc.top);
			width = std::max(width, 1u);
			height = std::max(height, 1u);
		}

		void Window::updateCursor(HCURSOR cursor) const {
			SetClassLongPtr(mHandle, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>(cursor));
		}

		LRESULT Window::onMessage(HWND hWindow, const UINT uMsg, const WPARAM wParam, const LPARAM lParam) {
			switch(uMsg) {
			case WM_CLOSE: {
				PostQuitMessage(0);
				break;
			}

			case WM_SIZE: {
				onResize();
				break;
			}

			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			case WM_MOUSEMOVE:
			case WM_MOUSELEAVE:
			case WM_MOUSEWHEEL:
				handleMouseMessage(uMsg, wParam, lParam);
				break;

			case WM_CHAR:
			case WM_KEYDOWN:
			case WM_KEYUP:
				handleKeyboard(uMsg, wParam, lParam);
				break;

			case WM_MOVE:
				mApplicationContext->produce<ui::WebCore>()->handleWindowMove();
				break;

			default:
				break;

			}

			return DefWindowProc(hWindow, uMsg, wParam, lParam);
		}

	    float Window::getDpi() const {
#if BOOST_PLAT_WINDOWS_SDK_VERSION >= BOOST_WINAPI_WINDOWS_SDK_10_0
			return GetDpiForWindow(mHandle) / 96.0f;
#elif VER_PRODUCTBUILD >= BOOST_WINAPI_WINDOWS_SDK_8_0
			const auto monitor = MonitorFromWindow(mHandle, MONITOR_DEFAULTTONEAREST);
			UINT dpiX = 96, dpiY = 96;
			GetDpiForMonitor(monitor, MDT_EFFECTIVE_DPI, &dpiX, &dpiY);
			return dpiX / 96.0f;
#else
			return 1.0f;
#endif
	    }
	}
}
