#include "stdafx.h"
#include "ConstantBuffer.h"

namespace wowc {
    namespace gx {
        utils::Logger<ConstantBuffer> ConstantBuffer::mLogger;

        void ConstantBuffer::initializeBuffer(uint32_t size) {
            D3D11_BUFFER_DESC bufferDesc;
            bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
            bufferDesc.ByteWidth = size;
            bufferDesc.CPUAccessFlags = 0;
            bufferDesc.MiscFlags = 0;
            bufferDesc.StructureByteStride = 0;
            bufferDesc.Usage = D3D11_USAGE_DEFAULT;

            mBuffer = nullptr;
            const auto result = mDevice->CreateBuffer(&bufferDesc, nullptr, &mBuffer);
            if (FAILED(result)) {
                mLogger.error("Error creating constant buffer. HRESULT=", result);
                throw std::exception("Error creating buffer");
            }

            mBufferData.resize(size);
        }

        ConstantBuffer::ConstantBuffer(const uint32_t size, CComPtr<ID3D11Device> device,
                                       CComPtr<ID3D11DeviceContext> context)
            : mDevice(std::move(device)), mContext(std::move(context)) {
            initializeBuffer(size);
        }

        ConstantBuffer::ConstantBuffer(const std::size_t& size, uint8_t* dataPointer, CComPtr<ID3D11Device> device,
                                       CComPtr<ID3D11DeviceContext> context) : mDevice(std::move(device)),
                                                                               mContext(std::move(context)),
                                                                               mDataPointer(dataPointer),
                                                                               mDataSize(size) {
			initializeBuffer(static_cast<uint32_t>(size));
			mBufferData.clear();
        }

        void ConstantBuffer::updateData(const void* data, const uint32_t offset, const uint32_t size) {
            if (mDataPointer != nullptr) {
                if (offset + size > mDataSize) {
                    mLogger.error("Attempted to update buffer outside of its range");
                    throw std::exception("Attempted to write past the end of the buffer");
                }

                memcpy(mDataPointer + offset, data, size);
            } else {
                if (offset + size > mBufferData.size()) {
                    mLogger.error("Attempted to update buffer outside of its range");
                    throw std::exception("Attempted to write past the end of the buffer");
                }

                memcpy(mBufferData.data() + offset, data, size);
            }
            mHasChanged = true;
        }

        void ConstantBuffer::commit(bool forceUpdate) {
            if (mHasChanged || forceUpdate) {
                mHasChanged = false;
                if (mDataPointer != nullptr) {
                    mContext->UpdateSubresource(mBuffer, 0, nullptr, mDataPointer, static_cast<UINT>(mDataSize), 0);
                } else {
                    mContext->UpdateSubresource(mBuffer, 0, nullptr, mBufferData.data(),
                                                static_cast<UINT>(mBufferData.size()), 0);
                }
            }
        }

        void ConstantBuffer::bind(ShaderType type, uint32_t slot) const {
            ID3D11Buffer* bufferPtr = mBuffer;
            switch (type) {
            case ShaderType::PIXEL:
                mContext->PSSetConstantBuffers(slot, 1, &bufferPtr);
                break;

            case ShaderType::VERTEX:
                mContext->VSSetConstantBuffers(slot, 1, &bufferPtr);
                break;
            }

        }
    }
}
