#include "stdafx.h"
#include "BlendState.h"

namespace wowc {
	namespace gx {
		utils::Logger<BlendState> BlendState::mLogger;

		void BlendState::createBlendState() {
			mIsDirty = false;
			mBlendState = nullptr; 
			const auto result = mDevice->CreateBlendState(&mDescription, &mBlendState);
			if (FAILED(result)) {
				mLogger.error("Error creating blend state. HRESULT=", result);
				throw std::exception("Error creating blend state");
			}
		}

		BlendState::BlendState(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context) :
			mDevice(std::move(device)), mContext(std::move(context)) {

			mDescription.AlphaToCoverageEnable = FALSE;
			mDescription.IndependentBlendEnable = FALSE;
			auto& rt0 = mDescription.RenderTarget[0];
			rt0.BlendOp = D3D11_BLEND_OP_ADD;
			rt0.BlendOpAlpha = D3D11_BLEND_OP_ADD;
			rt0.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			rt0.SrcBlend = D3D11_BLEND_SRC_ALPHA;
			rt0.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			rt0.SrcBlendAlpha = D3D11_BLEND_ONE;
			rt0.DestBlendAlpha = D3D11_BLEND_ZERO;
			rt0.BlendEnable = TRUE;
			createBlendState();
		}

		void BlendState::setBlendEnabled(const bool enabled) {
			mDescription.RenderTarget[0].BlendEnable = enabled ? TRUE : FALSE;
			mIsDirty = true;
		}

		void BlendState::setSourceBlend(const D3D11_BLEND blendMode) {
			mDescription.RenderTarget[0].SrcBlend = blendMode;
			mIsDirty = true;
		}

		void BlendState::setSourceBlendAlpha(const D3D11_BLEND blendMode) {
			mDescription.RenderTarget[0].SrcBlendAlpha = blendMode;
			mIsDirty = true;
		}

		void BlendState::setDestinationBlend(const D3D11_BLEND blendMode) {
			mDescription.RenderTarget[0].DestBlend = blendMode;
			mIsDirty = true;
		}

		void BlendState::setDestinationBlendAlpha(D3D11_BLEND blendMode) {
			mDescription.RenderTarget[0].DestBlendAlpha = blendMode;
			mIsDirty = true;
		}

	    void BlendState::setAlphaBlendDefaults() {
			auto& rt0 = mDescription.RenderTarget[0];
			rt0.BlendOp = D3D11_BLEND_OP_ADD;
			rt0.BlendOpAlpha = D3D11_BLEND_OP_ADD;
			rt0.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
			rt0.SrcBlend = D3D11_BLEND_SRC_ALPHA;
			rt0.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			rt0.SrcBlendAlpha = D3D11_BLEND_ONE;
			rt0.DestBlendAlpha = D3D11_BLEND_ZERO;
			rt0.BlendEnable = TRUE;
			mIsDirty = true;
	    }

	    void BlendState::apply() {
			static FLOAT blendFactors[4]{};
			if(mIsDirty) {
				createBlendState();
			}

			mContext->OMSetBlendState(mBlendState, blendFactors, 0xFFFFFFFF);
		}
	}
}
