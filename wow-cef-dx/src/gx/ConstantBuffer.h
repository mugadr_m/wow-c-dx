#pragma once

#include <atlbase.h>
#include <d3d11.h>
#include "utils/Logger.h"
#include "ShaderType.h"

namespace wowc {
	namespace gx {
	    class ConstantBuffer {
			static utils::Logger<ConstantBuffer> mLogger;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11DeviceContext> mContext;

			CComPtr<ID3D11Buffer> mBuffer;
			std::vector<uint8_t> mBufferData;
			bool mHasChanged = false;
			uint8_t* mDataPointer = nullptr;
			std::size_t mDataSize = 0;

			void initializeBuffer(uint32_t size);

		public:
		    /**
			 * \brief Sets up all the parameters of this constant buffer that are later
			 *        used for creation and updating.
			 *        
			 * \param size     Maximum size of this buffer. Cannot be changed later
			 * \param device   Device used to create this buffer
			 * \param context  Context used to manipulate this buffer
			 */
			ConstantBuffer(uint32_t size, CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);

			ConstantBuffer(const std::size_t& size, uint8_t* dataPointer, CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context);

			/**
			 * \brief Updates the CPU memory of this buffer at the given offset with
			 *		  as many bytes as specified
			 * \param data		Data to read from
			 * \param offset	Offset inside the constant buffer to start writing to
			 * \param size		Number of bytes to write
			 */
			void updateData(const void* data, uint32_t offset, uint32_t size);

			/**
			 * \brief	Has to be run on the GPU thread. Will update the GPU
			 *			memory of the buffer to contain the same content as the CPU
			 *			memory if it has changed since the last call to commit.
			 */
			void commit(bool forceUpdate = false);

			/**
			 * \brief	Gets a pointer to the d3d buffer represented by this instance
			 * \return	Instance of ID3D11Buffer for this instance
			 */
			CComPtr<ID3D11Buffer> getBuffer() const { return mBuffer; }

			void bind(ShaderType shaderType, uint32_t slot) const;
		};

		SHARED_PTR(ConstantBuffer);
	}
}
