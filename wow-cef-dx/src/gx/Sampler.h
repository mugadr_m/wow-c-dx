#pragma once

#include <atlbase.h>
#include <d3d11.h>
#include "utils/Logger.h"

namespace wowc {
	namespace gx {
		enum class TextureAddressMode {
			WRAP = D3D11_TEXTURE_ADDRESS_WRAP,
			MIRROR = D3D11_TEXTURE_ADDRESS_MIRROR,
			CLAMP = D3D11_TEXTURE_ADDRESS_CLAMP,
			BORDER = D3D11_TEXTURE_ADDRESS_BORDER,
			MIRROR_ONCE = D3D11_TEXTURE_ADDRESS_MIRROR_ONCE
		};

		class Sampler {
			static utils::Logger<Sampler> mLogger;

			CComPtr<ID3D11Device> mDevice;
			CComPtr<ID3D11SamplerState> mSamplerState;
			D3D11_SAMPLER_DESC mSamplerDesc;
			bool mHasChanged = false;

		public:
			explicit Sampler(CComPtr<ID3D11Device> device);

			void setTextureAddressU(const TextureAddressMode& mode);
			void setTextureAddressV(const TextureAddressMode& mode);
			void setTextureFilter(uint32_t filter);
			void setAnisotropicFilter(int32_t level = -1);

			void onUpdate();

			CComPtr<ID3D11SamplerState> getState();
		};

		SHARED_PTR(Sampler);
	}
}
