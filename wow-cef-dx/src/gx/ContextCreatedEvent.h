#pragma once
#include <utility>
#include "GxContext.h"

namespace wowc {
    namespace gx {
        class ContextCreatedEvent {
			GxContextPtr mContext;

        public:
            explicit ContextCreatedEvent(GxContextPtr context) : mContext(std::move(context)) {
                
            }

			const GxContextPtr& getContext() const { return mContext; }
        };
    }
}
