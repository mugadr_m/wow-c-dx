#pragma once

#include "Buffer.h"
#include "utils/Logger.h"
#include <atlbase.h>
#include <d3d11.h>

namespace wowc {
	namespace gx {
		enum class IndexType {
			UINT16,
			UINT32
		};

		class IndexBuffer : public Buffer {
			static utils::Logger<IndexBuffer> mLogger;
		protected:
			IndexType mIndexType = IndexType::UINT16;

			CComPtr<ID3D11Buffer> mBuffer;
			CComPtr<ID3D11DeviceContext> mContext;
			CComPtr<ID3D11Device> mDevice;

			uint32_t mSize = 0;
			bool mIsInitialized = false;
			DXGI_FORMAT mIndexFormat = DXGI_FORMAT_UNKNOWN;
			bool mIsImmutable;

		public:
			IndexBuffer(bool immutable, const IndexType& indexType, CComPtr<ID3D11DeviceContext> context, CComPtr<ID3D11Device> device);

			void apply(const std::size_t& startOffset);
			void setData(const void* data, const std::size_t& size) override;
		};

		SHARED_PTR(IndexBuffer);
	}
}
