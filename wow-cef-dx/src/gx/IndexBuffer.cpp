#include "stdafx.h"
#include "IndexBuffer.h"

namespace wowc {
	namespace gx {
		utils::Logger<IndexBuffer> IndexBuffer::mLogger;

		IndexBuffer::IndexBuffer(
			bool immutable,
			const IndexType& indexType,
			CComPtr<ID3D11DeviceContext> context,
			CComPtr<ID3D11Device> device)
			: mContext(std::move(context)), mDevice(std::move(device)), mIsImmutable(immutable) {

			switch (indexType) {
			case IndexType::UINT16: {
				mIndexFormat = DXGI_FORMAT_R16_UINT;
				break;
			}

			case IndexType::UINT32: {
				mIndexFormat = DXGI_FORMAT_R32_UINT;
				break;
			}

			default: {
				mLogger.error("Attempted to bind unknown index format: ", static_cast<uint32_t>(indexType));
				throw std::exception("Invalid index format");
			}

			}
		}

		void IndexBuffer::apply(const std::size_t& startOffset) {
			if (!mIsInitialized) {
				mLogger.error("Attempted to bind index buffer before it was initialized");
				return;
			}

			mContext->IASetIndexBuffer(mBuffer, mIndexFormat, static_cast<UINT>(startOffset));
		}

		void IndexBuffer::setData(const void* data, const std::size_t& size) {
			if (mIsImmutable && mIsInitialized) {
				mLogger.error("Attempted to update immutable index buffer that has already been updated before");
				throw std::exception("Cannot update immutable buffer after it has been created");
			}

			if (size == 0 || data == nullptr) {
				mLogger.warn("Attempted to update index buffer with NULL or 0 bytes");
				return;
			}


			if (size != mSize || !mIsInitialized) {
				mBuffer.Release();

				mSize = static_cast<uint32_t>(size);

				auto desc = D3D11_BUFFER_DESC{};
				desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
				desc.ByteWidth = mSize;
				desc.CPUAccessFlags = 0;
				desc.StructureByteStride = 0;
				desc.Usage = mIsImmutable ? D3D11_USAGE_IMMUTABLE : D3D11_USAGE_DEFAULT;

				D3D11_SUBRESOURCE_DATA initialData;
				initialData.pSysMem = data;
				initialData.SysMemPitch = static_cast<uint32_t>(size);
				initialData.SysMemSlicePitch = 0;
				const auto result = mDevice->CreateBuffer(&desc, &initialData, &mBuffer);
				if (FAILED(result)) {
					mLogger.error("Failed to create vertex buffer for size ", size);
					throw std::exception("Buffer creation failed");
				}
			} else {
				D3D11_BOX dataBox;
				dataBox.back = 1;
				dataBox.front = 0;
				dataBox.top = 0;
				dataBox.bottom = 1;
				dataBox.left = 0;
				dataBox.right = static_cast<uint32_t>(size);
				mContext->UpdateSubresource(mBuffer, 0, &dataBox, data, static_cast<uint32_t>(size), 0);
			}

			mIsInitialized = true;
		}
	}
}