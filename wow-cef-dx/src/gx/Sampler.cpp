#include "stdafx.h"
#include "Sampler.h"

namespace wowc {
	namespace gx {
		utils::Logger<Sampler> Sampler::mLogger;

		Sampler::Sampler(CComPtr<ID3D11Device> device) :
			mDevice(std::move(device)), mSamplerDesc{} {

			mSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
			mSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
			mSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
			mSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
			mSamplerDesc.MaxAnisotropy = 0;
			mSamplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			mSamplerDesc.MipLODBias = 0.0f;
			mSamplerDesc.MaxLOD = std::numeric_limits<float>::max();
			mSamplerDesc.MinLOD = std::numeric_limits<float>::min();

			mHasChanged = true;
		}

		void Sampler::setTextureAddressU(const TextureAddressMode& mode) {
			if (mSamplerDesc.AddressU != static_cast<D3D11_TEXTURE_ADDRESS_MODE>(mode)) {
				mHasChanged = true;
				mSamplerDesc.AddressU = static_cast<D3D11_TEXTURE_ADDRESS_MODE>(mode);
			}
		}

		void Sampler::setTextureAddressV(const TextureAddressMode& mode) {
			if (mSamplerDesc.AddressV != static_cast<D3D11_TEXTURE_ADDRESS_MODE>(mode)) {
				mHasChanged = true;
				mSamplerDesc.AddressV = static_cast<D3D11_TEXTURE_ADDRESS_MODE>(mode);
			}
		}

		void Sampler::setTextureFilter(uint32_t filter) {
			if (mSamplerDesc.Filter != static_cast<D3D11_FILTER>(filter)) {
				mHasChanged = true;
				mSamplerDesc.Filter = static_cast<D3D11_FILTER>(filter);
			}
		}

	    void Sampler::setAnisotropicFilter(int32_t level) {
			mSamplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
			mSamplerDesc.MaxAnisotropy = level < 0 ? D3D11_REQ_MAXANISOTROPY : level;
			mHasChanged = true;
	    }

	    void Sampler::onUpdate() {
			if(!mHasChanged) {
				return;
			}

			mHasChanged = false;
			mSamplerState = nullptr;
			const auto res = mDevice->CreateSamplerState(&mSamplerDesc, &mSamplerState);
			if(FAILED(res)) {
				mLogger.error("Unable to create sampler state. HRESULT=", res);
				throw std::exception("Unable to create sampler state");
			}
		}

		CComPtr<ID3D11SamplerState> Sampler::getState() {
			onUpdate();
			return mSamplerState;
		}
	}
}
