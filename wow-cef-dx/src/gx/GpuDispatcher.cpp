#include "stdafx.h"
#include "GpuDispatcher.h"

namespace wowc {
    namespace gx {

        void GpuDispatcher::doPerFrameWork() {
            if (mWorkItems.empty()) {
                return;
            }

			const auto now = std::chrono::steady_clock::now();
			uint64_t diffTime;
            {
				std::lock_guard<std::mutex> l(mWorkLock);
				do {
					const auto workItem = mWorkItems.front();
					mWorkItems.pop_front();
					workItem();
					const auto nextFrame = std::chrono::steady_clock::now();
					diffTime = std::chrono::duration_cast<std::chrono::milliseconds>(nextFrame - now).count();
				} while (diffTime < 10 && !mWorkItems.empty());
            }
        }

        void GpuDispatcher::pushWork(const std::function<void()>& work) {
			std::lock_guard<std::mutex> l(mWorkLock);
			mWorkItems.emplace_back(work);
        }
    }
}
