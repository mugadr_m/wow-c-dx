#pragma once
#include "cdi/ApplicationContext.h"
#include "math/Vector2.h"

namespace wowc {
	namespace gx {
		class Window : public std::enable_shared_from_this<Window> {
			HWND mHandle;
			bool mIsRunning = false;
			cdi::ApplicationContextPtr mApplicationContext;

			void onResize();

			void handleMouseMessage(uint32_t messageType, WPARAM wParam, LPARAM lParam) const;
			void handleKeyboard(uint32_t messageType, WPARAM wParam, LPARAM lParam) const;

		public:
			explicit Window();

			void postConstruct(const cdi::ApplicationContextPtr& context);

			void initialize();

			bool runFrame();

			void showWindow() const;
			void hideWindow() const;

			void screenToClient(math::Vector2& screenPos) const;

			void getSize(uint32_t& width, uint32_t& height) const;

			void updateCursor(HCURSOR cursor) const;

			HWND getHandle() const { return mHandle; }

			LRESULT onMessage(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);

			float getDpi() const;
		};

		SHARED_PTR(Window);
	}
}
