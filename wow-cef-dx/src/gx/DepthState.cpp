#include "stdafx.h"
#include "DepthState.h"

namespace wowc {
    namespace gx {

        DepthState::DepthState(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context) :
            mDevice(std::move(device)),
            mContext(std::move(context)) {

            mDescription.DepthEnable = FALSE;
            mDescription.DepthFunc = D3D11_COMPARISON_ALWAYS;
            mDescription.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
            mDescription.StencilEnable = FALSE;
        }

        void DepthState::setDepthEnabled(bool enabled) {
            if (enabled) {
				mDescription.DepthEnable = TRUE;
				mDescription.DepthFunc = D3D11_COMPARISON_LESS;
				mDescription.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
            } else {
				mDescription.DepthEnable = FALSE;
				mDescription.DepthFunc = D3D11_COMPARISON_ALWAYS;
				mDescription.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
            }

			mIsChanged = true;
        }

        void DepthState::apply() {
            if (mIsChanged) {
                mIsChanged = false;
                mDepthState = nullptr;

                const auto result = mDevice->CreateDepthStencilState(&mDescription, &mDepthState);
                if (FAILED(result)) {
                    throw std::exception("Error creating depth state");
                }
            }

            mContext->OMSetDepthStencilState(mDepthState, 0);
        }
    }
}
