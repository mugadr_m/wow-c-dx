#include "stdafx.h"
#include "Mesh.h"
#include <utility>

namespace wowc {
    namespace gx {
        utils::Logger<Mesh> Mesh::mLogger;
        utils::Logger<VertexElement> VertexElement::mLogger;

        std::array<ID3D11ShaderResourceView*, 64> Mesh::mResourceViewBuffer;

        VertexElement::VertexElement(std::string semantic,
                                     const uint32_t index,
                                     const uint32_t components,
                                     const DataType dataType,
                                     const bool isNormalized,
                                     uint32_t stream,
                                     bool isInstanced)
            : mSemantic(std::move(semantic)),
              mIndex(index),
              mComponents(components),
              mStream(stream),
              mDataType(dataType),
              mIsNormalized(isNormalized),
              mIsPerInstance(isInstanced) {
        }

        DXGI_FORMAT VertexElement::getFormat() const {
            switch (mDataType) {
            case DataType::FLOAT: {
                switch (mComponents) {
                case 1:
                    return DXGI_FORMAT_R32_FLOAT;
                case 2:
                    return DXGI_FORMAT_R32G32_FLOAT;
                case 3:
                    return DXGI_FORMAT_R32G32B32_FLOAT;
                case 4:
                    return DXGI_FORMAT_R32G32B32A32_FLOAT;
                default:
                    mLogger.error("Invalid component count: ", mComponents, ". Must be 1, 2, 3 or 4");
                    throw std::exception("Invalid component count");
                }
            }
            case DataType::BYTE: {
                switch (mComponents) {
                case 1:
                    return mIsNormalized ? DXGI_FORMAT_R8_UNORM : DXGI_FORMAT_R8_UINT;
                case 2:
                    return mIsNormalized ? DXGI_FORMAT_R8G8_UNORM : DXGI_FORMAT_R8G8_UINT;
                case 4:
                    return mIsNormalized ? DXGI_FORMAT_R8G8B8A8_UNORM : DXGI_FORMAT_R8G8B8A8_UINT;
                default:
                    mLogger.error("Invalid component count: ", mComponents, ". Must be 1, 2 or 4");
                    throw std::exception("Invalid component count");
                }
            }
            default: {
                mLogger.error("Invalid data type: ", static_cast<int>(mDataType));
                throw std::exception("Invalid data type");
            }
            }
        }

        uint32_t VertexElement::getByteSize() const {
            if (mDataType == DataType::BYTE) {
                return mComponents;
            }

            return mComponents * sizeof(float);
        }

        Mesh::Mesh(CComPtr<ID3D11Device> device, CComPtr<ID3D11DeviceContext> context)
            : mDevice(std::move(device)), mContext(std::move(context)) {
        }

        void Mesh::setProgram(const ProgramPtr& program) {
            mProgram = program;
        }

        void Mesh::finalize() {
            if (mProgram == nullptr) {
                mLogger.error("Attempted to build mesh without a target program");
                throw std::exception("No program bound");
            }

            std::vector<D3D11_INPUT_ELEMENT_DESC> elementDescription{};
            for (auto& element : mElements) {
                D3D11_INPUT_ELEMENT_DESC desc{};
                desc.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
                desc.InputSlot = element.getStreamIndex();
                desc.SemanticName = _strdup(element.getSemantic().c_str());
                desc.SemanticIndex = element.getIndex();
				desc.InputSlotClass = element.isPerInstance() ? D3D11_INPUT_PER_INSTANCE_DATA : D3D11_INPUT_PER_VERTEX_DATA;
                desc.InstanceDataStepRate = element.isPerInstance() ? 1 : 0;
                desc.Format = element.getFormat();
                elementDescription.push_back(desc);
                if(element.isPerInstance()) {
					mInstanceStride += element.getByteSize();
				} else {
					mStride += element.getByteSize();
				}
            }

            const auto vsBlob = mProgram->getVertexShaderBlob();
            const auto result = mDevice->CreateInputLayout(elementDescription.data(),
                                                           static_cast<UINT>(elementDescription.size()),
                                                           vsBlob->GetBufferPointer(),
                                                           static_cast<SIZE_T>(vsBlob->GetBufferSize()), &mInputLayout);

            if (FAILED(result)) {
                mLogger.error("Error creating input layout. HRESULT=", result);
                throw std::exception("Failed to create input layout");
            }
        }

        void Mesh::beginFrame() const {
            mProgram->bind();

			updateBuffers();

            mContext->IASetInputLayout(mInputLayout);
            mContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

            if (mBlendState != nullptr) {
                mBlendState->apply();
            }

            if (mDepthState != nullptr) {
                mDepthState->apply();
            }
        }

        void Mesh::updateBuffers() const {
            mVertexBuffer->apply(0, mStride, 0);
            mIndexBuffer->apply(0);

            if(mInstanceBuffer != nullptr) {
				mInstanceBuffer->apply(1, mInstanceStride, 0);
            }
        }

        void Mesh::draw() const {
            if(mInstanceBuffer != nullptr && mInstanceCount > 0) {
				mContext->DrawIndexedInstanced(mIndexCount, mInstanceCount, mStartIndex, mStartVertex, 0);
			} else {
				mContext->DrawIndexed(mIndexCount, mStartIndex, mStartVertex);
			}
        }

        void Mesh::setConstantBuffer(const ShaderType type, const uint32_t slot,
                                     const ConstantBufferPtr& constantBuffer) const {
            const auto buffer = constantBuffer->getBuffer();
            auto ptr = buffer.p;
            switch (type) {
            case ShaderType::PIXEL:
                mContext->PSSetConstantBuffers(slot, 1, &ptr);
                break;

            case ShaderType::VERTEX:
                mContext->VSSetConstantBuffers(slot, 1, &ptr);
                break;
            }
        }

        void Mesh::setTextures(const ShaderType type, const uint32_t startSlot,
                               const std::vector<TexturePtr>& textures) const {
            auto index = 0;
            for (auto& tex : textures) {
                mResourceViewBuffer[index++] = tex->getView();
            }

            switch (type) {
            case ShaderType::PIXEL:
                mContext->PSSetShaderResources(startSlot, static_cast<UINT>(textures.size()),
                                               mResourceViewBuffer.data());
                break;

            case ShaderType::VERTEX:
                mContext->VSSetShaderResources(startSlot, static_cast<UINT>(textures.size()),
                                               mResourceViewBuffer.data());
                break;
            }
        }

        void Mesh::setTexture(const ShaderType type, const uint32_t slot, const TexturePtr& texture) const {
            mResourceViewBuffer[0] = texture->getView();

            switch (type) {
            case ShaderType::PIXEL:
                mContext->PSSetShaderResources(slot, 1, mResourceViewBuffer.data());
                break;

            case ShaderType::VERTEX:
                mContext->VSSetShaderResources(slot, 1, mResourceViewBuffer.data());
                break;
            }
        }

        void Mesh::setSampler(const ShaderType type, const uint32_t slot, const SamplerPtr& sampler) const {
            const auto state = sampler->getState();
            auto ptr = state.p;
            switch (type) {
            case ShaderType::PIXEL:
                mContext->PSSetSamplers(slot, 1, &ptr);
                break;

            case ShaderType::VERTEX:
                mContext->VSSetSamplers(slot, 1, &ptr);
                break;
            }
        }

        void Mesh::setBlendState(const BlendStatePtr& blendState) {
            mBlendState = blendState;
        }

        void Mesh::setDepthState(const DepthStatePtr& depthState) {
            mDepthState = depthState;
        }
    }
}
