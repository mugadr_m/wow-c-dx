#pragma once

namespace wowc {
    namespace gx {
        class GpuDispatcher {
			std::deque<std::function<void()>> mWorkItems;
			std::mutex mWorkLock;

        public:
			void doPerFrameWork();
			void pushWork(const std::function<void()>& work);
        };

		SHARED_PTR(GpuDispatcher);
    }
}
