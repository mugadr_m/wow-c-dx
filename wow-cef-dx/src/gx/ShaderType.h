#pragma once

namespace wowc {
    namespace gx {
        enum class ShaderType {
            PIXEL,
            VERTEX
        };
    }
}