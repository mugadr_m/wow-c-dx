#include "stdafx.h"
#include "IoModule.h"
#include "files/cdn/CdnFileManager.h"
#include "cef/IoWorkerThreadPool.h"
#include "ui/events/EventManager.h"
#include "files/DataLoadInitEvent.h"
#include "files/DataLoadProgressEvent.h"
#include "files/storage/DefinitionManager.h"
#include "files/storage/StorageManager.h"
#include "files/DataInitCompleteEvent.h"
#include "cef/LoadingScreenThreadPool.h"
#include "files/minimap/MinimapLeafletStorage.h"
#include "input/Mouse.h"
#include "files/map/MapTileLoader.h"
#include "input/Keyboard.h"
#include "files/blp/TextureManager.h"
#include "files/sky/SkyManager.h"
#include "files/JsFileHandler.h"
#include "files/casc/LocalCascHandler.h"
#include "files/m2/M2FileFactory.h"

namespace wowc {
    namespace io {
        void IoModule::initialize() {
            auto ctx = cdi::ApplicationContext::instance();

			ctx->registerSingleton<input::Mouse>();
			ctx->registerSingleton<input::Keyboard>();

            ctx->registerSingleton<files::cdn::CdnFileManager>();
			ctx->registerSingleton<files::casc::LocalCascHandler>();

            ctx->registerSingleton<files::storage::DefinitionManager>();
            ctx->registerSingleton<files::storage::StorageManager>();
            ctx->registerSingleton<files::minimap::MinimapLeafletStorage>();
			ctx->registerSingleton<files::map::MapTileLoader>();
			ctx->registerSingleton<files::blp::TextureManager>();
			ctx->registerSingleton<files::sky::SkyManager>();
			ctx->registerSingleton<files::m2::M2FileFactory>();

			ctx->registerSingleton<cef::LoadingScreenThreadPool>();
			ctx->registerSingleton<cef::IoWorkerThreadPool>();

			ctx->registerSingleton<files::JsFileHandler>();
        }

        void IoModule::initUiEvents() {
            auto ctx = cdi::ApplicationContext::instance();
            auto eventManager = ctx->produce<ui::events::EventManager>();

            ctx->produce<files::cdn::CdnFileManager>()->attachUiEvents(ctx);
			ctx->produce<files::casc::LocalCascHandler>()->attachUiEvents(ctx);
            ctx->produce<files::storage::StorageManager>()->attachUiEvents(ctx);

			files::minimap::MinimapLeafletStorage::attachUiEvents(ctx);
			files::JsFileHandler::attachUiEvents(ctx);

            eventManager->registerEvent<files::DataLoadInitEvent>();
            eventManager->registerEvent<files::DataLoadProgressEvent>();
            eventManager->registerEvent<files::DataInitCompleteEvent>();
        }
    }
}
