#include "stdafx.h"
#include "BinaryReader.h"

namespace wowc {
	namespace io {

		void BinaryReader::read(void* data, std::size_t numBytes) {
			if(mPosition + numBytes > mData.size()) {
				throw std::exception("attempted to read past the end of the stream");
			}

			memcpy(data, mData.data() + mPosition, numBytes);
			mPosition += numBytes;
		}
	}
}