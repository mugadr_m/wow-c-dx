#pragma once

#include "io/BinaryReader.h"
#include "../DefinitionManager.h"
#include "io/files/storage/IStorageFile.h"
#include "io/BitStream.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				namespace wdbx {
					enum class HeaderFlags : uint16_t {
						NONE = 0,
						OFFSET_MAP = 1,
						SECOND_INDEX = 2,
						INDEX_MAP = 4,
						UNK1 = 8,
						COMPRESSED = 16
					};

					inline std::underlying_type_t<HeaderFlags> operator &(const HeaderFlags& f1, const HeaderFlags& f2) {
						using T = std::underlying_type_t<HeaderFlags>;
						return static_cast<T>(f1) & static_cast<T>(f2);
					}

					enum class CompressionType : uint32_t {
						NONE,
						IMMEDIATE,
						SPARSE,
						PALETTE,
						PALLET_ARRAY,
						SIGNED_IMMEDIATE
					};

					class FieldStructureEntry {
						int16_t mBitCount;
						uint16_t mOffset;

					public:
						FieldStructureEntry(int16_t numBits, uint16_t offset) : mBitCount(numBits), mOffset(offset) {

						}

						FieldStructureEntry() : mBitCount(0), mOffset(0) {
							
						}


						uint16_t getBitCount() const {
							return mBitCount;
						}

						void setBitCount(const uint16_t bitCount) {
							mBitCount = bitCount;
						}

						uint16_t getOffset() const {
							return mOffset;
						}

						void setOffset(const uint16_t offset) {
							mOffset = offset;
						}
					};

					class ColumnStructureEntry {
						uint16_t mRecordOffset;
						uint16_t mSize;
						uint32_t mAdditionalDataSize;
						CompressionType mCompression;
						uint32_t mBitOffset;
						uint32_t mBitWidth;
						uint32_t mCardinality;

						std::vector<std::vector<uint8_t>> mPalletValues;
						std::map<uint32_t, std::vector<uint8_t>> mSparseValues;
						uint32_t mArraySize = 1;

					public:

						ColumnStructureEntry(uint16_t recordOffset, uint16_t size, uint32_t additionalDataSize,
						                     CompressionType compression, uint32_t bitOffset, uint32_t bitWidth, uint32_t cardinality)
							: mRecordOffset(recordOffset),
							  mSize(size),
							  mAdditionalDataSize(additionalDataSize),
							  mCompression(compression),
							  mBitOffset(bitOffset),
							  mBitWidth(bitWidth),
							  mCardinality(cardinality) {
						}

						ColumnStructureEntry() :
							mRecordOffset(0),
							mSize(0),
							mAdditionalDataSize(0),
							mCompression(CompressionType::NONE),
							mBitOffset(0),
							mBitWidth(0),
							mCardinality(0) {

						}

						uint16_t getRecordOffset() const {
							return mRecordOffset;
						}

						void setRecordOffset(const uint16_t recordOffset) {
							mRecordOffset = recordOffset;
						}

						uint16_t getSize() const {
							return mSize;
						}

						void setSize(const uint16_t size) {
							mSize = size;
						}

						uint32_t getAdditionalDataSize() const {
							return mAdditionalDataSize;
						}

						void setAdditionalDataSize(const uint32_t additionalDataSize) {
							mAdditionalDataSize = additionalDataSize;
						}

						CompressionType getCompression() const {
							return mCompression;
						}

						void setCompression(const CompressionType compression) {
							mCompression = compression;
						}

						uint32_t getBitOffset() const {
							return mBitOffset;
						}

						void setBitOffset(const uint32_t bitOffset) {
							mBitOffset = bitOffset;
						}

						uint32_t getBitWidth() const {
							return mBitWidth;
						}

						void setBitWidth(const uint32_t bitWidth) {
							mBitWidth = bitWidth;
						}

						uint32_t getCardinality() const {
							return mCardinality;
						}

						void setCardinality(const uint32_t cardinality) {
							mCardinality = cardinality;
						}

						const std::vector<std::vector<uint8_t>>& getPalletValues() const {
							return mPalletValues;
						}

						std::vector<std::vector<uint8_t>>& getPalletValues() {
							return mPalletValues;
						}

						void setPalletValues(const std::vector<std::vector<uint8_t>>& palletValues) {
							mPalletValues = palletValues;
						}

						const std::map<uint32_t, std::vector<uint8_t>>& getSparseValues() const {
							return mSparseValues;
						}

						std::map<uint32_t, std::vector<uint8_t>>& getSparseValues() {
							return mSparseValues;
						}

						void setSparseValues(const std::map<uint32_t, std::vector<uint8_t>>& sparseValues) {
							mSparseValues = sparseValues;
						}

						uint32_t getArraySize() const {
							return mArraySize;
						}

						void setArraySize(const uint32_t arraySize) {
							mArraySize = arraySize;
						}
					};

					class Wdc2RowNew : public IStorageRow {
						const std::map<uint32_t, std::string>& mStringBlock;
						BitStream mReader;
						const DefinitionManager::TableDefinition& mDefinition;
						const std::vector<ColumnStructureEntry>& mColumnMeta;
						const std::vector<FieldStructureEntry>& mFieldMeta;
						uint32_t mReferenceData;

						uint32_t mRecordOffset = 0;

						uint32_t mRowId = 0;
						bool mWithIndexTable = false;

					    static uint32_t convertUint32Value(uint32_t value, const FieldDefinition& definition);

						uint32_t getUint32Value(uint32_t id, uint32_t fieldIndex, const FieldDefinition& definition);
						float getFloatValue(uint32_t id, uint32_t fieldIndex);
						std::string getStringValue(uint32_t id, uint32_t fieldIndex);

						std::vector<float> readFloatArray(uint32_t id, uint32_t fieldIndex);
						std::vector<uint32_t> readUint32Array(uint32_t id, uint32_t fieldIndex, const FieldDefinition& definition);
						std::vector<std::string> readStringArray(uint32_t id, uint32_t fieldIndex);

					public:
						Wdc2RowNew(int32_t rowId, uint32_t idField, uint32_t recordOffset, const std::map<uint32_t, std::string>& stringBlock, BitStream stream, const DefinitionManager::TableDefinition& definition,
							const std::vector<ColumnStructureEntry>& columnMeta, const std::vector<FieldStructureEntry>& fieldMeta, uint32_t referenceData);

						int32_t getInt32(uint32_t fieldIndex) override;
						std::string getString(uint32_t fieldIndex) override;

						std::vector<float> getFloatArray(uint32_t fieldIndex) override;
						std::vector<uint32_t> getUint32Array(uint32_t fieldIndex) override;
						std::vector<std::string> getStringArray(uint32_t fieldIndex) override;

						float getFloat(uint32_t fieldIndex) override;

						uint32_t getId() const { return mRowId; }
					};

					class Wdc2File : public IStorageFile {
						uint32_t mRecordCount = 0;
						uint32_t mFieldCount = 0;
						uint32_t mRecordSize = 0;
						uint32_t mStringBlockSize = 0;
						uint32_t mCopyTableSize = 0;
						uint32_t mOffsetTableOffset = 0;
						uint32_t mRelationShipDataSize = 0;
						uint32_t mMinId = 0;
						uint32_t mMaxId = 0;
						uint32_t mIndexColumn = 0;
						uint32_t mRecordDataOffset = 0;

						HeaderFlags mFlags = HeaderFlags::NONE;

						std::vector<FieldStructureEntry> mFieldMetaData;
						std::vector<ColumnStructureEntry> mColumnMetaData;

						std::vector<std::pair<uint32_t, uint16_t>> mOffsetMapList;
						std::map<uint32_t, std::pair<uint32_t, uint32_t>> mDuplicateMap;
						std::map<uint32_t, std::vector<uint32_t>> mCopyMap;
						std::map<uint32_t, uint32_t> mRelationShipData;

						std::vector<uint32_t> mRecordOffsets;
						std::vector<uint32_t> mIndexList;

						std::vector<uint32_t> mColumnSizes;
						DefinitionManager::TableDefinition mDefinition;

						std::map<uint32_t, std::shared_ptr<Wdc2RowNew>> mRecordMapNew;
						std::vector<uint32_t> mIdList;
						std::map<uint32_t, std::string> mStringTable;

						std::vector<uint32_t> mColumnOffsets;
						std::vector<uint32_t> mFullOffsets;

						bool hasOffsetTable() const {
							return (mFlags & HeaderFlags::OFFSET_MAP) != 0;
						}

						bool hasIndexTable() const {
							return (mFlags & HeaderFlags::INDEX_MAP) != 0;
						}

						void calculateColumnSizes();
						void readHeader(BinaryReader& file);

						void loadFieldMetaData(BinaryReader& file);
						void loadColumnMetaData(BinaryReader& file);

						void loadOffsetTable(BinaryReader& file);
						void loadIndexTable(BinaryReader& file);
						void loadCopyMap(BinaryReader& file);
						void loadRelationShipData(BinaryReader& file);
						void loadRecordData(BinaryReader& file, const std::vector<uint8_t>& recordData);
						void loadStringTable(BinaryReader& file);

						template <typename T>
						static void concat(std::vector<uint8_t>& current, const T& newData) {
							const auto oldPos = current.size();
							current.resize(current.size() + sizeof(T));
							memcpy(current.data() + oldPos, &newData, sizeof(T));
						}

						template <typename T>
						static void concat(std::vector<uint8_t>& current, const T& newData, std::size_t numBytes) {
							if (numBytes > sizeof(T)) {
								throw std::exception("Invalid data requested. Byte size does not match value size");
							}

							const auto oldPos = current.size();
							current.resize(current.size() + sizeof(T));
							memcpy(current.data() + oldPos, &newData, numBytes);
						}

						template <typename T>
						static void concat(std::vector<uint8_t>& current, const std::vector<T>& newData) {
							const auto oldPos = current.size();
							current.resize(current.size() + newData.size() * sizeof(T));
							memcpy(current.data() + oldPos, newData.data(), newData.size() * sizeof(T));
						}

						template <typename T>
						static void concat(std::vector<uint8_t>& current, const std::vector<T>& newData, std::size_t numBytes) {
							const auto oldPos = current.size();
							current.resize(current.size() + numBytes);
							memcpy(current.data() + oldPos, newData.data(), numBytes);
						}

						template<typename T>
						static void concat(std::vector<uint8_t>& current, const std::vector<T>& newData, std::size_t numBytes, std::size_t offset) {
							const auto oldPos = current.size();
							current.resize(current.size() + numBytes);
							memcpy(current.data() + oldPos, newData.data() + offset, numBytes);
						}

					public:
						explicit Wdc2File(DefinitionManager::TableDefinition tableDefinition, BinaryReader& file);

						uint32_t getRowCount() override { return static_cast<uint32_t>(mIdList.size()); }
						IStorageRowPtr getRowByIndex(uint32_t index) override;
						IStorageRowPtr getRowById(uint32_t id);
					};
				}
			}
		}
	}
}
