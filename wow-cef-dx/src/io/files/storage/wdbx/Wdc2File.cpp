#include "stdafx.h"
#include "Wdc2File.h"
#include <utility>
#include "io/BitStream.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                namespace wdbx {
                    uint32_t Wdc2RowNew::convertUint32Value(uint32_t value, const FieldDefinition& definition) {
                        switch (definition.getFieldType()) {
                        case FieldType::SHORT:
                        case FieldType::USHORT:
                            return value & 0xFFFFu;

                        case FieldType::BYTE:
                        case FieldType::SBYTE:
                            return value & 0xFFu;

                        default:
                            return value;
                        }
                    }

                    uint32_t Wdc2RowNew::getUint32Value(uint32_t id, uint32_t fieldIndex,
                                                        const FieldDefinition& definition) {
                        const auto& columnMeta = mColumnMeta[fieldIndex];
                        const auto& fieldMeta = mFieldMeta[fieldIndex];
                        switch (columnMeta.getCompression()) {
                        case CompressionType::NONE: {
                            const auto bitSize = 32 - fieldMeta.getBitCount();
                            if (bitSize > 0) {
                                return convertUint32Value(mReader.read<uint32_t>(bitSize), definition);
                            }

                            return convertUint32Value(mReader.read<uint32_t>(columnMeta.getBitWidth()), definition);
                        }

                        case CompressionType::IMMEDIATE:
                        case CompressionType::SIGNED_IMMEDIATE: {
                            return mReader.read<uint32_t>(columnMeta.getBitWidth());
                        }

                        case CompressionType::SPARSE: {
                            const auto itr = columnMeta.getSparseValues().find(id);
                            if (itr != columnMeta.getSparseValues().end()) {
                                return convertUint32Value(*reinterpret_cast<const uint32_t*>(itr->second.data()),
                                                          definition);
                            }
                            return convertUint32Value(columnMeta.getBitOffset(), definition);
                        }

                        case CompressionType::PALETTE: {
                            const auto paletteIndex = mReader.read<uint32_t>(columnMeta.getBitWidth());
                            return convertUint32Value(
                                *reinterpret_cast<const uint32_t*>(columnMeta.getPalletValues()[paletteIndex].data()),
                                definition);
                        }

                        default:
                            throw std::exception("Cannot read value from unknown compression type");
                        }
                    }

                    float Wdc2RowNew::getFloatValue(uint32_t id, uint32_t fieldIndex) {
                        const auto& columnMeta = mColumnMeta[fieldIndex];
                        const auto& fieldMeta = mFieldMeta[fieldIndex];
                        switch (columnMeta.getCompression()) {
                        case CompressionType::NONE: {
                            const auto bitSize = 32 - fieldMeta.getBitCount();
                            if (bitSize > 0) {
                                return mReader.read<float>(bitSize);
                            }

                            return mReader.read<float>(columnMeta.getBitWidth());
                        }

                        case CompressionType::IMMEDIATE:
                        case CompressionType::SIGNED_IMMEDIATE: {
                            return mReader.read<float>(columnMeta.getBitWidth());
                        }

                        case CompressionType::SPARSE: {
                            const auto itr = columnMeta.getSparseValues().find(id);
                            if (itr != columnMeta.getSparseValues().end()) {
                                return *reinterpret_cast<const float*>(itr->second.data());
                            }
                            const auto intValue = columnMeta.getBitOffset();
                            return *reinterpret_cast<const float*>(&intValue);
                        }

                        case CompressionType::PALETTE: {
                            const auto paletteIndex = mReader.read<uint32_t>(columnMeta.getBitWidth());
                            return *reinterpret_cast<const float*>(columnMeta.getPalletValues()[paletteIndex].data());
                        }

                        default:
                            throw std::exception("Cannot read value from unknown compression type");
                        }
                    }

                    std::string Wdc2RowNew::getStringValue(uint32_t id, uint32_t fieldIndex) {
                        static const FieldDefinition STRING_FIELD{"", FieldType::STRING};
                        auto stringOffset = getUint32Value(id, fieldIndex, STRING_FIELD);
                        stringOffset += mColumnMeta[fieldIndex].getRecordOffset() / 8;
                        stringOffset += mRecordOffset;
                        return mStringBlock.at(stringOffset);
                    }

                    std::vector<float> Wdc2RowNew::readFloatArray(uint32_t id, uint32_t fieldIndex) {
                        const auto& columnMeta = mColumnMeta[fieldIndex];
                        const auto& fieldMeta = mFieldMeta[fieldIndex];
                        std::vector<float> ret;
                        switch (columnMeta.getCompression()) {
                        case CompressionType::NONE: {
                            const auto bitSize = 32 - fieldMeta.getBitCount();
                            const auto arraySize = columnMeta.getSize() / (sizeof(float) * 8);
                            ret.resize(arraySize);
                            for (auto i = 0u; i < arraySize; ++i) {
                                if (bitSize > 0) {
                                    ret[i] = mReader.read<float>(bitSize);
                                } else {
                                    ret[i] = mReader.read<float>(columnMeta.getBitWidth());
                                }
                            }

                            break;
                        }

                        case CompressionType::PALLET_ARRAY: {
                            const auto cardinality = columnMeta.getCardinality();
                            const auto paletteIndex = mReader.read<uint32_t>(columnMeta.getBitWidth());
                            ret.resize(cardinality);
                            for (auto i = 0u; i < cardinality; ++i) {
                                ret[i] = *reinterpret_cast<const float*>(columnMeta.getPalletValues()[paletteIndex].
                                    data() + i * sizeof(float));
                            }
                            break;
                        }

                        default:
                            throw std::exception("Only NONE and PALETTE_ARRAY are supported for arrays");
                        }

                        return ret;
                    }

                    std::vector<uint32_t> Wdc2RowNew::readUint32Array(uint32_t id, uint32_t fieldIndex,
                                                                      const FieldDefinition& definition) {
                        const auto& columnMeta = mColumnMeta[fieldIndex];
                        const auto& fieldMeta = mFieldMeta[fieldIndex];
                        std::vector<uint32_t> ret;
                        switch (columnMeta.getCompression()) {
                        case CompressionType::NONE: {
                            const auto bitSize = 32 - fieldMeta.getBitCount();
                            const auto arraySize = columnMeta.getSize() / (definition.getByteSize() * 8);
                            ret.resize(arraySize);
                            for (auto i = 0u; i < arraySize; ++i) {
                                if (bitSize > 0) {
                                    ret[i] = convertUint32Value(mReader.read<uint32_t>(bitSize), definition);
                                } else {
                                    ret[i] = convertUint32Value(mReader.read<uint32_t>(columnMeta.getBitWidth()),
                                                                definition);
                                }
                            }

                            break;
                        }

                        case CompressionType::PALLET_ARRAY: {
                            const auto cardinality = columnMeta.getCardinality();
                            const auto paletteIndex = mReader.read<uint32_t>(columnMeta.getBitWidth());
                            ret.resize(cardinality);
                            for (auto i = 0u; i < cardinality; ++i) {
                                ret[i] = convertUint32Value(
                                    *reinterpret_cast<const uint32_t*>(columnMeta.getPalletValues()[paletteIndex].data()
                                        + i * sizeof(uint32_t)), definition);
                            }
                            break;
                        }

                        default:
                            throw std::exception("Only NONE and PALETTE_ARRAY are supported for arrays");
                        }

                        return ret;
                    }

                    std::vector<std::string> Wdc2RowNew::readStringArray(uint32_t id, uint32_t fieldIndex) {
                        std::vector<std::string> ret;
						const auto& columnMeta = mColumnMeta[fieldIndex];
						const auto& fieldMeta = mFieldMeta[fieldIndex];
						const auto bitSize = 32 - fieldMeta.getBitCount();
						const auto arraySize = columnMeta.getSize() / (sizeof(uint32_t) * 8);
                        ret.reserve(arraySize);
                        for(auto i = 0u; i < arraySize; ++i) {
							const auto baseOffset = (mReader.tell() >> 3) + mRecordOffset;
							uint32_t finalOffset = 0;
                            if(bitSize > 0) {
								finalOffset = static_cast<uint32_t>(baseOffset + mReader.read<uint32_t>(bitSize));
                            } else {
								finalOffset = static_cast<uint32_t>(baseOffset + mReader.read<uint32_t>(columnMeta.getBitWidth()));
                            }

							ret.emplace_back(mStringBlock.at(finalOffset));
                        }

                        return ret;
                    }

                    Wdc2RowNew::Wdc2RowNew(const int32_t rowId,
                                           const uint32_t idField,
                                           uint32_t recordOffset,
                                           const std::map<uint32_t, std::string>& stringBlock,
                                           BitStream stream,
                                           const DefinitionManager::TableDefinition& definition,
                                           const std::vector<ColumnStructureEntry>& columnMeta,
                                           const std::vector<FieldStructureEntry>& fieldMeta,
                                           uint32_t referenceData) :
                        mStringBlock(stringBlock),
                        mReader(std::move(stream)),
                        mDefinition(definition),
                        mColumnMeta(columnMeta),
                        mFieldMeta(fieldMeta),
                        mReferenceData(referenceData),
                        mRecordOffset(recordOffset) {
                        if (rowId >= 0) {
                            mRowId = static_cast<uint32_t>(rowId);
                            mWithIndexTable = true;
                        } else {
                            mReader.seek(mColumnMeta[idField].getRecordOffset());
                            mRowId = getUint32Value(0, idField, mDefinition[idField]);
                            mWithIndexTable = false;
                        }
                    }

                    int32_t Wdc2RowNew::getInt32(uint32_t fieldIndex) {
                        const auto& definition = mDefinition[fieldIndex];
                        if (definition.isIsIndex()) {
                            return mRowId;
                        }

                        if (definition.getFieldType() == FieldType::REFERENCE) {
                            return mReferenceData;
                        }

                        if (mWithIndexTable) {
                            fieldIndex -= 1;
                        }

                        mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
                        return static_cast<int32_t>(getUint32Value(mRowId, fieldIndex, definition));
                    }

                    std::string Wdc2RowNew::getString(uint32_t fieldIndex) {
                        if (mDefinition[fieldIndex].getFieldType() == FieldType::REFERENCE) {
                            throw std::exception("Reference field cannot be a string");
                        }

                        if (mWithIndexTable) {
                            fieldIndex -= 1;
                        }

                        mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
                        return getStringValue(mRowId, fieldIndex);
                    }

                    std::vector<float> Wdc2RowNew::getFloatArray(uint32_t fieldIndex) {
                        if (mDefinition[fieldIndex].getFieldType() == FieldType::REFERENCE) {
                            throw std::exception("Reference field cannot be a float array");
                        }

                        if (mWithIndexTable) {
                            fieldIndex -= 1;
                        }

                        mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
                        return readFloatArray(mRowId, fieldIndex);
                    }

                    std::vector<uint32_t> Wdc2RowNew::getUint32Array(uint32_t fieldIndex) {
                        if (mDefinition[fieldIndex].getFieldType() == FieldType::REFERENCE) {
                            throw std::exception("Reference field cannot be an uint32 array");
                        }

                        const auto& definition = mDefinition[fieldIndex];
                        if (mWithIndexTable) {
                            fieldIndex -= 1;
                        }

                        mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
                        return readUint32Array(mRowId, fieldIndex, definition);
                    }

                    std::vector<std::string> Wdc2RowNew::getStringArray(uint32_t fieldIndex) {
						if (mDefinition[fieldIndex].getFieldType() == FieldType::REFERENCE) {
							throw std::exception("Reference field cannot be an string array");
						}

						if (mWithIndexTable) {
							fieldIndex -= 1;
						}

                        if(mColumnMeta[fieldIndex].getCompression() != CompressionType::NONE) {
							throw std::exception("Compressed string arrays are not supported");
                        }

						mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
						return readStringArray(mRowId, fieldIndex);
                    }

                    float Wdc2RowNew::getFloat(uint32_t fieldIndex) {
                        if (mDefinition[fieldIndex].getFieldType() == FieldType::REFERENCE) {
                            throw std::exception("Reference field cannot be a float");
                        }

                        if (mWithIndexTable) {
                            fieldIndex -= 1;
                        }

                        mReader.seek(mColumnMeta[fieldIndex].getRecordOffset());
                        return getFloatValue(mRowId, fieldIndex);
                    }

                    void Wdc2File::calculateColumnSizes() {
                        uint32_t curOffset = 0;
                        std::for_each(mDefinition.begin(), mDefinition.end(), [this, &curOffset](const auto& column) {
                            mColumnSizes.push_back(column.getByteSize());
                            mFullOffsets.push_back(curOffset);
                            curOffset += column.getByteSize() * column.getArraySize();
                        });
                    }

                    void Wdc2File::readHeader(BinaryReader& file) {
                        mRecordCount = file.read<uint32_t>();
                        mFieldCount = file.read<uint32_t>();
                        mRecordSize = file.read<uint32_t>();
                        mStringBlockSize = file.read<uint32_t>();
                        file.seekMod(4);
                        file.seekMod(4);
                        mMinId = file.read<uint32_t>();
                        mMaxId = file.read<uint32_t>();
                        file.seekMod(4);
                        mFlags = file.read<HeaderFlags>();
                        mIndexColumn = file.read<uint16_t>();
                        file.seekMod(4);
                        file.seekMod(4);

                        file.seekMod(4);
                        file.seekMod(4); // field storage info size
                        file.seekMod(4); // sparse data size
                        file.seekMod(4); // pallet data size

                        file.seekMod(4);
                        file.seekMod(2 * sizeof(uint32_t));
                        mRecordDataOffset = file.read<uint32_t>();
                        file.seekMod(4);
                        file.seekMod(4);
                        mCopyTableSize = file.read<uint32_t>();
                        mOffsetTableOffset = file.read<uint32_t>();
                        file.seekMod(4);
                        mRelationShipDataSize = file.read<uint32_t>();

                        loadFieldMetaData(file);
                        loadColumnMetaData(file);

                        std::vector<uint8_t> recordData(mRecordCount * mRecordSize);
                        file.read(recordData);

                        loadStringTable(file);

                        loadOffsetTable(file);
                        loadIndexTable(file);
                        loadCopyMap(file);
                        loadRelationShipData(file);
                        loadRecordData(file, recordData);
                    }

                    void Wdc2File::loadFieldMetaData(BinaryReader& file) {
                        mFieldMetaData.reserve(mFieldCount);

                        for (auto i = 0u; i < mFieldCount; ++i) {
                            auto bits = file.read<int16_t>();
                            auto offset = file.read<uint16_t>();
                            mFieldMetaData.emplace_back(bits, offset);
                        }
                    }

                    void Wdc2File::loadColumnMetaData(BinaryReader& file) {
                        mColumnMetaData.reserve(mFieldCount);

                        for (auto i = 0u; i < mFieldCount; ++i) {
                            const auto recordOffset = file.read<uint16_t>();
                            const auto size = file.read<uint16_t>();
                            const auto additionalDataSize = file.read<uint32_t>();
                            const auto compression = file.read<CompressionType>();
                            const auto bitOffset = file.read<uint32_t>();
                            const auto bitWidth = file.read<uint32_t>();
                            const auto cardinality = file.read<uint32_t>();
                            mColumnMetaData.emplace_back(
                                recordOffset, size, additionalDataSize,
                                compression, bitOffset, bitWidth, cardinality
                            );
                        }

                        uint32_t curIndex = 0;
                        for (auto& column : mColumnMetaData) {
                            const auto compression = column.getCompression();

                            if (compression == CompressionType::PALLET_ARRAY || compression == CompressionType::PALETTE
                            ) {
                                const auto numElements = column.getAdditionalDataSize() / 4;
                                const auto cardinality = std::max<uint32_t>(1u, column.getCardinality());

                                for (auto i = 0u; i < numElements / cardinality; ++i) {
                                    std::vector<uint8_t> data(4 * cardinality);
                                    file.read(data);
                                    column.getPalletValues().push_back(data);
                                }
                            }

                            ++curIndex;
                        }

                        curIndex = 0;
                        for (auto& column : mColumnMetaData) {
                            if (column.getCompression() == CompressionType::SPARSE) {
                                auto& sparseMap = column.getSparseValues();
                                for (auto i = 0u; i < column.getAdditionalDataSize() / 8; ++i) {
                                    std::vector<uint8_t> tmp(4);
                                    file.read(tmp);
                                    sparseMap.emplace(file.read<uint32_t>(), tmp);
                                }
                            }

                            ++curIndex;
                        }
                    }

                    void Wdc2File::loadOffsetTable(BinaryReader& file) {
                        if (hasOffsetTable() && mOffsetTableOffset > 0) {
                            file.seek(mOffsetTableOffset);

                            for (auto i = 0u; i < (mMaxId - mMinId + 1); ++i) {
                                const auto offset = file.read<uint32_t>();
                                const auto length = file.read<uint16_t>();

                                if (offset == 0 || length == 0) {
                                    continue;
                                }

                                if (mCopyTableSize == 0) {
                                    if (mDuplicateMap.find(offset) == mDuplicateMap.end()) {
                                        mDuplicateMap.emplace(offset, std::make_pair(
                                                                  static_cast<uint32_t>(mOffsetMapList.size()),
                                                                  static_cast<uint32_t>(mDuplicateMap.size())));
                                    } else {
                                        continue;
                                    }
                                }

                                mOffsetMapList.emplace_back(offset, length);
                            }
                        }
                    }

                    void Wdc2File::loadIndexTable(BinaryReader& file) {
                        if (hasIndexTable()) {
                            mIndexList.reserve(mRecordCount);

                            for (auto i = 0u; i < mRecordCount; ++i) {
                                mIndexList.push_back(file.read<uint32_t>());
                            }
                        }
                    }

                    void Wdc2File::loadCopyMap(BinaryReader& file) {
                        if (mCopyTableSize > 0) {
                            const auto end = file.tell() + mCopyTableSize;
                            while (file.tell() < end) {
                                const auto id = file.read<uint32_t>();
                                const auto idCopy = file.read<uint32_t>();
                                mCopyMap[id].push_back(idCopy);
                            }
                        }
                    }

                    void Wdc2File::loadRelationShipData(BinaryReader& file) {
                        if (mRelationShipDataSize > 0) {
                            const auto numRecords = file.read<uint32_t>();
                            file.seekMod(8); // minId, maxId
                            for (auto i = 0u; i < numRecords; ++i) {
                                const auto foreignKey = file.read<uint32_t>();
                                const auto index = file.read<uint32_t>();
                                mRelationShipData[index] = foreignKey;
                            }
                        }
                    }

                    void Wdc2File::loadRecordData(BinaryReader& file, const std::vector<uint8_t>& recordData) {
                        BitStream reader{recordData};
                        for (auto i = 0u; i < mRecordCount; ++i) {
                            if (hasOffsetTable() && hasIndexTable()) {
                                throw std::exception("Not yet implemented");
                            } else {
                                std::vector<uint8_t> rowData(mRecordSize);
                                memcpy(rowData.data(), recordData.data() + i * mRecordSize, mRecordSize);
                                BitStream bitStream(rowData);
                                auto fixedId = -1;
                                if (hasIndexTable()) {
                                    fixedId = mIndexList[i];
                                }

                                const auto row = std::make_shared<Wdc2RowNew>(
                                    fixedId, mIndexColumn, i * mRecordSize + mRecordDataOffset, mStringTable, bitStream,
                                    mDefinition, mColumnMetaData, mFieldMetaData,
                                    mRelationShipData.empty() ? 0 : mRelationShipData[i]);
                                mRecordMapNew.emplace(row->getId(), row);
                                mIdList.push_back(row->getId());

                                const auto copyItr = mCopyMap.find(row->getId());
                                if (copyItr != mCopyMap.end()) {
                                    for (const auto copyId : copyItr->second) {
                                        mRecordMapNew.emplace(copyId, std::make_shared<Wdc2RowNew>(
                                                                  fixedId, mIndexColumn,
                                                                  i * mRecordSize + mRecordDataOffset, mStringTable,
                                                                  bitStream,
                                                                  mDefinition, mColumnMetaData, mFieldMetaData,
                                                                  mRelationShipData.empty()
                                                                      ? 0
                                                                      : mRelationShipData[i]));
                                        mIdList.push_back(copyId);
                                    }
                                }
                            }
                        }
                    }

                    void Wdc2File::loadStringTable(BinaryReader& file) {
                        std::size_t numRead = 0;
                        while (numRead < mStringBlockSize) {
                            const auto* ptr = reinterpret_cast<const char*>(&file.getData()[file.tell()]);
                            const auto length = strlen(ptr);
                            const auto offset = file.tell();
                            mStringTable.emplace(static_cast<uint32_t>(offset), std::string(ptr, ptr + length));
                            file.seekMod(length + 1);
                            numRead += length + 1;
                        }
                    }

                    Wdc2File::Wdc2File(DefinitionManager::TableDefinition tableDefinition, BinaryReader& file) :
                        mDefinition(std::move(tableDefinition)) {
                        calculateColumnSizes();
                        file.seek(4);
                        readHeader(file);
                    }

                    IStorageRowPtr Wdc2File::getRowByIndex(uint32_t index) {
                        if (index >= mIdList.size()) {
                            throw std::out_of_range("Attempted to read beyond the number of available WDC columns");
                        }

                        const auto row = mRecordMapNew.at(mIdList[index]);
                        return row;
                    }

                    IStorageRowPtr Wdc2File::getRowById(uint32_t id) {
                        return mRecordMapNew.at(id);
                    }
                }
            }
        }
    }
}
