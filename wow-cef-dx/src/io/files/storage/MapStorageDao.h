#pragma once
#include "IStorageFile.h"
#include "DefinitionManager.h"
#include "io/BinaryReader.h"
#include "io/files/IFileProvider.h"
#include "io/files/minimap/MapPoi.h"
#include "AbstractDao.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                class MapEntry {
                    int32_t mMapId = 0;
                    std::string mMapName;
                    std::string mMapDirectory;
                    int32_t mLoadingScreen = 0;
                    std::string mLoadingScreenLink;
                    int32_t mInstanceType = 0;

                public:
                    MapEntry(int32_t mapId, std::string mapName, std::string mapDirectory, int32_t loadingScreen,
                             std::string loadingScreenLink, int32_t instanceType)
                        : mMapId(mapId),
                          mMapName(std::move(mapName)),
                          mMapDirectory(std::move(mapDirectory)),
                          mLoadingScreen(loadingScreen),
                          mLoadingScreenLink(std::move(loadingScreenLink)),
                          mInstanceType(instanceType) {
                    }

                    int32_t getMapId() const {
                        return mMapId;
                    }

                    const std::string& getMapName() const {
                        return mMapName;
                    }

                    const std::string& getMapDirectory() const {
                        return mMapDirectory;
                    }

                    int32_t getLoadingScreen() const {
                        return mLoadingScreen;
                    }

                    const std::string& getLoadingScreenLink() const {
                        return mLoadingScreenLink;
                    }

                    int32_t getInstanceType() const {
                        return mInstanceType;
                    }
                };

                class MapStorageDao : public AbstractDao {
                    static utils::Logger<MapStorageDao> mLogger;
                    static const std::string FALLBACK_LOADING_SCREEN;
                    static const std::string FALLBACK_LOADING_SCREEN_NAME;

                    IFileProviderPtr mFileProvider;

                    IStorageFilePtr mMapStorage;
                    IStorageFilePtr mLoadingScreensStorage;
                    IStorageFilePtr mAreaPoiStorage;
					IStorageFilePtr mAreaTableStorage;

                    uint32_t mBuildId = 0;
                    DefinitionManager::BuildDefinition mDefinitions;

                    uint32_t mMapIdField = 0;
                    uint32_t mMapNameField = 0;
                    uint32_t mMapDirectoryField = 0;
                    uint32_t mMapLoadLink = 0;
                    uint32_t mInstanceTypeField = 0;

                    bool mIsLoadScreenFileDataId = false;

                    uint32_t mLoadingScreenIdField = 0;
                    uint32_t mNarrowFileDataIdField = 0;
                    uint32_t mLoadScreenFileNameField = 0;
                    uint32_t mLoadScreenFilePathField = 0;

                    uint32_t mAreaPoiMapIdField = 0;
                    uint32_t mAreaPoiNameField = 0;
                    uint32_t mAreaPoiPositionField = 0;

                    uint32_t mAreaTableIdField = 0;
                    uint32_t mAreaTableNameField = 0;

                    std::map<uint32_t, uint32_t> mLoadingScreenIndexMap;
                    std::map<uint32_t, uint32_t> mMapIndexMap;
					std::map<uint32_t, uint32_t> mAreaTableIndexMap;

                    void initMapFieldIds();
                    void initLoadingScreenIds();
                    void initMapAreaPoi();
                    void initAreaTable();

                    std::string getLoadingScreenLink(int32_t loadingScreenId) const;

                public:
                    MapStorageDao(IFileProviderPtr fileProvider, DefinitionManager::BuildDefinition definitions,
                                  uint32_t buildId,
                                  const std::function<IStorageFilePtr(const std::string&)>& fileLoadFunction);

                    std::vector<MapEntry> getMaps() const;

                    std::string getMapDirectory(uint32_t mapId);
                    std::string getMapName(uint32_t mapId);
					std::string getAreaName(uint32_t areaId);

                    std::vector<minimap::MapPoi> getPoisForMap(uint32_t mapId) const;

                    BinaryReader openLoadingScreen(bool immediateMode, const std::string& data) const;
                };

                SHARED_PTR(MapStorageDao);
            }
        }
    }
}
