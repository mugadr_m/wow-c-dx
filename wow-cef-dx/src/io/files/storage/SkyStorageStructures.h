#pragma once
#include "math/Vector2.h"
#include "math/Vector3.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
#pragma pack(push, 1)
                struct LightDataEntry {
					enum Values {
						DIFFUSE,
						AMBIENT,
						SKY0,
						SKY1,
						SKY2,
						SKY3,
						SKY4,
						FOG,
						SUN,
						HALO,
						CLOUD,
						CLOUD_LAYER1_AMBIENT,
                        CLOUD_LAYER2_AMBIENT,
						OCEAN_NEAR,
						OCEAN_FAR,
						RIVER_NEAR,
						RIVER_FAR,
						SHADOW,
						FOG_END,
						FOG_SCALER,
                        CLOUD_DENSITY,
						FOG_DENSITY,
						NUM_DATA_VALUES
					};

					uint32_t id;
					uint32_t skyId;
					uint32_t timestamp;
					uint32_t data[NUM_DATA_VALUES];
                };

                struct LightEntry {
					uint32_t id = 0;
					math::Vector3 position{};
					float innerRadius = 0.0f;
					float outerRadius = 0.0f;
					uint32_t continentId = 0;
					uint32_t lightParamsId[8]{};

                    bool isGlobal() const {
						return innerRadius < 0.001f && outerRadius < 0.001f;
                    }
                };

                struct LightParamsEntry {
					uint32_t id;
					uint32_t skyId;
					uint32_t skyboxId;
					uint32_t cloudTypeId;
					float glow;
					float waterNearAlpha;
					float waterFarAlpha;
					float oceanNearAlpha;
					float oceanFarAlpha;
					uint32_t flags;
                };

				struct ZoneLightPoint {
					uint32_t id;
					math::Vector2 position;
					uint32_t pointOrder;
					uint32_t zoneLightId;
				};

                struct ZoneLight {
					uint32_t id;
					std::string name;
					uint32_t mapId;
					uint32_t lightId;
					uint32_t flags;

					std::vector<ZoneLightPoint> polygonPoints;
					std::vector<LightDataEntry> lightDataEntries;
					LightParamsEntry lightParams;
                };
#pragma pack(pop)
			}
		}
	}
}
