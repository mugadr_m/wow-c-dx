#include "stdafx.h"
#include "LiquidStorageDao.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {

                void LiquidStorageDao::initLiquidTypeFields() {
                    const auto tableDef = mDefinitions.at("LiquidType");

                    mLiquidIdField = findFieldDefinition(tableDef, "ID");
                    mLiquidNameField = findFieldDefinition(tableDef, "Name");
                    mLiquidTextureField = findFieldDefinition(tableDef, "Texture");
                }

                LiquidStorageDao::LiquidStorageDao(
                    const std::function<IStorageFilePtr(const std::string&)>& loadFunction,
                    DefinitionManager::BuildDefinition definitions) :
                    mDefinitions(std::move(definitions)) {

                    mLiquidStorage = loadFunction("LiquidType");

                    initLiquidTypeFields();
                }

                LiquidType LiquidStorageDao::getLiquidTypeById(uint32_t id) const {
					const auto row = mLiquidStorage->getRowById(id);
					const auto liquidId = row->getUint32(mLiquidIdField);
					const auto name = row->getString(mLiquidNameField);
					const auto textures = row->getStringArray(mLiquidTextureField);

                    return LiquidType {
						liquidId,
                        name,
                        textures
					};
                }
            }
        }
    }
}
