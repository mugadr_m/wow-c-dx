#include "stdafx.h"
#include "MapStorageDao.h"
#include "io/http/HttpClient.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                utils::Logger<MapStorageDao> MapStorageDao::mLogger;

                const std::string MapStorageDao::FALLBACK_LOADING_SCREEN_NAME = "Interface/Glues/loading.blp";
                const std::string MapStorageDao::FALLBACK_LOADING_SCREEN =
                    std::string(R"(loading-screen://localhost/immediate/)") + FALLBACK_LOADING_SCREEN_NAME;

                void MapStorageDao::initMapFieldIds() {
                    const auto mapDef = mDefinitions.at("Map");
                    mMapIdField = findFieldDefinition(mapDef, "ID");
                    mMapNameField = findFieldDefinition(mapDef, "MapName");
                    mMapDirectoryField = findFieldDefinition(mapDef, "Directory");
                    mMapLoadLink = findFieldDefinition(mapDef, "LoadingScreenId");
                    mInstanceTypeField = findFieldDefinition(mapDef, "InstanceType");

                    for (auto i = 0u; i < mMapStorage->getRowCount(); ++i) {
                        const auto mapRow = mMapStorage->getRowByIndex(i);
                        const auto mapId = mapRow->getInt32(mMapIdField);
                        mMapIndexMap.emplace(mapId, i);
                    }
                }

                void MapStorageDao::initLoadingScreenIds() {
                    const auto tableDef = mDefinitions.at("LoadingScreens");
                    mLoadingScreenIdField = findFieldDefinition(tableDef, "ID");

                    for (auto i = 0u; i < mLoadingScreensStorage->getRowCount(); ++i) {
                        auto lsRow = mLoadingScreensStorage->getRowByIndex(i);
                        const auto id = lsRow->getInt32(mLoadingScreenIdField);
                        mLoadingScreenIndexMap.emplace(id, i);
                    }

                    try {
                        mNarrowFileDataIdField = findFieldDefinition(tableDef, "NarrowScreenFileDataId");
                        mIsLoadScreenFileDataId = true;
                    } catch (std::exception&) {
                        mLoadScreenFileNameField = findFieldDefinition(tableDef, "FileName");
                        mLoadScreenFilePathField = findFieldDefinition(tableDef, "Name");
                    }
                }

                void MapStorageDao::initMapAreaPoi() {
                    const auto tableDef = mDefinitions.at("AreaPOI");

                    mAreaPoiNameField = findFieldDefinition(tableDef, "Name");
                    mAreaPoiPositionField = findFieldDefinition(tableDef, "Pos");
                    mAreaPoiMapIdField = findFieldDefinition(tableDef, "ContinentId");
                }

                void MapStorageDao::initAreaTable() {
					const auto tableDef = mDefinitions.at("AreaTable");

					mAreaTableIdField = findFieldDefinition(tableDef, "ID");
					mAreaTableNameField = findFieldDefinition(tableDef, "AreaName");

                    for(auto i = 0u; i < mAreaTableStorage->getRowCount(); ++i) {
						const auto row = mAreaTableStorage->getRowByIndex(i);
						const auto id = row->getUint32(mAreaTableIdField);
						mAreaTableIndexMap.emplace(id, i);
                    }
                }

                std::string MapStorageDao::getLoadingScreenLink(int32_t loadingScreenId) const {
                    if (loadingScreenId <= 0) {
                        return FALLBACK_LOADING_SCREEN;
                    }

                    const auto itr = mLoadingScreenIndexMap.find(loadingScreenId);
                    if (itr == mLoadingScreenIndexMap.end()) {
                        return FALLBACK_LOADING_SCREEN;
                    }

                    const auto lsRow = mLoadingScreensStorage->getRowByIndex(itr->second);
                    if (mIsLoadScreenFileDataId) {
                        const auto fileDataId = lsRow->getInt32(mNarrowFileDataIdField);
                        std::stringstream strm;
                        strm << "loading-screen://localhost/fileDataId/" << fileDataId;
                        return strm.str();
                    }

                    const auto screenPath = lsRow->getString(mLoadScreenFilePathField);
                    const auto screenName = lsRow->getString(mLoadScreenFileNameField);

                    std::stringstream strm;
                    strm << screenPath << "\\" << screenName;
                    const auto loadScreenName = http::HttpClient::urlEncode(strm.str());

                    strm.str(std::string());

                    strm << "loading-screen://localhost/immediate/" << loadScreenName;
                    return strm.str();
                }

                MapStorageDao::MapStorageDao(IFileProviderPtr fileProvider,
                                             DefinitionManager::BuildDefinition definitions,
                                             uint32_t buildId,
                                             const std::function<IStorageFilePtr(const std::string&)>& fileLoadFunction) :
                    mFileProvider(std::move(fileProvider)),
                    mBuildId(buildId), mDefinitions(std::move(definitions)) {

					mLoadingScreensStorage = fileLoadFunction("LoadingScreens");
					mMapStorage = fileLoadFunction("Map");
					mAreaPoiStorage = fileLoadFunction("AreaPOI");
					mAreaTableStorage = fileLoadFunction("AreaTable");

                    initMapFieldIds();
                    initLoadingScreenIds();
                    initMapAreaPoi();
					initAreaTable();
                }

                std::vector<MapEntry> MapStorageDao::getMaps() const {
                    std::vector<MapEntry> ret;
                    for (auto i = 0u; i < mMapStorage->getRowCount(); ++i) {
                        auto mapRow = mMapStorage->getRowByIndex(i);
                        const auto mapName = mapRow->getString(mMapNameField);
                        const auto mapDir = mapRow->getString(mMapDirectoryField);
                        const auto loadScreen = mapRow->getInt32(mMapLoadLink);
                        const auto mapId = mapRow->getInt32(mMapIdField);
                        const auto instanceType = mapRow->getInt32(mInstanceTypeField);
                        ret.emplace_back(mapId, mapName, mapDir, loadScreen, getLoadingScreenLink(loadScreen),
                                         instanceType);
                    }

                    return ret;
                }

                std::string MapStorageDao::getMapDirectory(uint32_t mapId) {
                    const auto itr = mMapIndexMap.find(mapId);
                    if (itr == mMapIndexMap.end()) {
                        mLogger.warn("Unable to find map from id: ", mapId);
                        throw std::exception("Unable to find map");
                    }

                    const auto row = mMapStorage->getRowByIndex(itr->second);
                    return row->getString(mMapDirectoryField);
                }

                std::string MapStorageDao::getMapName(uint32_t mapId) {
                    const auto itr = mMapIndexMap.find(mapId);
                    if (itr == mMapIndexMap.end()) {
                        mLogger.warn("Unable to find map from id: ", mapId);
                        throw std::exception("Unable to find map");
                    }

                    const auto row = mMapStorage->getRowByIndex(itr->second);
                    return row->getString(mMapNameField);
                }

                std::string MapStorageDao::getAreaName(uint32_t areaId) {
					const auto itr = mAreaTableIndexMap.find(areaId);
                    if(itr == mAreaTableIndexMap.end()) {
						mLogger.warn("Area not found: ", areaId);
						throw std::exception("Area not found");
                    }

					const auto row = mAreaTableStorage->getRowByIndex(itr->second);
					return row->getString(mAreaTableNameField);
                    
                }

                std::vector<minimap::MapPoi> MapStorageDao::getPoisForMap(uint32_t mapId) const {
                    std::vector<minimap::MapPoi> mapPoiList;

                    for (auto i = 0u; i < mAreaPoiStorage->getRowCount(); ++i) {
                        const auto row = mAreaPoiStorage->getRowByIndex(i);
                        const auto id = row->getInt32(mAreaPoiMapIdField);
                        if (id != static_cast<int32_t>(mapId)) {
                            continue;
                        }

                        const auto name = row->getString(mAreaPoiNameField);
                        const auto position = row->getFloatArray(mAreaPoiPositionField);

                        mapPoiList.emplace_back(name, position[0], position[1]);
                    }
                    return mapPoiList;
                }

                BinaryReader MapStorageDao::openLoadingScreen(bool immediateMode, const std::string& data) const {
                    if (!immediateMode) {
                        const auto fileDataId = std::stoi(data);
                        if (fileDataId == 0) {
                            mLogger.warn("FileDataId is possibly wrong: ", data, ". std::stoi said its 0");
                        }

                        try {
                            return mFileProvider->openFileByFileId(fileDataId);
                        } catch (std::exception&) {
                            return mFileProvider->openFile(FALLBACK_LOADING_SCREEN_NAME);
                        }
                    }

                    return mFileProvider->openFile(data);
                }
            }
        }
    }
}
