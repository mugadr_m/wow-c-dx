#pragma once
#include "IStorageFile.h"
#include "DefinitionManager.h"
#include "AbstractDao.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                class LiquidType {
					uint32_t mId = 0;
                    std::string mName;
                    std::vector<std::string> mTextures;

                public:
                    LiquidType() { }

                    explicit LiquidType(uint32_t id, std::string name, std::vector<std::string> textures) :
                        mId(id),
                        mName(std::move(name)),
                        mTextures(std::move(textures)) {

                    }

                    uint32_t getId() const {
                        return mId;
                    }

                    const std::string& getName() const {
                        return mName;
                    }

                    const std::vector<std::string>& getTextures() const {
                        return mTextures;
                    }
                };

                class LiquidStorageDao : public AbstractDao {
					IStorageFilePtr mLiquidStorage;

					uint32_t mLiquidIdField = -1;
					uint32_t mLiquidNameField = -1;
					uint32_t mLiquidTextureField = -1;

					DefinitionManager::BuildDefinition mDefinitions;

					void initLiquidTypeFields();

                public:
					explicit LiquidStorageDao(const std::function<IStorageFilePtr (const std::string&)>& loadFunction, DefinitionManager::BuildDefinition definitions);

					LiquidType getLiquidTypeById(uint32_t id) const;
                };

				SHARED_PTR(LiquidStorageDao);
            }
        }
    }
}
