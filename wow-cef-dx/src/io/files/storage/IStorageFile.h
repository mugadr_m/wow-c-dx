#pragma once
#include "IStorageRow.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				class IStorageFile {
				public:
					virtual ~IStorageFile() = default;

					virtual uint32_t getRowCount() = 0;
					virtual IStorageRowPtr getRowByIndex(uint32_t index) = 0;
					virtual IStorageRowPtr getRowById(uint32_t id) = 0;
				};

				SHARED_PTR(IStorageFile);
			}
		}
	}
}
