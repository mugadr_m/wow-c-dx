#include "stdafx.h"
#include "AbstractDao.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
				utils::Logger<AbstractDao> AbstractDao::mDaoLogger;

                uint32_t AbstractDao::findFieldDefinition(const DefinitionManager::TableDefinition& definition,
                    const std::string& name) {
					const auto itr = std::find_if(definition.begin(), definition.end(),
						[&](const auto& value) { return value.getFieldName() == name; });
					if (itr == definition.end()) {
						mDaoLogger.error("Unable to find definition for field: ", name);
						throw std::exception("Unable to find definition for field");
					}

					return static_cast<uint32_t>(std::distance(definition.begin(), itr));
                }
            }
        }
    }
}