#include "stdafx.h"
#include "DefinitionManager.h"
#include "utils/CommandLine.h"
#include <boost/filesystem.hpp>
#include "utils/String.h"
#include <rapidxml.hpp>
#include <boost/timer/timer.hpp>

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                utils::Logger<DefinitionManager> DefinitionManager::mLogger;
                std::map<std::string, FieldType> DefinitionManager::mTypeMap = {
                    {"int", FieldType::INT},
                    {"uint", FieldType::UINT},
                    {"string", FieldType::STRING},
                    {"byte", FieldType::BYTE},
                    {"sbyte", FieldType::SBYTE},
                    {"ushort", FieldType::USHORT},
                    {"short", FieldType::SHORT},
                    {"float", FieldType::FLOAT},
                    {"ulong", FieldType::ULONG},
                    {"long", FieldType::LONG},
                    {"bool", FieldType::BOOL},
                    {"loc", FieldType::LOC},
                    {"ref", FieldType::REFERENCE}
                };

                FieldDefinition::FieldDefinition(std::string fieldName, const FieldType fieldType) :
                    mFieldName(std::move(fieldName)),
                    mFieldType(fieldType) {
                }

                uint32_t FieldDefinition::getByteSize() const {
                    static std::map<FieldType, uint32_t> sizeMap{
                        {FieldType::BYTE, 1},
                        {FieldType::BOOL, 1},
                        {FieldType::FLOAT, 4},
                        {FieldType::INT, 4},
                        {FieldType::LOC, 17 * 4},
                        {FieldType::LONG, 8},
                        {FieldType::SBYTE, 1},
                        {FieldType::SHORT, 2},
                        {FieldType::USHORT, 2},
                        {FieldType::STRING, 4},
                        {FieldType::UINT, 4},
                        {FieldType::ULONG, 8},
                        {FieldType::REFERENCE, 0}
                    };

                    return sizeMap.at(mFieldType);
                }

                bool FieldDefinition::isIntegral() const {
                    switch (mFieldType) {
                    case FieldType::INT:
                    case FieldType::UINT:
                    case FieldType::BYTE:
                    case FieldType::SBYTE:
                    case FieldType::USHORT:
                    case FieldType::SHORT:
                    case FieldType::ULONG:
                    case FieldType::LONG:
                        return true;

                    default:
                        return false;
                    }
                }

                FieldType DefinitionManager::parseFieldType(const std::string& type) {
                    const auto fieldType = utils::String::toLower(type);
                    const auto itr = mTypeMap.find(fieldType);
                    if (itr != mTypeMap.end()) {
                        return itr->second;
                    }

                    return FieldType::UNKNOWN;
                }

                void DefinitionManager::handleDefinition(const std::string& filePath) {
                    rapidxml::xml_document<char> document;
                    std::vector<char> fileData;
                    std::ifstream inFile(filePath, std::ios::in | std::ios::binary);
                    if (!inFile.is_open()) {
                        mLogger.error("Error opening file");
                        throw std::exception("Error parsing definition");
                    }

                    inFile.seekg(0, std::ios::end);
                    const auto fileSize = static_cast<std::size_t>(inFile.tellg());
                    inFile.seekg(0, std::ios::beg);
                    fileData.resize(fileSize);
                    inFile.read(fileData.data(), fileSize);
                    inFile.close();
                    fileData.push_back(0);

                    try {
                        document.parse<rapidxml::parse_fastest>(fileData.data());
                    } catch (rapidxml::parse_error& e) {
                        mLogger.error("Error parsing file: ", e.what(), " starting with: ", e.where<char>());
                        return;
                    }

                    const auto definitionNode = document.first_node("Definition");
                    if (definitionNode == nullptr) {
                        mLogger.warn("No <Definition> node found in definition, ignoring");
                        return;
                    }

                    auto tableNode = definitionNode->first_node("Table");
                    if (tableNode == nullptr) {
                        mLogger.warn("No <Table> nodes found in definition, ignoring");
                        return;
                    }

                    do {
                        handleTable(tableNode);
                        tableNode = tableNode->next_sibling("Table");
                    } while (tableNode != nullptr);
                }

                void DefinitionManager::handleTable(const rapidxml::xml_node<char>* tableNode) {
                    const auto tableNameAttr = tableNode->first_attribute("Name");
                    const auto buildAttr = tableNode->first_attribute("Build");

                    if (tableNameAttr == nullptr) {
                        mLogger.warn("Missing attribute Name in table node, ignoring table");
                        return;
                    }

                    const auto nameStart = tableNameAttr->value();
                    const auto nameEnd = nameStart + tableNameAttr->value_size();
                    const std::string tableName(nameStart, nameEnd);

                    if (buildAttr == nullptr) {
                        mLogger.warn("No build attribute found in table ", tableName, " ignoring.");
                        return;
                    }

                    const auto buildStart = buildAttr->value();
                    const auto buildEnd = buildStart + buildAttr->value_size();
                    const std::string buildIdString(buildStart, buildEnd);
                    const auto buildId = std::stoi(buildIdString);

                    const auto buildEntry = mDefinitions.find(buildId);
                    if (buildEntry == mDefinitions.end()) {
                        mDefinitions.emplace(buildId, BuildDefinition());
                    }

                    TableDefinition fieldMap;
                    parseTableFields(tableNode, fieldMap);
                    mDefinitions.find(buildId)->second.emplace(tableName, fieldMap);
                }

                void DefinitionManager::parseTableFields(const rapidxml::xml_node<char>* tableNode,
                                                         TableDefinition& fieldMap) {
                    auto fieldNode = tableNode->first_node("Field");
                    if (fieldNode == nullptr) {
                        mLogger.warn("No fields found for table");
                        return;
                    }

                    do {
                        const auto nameAttr = fieldNode->first_attribute("Name");
                        const auto typeAttr = fieldNode->first_attribute("Type");
                        const auto indexAttr = fieldNode->first_attribute("IsIndex");
                        const auto arraySizeAttr = fieldNode->first_attribute("ArraySize");
                        if (nameAttr == nullptr || nameAttr->value_size() == 0) {
                            mLogger.warn("Missing/empty name attribute in field definition");
                            fieldNode = fieldNode->next_sibling("Field");
                            continue;
                        }

                        if (typeAttr == nullptr || typeAttr->value_size() == 0) {
                            mLogger.warn("Missing/empty type attribute in field definition");
                            fieldNode = fieldNode->next_sibling("Field");
                            continue;
                        }

                        const std::string fieldName(nameAttr->value(), nameAttr->value() + nameAttr->value_size());
                        const std::string fieldType(typeAttr->value(), typeAttr->value() + typeAttr->value_size());

                        const auto actualType = parseFieldType(fieldType);
                        if (actualType == FieldType::UNKNOWN) {
                            mLogger.warn("Unable to parse field type '", fieldType, "' for field '", fieldName, "'");
                            fieldNode = fieldNode->next_sibling("Field");
                            continue;
                        }

                        FieldDefinition definition(fieldName, actualType);
                        if (indexAttr != nullptr && indexAttr->value_size() > 0) {
                            const std::string indexStr(indexAttr->value(),
                                                       indexAttr->value() + indexAttr->value_size());
                            const auto isIndex = utils::String::toLower(indexStr) == "true";
                            definition.setIsIndex(isIndex);
                        }

                        if (arraySizeAttr != nullptr && arraySizeAttr->value_size() > 0) {
                            const std::string arraySizeStr(arraySizeAttr->value(),
                                                           arraySizeAttr->value() + arraySizeAttr->value_size());
                            const auto arraySize = std::stoi(arraySizeStr);
                            definition.setArraySize(arraySize);
                        }

                        fieldMap.emplace_back(definition);

                        fieldNode = fieldNode->next_sibling("Field");
                    } while (fieldNode != nullptr);
                }

                void DefinitionManager::postConstruct(const cdi::ApplicationContextPtr& context) {
                    boost::timer::cpu_timer timer;
                    namespace fs = boost::filesystem;
                    const auto commandLine = context->produce<utils::CommandLine>();
                    const auto baseFolder = commandLine->getOption("definitions-folder", "definitions");
                    fs::path basePath(baseFolder);
                    basePath = absolute(basePath);
                    if (!exists(basePath) || status(basePath).type() != fs::directory_file) {
                        mLogger.error("Folder ", basePath.string(), " is not a valid directory.");
                        throw std::exception("Error loading definitions");
                    }

                    fs::directory_iterator itr(basePath);
                    const auto end = fs::directory_iterator();
                    for (; itr != end; ++itr) {
                        const auto curFile = (*itr).path();
                        if (!curFile.has_extension()) {
                            mLogger.info("Skipping file ", curFile.string());
                            continue;
                        }

                        const auto extension = utils::String::toLower(curFile.extension().string());
                        if (extension != ".xml") {
                            mLogger.info("Skipping file ", curFile.string());
                            continue;
                        }

                        mLogger.info("Handling file ", curFile.filename().string());
                        handleDefinition(absolute(curFile).string());
                    }

                    for (auto& pair : mDefinitions) {
                        mBuildIds.push_back(pair.first);
                    }

                    std::sort(mBuildIds.begin(), mBuildIds.end());
                    const auto times = timer.format();
                    mLogger.info(times.substr(0, times.length() - 1));
                }

                DefinitionManager::BuildDefinition DefinitionManager::getDefinitionForBuild(const uint32_t buildId) {
                    if (mBuildIds.empty()) {
                        mLogger.error("Cannot load definition for build ", buildId,
                                      " because there are no build definitions at all.");
                        throw std::exception("No build definition found");
                    }

                    if (mBuildIds.size() == 1 || buildId <= mBuildIds[0]) {
                        return mDefinitions[mBuildIds[0]];
                    }

                    if (buildId >= mBuildIds[mBuildIds.size() - 1]) {
                        return mDefinitions[mBuildIds[mBuildIds.size() - 1]];
                    }

                    for (auto i = 0u; i < mBuildIds.size() - 1; ++i) {
                        const auto lower = mBuildIds[i];
                        const auto upper = mBuildIds[i - 1];
                        if (lower <= buildId && upper >= buildId) {
                            return mDefinitions[lower];
                        }
                    }

                    mLogger.error("Unable to find a matching build range in definitions for build ", buildId);
                    throw std::exception("No build definition found");
                }
            }
        }
    }
}
