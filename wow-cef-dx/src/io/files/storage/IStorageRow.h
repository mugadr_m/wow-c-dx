#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                class IStorageRow {
                public:
                    virtual ~IStorageRow() = default;

                    virtual std::string getString(uint32_t fieldIndex) = 0;
                    virtual int32_t getInt32(uint32_t fieldIndex) = 0;

                    virtual uint32_t getUint32(uint32_t fieldIndex) {
                        return static_cast<uint32_t>(getInt32(fieldIndex));
                    }

					virtual float getFloat(uint32_t fieldIndex) = 0;

                    virtual std::vector<float> getFloatArray(uint32_t fieldIndex) = 0;
					virtual std::vector<uint32_t> getUint32Array(uint32_t fieldIndex) = 0;
					virtual std::vector<std::string> getStringArray(uint32_t fieldIndex) = 0;
                };

                SHARED_PTR(IStorageRow);
            }
        }
    }
}
