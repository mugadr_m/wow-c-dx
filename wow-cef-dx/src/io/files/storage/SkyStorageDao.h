#pragma once
#include "IStorageFile.h"
#include "SkyStorageStructures.h"
#include "DefinitionManager.h"
#include "MapLightEntry.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                class SkyStorageDao {
                    static utils::Logger<SkyStorageDao> mLogger;

                    std::function<IStorageFilePtr(const std::string&)> mFileLoadFunction;

                    IStorageFilePtr mLightData;
                    IStorageFilePtr mLight;
                    IStorageFilePtr mLightParams;

                    IStorageFilePtr mZoneLight;
                    IStorageFilePtr mZoneLightPoint;

                    DefinitionManager::BuildDefinition mDefinitions;

                    std::map<uint32_t, std::vector<MapLightEntry>> mMapLights;
					std::map<uint32_t, std::vector<ZoneLight>> mZoneLights;

					MapLightEntry mFallbackGlobalLight;

					std::unordered_map<uint32_t, LightParamsEntry> loadLightParamsStorage() const;
                    std::vector<LightDataEntry> loadLightDataStorage() const;
                    std::vector<LightEntry> loadLightStorage() const;
                    std::map<uint32_t, ZoneLight> loadZoneLightStorage() const;
                    void loadZoneLightPointStorage(std::map<uint32_t, ZoneLight>& zoneLights) const;

                    static uint32_t findFieldDefinition(const DefinitionManager::TableDefinition& definition,
                                                        const std::string& name);

                    void fillLights();
                    void fillZoneLights();
                public:
                    explicit SkyStorageDao(std::function<IStorageFilePtr(const std::string&)> fileLoadFunction,
                                           DefinitionManager::BuildDefinition definitions);

					std::vector<MapLightEntry> getLightsForMap(uint32_t mapId) const;
					std::vector<ZoneLight> getZoneLightsForMap(uint32_t mapId) const;

					MapLightEntry getFallbackGlobalLight() const {
						return mFallbackGlobalLight;
					}
                };

                SHARED_PTR(SkyStorageDao);
            }
        }
    }
}
