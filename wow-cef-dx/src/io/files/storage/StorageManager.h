#pragma once
#include "cdi/ApplicationContext.h"
#include "io/files/DataLoadCompleteEvent.h"
#include "IStorageFile.h"
#include "io/files/IFileProvider.h"
#include "DefinitionManager.h"
#include "MapStorageDao.h"
#include "MapListQueryEvent.h"
#include "SkyStorageDao.h"
#include "LiquidStorageDao.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				class StorageManager {
					static utils::Logger<StorageManager> mLogger;

					IStorageFilePtr getStorageFileByName(const IFileProviderPtr& fileProvider, const std::string& name);
					DefinitionManager::BuildDefinition mStorageDefinitions;

					MapStorageDaoPtr mMapStorageDao;
					SkyStorageDaoPtr mSkyStorageDao;
					LiquidStorageDaoPtr mLiquidStorageDao;

					void onLoadComplete(DataLoadCompleteEvent&);
					void onQueryMaps(MapListQueryEvent& event) const;
				public:
					void postConstruct(const cdi::ApplicationContextPtr& ctx);
					static void attachUiEvents(const cdi::ApplicationContextPtr& ctx);

					MapStorageDaoPtr getMapStroageDao() const { return mMapStorageDao; }
					SkyStorageDaoPtr getSkyStorageDao() const { return mSkyStorageDao; }
					LiquidStorageDaoPtr getLiquidStorageDao() const { return mLiquidStorageDao; }
                    
				};

				SHARED_PTR(StorageManager);
			}
		}
	}
}
