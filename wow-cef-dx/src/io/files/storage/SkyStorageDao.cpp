#include "stdafx.h"
#include "SkyStorageDao.h"
#include <utility>
#include "utils/Constants.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                utils::Logger<SkyStorageDao> SkyStorageDao::mLogger;

                std::unordered_map<uint32_t, LightParamsEntry> SkyStorageDao::loadLightParamsStorage() const {
					const auto& definition = mDefinitions.at("LightParams");

					const auto idField = findFieldDefinition(definition, "ID");
					const auto riverNearAlphaField = findFieldDefinition(definition, "WaterShallowAlpha");
					const auto riverFarAlphaField = findFieldDefinition(definition, "WaterDeepAlpha");
					const auto oceanNearAlphaField = findFieldDefinition(definition, "OceanShallowAlpha");
					const auto oceanFarAlphaField = findFieldDefinition(definition, "OceanDeepAlpha");

					std::unordered_map<uint32_t, LightParamsEntry> ret;
                    for(auto i = 0u; i < mLightParams->getRowCount(); ++i) {
						const auto row = mLightParams->getRowByIndex(i);

						const auto id = row->getUint32(idField);
						LightParamsEntry entry{};
						entry.id = id;
						entry.waterNearAlpha = row->getFloat(riverNearAlphaField);
						entry.waterFarAlpha = row->getFloat(riverFarAlphaField);
						entry.oceanNearAlpha = row->getFloat(oceanNearAlphaField);
						entry.oceanFarAlpha = row->getFloat(oceanFarAlphaField);

						ret.emplace(id, entry);
                    }

					return ret;
                }

                std::vector<LightDataEntry> SkyStorageDao::loadLightDataStorage() const {
                    const auto& definition = mDefinitions.at("LightData");

                    const auto idField = findFieldDefinition(definition, "ID");
                    const auto skyIdField = findFieldDefinition(definition, "LightParamId");
                    const auto timeValuesField = findFieldDefinition(definition, "Time");
                    const auto diffuseField = findFieldDefinition(definition, "DiffuseColor");
                    const auto ambientField = findFieldDefinition(definition, "AmbientColor");
                    const auto sky0Field = findFieldDefinition(definition, "SkyTopColor");
                    const auto sky1Field = findFieldDefinition(definition, "SkyMiddleColor");
                    const auto sky2Field = findFieldDefinition(definition, "SkyBand1Color");
                    const auto sky3Field = findFieldDefinition(definition, "SkyBand2Color");
                    const auto sky4Field = findFieldDefinition(definition, "SkySmogColor");
                    const auto fogField = findFieldDefinition(definition, "SkyFogColor");
					const auto sunField = findFieldDefinition(definition, "SunColor");
					const auto cloudSunField = findFieldDefinition(definition, "CloudSunColor");
					const auto cloudEmissiveField = findFieldDefinition(definition, "CloudEmissiveColor");
					const auto cloud1AmbientField = findFieldDefinition(definition, "CloudLayer1AmbientColor");
					const auto cloud2AmbientField = findFieldDefinition(definition, "CloudLayer2AmbientColor");
					const auto oceanNearField = findFieldDefinition(definition, "OceanShallowColor");
					const auto oceanFarField = findFieldDefinition(definition, "OceanDeepColor");
					const auto riverNearField = findFieldDefinition(definition, "RiverShallowColor");
					const auto riverFarField = findFieldDefinition(definition, "RiverDeepColor");

                    std::vector<LightDataEntry> lightDataEntries;

                    for (auto i = 0u; i < mLightData->getRowCount(); ++i) {
                        const auto& row = mLightData->getRowByIndex(i);
                        LightDataEntry entry{};
                        entry.id = row->getUint32(idField);
                        entry.skyId = row->getUint32(skyIdField);
                        entry.timestamp = row->getUint32(timeValuesField);
                        entry.data[0] = row->getUint32(diffuseField);
                        entry.data[1] = row->getUint32(ambientField);
                        entry.data[2] = row->getUint32(sky0Field);
                        entry.data[3] = row->getUint32(sky1Field);
                        entry.data[4] = row->getUint32(sky2Field);
                        entry.data[5] = row->getUint32(sky3Field);
                        entry.data[6] = row->getUint32(sky4Field);
                        entry.data[7] = row->getUint32(fogField);
						entry.data[8] = row->getUint32(sunField);
						entry.data[9] = row->getUint32(cloudSunField);
						entry.data[10] = row->getUint32(cloudEmissiveField);
						entry.data[11] = row->getUint32(cloud1AmbientField);
						entry.data[12] = row->getUint32(cloud2AmbientField);
						entry.data[13] = row->getUint32(oceanNearField);
						entry.data[14] = row->getUint32(oceanFarField);
						entry.data[15] = row->getUint32(riverNearField);
						entry.data[16] = row->getUint32(riverFarField);

                        // TODO: More fields

                        lightDataEntries.push_back(entry);
                    }

                    return lightDataEntries;
                }

                std::vector<LightEntry> SkyStorageDao::loadLightStorage() const {
                    const auto& definition = mDefinitions.at("Light");

                    const auto idField = findFieldDefinition(definition, "ID");
                    const auto coordsField = findFieldDefinition(definition, "GameCoords");
                    const auto innerField = findFieldDefinition(definition, "GameFalloffStart");
                    const auto outerField = findFieldDefinition(definition, "GameFalloffEnd");
                    const auto mapIdField = findFieldDefinition(definition, "ContinentId");
                    const auto paramsField = findFieldDefinition(definition, "LightParamsId");
                    
                    std::vector<LightEntry> lightEntries;
                    for (auto i = 0u; i < mLight->getRowCount(); ++i) {
                        const auto& row = mLight->getRowByIndex(i);
                        LightEntry le{};
                        le.id = row->getUint32(idField);
                        const auto position = row->getFloatArray(coordsField);
						le.position.set(utils::MAP_MID_POINT - position[1], utils::MAP_MID_POINT - position[0], position[2]);

                        le.innerRadius = row->getFloat(innerField);
                        le.outerRadius = row->getFloat(outerField);
                        le.continentId = row->getInt32(mapIdField);
                        const auto paramsArray = row->getUint32Array(paramsField);
                        memcpy(le.lightParamsId, paramsArray.data(), paramsArray.size() * sizeof(uint32_t));
                        lightEntries.push_back(le);
                    }

                    return lightEntries;
                }

                std::map<uint32_t, ZoneLight> SkyStorageDao::loadZoneLightStorage() const {
                    const auto& definition = mDefinitions.at("ZoneLight");

                    const auto idField = findFieldDefinition(definition, "ID");
                    const auto nameField = findFieldDefinition(definition, "Name");
                    const auto mapIdField = findFieldDefinition(definition, "MapId");
                    const auto lightIdField = findFieldDefinition(definition, "LightId");
                    const auto flagsField = findFieldDefinition(definition, "Flags");

                    std::map<uint32_t, ZoneLight> zoneLights;
                    for (auto i = 0u; i < mZoneLight->getRowCount(); ++i) {
                        const auto& row = mZoneLight->getRowByIndex(i);

                        ZoneLight zl{};
                        zl.id = row->getUint32(idField);
                        zl.name = row->getString(nameField);
                        zl.mapId = row->getUint32(mapIdField);
                        zl.lightId = row->getUint32(lightIdField);
                        zl.flags = row->getUint32(flagsField);
                        zoneLights.emplace(zl.id, zl);
                    }

                    return zoneLights;
                }

                void SkyStorageDao::loadZoneLightPointStorage(std::map<uint32_t, ZoneLight>& zoneLights) const {
                    const auto& definition = mDefinitions.at("ZoneLightPoint");

                    const auto idField = findFieldDefinition(definition, "ID");
                    const auto positionField = findFieldDefinition(definition, "Pos");
                    const auto orderField = findFieldDefinition(definition, "PointOrder");
                    const auto keyField = findFieldDefinition(definition, "ZoneLightId");

                    for (auto i = 0u; i < mZoneLightPoint->getRowCount(); ++i) {
                        const auto& row = mZoneLightPoint->getRowByIndex(i);

                        const auto zoneLightKey = row->getUint32(keyField);
                        const auto itr = zoneLights.find(zoneLightKey);
                        if (itr == zoneLights.end()) {
                            continue;
                        }

                        ZoneLightPoint zlp{};
                        zlp.id = row->getUint32(idField);
                        const auto position = row->getFloatArray(positionField);
                        zlp.position.set(utils::MAP_MID_POINT - position[1], utils::MAP_MID_POINT + position[0]);
                        zlp.pointOrder = row->getUint32(orderField);
                        zlp.zoneLightId = zoneLightKey;
                        itr->second.polygonPoints.emplace_back(zlp);
                    }

                    for (auto& itr : zoneLights) {
                        std::sort(itr.second.polygonPoints.begin(), itr.second.polygonPoints.end(),
                                  [](const auto& a1, const auto& a2) {
                                      return a1.pointOrder < a2.pointOrder;
                                  });
                    }
                }

                uint32_t SkyStorageDao::findFieldDefinition(const DefinitionManager::TableDefinition& definition,
                                                            const std::string& name) {
                    const auto itr = std::find_if(definition.begin(), definition.end(),
                                                  [&](const auto& value) { return value.getFieldName() == name; });
                    if (itr == definition.end()) {
                        mLogger.error("Unable to find definition for field: ", name);
                        throw std::exception("Unable to find definition for field");
                    }

                    return static_cast<uint32_t>(std::distance(definition.begin(), itr));
                }

                void SkyStorageDao::fillLights() {
                    mLightData = mFileLoadFunction("LightData");
                    mLight = mFileLoadFunction("Light");
                    mLightParams = mFileLoadFunction("LightParams");

                    const auto lightDataEntries = loadLightDataStorage();
                    const auto lights = loadLightStorage();
					const auto lightParams = loadLightParamsStorage();

                    std::multimap<uint32_t, LightDataEntry> dataEntries;
                    for (const auto& lightData : lightDataEntries) {
                        dataEntries.emplace(lightData.skyId, lightData);
                    }

                    for (auto& light : lights) {
                        const auto defaultSky = light.lightParamsId[0];
                        const auto lightDataEntry = dataEntries.equal_range(defaultSky);
                        const auto numElements = std::distance(lightDataEntry.first, lightDataEntry.second);
                        MapLightEntry mapLight{};
                        mapLight.lightEntry = light;
                        if (numElements != 0) {
                            for (auto itr = lightDataEntry.first; itr != lightDataEntry.second; ++itr) {
                                mapLight.lightDataEntries.emplace_back(itr->second);
                            }

                            std::sort(mapLight.lightDataEntries.begin(), mapLight.lightDataEntries.end(), [](const auto& l1, const auto& l2) {
								return l1.timestamp < l2.timestamp;
							});
                        }

						mapLight.lightParams = lightParams.at(light.lightParamsId[0]);

						auto itr = mMapLights.find(light.continentId);
                        if(itr == mMapLights.end()) {
							mMapLights.emplace(light.continentId, std::vector<MapLightEntry>{ mapLight });
                        } else {
							itr->second.emplace_back(mapLight);
                        }

                        if(light.id == 1) {
							mFallbackGlobalLight = mapLight;
							mFallbackGlobalLight.lightEntry.outerRadius = 0.0f;
							mFallbackGlobalLight.lightEntry.innerRadius = 0.0f;
							mFallbackGlobalLight.lightEntry.position.set(0, 0, 0);
                        }
                    }
                }

                void SkyStorageDao::fillZoneLights() {
                    mZoneLight = mFileLoadFunction("ZoneLight");
                    mZoneLightPoint = mFileLoadFunction("ZoneLightPoint");

                    auto zoneLights = loadZoneLightStorage();
                    loadZoneLightPointStorage(zoneLights);

                    for (auto& pair : zoneLights) {
						auto zoneItr = mZoneLights.find(pair.second.mapId);
                        if(zoneItr == mZoneLights.end()) {
							mZoneLights.emplace(pair.second.mapId, std::vector<ZoneLight> { pair.second });
                        } else {
							zoneItr->second.emplace_back(pair.second);
                        }

                        auto itr = mMapLights.find(pair.second.mapId);
                        if (itr == mMapLights.end()) {
                            continue;
                        }

                        auto& mapLights = itr->second;
                        const auto lightEntryItr =
                            std::find_if(mapLights.begin(), mapLights.end(),
                                         [&](const auto& mapLight) {
                                             return mapLight.lightEntry.id == pair.second.lightId;
                                         });

                        if (lightEntryItr == mapLights.end()) {
                            continue;
                        }

						auto mapZlEntries = mZoneLights.find(pair.second.mapId);
                        const auto zlEntry = std::find_if(mapZlEntries->second.begin(), mapZlEntries->second.end(), [&](const ZoneLight& zl) {
							return zl.id == pair.second.id;
						});

						(*zlEntry).lightDataEntries = lightEntryItr->lightDataEntries;
						(*zlEntry).lightParams = lightEntryItr->lightParams;
                        mapLights.erase(lightEntryItr);
                    }
                }

                SkyStorageDao::SkyStorageDao(std::function<IStorageFilePtr(const std::string&)> fileLoadFunction,
                                             DefinitionManager::BuildDefinition definitions)
                    : mFileLoadFunction(std::move(fileLoadFunction)),
                      mDefinitions(std::move(definitions)) {
                    fillLights();
                    fillZoneLights();
                }

                std::vector<MapLightEntry> SkyStorageDao::getLightsForMap(uint32_t mapId) const {
					const auto itr = mMapLights.find(mapId);
                    if(itr == mMapLights.end()) {
						return {};
                    }

					return itr->second;
                }

                std::vector<ZoneLight> SkyStorageDao::getZoneLightsForMap(uint32_t mapId) const {
					const auto itr = mZoneLights.find(mapId);
                    if(itr == mZoneLights.end()) {
						return {};
                    }

					return itr->second;
                }
            }
        }
    }
}
