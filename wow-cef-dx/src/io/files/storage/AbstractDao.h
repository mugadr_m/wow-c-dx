#pragma once
#include "DefinitionManager.h"


namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                class AbstractDao abstract {
                private:
					static utils::Logger<AbstractDao> mDaoLogger;

                protected:
					static uint32_t findFieldDefinition(const DefinitionManager::TableDefinition& definition,
						const std::string& name);
                };
            }
        }
    }
}
