#pragma once
#include "SkyStorageStructures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                struct MapLightEntry {
					std::vector<LightDataEntry> lightDataEntries;
					LightEntry lightEntry;
					LightParamsEntry lightParams;
                };
            }
        }
    }
}
