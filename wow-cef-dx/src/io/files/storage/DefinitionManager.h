#pragma once
#include "cdi/ApplicationContext.h"
#include <rapidxml.hpp>
#include <unordered_map>

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				enum class FieldType {
					STRING,
					UINT,
					INT,
					USHORT,
					SHORT,
					BYTE,
					SBYTE,
					FLOAT,
					LOC,
					ULONG,
					LONG,
					BOOL,
                    REFERENCE,
					UNKNOWN
				};

				class FieldDefinition {
					std::string mFieldName;
					FieldType mFieldType = FieldType::UNKNOWN;
					bool mIsIndex = false;
					uint32_t mArraySize = 1;

				public:
					FieldDefinition(std::string fieldName, FieldType fieldType);
					FieldDefinition() = default;

					const std::string& getFieldName() const {
						return mFieldName;
					}

					void setFieldName(const std::string& fieldName) {
						mFieldName = fieldName;
					}

					FieldType getFieldType() const {
						return mFieldType;
					}

					void setFieldType(const FieldType fieldType) {
						mFieldType = fieldType;
					}

					bool isIsIndex() const {
						return mIsIndex;
					}

					void setIsIndex(const bool isIndex) {
						mIsIndex = isIndex;
					}

					uint32_t getArraySize() const {
						return mArraySize;
					}

					void setArraySize(const uint32_t arraySize) {
						mArraySize = arraySize;
					}

					uint32_t getByteSize() const;
					bool isIntegral() const;
				};

				class DefinitionManager {
				public:
					typedef std::vector<FieldDefinition> TableDefinition;
					typedef std::unordered_map<std::string, TableDefinition> BuildDefinition;
				private:

					static utils::Logger<DefinitionManager> mLogger;

					static std::map<std::string, FieldType> mTypeMap;

					std::unordered_map<uint32_t, BuildDefinition> mDefinitions;
					std::vector<uint32_t> mBuildIds;

					static FieldType parseFieldType(const std::string& type);

					void handleDefinition(const std::string& filePath);
					void handleTable(const rapidxml::xml_node<char>* tableNode);
					static void parseTableFields(const rapidxml::xml_node<char>* tableNode, TableDefinition& fieldMap);
				public:
					void postConstruct(const cdi::ApplicationContextPtr& context);

					BuildDefinition getDefinitionForBuild(uint32_t buildId);
				};

				SHARED_PTR(DefinitionManager);
			}
		}
	}
}
