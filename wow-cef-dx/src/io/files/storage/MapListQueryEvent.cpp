#include "stdafx.h"
#include "MapListQueryEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				rapidjson::Document MapListQueryEvent::getResponseJson() const {
					rapidjson::Document document;
					auto& allocator = document.GetAllocator();

					auto& root = document.SetObject();
					rapidjson::Value mapListArray(rapidjson::kArrayType);
					for(auto& mapEntry : mMapList) {
						const auto mapId = mapEntry.getMapId();
						const auto& mapName = mapEntry.getMapName();
						const auto& mapDirectory = mapEntry.getMapDirectory();
						const auto& lsLink = mapEntry.getLoadingScreenLink();
						const auto instanceType = mapEntry.getInstanceType();

						rapidjson::Value mapIdValue(rapidjson::kNumberType);
						mapIdValue.SetInt(mapId);

						rapidjson::Value mapNameValue(rapidjson::kStringType);
						mapNameValue.SetString(mapName.c_str(), allocator);

						rapidjson::Value mapDirectoryValue(rapidjson::kStringType);
						mapDirectoryValue.SetString(mapDirectory.c_str(), allocator);

						rapidjson::Value lsLinkValue(rapidjson::kStringType);
						lsLinkValue.SetString(lsLink.c_str(), allocator);

						rapidjson::Value instanceTypeValue(rapidjson::kNumberType);
						instanceTypeValue.SetInt(instanceType);

						rapidjson::Value objectEntry(rapidjson::kObjectType);
						objectEntry.AddMember("mapId", mapIdValue, allocator);
						objectEntry.AddMember("mapName", mapNameValue, allocator);
						objectEntry.AddMember("mapDirectory", mapDirectoryValue, allocator);
						objectEntry.AddMember("loadingScreen", lsLinkValue, allocator);
						objectEntry.AddMember("instanceType", instanceType, allocator);
						mapListArray.PushBack(objectEntry, allocator);
					}

					root.AddMember("maps", mapListArray, allocator);

					return document;
				}
			}
		}
	}
}