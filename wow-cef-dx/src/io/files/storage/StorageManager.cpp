#include "stdafx.h"
#include "StorageManager.h"
#include "io/files/IFileProvider.h"
#include "wdbx/Wdc2File.h"
#include "ui/events/EventManager.h"
#include "io/files/DataLoadProgressEvent.h"
#include "io/files/DataInitCompleteEvent.h"
#include "MapListQueryEvent.h"
#include <boost/timer/timer.hpp>
#include "io/files/sky/SkyManager.h"
#include "io/files/m2/M2FileFactory.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace storage {
                utils::Logger<StorageManager> StorageManager::mLogger;

                IStorageFilePtr StorageManager::getStorageFileByName(const IFileProviderPtr& fileProvider,
                                                                     const std::string& name) {
                    auto itr = mStorageDefinitions.find(name);
                    if (itr == mStorageDefinitions.end()) {
                        mLogger.error("No build definition found for table: ", name);
                        throw std::exception("No table definition found");
                    }

                    std::stringstream strm;
                    strm << "DBFilesClient\\" << name << ".db2";
                    try {
                        auto db2File = fileProvider->openFile(strm.str());

                        boost::timer::cpu_timer timer;
                        const auto signature = db2File.read<uint32_t>();
                        switch (signature) {
                        case '2CDW': {
                            auto ret = std::make_shared<wdbx::Wdc2File>(itr->second, db2File);
                            mLogger.info(timer.format());
                            return ret;
                        }

                        default:
                            mLogger.error("Table file is in an unrecognized format: ", strm.str());
                            throw std::exception("Unknown table format");

                        }
                    } catch (std::exception&) {
                        mLogger.error("Table not found: ", strm.str());
                        throw std::exception("Unknown table");
                    }

                }

                void StorageManager::onLoadComplete(DataLoadCompleteEvent& event) {
                    auto applicationManager = cdi::ApplicationContext::instance();
                    applicationManager->produce<ui::events::EventManager>()->onApplicationEvent(
                        DataLoadProgressEvent("Loading DBC/DB2 files..."));
                    const auto fileManager = applicationManager->produce<IFileProvider>();
                    mStorageDefinitions = applicationManager->produce<DefinitionManager>()->getDefinitionForBuild(
                        event.getBuildNumber());

                    auto fileLoadFunction = std::bind(&StorageManager::getStorageFileByName, this, fileManager,
                                                      std::placeholders::_1);

                    mMapStorageDao = std::make_shared<MapStorageDao>(fileManager, mStorageDefinitions,
                                                                     event.getBuildNumber(),
                                                                     fileLoadFunction);

                    mSkyStorageDao = std::make_shared<SkyStorageDao>(fileLoadFunction, mStorageDefinitions);
					mLiquidStorageDao = std::make_shared<LiquidStorageDao>(fileLoadFunction, mStorageDefinitions);

                    applicationManager->produce<sky::SkyManager>()->onLoad();

                    applicationManager->produce<ui::events::EventManager>()->
                                        onApplicationEvent(DataInitCompleteEvent());

                }

                void StorageManager::onQueryMaps(MapListQueryEvent& event) const {
                    const auto maps = mMapStorageDao->getMaps();
                    event.setMapEntries(maps);
                }

                void StorageManager::postConstruct(const cdi::ApplicationContextPtr& ctx) {
                    ctx->registerEvent<DataLoadCompleteEvent>(
                        std::bind(&StorageManager::onLoadComplete, this, std::placeholders::_1));
                    ctx->registerEvent<MapListQueryEvent>(
                        std::bind(&StorageManager::onQueryMaps, this, std::placeholders::_1));
                }

                void StorageManager::attachUiEvents(const cdi::ApplicationContextPtr& ctx) {
                    auto eventMgr = ctx->produce<ui::events::EventManager>();
                    eventMgr->registerEvent<MapListQueryEvent>();
                }
            }
        }
    }
}
