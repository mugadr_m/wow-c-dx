#pragma once
#include <document.h>
#include "MapStorageDao.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace storage {
				class MapListQueryEvent {
					std::vector<MapEntry> mMapList;

				public:
					void addMapEntry(const MapEntry& mapEntry) {
						mMapList.push_back(mapEntry);
					}

					void setMapEntries(const std::vector<MapEntry>& mapEntries) {
						mMapList = mapEntries;
					}

				    void toJson(rapidjson::Document& document) const {
				    }

				    rapidjson::Document getResponseJson() const;
				    static MapListQueryEvent fromJson(const rapidjson::Document& document) { return {}; }
				};
			}
		}
	}
}
