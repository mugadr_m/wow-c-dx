#include "stdafx.h"
#include "TextureManager.h"
#include "gx/ContextCreatedEvent.h"
#include "io/files/DataLoadCompleteEvent.h"
#include "utils/String.h"
#include "BlpTextureLoader.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace blp {
                utils::Logger<TextureManager> TextureManager::mLogger;

                void TextureManager::loadThreadCallback() {
                    while (mIsRunning) {
                        LoadItem loadItem;
                        {
                            std::unique_lock<std::mutex> l(mWorkLock);
                            if (!mLoadItems.empty()) {
                                loadItem = mLoadItems.front();
                                mLoadItems.pop_front();
                            } else {
                                mWorkEvent.wait(l, [&]() { return !mIsRunning || !mLoadItems.empty(); });
                                continue;
                            }
                        }

                        handleTextureLoad(loadItem);
                    }
                }

                void TextureManager::handleTextureLoad(const LoadItem& loadItem) const {
                    if (loadItem.texture == nullptr || loadItem.fileName.empty()) {
                        return;
                    }

                    BinaryReader fileReader{{}};
                    try {
                        fileReader = mFileProvider->openFile(loadItem.fileName);
                    } catch (std::exception& ) {
                        mLogger.error("Error loading texture: ", loadItem.fileName, ". File not found");
                        return;
                    }

                    BlpTextureLoader loader{fileReader};

                    mGxContext->getDispatcher()->pushWork([loader, loadItem]() {
						loadItem.texture->loadFromBlp(loader.getWidth(), loader.getHeight(), loader.getFormat(),
							loader.getRowPitches(), loader.getLayers());
					});
                }

                void TextureManager::postConstruct(const cdi::ApplicationContextPtr& context) {
                    context->registerEvent<DataLoadCompleteEvent>([=](auto& /*event*/) {
                        mFileProvider = context->produce<IFileProvider>();
                    });

                    context->registerEvent<gx::ContextCreatedEvent>([=](auto&) {
                        mGxContext = context->produce<gx::GxContext>();
                    });

                    for (auto i = 0u; i < std::thread::hardware_concurrency(); ++i) {
                        mLoaderThreads.emplace_back(std::bind(&TextureManager::loadThreadCallback, this));
                    }
                }

                void TextureManager::preDestroy() {
                    mIsRunning = false;
					mWorkEvent.notify_all();
                    for (auto& thread : mLoaderThreads) {
                        if (thread.joinable()) {
                            thread.join();
                        }
                    }
                }

                gx::TexturePtr TextureManager::getTexture(const std::string& texture, bool blankDefault) {
                    static std::hash<std::string> hasher;
                    const auto upperTexture = utils::String::toUpper(texture);
                    const auto hash = hasher(upperTexture);
                    gx::TexturePtr retTexture;
                    {
                        std::lock_guard<std::mutex> l(mCacheLock);
                        const auto itr = mTextureCache.find(hash);
                        if (itr != mTextureCache.end()) {
                            const auto ret = itr->second.lock();
                            if (ret != nullptr) {
                                return ret;
                            }

                            mTextureCache.erase(itr);
                        }

                        retTexture = mGxContext->createTexture(blankDefault);
						retTexture->setTag(texture);
                        mTextureCache.emplace(hash, retTexture);
                    }

                    {
                        std::unique_lock<std::mutex> l(mWorkLock);
                        mLoadItems.emplace_back(retTexture, texture);
						mWorkEvent.notify_one();
                    }


                    return retTexture;
                }

                void TextureManager::releaseTexture(gx::TexturePtr texture) {
					static std::hash<std::string> hasher;

					const auto tag = texture->getTag();
                    if(tag.empty()) {
						return;
                    }

					const auto upperTexture = utils::String::toUpper(tag);
					const auto hash = hasher(upperTexture);
					texture.reset();
					{
						std::lock_guard<std::mutex> l(mCacheLock);
						const auto itr = mTextureCache.find(hash);
						if (itr != mTextureCache.end()) {
							const auto ret = itr->second.lock();
							if (ret != nullptr) {
								return;
							}

							mTextureCache.erase(itr);
						}
					}
                }

                bool TextureManager::exists(const std::string& texture) {
					static std::hash<std::string> hasher;
					const auto upperTexture = utils::String::toUpper(texture);
					const auto hash = hasher(upperTexture);

					{
						std::lock_guard<std::mutex> l(mCacheLock);
						const auto itr = mTextureCache.find(hash);
                        if(itr != mTextureCache.end()) {
							return true;
                        }
					}

					return mFileProvider->exists(texture);
                }
            }
        }
    }
}
