#include "stdafx.h"
#include "BlpHeader.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace blp {

                BlpImageFormat BlpHeader::getFormat() const {
					switch (compression) {
					case 1:
						return BlpImageFormat::RGB_PALETTE;

					case 2: {
						switch (alphaCompression) {
						case 0:
							return BlpImageFormat::BC1;
						case 1:
							return BlpImageFormat::BC2;
						case 7:
							return BlpImageFormat::BC3;
						default:
							return BlpImageFormat::UNKNOWN;
						}
					}
					case 3:
						return BlpImageFormat::RGB;

					default:
						return BlpImageFormat::UNKNOWN;
					}
                }
            }
        }
    }
}
