#include "stdafx.h"
#include "BlpTextureLoader.h"
#include "BlpHeader.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace blp {
				static const uint32_t ALPHA_LOOKUP1[] = { 0x00, 0xFF };
				static const uint32_t ALPHA_LOOKUP4[] = {
					0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
					0xCC, 0xDD, 0xEE, 0xFF
				};

                void BlpTextureLoader::loadAsDxtImage(BinaryReader& reader, const BlpHeader& header,
                                                      BlpImageFormat format) {
                    uint32_t blockSize;
                    switch (format) {
                    case BlpImageFormat::BC1:
                        mFormat = gx::TextureDataFormat::BC1;
                        blockSize = 8;
                        break;

                    case BlpImageFormat::BC2:
                        mFormat = gx::TextureDataFormat::BC2;
                        blockSize = 16;
                        break;

                    case BlpImageFormat::BC3:
                        mFormat = gx::TextureDataFormat::BC3;
                        blockSize = 16;
                        break;

                    default:
                        throw std::exception("Invalid texture format specified");
                    }

                    for (auto i = 0u; i < 16; ++i) {
                        if (header.sizes[i] == 0 || header.offsets[i] == 0) {
                            break;
                        }

                        reader.seek(header.offsets[i]);
                        const auto curWidth = std::max(1u, header.width >> i);
                        mRowPitches.push_back(((curWidth + 3) / 4) * blockSize);
                        std::vector<uint8_t> data(header.sizes[i]);
                        reader.read(data);
                        mLayers.emplace_back(data);
                    }
                }

                void BlpTextureLoader::loadUncompressed(BinaryReader& reader, const BlpHeader& header) {
					mFormat = gx::TextureDataFormat::ARGB8;
                    for (auto i = 0u; i < 16; ++i) {
                        if (header.sizes[i] == 0 || header.offsets[i] == 0) {
                            break;
                        }

                        reader.seek(header.offsets[i]);
                        const auto curWidth = std::max(1u, header.width >> i);
                        mRowPitches.push_back(curWidth * 4);
                        std::vector<uint8_t> data(header.sizes[i]);
                        reader.read(data);
                        mLayers.emplace_back(data);
                    }
                }

                void BlpTextureLoader::loadPaletteFastPath(BinaryReader& reader, const BlpHeader& header) {
					reader.seek(sizeof(BlpHeader));
					uint32_t palette[256]{};
					reader.read(palette);

                    for(auto i = 0; i < 16; ++i) {
                        if(header.sizes[i] == 0 || header.offsets[i] == 0) {
							break;
                        }

						reader.seek(header.offsets[i]);
						std::vector<uint8_t> indices(header.sizes[i]);
						reader.read(indices);

						const auto width = std::max(header.width >> i, 1u);
						const auto height = std::max(header.height >> i, 1u);
						std::vector<uint32_t> layerData(width * height);

						const auto numEntries = width * height;
						for (auto k = 0u; k < width * height; ++k) {
							const auto indexColor = indices[k];
							const auto alpha = indices[k + numEntries];
							auto color = palette[indexColor];
							color &= 0x00FFFFFF;
							color |= alpha << 24;
							layerData[k] = color;
                        }

						mRowPitches.emplace_back(width * 4);
						std::vector<uint8_t> actualData(layerData.size() * 4);
						memcpy(actualData.data(), layerData.data(), actualData.size());
						mLayers.emplace_back(actualData);
                    }
                }

                void BlpTextureLoader::loadPaletteArgb(BinaryReader& reader, const BlpHeader& header) {
					reader.seek(sizeof(BlpHeader));
					uint32_t palette[256]{};
					reader.read(palette);
					for (auto i = 0; i < 16; ++i) {
						if (header.sizes[i] == 0 || header.offsets[i] == 0) {
							break;
						}

						reader.seek(header.offsets[i]);
						std::vector<uint8_t> indices(header.sizes[i]);
						reader.read(indices);

						const auto width = std::max(header.width >> i, 1u);
						const auto height = std::max(header.height >> i, 1u);
						std::vector<uint32_t> layerData(width * height);

						const auto numEntries = width * height;
						for (auto k = 0u; k < numEntries; ++k) {
							const auto index = indices[k];
							auto color = palette[index];
							color = (color & 0x00FFFFFFu) | 0xFF000000u;
							layerData[k] = color;
						}

						switch (header.alphaDepth) {
						case 0:
							break;

						case 1: {
							auto colorIndex = 0u;
							for (auto k = 0u; k < (numEntries / 8u); ++k) {
								const auto value = indices[k + numEntries];
								for (auto j = 0u; j < 8; ++j, ++colorIndex) {
									auto& color = layerData[colorIndex];
									color &= 0x00FFFFFF;
									color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
								}
							}

							if ((numEntries % 8) != 0) {
								const auto value = indices[numEntries + numEntries / 8];
								for (auto j = 0u; j < (numEntries % 8); ++j, ++colorIndex) {
									auto& color = layerData[colorIndex];
									color &= 0x00FFFFFF;
									color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
								}
							}

							break;
						}

						case 4: {
							auto colorIndex = 0u;
							for (auto k = 0u; k < (numEntries / 2u); ++k) {
								const auto value = indices[k + numEntries];
								const uint8_t alpha0 = ALPHA_LOOKUP4[value & 0x0Fu];
								const uint8_t alpha1 = ALPHA_LOOKUP4[value >> 4u];
								auto& color1 = layerData[colorIndex++];
								auto& color2 = layerData[colorIndex++];
								color1 = (color1 & 0x00FFFFFFu) | (alpha0 << 24u);
								color2 = (color2 & 0x00FFFFFFu) | (alpha1 << 24u);
							}

							if ((numEntries % 2) != 0) {
								const auto value = indices[numEntries + numEntries / 2];
								const uint8_t alpha = ALPHA_LOOKUP4[value & 0x0Fu];
								auto& color = layerData[colorIndex];
								color = (color & 0x00FFFFFFu) | (alpha << 24u);
							}

							break;
						}

						default: {
							throw std::exception("Invalid alpha depth");
						}
						}

						mRowPitches.emplace_back(width * 4);
						std::vector<uint8_t> actualData(layerData.size() * 4);
						memcpy(actualData.data(), layerData.data(), actualData.size());
						mLayers.emplace_back(actualData);
					}
                }

                void BlpTextureLoader::loadPalette(BinaryReader& reader, const BlpHeader& header) {
					mFormat = gx::TextureDataFormat::ARGB8;
                    if (header.alphaDepth == 8) {
                        loadPaletteFastPath(reader, header);
                    } else {
						loadPaletteArgb(reader, header);
                    }
                }

                BlpTextureLoader::BlpTextureLoader(BinaryReader& fileReader) {
                    const auto header = fileReader.read<BlpHeader>();
                    const auto format = header.getFormat();

					mWidth = header.width;
					mHeight = header.height;

                    switch (format) {
                    case BlpImageFormat::BC1:
                    case BlpImageFormat::BC2:
                    case BlpImageFormat::BC3:
                        loadAsDxtImage(fileReader, header, format);
                        break;

                    case BlpImageFormat::RGB:
                        loadUncompressed(fileReader, header);
                        break;

                    case BlpImageFormat::RGB_PALETTE:
                        loadPalette(fileReader, header);
                        break;

                    default:
                        throw std::exception("Invalid/Unknown/Unsupported BLP image format");
                    }
                }
            }
        }
    }
}
