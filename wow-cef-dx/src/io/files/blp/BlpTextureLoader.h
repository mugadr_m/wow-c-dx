#pragma once
#include "gx/Texture.h"
#include "io/BinaryReader.h"
#include "io/image/BlpImageLoader.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace blp {
                class BlpTextureLoader {
					uint32_t mWidth = 0, mHeight = 0;
					uint32_t mLayerCount = 0;
					gx::TextureDataFormat mFormat = gx::TextureDataFormat::UNKNOWN;

					std::vector<uint32_t> mRowPitches;
					std::vector<std::vector<uint8_t>> mLayers;

					void loadAsDxtImage(BinaryReader& reader, const BlpHeader& header, BlpImageFormat format);
					void loadUncompressed(BinaryReader& reader, const BlpHeader& header);

					void loadPaletteFastPath(BinaryReader& reader, const BlpHeader& header);
					void loadPaletteArgb(BinaryReader& reader, const BlpHeader& header);
					void loadPalette(BinaryReader& reader, const BlpHeader& header);

                public:
					explicit BlpTextureLoader(BinaryReader& fileReader);

                    uint32_t getWidth() const {
                        return mWidth;
                    }

                    uint32_t getHeight() const {
                        return mHeight;
                    }

                    uint32_t getLayerCount() const {
                        return mLayerCount;
                    }

                    gx::TextureDataFormat getFormat() const {
                        return mFormat;
                    }

                    const std::vector<uint32_t>& getRowPitches() const {
                        return mRowPitches;
                    }

                    const std::vector<std::vector<uint8_t>>& getLayers() const {
                        return mLayers;
                    }
                };
            }
        }
    }
}
