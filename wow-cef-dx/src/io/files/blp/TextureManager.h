#pragma once
#include "cdi/ApplicationContext.h"
#include "io/files/IFileProvider.h"
#include <unordered_map>
#include "gx/Texture.h"
#include "gx/GxContext.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace blp {
                class TextureManager {
                    struct LoadItem {
                        gx::TexturePtr texture;
                        std::string fileName;

						LoadItem() = default;
                        LoadItem(gx::TexturePtr texture, std::string fileName) : texture(std::move(texture)), fileName(std::move(fileName)) {
                            
                        }
                    };

                    static utils::Logger<TextureManager> mLogger;

                    gx::GxContextPtr mGxContext;
                    IFileProviderPtr mFileProvider;

                    std::vector<std::thread> mLoaderThreads;

                    std::mutex mCacheLock;
                    std::unordered_map<std::size_t, std::weak_ptr<gx::Texture>> mTextureCache;

                    bool mIsRunning = true;
                    std::condition_variable mWorkEvent;
                    std::mutex mWorkLock;
                    std::deque<LoadItem> mLoadItems;

                    void loadThreadCallback();
                    void handleTextureLoad(const LoadItem& loadItem) const;
                public:
                    void postConstruct(const cdi::ApplicationContextPtr& context);
                    void preDestroy();

                    gx::TexturePtr getTexture(const std::string& texture, bool blankDefault = false);
                    void releaseTexture(gx::TexturePtr texture);

					bool exists(const std::string& texture);
                };

                SHARED_PTR(TextureManager);
            }
        }
    }
}
