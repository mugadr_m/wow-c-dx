#pragma once

namespace wowc {
	namespace io {
		namespace files {
			class DataLoadCompleteEvent {
				uint32_t mBuildNumber;

			public:
				explicit DataLoadCompleteEvent(const uint32_t& buildNumber) : mBuildNumber(buildNumber) {
					
				}

				uint32_t getBuildNumber() const {
					return mBuildNumber;
				}
			};
		}
	}
}
