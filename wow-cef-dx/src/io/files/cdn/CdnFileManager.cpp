#include "stdafx.h"
#include "CdnFileManager.h"
#include "CdnGetBuildVersionsEvent.h"
#include "ui/events/EventManager.h"
#include "io/http/HttpClient.h"
#include "io/files/casc/TokenConfig.h"
#include "OpenFromCdnEvent.h"
#include "io/files/DataLoadInitEvent.h"
#include "utils/String.h"
#include <future>
#include "io/BinaryReader.h"
#include "io/files/casc/CascKeys.h"
#include <boost/thread.hpp>
#include "io/files/DataLoadProgressEvent.h"
#include "io/files/casc/BlteHandler.h"
#include "io/files/casc/EncodingFileParser.h"
#include <boost/timer/timer.hpp>
#include <boost/algorithm/hex.hpp>
#include "io/files/casc/RootParser.h"
#include "io/files/casc/Jenkins96.h"
#include "io/files/DataLoadCompleteEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				const std::string CdnFileManager::VERSION_URL = "http://us.patch.battle.net:1119/{{GAME_TYPE}}/versions";
				const std::string CdnFileManager::CDN_URL = "http://{{REGION}}.patch.battle.net:1119/{{GAME_TYPE}}/cdns";
				const std::regex GAME_TYPE_REGEX(R"(\{\{GAME_TYPE\}\})");
				const std::regex REGION_REGEX(R"(\{\{REGION\}\})");

				utils::Logger<CdnFileManager> CdnFileManager::mLogger;

				void CdnFileManager::publishProgress(const std::string& message) const {
					mEventManager->onApplicationEvent(DataLoadProgressEvent(message));
				}

				std::list<BuildVersion> CdnFileManager::getAvailableBuildVersions(const std::string& gameType) const {
					const auto url = std::regex_replace(VERSION_URL, GAME_TYPE_REGEX, gameType);
					auto client = std::make_shared<http::HttpClient>();
					client->setUrl(url);
					try {
						client->execute();
					} catch (std::exception& e) {
						mLogger.error("Error fetching build versions: ", e.what());
						return std::list<BuildVersion>();
					}

					if (client->getResponseCode() != 200) {
						return std::list<BuildVersion>();
					}

					const auto& response = client->getResponse();
					const std::string versionInfo(response.data(), response.data() + response.size());
					auto config = casc::TokenConfig();
					std::stringstream data(versionInfo);
					config.parse(data);
					std::list<BuildVersion> ret;
					for (auto& buildConfig : config) {
						ret.emplace_back(BuildVersion(
							buildConfig["Region"],
							buildConfig["BuildConfig"],
							buildConfig["CDNConfig"],
							std::stoi(buildConfig["BuildId"]),
							buildConfig["VersionsName"]
						));
					}
					return ret;
				}

				std::vector<uint8_t> CdnFileManager::downloadFileDirect(const std::string& fileKey) const {
					std::stringstream strm;
					strm << mCdnBasePath << "config/" << fileKey.substr(0, 2) << "/" << fileKey.substr(2, 2) << "/" << fileKey;
					http::HttpClient client;
					client.setUrl(strm.str());
					client.execute();
					if (client.getResponseCode() != 200) {
						mLogger.error("Error download direct file: GET ", strm.str(), " returned ", client.getResponseCode());
						throw std::exception("Error downloading file");
					}

					return client.getResponse();
				}

				std::vector<uint8_t> CdnFileManager::downloadIndexFile(const std::string& indexFile) const {
					std::stringstream strm;
					strm << mCdnBasePath << "data/" << indexFile.substr(0, 2) << "/" << indexFile.substr(2, 2) << "/" << indexFile <<
						".index";
					http::HttpClient client;
					client.setUrl(strm.str());
					client.execute();
					if (client.getResponseCode() != 200) {
						mLogger.error("Error download index file: GET ", strm.str(), " returned ", client.getResponseCode());
						throw std::exception("Error downloading file");
					}

					return client.getResponse();
				}

				std::vector<uint8_t> CdnFileManager::downloadDataDirect(const std::string& fileKey) const {
					std::stringstream strm;
					strm << mCdnBasePath << "data/" << fileKey.substr(0, 2) << "/" << fileKey.substr(2, 2) << "/" << fileKey;
					http::HttpClient client;
					client.setUrl(strm.str());
					client.execute();
					if (client.getResponseCode() != 200) {
						mLogger.error("Error downloading file: GET ", strm.str(), " returned ", client.getResponseCode());
						throw std::exception("Error downloading file");
					}

					return client.getResponse();
				}

				std::vector<uint8_t> CdnFileManager::downloadDataDirect(const casc::FileKeyMd5& fileKey) const {
					std::string hexStr;
					const auto begin = fileKey.cbegin();
					const auto end = fileKey.cend();
					const auto numBytes = std::distance(begin, end);
					hexStr.resize(numBytes * 2);
					boost::algorithm::hex_lower(begin, end, hexStr.begin());
					return downloadDataDirect(hexStr);
				}

				std::vector<uint8_t> CdnFileManager::downloadFile(const casc::IndexEntry& indexEntry) const {
					const auto archive = mCdnConfig["archives"][indexEntry.index];
					std::stringstream urlStream;
					urlStream << mCdnBasePath << "data/" << archive.substr(0, 2) << "/" << archive.substr(2, 2) << "/" << archive;
					http::HttpClient client;
					client.setUrl(urlStream.str());

					std::stringstream headerStream;
					headerStream << "bytes=" << indexEntry.offset << "-" << (indexEntry.offset + indexEntry.size);
					client.addHeader("Range", headerStream.str());
					client.execute();
					if ((client.getResponseCode() / 100) != 2) {
						mLogger.error("Error downloading file: GET ", urlStream.str(), " returned ", client.getResponseCode());
						throw std::exception("Error downloading file");
					}

					return client.getResponse();
				}

				void CdnFileManager::handleIndexFile(const std::vector<uint8_t>& content, uint32_t index) {
					BinaryReader reader(content);
					reader.seek(reader.getSize() - 12);
					const auto numElements = reader.read<uint32_t>();
					reader.seek(0);

					auto hash = casc::FileKeyMd5();
					for (auto i = 0u; i < numElements; ++i) {
						reader.read(hash);
						if (!hash.isNotZero()) {
							reader.read(hash);
							if (!hash.isNotZero()) {
								mLogger.error("Found zero key with zero backup");
								return;
							}
						}

						const auto size = reader.readIntBE();
						const auto offset = reader.readIntBE();

						mIndexEntries.emplace(std::make_pair(hash, casc::IndexEntry(index, offset, size)));
					}
				}

				void CdnFileManager::loadCdnConfig() {
					auto client = std::make_shared<http::HttpClient>();
					const auto url = std::regex_replace(std::regex_replace(CDN_URL, REGION_REGEX, mSelectedBuild.getRegion()),
					                                    GAME_TYPE_REGEX, mGameType);
					client->setUrl(url);
					client->execute();
					if (client->getResponseCode() != 200) {
						mLogger.error("Unable to download CDN configuration. GET ", url, " returned ", client->getResponseCode());
						throw std::exception("Error downloading CDN configuration");
					}

					const auto& response = client->getResponse();
					const std::string cdnInfo(response.data(), response.data() + response.size());
					std::stringstream data(cdnInfo);
					auto cdnConfig = casc::TokenConfig();
					cdnConfig.parse(data);

					std::string cdnHost;
					std::string cdnPath;

					for (auto& cdn : cdnConfig) {
						const auto name = cdn["Name"];
						const auto host = cdn["Hosts"];
						if (name.empty() || host.empty()) {
							continue;
						}

						const auto hostList = utils::String::split(host, ' ');
						if (hostList.empty()) {
							continue;
						}

						if (cdnHost.empty() || name == mSelectedBuild.getRegion()) {
							cdnHost = hostList[0];
							cdnPath = cdn["Path"];
						}
					}

					if (cdnHost.empty() || cdnPath.empty()) {
						mLogger.error("Unable to find a matching CDN configuration");
						throw std::exception("Unable to find CDN config");
					}

					mCdnBasePath = std::string("http://") + cdnHost + "/" + cdnPath + "/";
				}

				void CdnFileManager::loadConfigFiles() {
					auto rawBuildConfig = downloadFileDirect(mSelectedBuild.getBuildConfigKey());
					auto rawCdnConfig = downloadFileDirect(mSelectedBuild.getCdnConfigKey());

					std::stringstream buildConfigString(std::string(rawBuildConfig.begin(), rawBuildConfig.end()));
					std::stringstream cdnConfigString(std::string(rawCdnConfig.begin(), rawCdnConfig.end()));

					mCdnConfig.parse(cdnConfigString);
					mBuildConfig.parse(buildConfigString);
				}

				void CdnFileManager::loadIndexFiles() {
					const auto archives = mCdnConfig["archives"];
					uint32_t index = 0;

					std::vector<std::shared_future<std::vector<uint8_t>>> loadFutures;
					for (auto& archive : archives) {
						loadFutures.push_back(mThreadPool.queueWork([this, archive]() {
							return downloadIndexFile(archive);
						}));
					}

					std::for_each(loadFutures.begin(), loadFutures.end(), [this, &index, &archives](const auto& future) {
						std::stringstream strm;
						strm << "Handling index file " << archives[index];
						publishProgress(strm.str());
						handleIndexFile(future.get(), index++);
					});
				}

				void CdnFileManager::loadRootFile() {
					const auto rootKeyString = mBuildConfig["root"].at(0);
					mLogger.info("Loading root file from ", rootKeyString);
					casc::FileKeyMd5 rootKey;
					boost::algorithm::unhex(rootKeyString.begin(), rootKeyString.end(), rootKey.begin());

					const auto itr = mEncodingEntries.find(rootKey);
					if (itr == mEncodingEntries.end()) {
						throw std::exception("Unable to find root file in encoding table");
					}

					const auto encodingEntry = itr->second;
					auto rootFile = BinaryReader(downloadDataDirect(encodingEntry.key));

					publishProgress("Decoding root file...");
					casc::BlteHandler handler(rootFile);
					auto rootReader = BinaryReader(handler.getDecompressedData());

					publishProgress("Parsing root file...");
					casc::RootParser rootFileParser(mRootData, mFileIdData);
					boost::timer::cpu_timer timer;
					rootFileParser.parse(rootReader, mLocaleFlags);
					const auto times = timer.format();
					mLogger.info(times.substr(0, times.length() - 1));
				}

				void CdnFileManager::loadEncodingFile() {
					const auto encodingKeyString = mBuildConfig["encoding"].at(1);
					mLogger.info("Loading encoding file from ", encodingKeyString);

					auto encodingFile = BinaryReader(downloadDataDirect(encodingKeyString));

					publishProgress("Decoding encoding file...");
					casc::BlteHandler handler(encodingFile);
					auto encodingReader = BinaryReader(handler.getDecompressedData());

					publishProgress("Parsing encoding file...");
					casc::EncodingFileParser encodingFileParser(mEncodingEntries);
					boost::timer::cpu_timer timer;
					encodingFileParser.parse(encodingReader);
					const auto times = timer.format();
					mLogger.info(times.substr(0, times.length() - 1));
				}

				void CdnFileManager::handleBuildSelected(const BuildVersion& buildVersion) {
					async(boost::launch::async, [this, buildVersion]() {
						try {
							mEventManager->onApplicationEvent(DataLoadInitEvent());
							mSelectedBuild = buildVersion;
							publishProgress("Loading CDN configuration");
							loadCdnConfig();
							publishProgress("Loading build configuration");
							loadConfigFiles();
							publishProgress("Loading index files");
							loadIndexFiles();
							publishProgress("Downloading encoding file");
							loadEncodingFile();
							publishProgress("Downloading root file");
							loadRootFile();
							publishProgress("Done");
							const auto ctx = cdi::ApplicationContext::instance();
							ctx->registerSingleton<IFileProvider>(shared_from_this());
							DataLoadCompleteEvent completeEvent(mSelectedBuild.getBuildId());
							ctx->publishEvent(completeEvent);
						} catch (const std::exception& e) {
							mLogger.error("Unhandled exception: ", e.what());
						}
					});
				}

				void CdnFileManager::postConstruct(const cdi::ApplicationContextPtr& context) {
					mThreadPool.launchThreads();

					context->registerEvent<CdnGetBuildVersionsEvent>([this](CdnGetBuildVersionsEvent& event) {
						mGameType = event.getGameType();
						auto builds = getAvailableBuildVersions(event.getGameType());
						for (auto& build : builds) {
							event.addBuild(build);
						}
					});

					context->registerEvent<OpenFromCdnEvent>([this](OpenFromCdnEvent& event) {
						mLocaleFlags = static_cast<casc::LocaleFlags>(event.getLocale());
						handleBuildSelected(event.getBuildVersion());
					});
				}

				void CdnFileManager::preDestroy() {
					mThreadPool.shutdown();
				}

				void CdnFileManager::attachUiEvents(const cdi::ApplicationContextPtr& context) {
					auto eventMgr = context->produce<ui::events::EventManager>();
					mEventManager = eventMgr;
					eventMgr->registerEvent<CdnGetBuildVersionsEvent>();
					eventMgr->registerEvent<OpenFromCdnEvent>();
				}

				BinaryReader CdnFileManager::openFile(const std::string& fileName) {
					auto upperName = utils::String::toUpper(fileName);
					upperName = utils::String::replaceAll(upperName, "/", "\\");
					const auto hash = casc::Jenkins96().computeHash(upperName);
					const auto rootItr = mRootData.find(hash);
					if (rootItr == mRootData.end()) {
						throw std::exception("File not found");
					}

					const auto encodingItr = mEncodingEntries.find(rootItr->second);
					if (encodingItr == mEncodingEntries.end()) {
						throw std::exception("File not found");
					}

					const auto indexItr = mIndexEntries.find(encodingItr->second.key);
					if (indexItr == mIndexEntries.end()) {
						BinaryReader contentReader(downloadDataDirect(encodingItr->second.key));
						return BinaryReader(casc::BlteHandler(contentReader).getDecompressedData());
					}

					BinaryReader contentReader(downloadFile(indexItr->second));
					return BinaryReader(casc::BlteHandler(contentReader).getDecompressedData());
				}

				BinaryReader CdnFileManager::openFileByFileId(const uint32_t& fileId) {
					const auto rootItr = mFileIdData.find(fileId);
					if (rootItr == mFileIdData.end()) {
						throw std::exception("File not found");
					}

					const auto encodingItr = mEncodingEntries.find(rootItr->second);
					if (encodingItr == mEncodingEntries.end()) {
						throw std::exception("File not found");
					}

					const auto indexItr = mIndexEntries.find(encodingItr->second.key);
					if (indexItr == mIndexEntries.end()) {
						return BinaryReader(downloadDataDirect(encodingItr->second.key));
					}

					BinaryReader contentReader(downloadFile(indexItr->second));
					return BinaryReader(casc::BlteHandler(contentReader).getDecompressedData());
				}

			    bool CdnFileManager::exists(const std::string& fileName) {
					auto upperName = utils::String::toUpper(fileName);
					upperName = utils::String::replaceAll(upperName, "/", "\\");
					const auto hash = casc::Jenkins96().computeHash(upperName);
					const auto rootItr = mRootData.find(hash);
					if (rootItr == mRootData.end()) {
						return false;
					}

					const auto encodingItr = mEncodingEntries.find(rootItr->second);
			        return encodingItr != mEncodingEntries.end();
			    }
			}
		}
	}
}
