#include "stdafx.h"
#include "OpenFromCdnEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {

				OpenFromCdnEvent OpenFromCdnEvent::fromJson(const rapidjson::Document& document) {
					const auto buildMember = document.FindMember("buildInfo");
					const auto localeMember = document.FindMember("locale");

					if(buildMember == document.MemberEnd()) {
						throw std::exception("Missing buildInfo");
					}

					if(localeMember == document.MemberEnd()) {
						throw std::exception("Missing locale");
					}

					auto& buildVersion = buildMember->value;
					const auto region = std::string(buildVersion.FindMember("regionName")->value.GetString());
					const auto buildConfig = std::string(buildVersion.FindMember("buildConfigKey")->value.GetString());
					const auto cdnConfig = std::string(buildVersion.FindMember("cdnConfigKey")->value.GetString());
					const auto buildId = buildVersion.FindMember("buildId")->value.GetInt();
					const auto versionName = std::string(buildVersion.FindMember("versionName")->value.GetString());

					return OpenFromCdnEvent(BuildVersion(region, buildConfig, cdnConfig, buildId, versionName), static_cast<uint32_t>(localeMember->value.GetInt()));
				}
			}
		}
	}
}