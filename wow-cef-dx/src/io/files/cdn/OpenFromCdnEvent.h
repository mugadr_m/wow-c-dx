#pragma once
#include "BuildVersion.h"
#include <document.h>

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				class OpenFromCdnEvent {
					BuildVersion mBuildVersion;
					uint32_t mLocale;

				public:
					explicit OpenFromCdnEvent(const BuildVersion& buildVersion, uint32_t locale) : mBuildVersion(buildVersion), mLocale(locale) { }

					const BuildVersion& getBuildVersion() const { return mBuildVersion; }
					const uint32_t& getLocale() const { return mLocale; }

					void toJson(rapidjson::Document& document) const {}
					rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
					static OpenFromCdnEvent fromJson(const rapidjson::Document& document);
				};
			}
		}
	}
}
