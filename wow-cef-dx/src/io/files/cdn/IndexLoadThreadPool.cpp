#include "stdafx.h"
#include "IndexLoadThreadPool.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {

				void IndexLoadThreadPool::loaderThreadCallback() {
					while (mIsRunning) {
						std::packaged_task<std::vector<uint8_t>()> workItem;
						
						{
							std::unique_lock<std::mutex> l(mWorkLock);
							if(!mWorkItems.empty()) {
								workItem = std::move(mWorkItems.front());
								mWorkItems.pop_front();
							} else {
								mWorkEvent.wait(l, [this]() { return !mIsRunning || !mWorkItems.empty(); });
								if (!mIsRunning) {
									return;
								}
								workItem = std::move(mWorkItems.front());
								mWorkItems.pop_front();
							}
						}

						workItem();
					}
				}

				void IndexLoadThreadPool::launchThreads() {
					mIsRunning = true;

					const auto numThreads = std::thread::hardware_concurrency() * 2;
					for (auto i = 0u; i < numThreads; ++i) {
						mLoaderThreads.emplace_back(std::bind(&IndexLoadThreadPool::loaderThreadCallback, this));
					}
				}

				void IndexLoadThreadPool::shutdown() {
					mIsRunning = false;
					mWorkEvent.notify_all();
					for (auto& thread : mLoaderThreads) {
						if (thread.joinable()) {
							thread.join();
						}
					}
				}

				std::shared_future<std::vector<uint8_t>> IndexLoadThreadPool::queueWork(const std::function<std::vector<uint8_t>()>& callback) {
					auto task = std::packaged_task<std::vector<uint8_t>()>(callback);
					auto future = task.get_future();
					{
						std::unique_lock<std::mutex> l(mWorkLock);
						mWorkItems.push_back(std::move(task));
					}

					mWorkEvent.notify_one();
					return future;
				}
			}
		}
	}
}
