#pragma once
#include "cdi/ApplicationContext.h"
#include "BuildVersion.h"
#include <regex>
#include "ui/events/EventManager.h"
#include "utils/Logger.h"
#include "io/files/casc/KeyValueConfig.h"
#include "io/files/casc/CascKeys.h"
#include "io/files/casc/Structures.h"
#include <unordered_map>
#include "IndexLoadThreadPool.h"
#include "io/BinaryReader.h"
#include "io/files/IFileProvider.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				class CdnFileManager : public IFileProvider, public std::enable_shared_from_this<CdnFileManager> {
					static const std::string VERSION_URL;
					static const std::string CDN_URL;

					static utils::Logger<CdnFileManager> mLogger;

					ui::events::EventManagerPtr mEventManager;

					IndexLoadThreadPool mThreadPool;

					BuildVersion mSelectedBuild;
					std::string mGameType;
					casc::LocaleFlags mLocaleFlags = casc::LocaleFlags::NONE;

					std::string mCdnBasePath;

					casc::KeyValueConfig mCdnConfig;
					casc::KeyValueConfig mBuildConfig;

					std::unordered_map<casc::FileKeyMd5, casc::IndexEntry, casc::FileKeyMd5::Hash> mIndexEntries;
					std::unordered_map<casc::FileKeyMd5, casc::EncodingEntry, casc::FileKeyMd5::Hash> mEncodingEntries;
					std::unordered_multimap<uint64_t, casc::FileKeyMd5> mRootData;
					std::unordered_multimap<uint32_t, casc::FileKeyMd5> mFileIdData;

					void publishProgress(const std::string& message) const;

					std::list<BuildVersion> getAvailableBuildVersions(const std::string& gameType) const;

					std::vector<uint8_t> downloadFileDirect(const std::string& fileKey) const;
					std::vector<uint8_t> downloadIndexFile(const std::string& indexFile) const;
					std::vector<uint8_t> downloadDataDirect(const std::string& fileKey) const;
					std::vector<uint8_t> downloadDataDirect(const casc::FileKeyMd5& fileKey) const;
					std::vector<uint8_t> downloadFile(const casc::IndexEntry& indexEntry) const;

					void handleIndexFile(const std::vector<uint8_t>& content, uint32_t index);

					void loadCdnConfig();
					void loadConfigFiles();
					void loadIndexFiles();
					void loadRootFile();
					void loadEncodingFile();

					void handleBuildSelected(const BuildVersion& buildVersion);
				public:
					CdnFileManager() = default;
					CdnFileManager(const CdnFileManager&) = delete;
					CdnFileManager(CdnFileManager&&) = delete;
					~CdnFileManager() override = default;

					void operator = (const CdnFileManager&) = delete;
					void operator = (CdnFileManager&&) = delete;

					void postConstruct(const cdi::ApplicationContextPtr& context);
					void preDestroy();

					void attachUiEvents(const cdi::ApplicationContextPtr& context);

					BinaryReader openFile(const std::string& fileName) override;
					BinaryReader openFileByFileId(const uint32_t& fileId) override;
					bool exists(const std::string& fileName) override;
				};
			}
		}
	}
}
