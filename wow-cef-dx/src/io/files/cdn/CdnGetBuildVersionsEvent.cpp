#include "stdafx.h"
#include "CdnGetBuildVersionsEvent.h"
#include <utility>

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				CdnGetBuildVersionsEvent::CdnGetBuildVersionsEvent(std::string gameType) : mGameType(std::move(gameType)) {
				}

				void CdnGetBuildVersionsEvent::toJson(rapidjson::Document& document) const {
					auto& allocator = document.GetAllocator();

					document.SetObject();
					rapidjson::Value regions(rapidjson::kArrayType);
					for(auto& buildVersion : mBuildVersions) {
						rapidjson::Value build(rapidjson::kObjectType);
						rapidjson::Value regionName(rapidjson::kStringType);
						rapidjson::Value buildConfigKey(rapidjson::kStringType);
						rapidjson::Value cdnConfigKey(rapidjson::kStringType);
						rapidjson::Value buildId(rapidjson::kNumberType);
						rapidjson::Value versionName(rapidjson::kStringType);
						regionName.SetString(buildVersion.getRegion().c_str(), allocator);
						buildConfigKey.SetString(buildVersion.getBuildConfigKey().c_str(), allocator);
						cdnConfigKey.SetString(buildVersion.getCdnConfigKey().c_str(), allocator);
						buildId.SetInt(buildVersion.getBuildId());
						versionName.SetString(buildVersion.getVersionName().c_str(), allocator);

						build.AddMember("regionName", regionName, allocator);
						build.AddMember("buildConfigKey", buildConfigKey, allocator);
						build.AddMember("cdnConfigKey", cdnConfigKey, allocator);
						build.AddMember("buildId", buildId, allocator);
						build.AddMember("versionName", versionName, allocator);

						regions.PushBack(build, allocator);;
					}

					document.AddMember("builds", regions, allocator);
				}

				rapidjson::Document CdnGetBuildVersionsEvent::getResponseJson() const {
					rapidjson::Document ret;
					toJson(ret);
					return ret;
				}

				CdnGetBuildVersionsEvent CdnGetBuildVersionsEvent::fromJson(const rapidjson::Document& document) {
					const auto itr = document.FindMember("gameType");
					if(itr == document.MemberEnd()) {
						throw std::exception("Missing gameType");
					}

					return CdnGetBuildVersionsEvent(itr->value.GetString());
				}
			}
		}
	}
}