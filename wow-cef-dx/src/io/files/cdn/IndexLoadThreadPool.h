#pragma once

#include <future>
#include <thread>

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				class IndexLoadThreadPool {
					std::vector<std::thread> mLoaderThreads;

					std::condition_variable mWorkEvent;
					std::mutex mWorkLock;
					std::deque<std::packaged_task<std::vector<uint8_t>()>> mWorkItems;

					bool mIsRunning = false;

					void loaderThreadCallback();

				public:
					void launchThreads();
					void shutdown();

					std::shared_future<std::vector<uint8_t>> queueWork(const std::function<std::vector<uint8_t>()>& callback);
				};
			}
		}
	}
}
