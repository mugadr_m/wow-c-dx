#pragma once

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				class BuildVersion {
					std::string mRegion;
					std::string mBuildConfigKey;
					std::string mCdnConfigKey;
					uint32_t mBuildId = 0;
					std::string mVersionName;

				public:
					BuildVersion() = default;

					BuildVersion(std::string region, std::string buildConfigKey, std::string cdnConfigKey, const uint32_t buildId,
						std::string versionName)
						: mRegion(std::move(region)),
						  mBuildConfigKey(std::move(buildConfigKey)),
						  mCdnConfigKey(std::move(cdnConfigKey)),
						  mBuildId(buildId),
						  mVersionName(std::move(versionName)) {
					}

					const std::string& getRegion() const {
						return mRegion;
					}

					void setRegion(const std::string& region) {
						mRegion = region;
					}

					const std::string& getBuildConfigKey() const {
						return mBuildConfigKey;
					}

					void setBuildConfigKey(const std::string& buildConfigKey) {
						mBuildConfigKey = buildConfigKey;
					}

					const std::string& getCdnConfigKey() const {
						return mCdnConfigKey;
					}

					void setCdnConfigKey(const std::string& cdnConfigKey) {
						mCdnConfigKey = cdnConfigKey;
					}

					uint32_t getBuildId() const {
						return mBuildId;
					}

					void setBuildId(const uint32_t buildId) {
						mBuildId = buildId;
					}

					const std::string& getVersionName() const {
						return mVersionName;
					}

					void setVersionName(const std::string& versionName) {
						mVersionName = versionName;
					}
				};
			}
		}
	}
}