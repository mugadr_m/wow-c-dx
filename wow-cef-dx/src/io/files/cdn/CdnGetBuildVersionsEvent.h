#pragma once

#include <document.h>
#include "BuildVersion.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace cdn {
				class CdnGetBuildVersionsEvent {
					std::string mGameType;
					std::list<BuildVersion> mBuildVersions;
				public:
					explicit CdnGetBuildVersionsEvent(std::string gameType);

					void addBuild(const BuildVersion& buildVersion) {
						mBuildVersions.emplace_back(buildVersion);
					}

					const std::string& getGameType() const { return mGameType; }

					void toJson(rapidjson::Document& document) const;
					rapidjson::Document getResponseJson() const;
					static CdnGetBuildVersionsEvent fromJson(const rapidjson::Document& document);
				};
			}
		}
	}
}
