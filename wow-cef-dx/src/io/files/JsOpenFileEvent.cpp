#include "stdafx.h"
#include "JsOpenFileEvent.h"

namespace wowc {
    namespace io {
        namespace files {

            JsOpenFileEvent::JsOpenFileEvent(const std::string& filePath, uint32_t fileDataId) {
                if(!filePath.empty()) {
					mIsFileDataId = false;
					mFilePath = filePath;
                } else {
					mIsFileDataId = true;
					mFileDataId = fileDataId;
                }
            }

            void JsOpenFileEvent::toJson(rapidjson::Document& document) const {
			    // this event is only fired from javascript its not supposed to be sent to JS
            }

            rapidjson::Document JsOpenFileEvent::getResponseJson() const {
				rapidjson::Document ret;
				auto& allocator = ret.GetAllocator();

				ret.SetArray();
                for(const auto& elem : mResponseData) {
					rapidjson::Value element(rapidjson::kNumberType);
					element.SetInt(elem);
					ret.PushBack(element, allocator);
                }

				return ret;
            }

            JsOpenFileEvent JsOpenFileEvent::fromJson(const rapidjson::Document& document) {
				const auto fileNameMember = document.FindMember("fileName");
				const auto fileDataMember = document.FindMember("fileDataId");

                if(fileNameMember != document.MemberEnd()) {
					const auto length = fileNameMember->value.GetStringLength();
					const auto ptr = fileNameMember->value.GetString();
                    const std::string fileName(ptr, ptr + length);
					return JsOpenFileEvent(fileName);
                }
 
                if(fileDataMember != document.MemberEnd()) {
                    return JsOpenFileEvent(std::string(), fileDataMember->value.GetInt());
                }

                throw std::exception("Either fileName or fileDataId must be set for JsOpenFileEvent");
            }
        }
    }
}