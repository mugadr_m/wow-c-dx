#include "stdafx.h"
#include "JsFileHandler.h"
#include "DataLoadCompleteEvent.h"
#include "ui/events/EventManager.h"
#include "JsOpenFileEvent.h"

namespace wowc {
    namespace io {
        namespace files {

            void JsFileHandler::onOpenFileRequest(JsOpenFileEvent& event) {
            }

            void JsFileHandler::postConstruct(const cdi::ApplicationContextPtr& context) {
                context->registerEvent<DataLoadCompleteEvent>([=](auto&) {
					mFileProvider = context->produce<IFileProvider>();
				});

				context->registerEvent<JsOpenFileEvent>(std::bind(&JsFileHandler::onOpenFileRequest, this, std::placeholders::_1));
            }

            void JsFileHandler::attachUiEvents(const cdi::ApplicationContextPtr& context) {
				const auto eventManager = context->produce<ui::events::EventManager>();
				eventManager->registerEvent<JsOpenFileEvent>();
            }
        }
    }
}
