#pragma once

#include <boost/geometry.hpp>
#include "io/files/storage/MapLightEntry.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                class ZoneLightPolygon {
					typedef boost::geometry::model::d2::point_xy<float> TPolygonPoint;
					typedef boost::geometry::model::polygon<TPolygonPoint> TPolygon;

					TPolygon mPolygon;
                public:
					explicit ZoneLightPolygon(const std::vector<storage::ZoneLightPoint>& polygonPoints);

					bool isContained(float x, float y) const;
                    bool isContained(const math::Vector2& point) const {
						return isContained(point.x(), point.y());
                    }

					float distance(float x, float y) const;
                    float distance(const math::Vector2& point) const {
						return distance(point.x(), point.y());
                    }
                };
            }
        }
    }
}
