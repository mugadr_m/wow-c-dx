#include "stdafx.h"
#include "MapLightHandler.h"
#include "SkyColorType.h"
#include <numeric>

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {

                math::Vector3 MapLightHandler::getColorForLight(uint32_t time, storage::LightDataEntry::Values color,
                                                                const std::vector<storage::LightDataEntry>&
                                                                lightDataEntries) {
                    time %= 2880;
                    const auto& entries = lightDataEntries;
                    if (entries.empty()) {
                        return {};
                    }

                    if (entries.size() == 1) {
                        return math::Vector3::fromXrgb(entries[0].data[color]);
                    }

                    uint32_t t0 = 0;
                    uint32_t t1 = 0;
                    math::Vector3 c1;
                    math::Vector3 c2;

                    for (auto i = 0u; i < entries.size(); ++i) {
                        const auto& cur = entries[i];

                        if ((i + 1) >= entries.size()) {
                            t0 = cur.timestamp;
                            t1 = entries[0].timestamp + 2880;
                            c1 = math::Vector3::fromXrgb(cur.data[color]);
                            c2 = math::Vector3::fromXrgb(entries[0].data[color]);
                            break;
                        }

                        const auto& next = entries[i + 1];
                        if (cur.timestamp <= time && next.timestamp >= time) {
                            t0 = cur.timestamp;
                            t1 = next.timestamp;
                            c1 = math::Vector3::fromXrgb(cur.data[color]);
                            c2 = math::Vector3::fromXrgb(next.data[color]);
                            break;
                        }
                    }

                    if (t0 == 0 && t1 == 0) {
                        return {};
                    }

                    const auto factor = (time - t0) / static_cast<float>(t1 - t0);
                    return c2 * factor + c1 * (1.0f - factor);

                }

                float MapLightHandler::updateMapLightWeights(const math::Vector3& position) {
                    mLightWeights.assign(mLightWeights.size(), 0.0f);

                    auto counter = mMapLights.size() - 1;
                    for (auto itr = mMapLights.rbegin(); itr != mMapLights.rend(); ++itr, --counter) {
                        const auto& light = *itr;
                        if (light.lightEntry.isGlobal()) {
                            continue;
                        }

                        const auto distance = (position - light.lightEntry.position).length2D();
                        if (distance <= light.lightEntry.innerRadius) {
                            mLightWeights[counter] = 1.0f;
                            for (auto j = counter + 1; j < mLightWeights.size(); ++j) {
                                mLightWeights[j] = 0.0f;
                            }
                        } else if (distance <= light.lightEntry.outerRadius) {
                            const auto sat = (distance - light.lightEntry.innerRadius) / (light.lightEntry.outerRadius -
                                light.lightEntry.innerRadius);
                            mLightWeights[counter] = 1.0f - sat;
                            for (auto j = counter + 1; j < mLightWeights.size(); ++j) {
                                mLightWeights[j] *= sat;
                            }
                        } else {
                            mLightWeights[counter] = 0.0f;
                        }
                    }

                    return std::accumulate(mLightWeights.begin(), mLightWeights.end(), 0.0f);
                }

                float MapLightHandler::updateZoneLights(const math::Vector3& position, float totalWeight,
                                                        bool& hasLight) {
                    auto fullZoneLight = 0u;
                    auto hasFullZoneLight = false;
                    std::vector<uint32_t> partialLights;

                    for (auto i = 0u; i < mZoneLights.size(); ++i) {
                        const auto& zl = mZoneLights[i];
                        if (zl->getDistance(position) < 50.0f) {
                            partialLights.push_back(i);
                        } else if (zl->isInside(position)) {
                            fullZoneLight = i;
                            hasFullZoneLight = true;
                        } else {
                            mZoneLightWeights[i] = 0.0f;
                        }
                    }

                    for (auto i = 0u; i < mZoneLights.size(); ++i) {
                        mZoneLightWeights[i] = (!hasFullZoneLight || i != fullZoneLight) ? 0.0f : 1.0f;
                    }

                    for (const auto& i : partialLights) {
                        const auto& zl = mZoneLights[i];
                        auto distance = zl->getDistance(position);
                        const auto isInner = zl->isInside(position);
                        if (isInner) {
                            distance = 50 - distance;
                        } else {
                            distance += 50;
                        }

                        const auto sat = distance / 100.0f;
                        mZoneLightWeights[i] = 1.0f - sat;
                        for (auto j = 0u; j < i; ++j) {
                            mZoneLightWeights[j] *= sat;
                        }

                        if (hasFullZoneLight && fullZoneLight > i) {
                            mZoneLightWeights[fullZoneLight] *= sat;
                        }
                    }

                    const auto fac = 1.0f - totalWeight;
                    for (auto& weight : mZoneLightWeights) {
                        weight *= fac;
                        totalWeight += weight;
                    }

                    hasLight = !partialLights.empty() || hasFullZoneLight;
                    return totalWeight;
                }

                void MapLightHandler::updateWeights(math::Vector3 position) {
                    auto totalWeight = updateMapLightWeights(position);
                    auto remain = 1.0f - totalWeight;
                    auto hasZoneLight = false;
                    if (remain > 1e-5) {
                        totalWeight = updateZoneLights(position, totalWeight, hasZoneLight);
                    } else {
                        mZoneLightWeights.assign(mZoneLightWeights.size(), 0.0f);
                    }

                    remain = 1.0f - totalWeight;
                    if (remain > 1e-5) {
                        const auto perGlobalW = remain / mGlobalLights.size();
                        for (const auto& gl : mGlobalLights) {
                            mLightWeights[gl] = perGlobalW;
                        }
                    }

                    if (mMapLights.empty() && !hasZoneLight && !mZoneLights.empty()) {
                        mZoneLightWeights[0] = 1.0f;
                    }
                }

                MapLightHandler::MapLightHandler(storage::SkyStorageDaoPtr skyStorageDao,
                                                 scene::sky::SkySpherePtr skySphere) :
                    mLightColors(NUM_SKY_COLOR_TYPES), mSkyStorageDao(std::move(skyStorageDao)),
                    mSkySphere(std::move(skySphere)) {

                }

                void MapLightHandler::onEnterWorld(const uint32_t& mapId) {
                    std::lock_guard<std::mutex> l(mLightLock);

                    mStartTime = std::chrono::steady_clock::now();

                    mMapLights = mSkyStorageDao->getLightsForMap(mapId);
                    for (auto i = 0u; i < mMapLights.size(); ++i) {
                        if (mMapLights[i].lightEntry.isGlobal()) {
                            mGlobalLights.push_back(i);
                        }
                    }

                    if(mGlobalLights.empty()) {
						mMapLights.push_back(mSkyStorageDao->getFallbackGlobalLight());
						mGlobalLights.push_back(static_cast<uint32_t>(mMapLights.size() - 1));
                    }

                    mZoneLights.clear();

                    const auto zoneLights = mSkyStorageDao->getZoneLightsForMap(mapId);
                    for (const auto& zl : zoneLights) {
                        mZoneLights.emplace_back(std::make_shared<ZoneLight>(zl));
                    }

                    mZoneLightWeights.resize(mZoneLights.size());
                    mLightWeights.resize(mMapLights.size());
                }

                void MapLightHandler::updatePosition(const math::Vector3& position) {
                    mPosition = position;
                    mPositionChanged = true;
                }

                void MapLightHandler::asyncUpdate() {
                    std::lock_guard<std::mutex> l(mLightLock);

                    if (mPositionChanged) {
                        updateWeights(mPosition);
                        mPositionChanged = false;
                    }

                    const auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::steady_clock::now() - mStartTime).count() / 10;

                    math::Vector3 lightColors[storage::LightDataEntry::NUM_DATA_VALUES]{};
                    auto riverNearAlpha = 0.0f;
                    auto riverFarAlpha = 0.0f;
                    auto oceanNearAlpha = 0.0f;
                    auto oceanFarAlpha = 0.0f;

                    for (auto i = 0u; i < mMapLights.size(); ++i) {
                        const auto weight = mLightWeights[i];
                        if (weight < 1e-5) {
                            continue;
                        }

                        const auto& light = mMapLights[i];

                        for (auto k = 0u; k < storage::LightDataEntry::NUM_DATA_VALUES; ++k) {
                            lightColors[k] += getColorForLight(time, static_cast<storage::LightDataEntry::Values>(k),
                                                               light.lightDataEntries) * weight;
                        }

                        riverNearAlpha += light.lightParams.waterNearAlpha * weight;
                        riverFarAlpha += light.lightParams.waterFarAlpha * weight;
                        oceanNearAlpha += light.lightParams.oceanNearAlpha * weight;
                        oceanFarAlpha += light.lightParams.oceanFarAlpha * weight;
                    }

                    for (auto i = 0u; i < mZoneLights.size(); ++i) {
                        const auto weight = mZoneLightWeights[i];
                        if (weight < 1e-5) {
                            continue;
                        }

                        const auto& light = mZoneLights[i];
                        for (auto k = 0u; k < storage::LightDataEntry::NUM_DATA_VALUES; ++k) {
                            lightColors[k] += getColorForLight(time, static_cast<storage::LightDataEntry::Values>(k),
                                                               light->getLightDataEntries()) * weight;
                        }

                        const auto& params = light->getLightParams();
                        riverNearAlpha += params.waterNearAlpha * weight;
                        riverFarAlpha += params.waterFarAlpha * weight;
                        oceanNearAlpha += params.oceanNearAlpha * weight;
                        oceanFarAlpha += params.oceanFarAlpha * weight;
                    }

                    setColor(SkyColorType::AMBIENT, lightColors[storage::LightDataEntry::AMBIENT]);
                    setColor(SkyColorType::DIFFUSE, lightColors[storage::LightDataEntry::DIFFUSE]);
                    setColor(SkyColorType::SKY_COLOR0, lightColors[storage::LightDataEntry::SKY0]);
                    setColor(SkyColorType::SKY_COLOR1, lightColors[storage::LightDataEntry::SKY1]);
                    setColor(SkyColorType::SKY_COLOR2, lightColors[storage::LightDataEntry::SKY2]);
                    setColor(SkyColorType::SKY_COLOR3, lightColors[storage::LightDataEntry::SKY3]);
                    setColor(SkyColorType::SKY_COLOR4, lightColors[storage::LightDataEntry::SKY4]);
                    setColor(SkyColorType::FOG_COLOR, lightColors[storage::LightDataEntry::FOG]);
                    setColor(SkyColorType::RIVER_NEAR_COLOR, lightColors[storage::LightDataEntry::RIVER_NEAR]);
                    setColor(SkyColorType::RIVER_FAR_COLOR, lightColors[storage::LightDataEntry::RIVER_FAR]);
                    setColor(SkyColorType::OCEAN_NEAR_COLOR, lightColors[storage::LightDataEntry::OCEAN_NEAR]);
                    setColor(SkyColorType::OCEAN_FAR_COLOR, lightColors[storage::LightDataEntry::OCEAN_FAR]);

                    mRiverNearAlpha = riverNearAlpha;
                    mRiverFarAlpha = riverFarAlpha;
                    mOceanNearAlpha = oceanNearAlpha;
                    mOceanFarAlpha = oceanFarAlpha;
                    mColorsChanged = true;
                }

                void MapLightHandler::syncUpdate(scene::Scene& scene) {
                    if (!mColorsChanged || mSkySphere == nullptr) {
                        return;
                    }

                    mColorsChanged = false;
                    auto& bufferMgr = scene.getBufferManager();
                    bufferMgr.updateLightColors(getColor(SkyColorType::AMBIENT), getColor(SkyColorType::DIFFUSE));

                    bufferMgr.updateWaterColors(getColor(SkyColorType::OCEAN_NEAR_COLOR),
                                                getColor(SkyColorType::OCEAN_FAR_COLOR),
                                                getColor(SkyColorType::RIVER_NEAR_COLOR),
                                                getColor(SkyColorType::RIVER_FAR_COLOR));

                    bufferMgr.updateWaterAlpha(mOceanNearAlpha, mOceanFarAlpha, mRiverNearAlpha, mRiverFarAlpha);

                    mSkySphere->updateColors(mLightColors);
                }
            }
        }
    }
}
