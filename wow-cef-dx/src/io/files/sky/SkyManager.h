#pragma once
#include "cdi/ApplicationContext.h"
#include "io/files/storage/SkyStorageDao.h"
#include "MapLightHandler.h"
#include "scene/Scene.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                class SkyManager {
					static const uint32_t LIGHT_UPDATE_FREQUENCY_MS = 33;

                    storage::SkyStorageDaoPtr mSkyStorageDao;
					MapLightHandlerPtr mMapLightHandler;

					bool mIsRunning = false;
					bool mIsLoaderStarted = false;
					std::thread mLoaderThread;

					std::condition_variable mTimerVariable;
					std::mutex mTimerLock;

					void onInitialize(const cdi::ApplicationContextPtr& context);

					void loaderCallback();
                public:
                    void onLoad();
					void onShutdown();

					void onEnterWorld(uint32_t mapId);
					void onPositionChanged(const math::Vector3& position) const;

					void onFrame(scene::Scene& scene) const;
                };

				SHARED_PTR(SkyManager);
            }
        }
    }
}
