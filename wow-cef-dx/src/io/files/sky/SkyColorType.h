#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                enum class SkyColorType {
                    AMBIENT,
                    DIFFUSE,
                    SKY_COLOR0,
                    SKY_COLOR1,
                    SKY_COLOR2,
                    SKY_COLOR3,
                    SKY_COLOR4,
                    FOG_COLOR,
                    RIVER_NEAR_COLOR,
                    RIVER_FAR_COLOR,
                    OCEAN_NEAR_COLOR,
                    OCEAN_FAR_COLOR,
					MAX_SKY_COLOR_TYPE
                };

				constexpr uint32_t NUM_SKY_COLOR_TYPES = static_cast<uint32_t>(SkyColorType::MAX_SKY_COLOR_TYPE);
            }
        }
    }
}