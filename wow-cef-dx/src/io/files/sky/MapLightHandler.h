#pragma once
#include "io/files/storage/MapLightEntry.h"
#include "ZoneLight.h"
#include "io/files/storage/SkyStorageDao.h"
#include "math/Vector3.h"
#include "SkyColorType.h"
#include "scene/Scene.h"
#include "scene/sky/SkySphere.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                class MapLightHandler {
					std::vector<storage::MapLightEntry> mMapLights;
					std::vector<ZoneLightPtr> mZoneLights;
					std::vector<uint32_t> mGlobalLights;

					std::vector<float> mLightWeights;
					std::vector<float> mZoneLightWeights;
					std::mutex mLightLock;

					std::mutex mColorLock;
					std::vector<uint32_t> mLightColors;
					float mOceanNearAlpha = 1.0f;
					float mOceanFarAlpha = 1.0f;
					float mRiverNearAlpha = 1.0f;
					float mRiverFarAlpha = 1.0f;
					bool mColorsChanged = false;

					storage::SkyStorageDaoPtr mSkyStorageDao;
					scene::sky::SkySpherePtr mSkySphere;

					math::Vector3 mPosition;
					bool mPositionChanged = false;

					std::chrono::steady_clock::time_point mStartTime;

                    static math::Vector3 getColorForLight(uint32_t time, storage::LightDataEntry::Values color, const std::vector<storage::LightDataEntry>& lightDataEntries);

					float updateMapLightWeights(const math::Vector3& position);
					float updateZoneLights(const math::Vector3& position, float totalWeight, bool& hasLight);

					void updateWeights(math::Vector3 position);

                    void setColor(SkyColorType type, const math::Vector3& color) {
						mLightColors[static_cast<uint32_t>(type)] = color.toXrgb();
                    }

                    uint32_t getColor(SkyColorType type) {
						return mLightColors[static_cast<uint32_t>(type)];
                    }

                public:
					explicit MapLightHandler(storage::SkyStorageDaoPtr skyStorageDao, scene::sky::SkySpherePtr skySphere);

					void onEnterWorld(const uint32_t& mapId);

					void updatePosition(const math::Vector3& position);
					void asyncUpdate();
					void syncUpdate(scene::Scene& scene);
                };

				SHARED_PTR(MapLightHandler);
            }
        }
    }
}
