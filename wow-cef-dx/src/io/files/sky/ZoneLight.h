#pragma once
#include "ZoneLightPolygon.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                class ZoneLight {
					storage::ZoneLight mZoneLight;
					ZoneLightPolygon mPolygon;

                public:
					explicit ZoneLight(storage::ZoneLight zoneLight);

					float getDistance(const math::Vector3& position) const;
					bool isInside(const math::Vector3& position) const;

                    const std::vector<storage::LightDataEntry>& getLightDataEntries() const {
						return mZoneLight.lightDataEntries;
                    }

                    const storage::LightParamsEntry& getLightParams() const {
						return mZoneLight.lightParams;
                    }
                };

				SHARED_PTR(ZoneLight);
            }
        }
    }
}
