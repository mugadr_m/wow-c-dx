#include "stdafx.h"
#include "SkyManager.h"
#include "io/files/storage/StorageManager.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {

                void SkyManager::onInitialize(const cdi::ApplicationContextPtr& context) {
					mSkyStorageDao = context->produce<storage::StorageManager>()->getSkyStorageDao();
					const auto skySphere = context->produce<scene::sky::SkySphere>();
					mMapLightHandler = std::make_shared<MapLightHandler>(mSkyStorageDao, skySphere);
                }

                void SkyManager::loaderCallback() {
					while(mIsRunning) {
						{
							std::unique_lock<std::mutex> l(mTimerLock);
							mTimerVariable.wait_for(l, std::chrono::milliseconds(LIGHT_UPDATE_FREQUENCY_MS));
                            if(!mIsRunning) {
								return;
                            }
						}

						mMapLightHandler->asyncUpdate();
					}
                }

                void SkyManager::onLoad() {
					onInitialize(cdi::ApplicationContext::instance());
                }

                void SkyManager::onShutdown() {
					mIsRunning = false;
					mTimerVariable.notify_all();
                    if(mLoaderThread.joinable()) {
						mLoaderThread.join();
                    }
                }

                void SkyManager::onEnterWorld(uint32_t mapId) {
					mMapLightHandler->onEnterWorld(mapId);
                    if(!mIsLoaderStarted) {
						mIsRunning = true;
						mIsLoaderStarted = true;
						mLoaderThread = std::thread(std::bind(&SkyManager::loaderCallback, this));
                    }
                }

                void SkyManager::onPositionChanged(const math::Vector3& position) const {
                    if(mMapLightHandler == nullptr) {
						return;
                    }

					mMapLightHandler->updatePosition(position);
                }

                void SkyManager::onFrame(scene::Scene& scene) const {
                    if(mMapLightHandler == nullptr) {
						return;
                    }

					mMapLightHandler->syncUpdate(scene);
                }
            }
        }
    }
}
