#include "stdafx.h"
#include "ZoneLight.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {
                ZoneLight::ZoneLight(storage::ZoneLight zoneLight) : mZoneLight(std::move(zoneLight)),
                                                                     mPolygon(mZoneLight.polygonPoints) {
                }

                float ZoneLight::getDistance(const math::Vector3& position) const {
					return mPolygon.distance(position.x(), position.y());
                }

                bool ZoneLight::isInside(const math::Vector3& position) const {
					return mPolygon.isContained(position.x(), position.y());
                }
            }
        }
    }
}
