#include "stdafx.h"
#include "ZoneLightPolygon.h"
#include <boost/geometry.hpp>

namespace wowc {
    namespace io {
        namespace files {
            namespace sky {

                ZoneLightPolygon::ZoneLightPolygon(const std::vector<storage::ZoneLightPoint>& polygonPoints) {
                    std::vector<TPolygonPoint> points(polygonPoints.size());
                    std::for_each(polygonPoints.begin(), polygonPoints.end(),
                                  [&](const storage::ZoneLightPoint& point) {
                                      boost::geometry::append(
                                          mPolygon, TPolygonPoint(point.position.x(), point.position.y()));
                                  });
                }

                bool ZoneLightPolygon::isContained(float x, float y) const {
                    return boost::geometry::within(TPolygonPoint(x, y), mPolygon);
                }

                float ZoneLightPolygon::distance(float x, float y) const {
					typedef boost::geometry::model::segment<TPolygonPoint> TSegment;

                    const TPolygonPoint pt{x, y};
                    float distance;

                    if (boost::geometry::within(pt, mPolygon)) {
                        auto closestDistance = std::numeric_limits<float>::max();
						TSegment closestSegment;
                        auto hasSegment = false;
                        boost::geometry::for_each_segment(mPolygon, [&closestDistance, &pt, &hasSegment, &closestSegment](const auto& segment) {
							const auto curDistance = boost::geometry::comparable_distance(segment, pt);
                            if(curDistance < closestDistance) {
								closestDistance = curDistance;
								closestSegment = TSegment(segment.first, segment.second);
								hasSegment = true;
                            }
                        });

						distance = boost::geometry::distance(closestSegment, pt);
                    } else {
                        distance = boost::geometry::distance(pt, mPolygon);
                    }

                    return distance;
                }
            }
        }
    }
}
