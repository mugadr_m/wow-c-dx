#pragma once
#include "io/BinaryReader.h"

namespace wowc {
	namespace io {
		namespace files {
			class IFileProvider abstract {
			public:
				virtual ~IFileProvider() = default;
				virtual BinaryReader openFile(const std::string& name) = 0;
				virtual BinaryReader openFileByFileId(const uint32_t& fileId) = 0;
				virtual bool exists(const std::string& name) = 0;
			};

			SHARED_PTR(IFileProvider);
		}
	}
}
