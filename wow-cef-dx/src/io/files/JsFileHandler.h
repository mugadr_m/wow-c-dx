#pragma once
#include "cdi/ApplicationContext.h"
#include "IFileProvider.h"
#include "JsOpenFileEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            class JsFileHandler {
				IFileProviderPtr mFileProvider;

				void onOpenFileRequest(JsOpenFileEvent& event);

            public:
				void postConstruct(const cdi::ApplicationContextPtr& context);

                static void attachUiEvents(const cdi::ApplicationContextPtr& context);
            };
        }
    }
}
