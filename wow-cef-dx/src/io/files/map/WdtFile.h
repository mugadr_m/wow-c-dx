#pragma once

#include "io/BinaryReader.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
				struct WdtFlags {
					uint32_t hasGlobal : 1;
					uint32_t hasMccv : 1;
					uint32_t hasUncompressedAlpha : 1;
					uint32_t hasDoodadsBySize : 1;
					uint32_t hasMclv : 1;
					uint32_t isUpsideDown : 1;
					uint32_t unk1 : 1;
					uint32_t hasHeightTextures : 1;
				};

                class WdtFile {
					WdtFlags mFlags{};

                public:
					explicit WdtFile(BinaryReader& file);

					const WdtFlags& getFlags() const { return mFlags; }
                };
            }
        }
    }
}