#pragma once
#include "io/BinaryReader.h"
#include "utils/Logger.h"
#include "MapChunk.h"
#include "io/files/map/WdtFile.h"
#include "io/files/liquid/TileLiquidManager.h"
#include "io/files/storage/StorageManager.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                namespace bfa {
                    struct M2ModelEntry {
						Mddf definition;
						uint32_t fileDataId = 0;
						std::string fileName{};
						bool isFileDataId = false;
                    };

                    class MapTile {
						static utils::Logger<MapTile> mLogger;

						uint32_t mIndexX = 0, mIndexY = 0;

						WdtFlags mWdtFlags;

						std::multimap<uint32_t, BinaryReader> mChunkData;
						std::vector<BinaryReader> mBaseChunks;
						std::vector<BinaryReader> mTexChunks;
						std::vector<BinaryReader> mObjChunks;

						std::vector<std::string> mTextures;
						std::vector<std::string> mHeightTextures;
						std::vector<std::string> mSpecularTextures;

						std::vector<float> mTextureScales;
						std::vector<float> mHeightScales;
						std::vector<float> mHeightOffsets;

						std::vector<M2ModelEntry> mModelDefinitions;

						std::vector<MapChunkPtr> mMapChunks;

						std::vector<scene::terrain::MapTileVertex> mVertices;
						std::vector<math::Vector3> mGridVertices;

						math::Vector3 mBboxMin;
						math::Vector3 mBboxMax;

						liquid::TileLiquidManagerPtr mLiquidManager;

                        static bool compareChunks(BinaryReader& c1, BinaryReader& c2);

						std::string getLogHeader() const;

						void readAllChunks(BinaryReader& reader, std::vector<BinaryReader>& mcnkList);

						void loadSpecularTextures();
						void loadTextureFlags();
						void loadTextures();
						void loadChunks();

						void loadModels();

						void loadLiquids();

						void combineBuffers();

                    public:
						MapTile(uint32_t indexX, uint32_t indexY, 
							BinaryReader baseData, BinaryReader texData, BinaryReader objData, 
							const WdtFlags& wdtFlags,
							const storage::StorageManagerPtr& storageManager);

                        liquid::TileLiquidManagerPtr getLiquidManager() const {
							return mLiquidManager;
                        }

						const std::vector<scene::terrain::MapTileVertex>& getVertices() const { return mVertices; }
						const std::vector<math::Vector3>& getGridVertices() const { return mGridVertices; }
						const std::vector<MapChunkPtr>& getChunks() const { return mMapChunks; }

						const math::Vector3& getBboxMin() const { return mBboxMin; }
						const math::Vector3& getBboxMax() const { return mBboxMax; }

						const std::vector<std::string>& getTextures() const { return mTextures; }
						const std::vector<std::string>& getHeightTextures() const { return mHeightTextures; }
						const std::vector<std::string>& getSpecularTextures() const { return mSpecularTextures; }

						const std::vector<float>& getTextureScales() const { return mTextureScales; }
						const std::vector<float>& getHeightScales() const { return mHeightScales; }
						const std::vector<float>& getHeightOffsets() const { return mHeightOffsets; }

                        bool hasHeightTextures() const { return mWdtFlags.hasHeightTextures != 0; }

                        const std::vector<M2ModelEntry>& getM2Models() const {
							return mModelDefinitions;
                        }
                    };

					SHARED_PTR(MapTile);
                }
            }
        }
    }
}
