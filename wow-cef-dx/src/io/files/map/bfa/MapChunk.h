#pragma once
#include "BfaMapStructures.h"
#include "io/BinaryReader.h"
#include "utils/Logger.h"
#include "math/Vector3.h"
#include "scene/terrain/MapTileVertex.h"
#include "io/files/map/WdtFile.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                namespace bfa {
                    class MapChunk {
                        static utils::Logger<MapChunk> mLogger;

                        McnkHeader mHeader;
						WdtFlags mWdtFlags;

                        std::map<uint32_t, BinaryReader> mChunkMap;
                        std::vector<scene::terrain::MapTileVertex> mVertices;
						std::vector<math::Vector3> mGridVertices;
                        math::Vector3 mBboxMin, mBboxMax;

						std::vector<Mcly> mLayers;
						std::vector<uint32_t> mTextures;
						std::vector<uint32_t> mAlphaValues;

						void loadBaseLayer(uint32_t layer);
						void loadUncompressed(uint32_t layer, const uint8_t* alphaData);
						void loadCompressed(uint32_t layer, const uint8_t* alphaData);
						void loadRle(uint32_t layer, const uint8_t* alphaData);

                        void readChunks(BinaryReader& reader);

                        void loadVertices();
                        void loadNormals();
                        void loadVertexColors();
                        void loadVertexLights();

						void loadAlpha();
						void loadLayers();

						void loadGrid();

                    public:
                        MapChunk(McnkHeader header, BinaryReader& baseChunk, BinaryReader& texChunk,
                                 BinaryReader& objChunk, const WdtFlags& wdtFlags);

                        uint32_t getAreaId() const { return mHeader.areaId; }

                        const std::vector<scene::terrain::MapTileVertex>& getVertices() const { return mVertices; }
						const std::vector<math::Vector3>& getGridVertices() const { return mGridVertices; }

						const math::Vector3& getBboxMin() const { return mBboxMin; }
						const math::Vector3& getBboxMax() const { return mBboxMax; }

						const std::vector<uint32_t>& getAlphaValues() const { return mAlphaValues; }

						const std::vector<uint32_t>& getTextures() const { return mTextures; }

                        void clearVertices() {
							mVertices.clear();
							mGridVertices.clear();
                        }
                    };

                    SHARED_PTR(MapChunk);
                }
            }
        }
    }
}
