#include "stdafx.h"
#include "MapTile.h"
#include "io/files/liquid/H2OParser.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                namespace bfa {
                    utils::Logger<MapTile> MapTile::mLogger;

                    bool MapTile::compareChunks(BinaryReader& c1, BinaryReader& c2) {
                        c1.seek(12);
                        c2.seek(12);
                        const auto x1 = c1.read<uint32_t>();
                        const auto x2 = c2.read<uint32_t>();
                        const auto y1 = c1.read<uint32_t>();
                        const auto y2 = c2.read<uint32_t>();
                        c1.seek(0);
                        c2.seek(0);

                        return (x2 * 16 + x1) < (y2 * 16 + y1);
                    }

                    std::string MapTile::getLogHeader() const {
                        std::stringstream strm;
                        strm << "[X: " << mIndexX << " - Y: " << mIndexY << "]: ";
                        return strm.str();
                    }

                    void MapTile::readAllChunks(BinaryReader& reader, std::vector<BinaryReader>& mcnkList) {
                        while (reader.available() >= 8) {
                            const auto signature = reader.read<uint32_t>();
                            const auto size = reader.read<uint32_t>();
                            std::vector<uint8_t> chunkData(size);
                            if (size != 0) {
                                reader.read(chunkData);
                            }

                            if (signature == 'MCNK') {
                                mcnkList.emplace_back(chunkData);
                            } else {
                                mChunkData.emplace(signature, BinaryReader(chunkData));
                            }
                        }
                    }

                    void MapTile::loadSpecularTextures() {
                        for(const auto& texture : mTextures) {
							const auto extensionEnd = texture.rfind('.');
							const auto specularTexture = extensionEnd != std::string::npos ? (texture.substr(0, extensionEnd) + "_s.blp") : (texture + "_s.blp");
							mSpecularTextures.emplace_back(specularTexture);
                        }
                    }

                    void MapTile::loadTextureFlags() {
						const auto mtxpItr = mChunkData.find('MTXP');
                        if(mtxpItr != mChunkData.end()) {
							const auto numEntries = mtxpItr->second.getSize() / sizeof(TextureParameters);
							std::vector<TextureParameters> params(numEntries);
							mtxpItr->second.read(params);
                            for(auto i = 0u; i < params.size() && i < mTextures.size(); ++i) {
								const auto scale = params[i].flags.textureScale;
								mTextureScales[i] = static_cast<float>(scale);
								mHeightScales[i] = params[i].heightScale;
								mHeightOffsets[i] = params[i].heightOffset;
                            }

							if (hasHeightTextures()) {
								for (const auto& texture : mTextures) {
									const auto extensionEnd = texture.rfind('.');
									const auto heightTexture = extensionEnd != std::string::npos ? (texture.substr(0, extensionEnd) + "_h.blp") : (texture + "_h.blp");
									mHeightTextures.emplace_back(heightTexture);
								}
							}
							return;
                        }

						const auto itr = mChunkData.find('MTXF');
                        if(itr == mChunkData.end()) {
							return;
                        }

						std::vector<MtxfFlags> flags(itr->second.getSize() / sizeof(MtxfFlags));
						itr->second.read(flags);
                        for(auto i = 0u; i < flags.size() && i < mTextures.size(); ++i) {
							const auto scale = flags[i].textureScale;
							mTextureScales[i] = static_cast<float>(scale);
                        }
                    }

                    void MapTile::loadTextures() {
                        const auto itr = mChunkData.find('MTEX');
                        if (itr == mChunkData.end()) {
                            mLogger.warn(getLogHeader(), "Unable to find MTEX chunk. Tile probably will look strange");
                            return;
                        }

                        const auto dataPtr = reinterpret_cast<const char*>(itr->second.getData().data());
                        const auto stringSizes = itr->second.getSize();
                        auto curOffset = 0u;
                        while (curOffset < stringSizes) {
                            std::string str = dataPtr + curOffset;
                            curOffset += static_cast<uint32_t>(str.size()) + 1;
                            mTextures.push_back(str);
							mTextureScales.emplace_back(1.0f);
							mHeightScales.emplace_back(0.0f);
							mHeightOffsets.emplace_back(1.0f);
                        }

						loadSpecularTextures();
                    }

                    void MapTile::loadChunks() {
                        if (mBaseChunks.size() != 256 || mTexChunks.size() != 256 || mObjChunks.size() != 256) {
                            mLogger.error(getLogHeader(),
                                          "Invalid tile. Mismatch in number of MCNK chunks (should be 256): base=",
                                          mBaseChunks.size(), ", tex=", mTexChunks.size(), ", obj=", mObjChunks.size());
                            throw std::exception("Invalid tile");
                        }

                        mMapChunks.resize(256);

                        const auto floatMax = std::numeric_limits<float>::max();
                        const auto floatMin = -floatMax;

                        mBboxMin.set(floatMax, floatMax, floatMax);
                        mBboxMax.set(floatMin, floatMin, floatMin);

                        for (auto i = 0u; i < 256; ++i) {
                            auto& baseChunk = mBaseChunks[i];
                            auto& texChunk = mTexChunks[i];
                            auto& objChunk = mObjChunks[i];

                            const auto header = baseChunk.read<McnkHeader>();
                            const auto ix = header.indexX;
                            const auto iy = header.indexY;
                            if (ix >= 16 || iy >= 16) {
                                mLogger.warn(getLogHeader(), "Invalid chunk: ", i, ". header.indexX=", ix,
                                             ", header.indexY=", iy);
                                continue;
                            }

                            const auto chunk = std::make_shared<MapChunk>(header, baseChunk, texChunk, objChunk, mWdtFlags);
                            mMapChunks[ix + iy * 16] = chunk;

                            mBboxMin.takeMin(chunk->getBboxMin());
                            mBboxMax.takeMax(chunk->getBboxMax());
                        }
                    }

                    void MapTile::loadModels() {
						std::vector<uint32_t> mmid;
						std::vector<char> mmdx;

						auto itr = mChunkData.find('MMID');
                        if(itr != mChunkData.end() && itr->second.getSize() > 0) {
							const auto numElements = itr->second.getSize() / sizeof(uint32_t);
							mmid.resize(numElements);
							itr->second.read(mmid);
                        }

						itr = mChunkData.find('MMDX');
                        if(itr != mChunkData.end() && itr->second.getSize() > 0) {
							mmdx.resize(itr->second.getSize());
							itr->second.read(mmdx);
							mmdx.push_back(0);
                        }

						itr = mChunkData.find('MDDF');
                        if(itr == mChunkData.end() || itr->second.getSize() < sizeof(Mddf)) {
							return;
                        }

						std::vector<Mddf> mddfEntries;
						mddfEntries.resize(itr->second.getSize() / sizeof(Mddf));
						itr->second.read(mddfEntries);

						mModelDefinitions.reserve(mddfEntries.size());
                        for(const auto& mddf : mddfEntries) {
							M2ModelEntry e{};
							e.definition = mddf;
                            if(mddf.flags.isFileDataId != 0) {
								e.isFileDataId = true;
								e.fileDataId = mddf.nameId;
                            } else {
								e.isFileDataId = false;
                                if(mddf.nameId >= mmid.size()) {
									mLogger.warn("Invalid model definition found (mddf.nameId >= mmid.size). UUID: ", mddf.uniqueId);
									continue;
                                }

								const auto nameOffset = mmid.at(mddf.nameId);
                                if(nameOffset >= mmdx.size()) {
									mLogger.warn("Invalid model definition found (mmid[mddf.nameId] >= mmdx.size). UUID: ", mddf.uniqueId);
									continue;
                                }

								e.fileName = &mmdx[nameOffset];
                            }

							mModelDefinitions.emplace_back(e);
                        }

						mModelDefinitions.shrink_to_fit();
                    }

                    void MapTile::loadLiquids() {
						const auto itr = mChunkData.find('MH2O');
                        if(itr == mChunkData.end()) {
							return;
                        }

						liquid::H2OParser(mLiquidManager, itr->second);
                    }

                    void MapTile::combineBuffers() {
						mGridVertices.resize(8 * 145 * 256);
                        mVertices.resize(145 * 256);
                        for (auto i = 0u; i < 256; ++i) {
                            memcpy(mVertices.data() + i * 145, mMapChunks[i]->getVertices().data(),
                                   145 * sizeof(scene::terrain::MapTileVertex));
							memcpy(mGridVertices.data() + i * 8 * 145, mMapChunks[i]->getGridVertices().data(), 8 * 145 * sizeof(math::Vector3));
							mMapChunks[i]->clearVertices();
                        }
                    }

                    MapTile::MapTile(uint32_t indexX, uint32_t indexY, BinaryReader baseData, BinaryReader texData,
                                     BinaryReader objData, const WdtFlags& wdtFlags, const storage::StorageManagerPtr& storageManager) :
                        mIndexX(indexX), mIndexY(indexY), mWdtFlags(wdtFlags) {
						mLiquidManager = std::make_shared<liquid::TileLiquidManager>(storageManager->getLiquidStorageDao());

                        readAllChunks(baseData, mBaseChunks);
                        readAllChunks(objData, mObjChunks);
                        readAllChunks(texData, mTexChunks);

                        loadTextures();
						loadTextureFlags();

						loadLiquids();
						loadModels();

                        loadChunks();
                        combineBuffers();
                    }
                }
            }
        }
    }
}
