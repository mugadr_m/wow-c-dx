#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                namespace bfa {
#pragma pack(push, 1)
                    struct McnkHeader {
                        struct {
                            uint32_t mcsh : 1;
                            uint32_t impass : 1;
                            uint32_t liquidRiver : 1;
                            uint32_t liquidOcean : 1;
                            uint32_t liquidMagma : 1;
                            uint32_t liquidSlime : 1;
                            uint32_t hasMccv : 1;
                            uint32_t unk1 : 1;
                            uint32_t unk2 : 7;
                            uint32_t smallAlpha : 1;
                        } flags;

                        uint32_t indexX;
                        uint32_t indexY;
                        uint32_t numLayers;
						uint32_t numDoodadRefs;
                        uint64_t holes;
                        uint32_t ofsLayers;
						uint32_t ofsReferences;
                        uint32_t ofsAlpha;
                        uint32_t sizeAlpha;
                        uint32_t ofsShadow;
                        uint32_t sizeShadow;
                        uint32_t areaId;
                        uint32_t numMapObjectRefs;
                        uint16_t lowResHoles;
                        uint16_t unk1;
                        uint64_t detailDoodad;
						uint64_t detailDoodad2;
                        uint64_t noEffectDoodad;
                        uint32_t ofsSndEmitters;
                        uint32_t nSndEmitters;
                        uint32_t ofsLiquid;
                        uint32_t sizeLiquid;
                        float positionX, positionY, positionZ;
                        uint32_t ofsMccv;
                        uint32_t ofsMclv;
                        uint32_t unk2;
                    };

                    struct MapVertex {
						float x = 0.0f, y = 0.0f, z = 0.0f;
						float tx = 0.0f, ty = 0.0f;
						float ax = 0.0f, ay = 0.0f;
						float nx = 0.0f, ny = 0.0f, nz = 0.0f;
						float cvx = 1.0f, cvy = 1.0f, cvz = 1.0f, cvw = 1.0f;
						uint32_t clr = 0;
                    };

                    struct Mcly {
						uint32_t textureId;
                        struct {
							uint32_t animationRotation : 3;
							uint32_t animationSpeed : 3;
							uint32_t hasAnimation : 1;
							uint32_t bright : 1;
							uint32_t hasAlpha : 1;
							uint32_t alphaCompressed : 1;
							uint32_t useCubeMap : 1;
                        } flags;
						uint32_t alphaOffset;
						int32_t groundEffectId;
                    };

                    struct MtxfFlags {
						uint32_t useCubeMap : 1;
						uint32_t unk1 : 3;
						uint32_t textureScale : 4;
						uint32_t unk2 : 24;
                    };

                    struct TextureParameters {
						MtxfFlags flags{};
						float heightScale = 0.0f;
						float heightOffset = 1.0f;
						uint32_t padding = 0;
                    };

                    struct Mddf {
						uint32_t nameId;
						uint32_t uniqueId;
						float x, y, z;
						float rx, ry, rz;
						uint16_t scale;

						struct {
							uint16_t biodome : 1;
							uint16_t shrubs : 1;
							uint16_t unk4 : 1;
							uint16_t unk8 : 1;
							uint16_t unk16 : 1;
							uint16_t liquidSomething : 1;
							uint16_t isFileDataId : 1;
						} flags;
                    };
#pragma pack(pop)
                }
            }
        }
    }
}