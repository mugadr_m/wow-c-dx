#include "stdafx.h"
#include "MapChunk.h"
#include "utils/Constants.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                namespace bfa {
                    utils::Logger<MapChunk> MapChunk::mLogger;

                    void MapChunk::loadBaseLayer(uint32_t layer) {
                        for (auto i = 0u; i < 4096; ++i) {
                            mAlphaValues[i] |= 0xFF << layer * 8;
                        }
                    }

                    void MapChunk::loadUncompressed(uint32_t layer, const uint8_t* alphaData) {
						const auto* basePtr = alphaData + mLayers[layer].alphaOffset;
                        for(auto i = 0u; i < 4096; ++i) {
							mAlphaValues[i] |= basePtr[i] << layer * 8;
                        }
                    }

                    void MapChunk::loadCompressed(uint32_t layer, const uint8_t* alphaData) {
						const auto* basePtr = alphaData + mLayers[layer].alphaOffset;
						const auto fullAlpha = mHeader.flags.smallAlpha != 0;
						auto* outPtr = mAlphaValues.data();

                        for(auto k = 0u; k < (fullAlpha ? 64u : 63u); ++k) {
                            for(auto j = 0u; j < 32u; ++j) {
								const auto value = *basePtr++;
								auto val1 = value & 0x0F;
								auto val2 = j == 31 && !fullAlpha ? val1 : value >> 4 & 0xFF;

								val1 = static_cast<uint8_t>((val1 / 15.0f) * 255.0f);
								val2 = static_cast<uint8_t>((val2 / 15.0f) * 255.0f);

								*outPtr++ |= val1 << layer * 8;
								*outPtr++ |= val2 << layer * 8;
                            }
                        }

                        if(!fullAlpha) {
                            for(auto j = 0u; j < 64; ++j) {
								mAlphaValues[63 * 64 + j] |= ((mAlphaValues[62 * 64 + j] >> (layer * 8)) & 0xFF) << (layer * 8);
                            }
                        }
                    }

                    void MapChunk::loadRle(uint32_t layer, const uint8_t* alphaData) {
						const auto* basePtr = alphaData + mLayers[layer].alphaOffset;

						auto numOut = 0u;
                        while(numOut < 4096) {
							const auto indicator = *basePtr++;
							const auto repeat = indicator & 0x7Fu;
                            if((indicator & 0x80) != 0) {
								const auto value = *basePtr++;
                                for(auto k = 0u; k < repeat && numOut < 4096; ++k, ++numOut) {
									mAlphaValues[numOut] |= value << layer * 8;
                                }
                            } else {
                                for(auto k = 0u; k < repeat && numOut < 4096; ++k, ++numOut) {
									mAlphaValues[numOut] |= *basePtr++ << layer * 8;
                                }
                            }
                        }
                    }

                    void MapChunk::readChunks(BinaryReader& reader) {
                        while (reader.available() >= 8) {
                            const auto signature = reader.read<uint32_t>();
                            const auto size = reader.read<uint32_t>();
                            std::vector<uint8_t> data;
                            if (size > 0) {
                                data.resize(size);
                                reader.read(data);
                            }

                            if (mChunkMap.find(signature) != mChunkMap.end()) {
                                throw std::exception("Duplicate chunk");
                            }

                            mChunkMap.emplace(signature, BinaryReader(data));
                        }
                    }

                    void MapChunk::loadVertices() {
                        const auto itrMcvt = mChunkMap.find('MCVT');
                        if (itrMcvt == mChunkMap.end()) {
                            mLogger.error("Unable to find MCVT chunk");
                            throw std::exception("Error loading chunk");
                        }

                        std::vector<float> heights(145);
                        itrMcvt->second.read(heights);

                        mVertices.resize(145);
                        const auto posX = utils::MAP_MID_POINT - mHeader.positionY;
                        const auto posY = utils::MAP_MID_POINT - mHeader.positionX;
                        const auto posZ = mHeader.positionZ;

                        const auto floatMax = std::numeric_limits<float>::max();
                        const auto floatMin = -floatMax;

                        mBboxMin.set(floatMax, floatMax, floatMax);
                        mBboxMax.set(floatMin, floatMin, floatMin);

                        auto counter = 0u;
                        for (auto i = 0u; i < 17; ++i) {
                            for (auto j = 0u; j < (i % 2 != 0 ? 8u : 9u); ++j) {
                                const auto height = posZ + heights[counter];
                                auto x = posX + j * utils::UNIT_SIZE;
                                if (i % 2 != 0) {
                                    x += 0.5f * utils::UNIT_SIZE;
                                }

                                const auto y = posY + i * utils::UNIT_SIZE * 0.5f;
                                mVertices[counter].x = x;
                                mVertices[counter].y = y;
                                mVertices[counter].z = height;
								mVertices[counter].tx = j + (i % 2 != 0 ? 0.5f : 0.0f);
                                mVertices[counter].ty = i * 0.5f;
								mVertices[counter].ax = j / 8.0f + ((i % 2 != 0) ? 0.5f / 8.0f : 0.0f);
								mVertices[counter].ay = i / 16.0f;
                                ++counter;

                                const math::Vector3 pos(x, y, height);
                                mBboxMin.takeMin(pos);
                                mBboxMax.takeMax(pos);
                            }
                        }

						loadGrid();
                    }

                    void MapChunk::loadNormals() {
                        const auto itr = mChunkMap.find('MCNR');
                        std::vector<int8_t> normals(145 * 3);
                        if (itr != mChunkMap.end()) {
                            itr->second.read(normals);
                        } else {
                            normals.assign(145 * 3, 127);
                        }

                        for (auto counter = 0u; counter < 145; ++counter) {
                            const auto nx = normals[counter * 3];
                            const auto ny = normals[counter * 3 + 1];
                            const auto nz = normals[counter * 3 + 2];
                            mVertices[counter].nx = nx / -127.0f;
                            mVertices[counter].ny = ny / -127.0f;
                            mVertices[counter].nz = nz / 127.0f;
                        }
                    }

                    void MapChunk::loadVertexColors() {
                        const auto itr = mChunkMap.find('MCCV');
                        std::vector<uint32_t> colorValues(145);

                        if (mHeader.flags.hasMccv == 0 || itr == mChunkMap.end()) {
                            colorValues.assign(145, 0x7F7F7F7F);
                        } else {
                            itr->second.read(colorValues);
                        }

                        for (auto counter = 0u; counter < 145; ++counter) {
                            const auto& color = colorValues[counter];
							mVertices[counter].mccv = color;
                        }
                    }

                    void MapChunk::loadVertexLights() {
                        const auto itr = mChunkMap.find('MCLV');
                        std::vector<uint32_t> lights(145);
                        if (itr != mChunkMap.end()) {
                            itr->second.read(lights);
                        } else {
                            lights.assign(145, 0x00000000);
                        }

                        for (auto counter = 0u; counter < 145; ++counter) {
                            mVertices[counter].mclv = lights[counter];
                        }
                    }

                    void MapChunk::loadAlpha() {
                        const auto itr = mChunkMap.find('MCAL');
                        if (itr == mChunkMap.end()) {
                            return;
                        }

                        const auto& alphaData = itr->second.getData();
                        const auto* ptr = alphaData.data();
                        for (auto i = 0u; i < mLayers.size(); ++i) {
                            const auto& layer = mLayers[i];
                            if (layer.flags.alphaCompressed != 0) {
								loadRle(i, ptr);
                            } else if (layer.flags.hasAlpha != 0) {
                                if(mWdtFlags.hasUncompressedAlpha != 0 || mWdtFlags.hasHeightTextures != 0) {
									loadUncompressed(i, ptr);
                                } else {
									loadCompressed(i, ptr);
                                }
                            } else {
                                loadBaseLayer(i);
                            }
                        }
                    }

                    void MapChunk::loadLayers() {
                        const auto itr = mChunkMap.find('MCLY');
                        if (itr == mChunkMap.end()) {
                            return;
                        }

                        const auto numLayers = itr->second.getSize() / sizeof(Mcly);
						mLayers.resize(numLayers);

                        itr->second.read(mLayers);
                        for(const auto& layer : mLayers) {
							mTextures.push_back(layer.textureId);
                        }
                    }

                    void MapChunk::loadGrid() {
						mGridVertices.reserve(145 * 8);
                        for(auto i = 0u; i < 145; ++i) {
							const auto xmin = mVertices[i].x - 0.2f;
							const auto xmax = xmin + 0.4f;
							const auto ymin = mVertices[i].y - 0.2f;
							const auto ymax = ymin + 0.4f;
							const auto zmin = mVertices[i].z - 0.2f;
							const auto zmax = zmin + 0.4f;

							mGridVertices.emplace_back(xmin, ymin, zmin);
							mGridVertices.emplace_back(xmax, ymin, zmin);
							mGridVertices.emplace_back(xmax, ymax, zmin);
							mGridVertices.emplace_back(xmin, ymax, zmin);

							mGridVertices.emplace_back(xmin, ymin, zmax);
							mGridVertices.emplace_back(xmax, ymin, zmax);
							mGridVertices.emplace_back(xmax, ymax, zmax);
							mGridVertices.emplace_back(xmin, ymax, zmax);
                        }
                    }

                    MapChunk::MapChunk(McnkHeader header, BinaryReader& baseChunk, BinaryReader& texChunk,
                                       BinaryReader& objChunk, const WdtFlags& wdtFlags) :
                        mHeader(header), mWdtFlags(wdtFlags), mAlphaValues(4096) {

                        baseChunk.seek(sizeof(McnkHeader));
                        readChunks(baseChunk);
                        readChunks(texChunk);
                        readChunks(objChunk);

                        loadVertices();
                        loadNormals();
                        loadVertexColors();
                        loadVertexLights();
						loadLayers();
                        loadAlpha();
                    }
                }
            }
        }
    }
}
