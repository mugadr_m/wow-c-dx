#include "stdafx.h"
#include "MapTileLoader.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
				utils::Logger<MapTileLoader> MapTileLoader::mLogger;

                void MapTileLoader::onLoadComplete(DataLoadCompleteEvent& event) {
					mFileProvider = cdi::ApplicationContext::instance()->produce<IFileProvider>();

                }

                void MapTileLoader::postConstruct(const cdi::ApplicationContextPtr& ctx) {
                    ctx->registerEvent<DataLoadCompleteEvent>(std::bind(&MapTileLoader::onLoadComplete, this, std::placeholders::_1));

					mStorageManager = ctx->produce<storage::StorageManager>();
                }

                void MapTileLoader::onEnterWorld(const std::string& continent) {
					std::stringstream strm;
					strm << "World\\Maps\\" << continent << "\\" << continent << ".wdt";
                    try {
						auto wdtData = mFileProvider->openFile(strm.str());
						WdtFile wdtFile{ wdtData };
						mWdtFlags = wdtFile.getFlags();
					} catch (std::exception&) {
						mWdtFlags = WdtFlags{};
					}
                }

                bfa::MapTilePtr MapTileLoader::loadTile(const std::string& tile, uint32_t indexX, uint32_t indexY) const {
					std::stringstream strm;
					strm << "World\\Maps\\" << tile << "\\" << tile << "_" << indexX << "_" << indexY;
                    const auto basePath = strm.str();

					strm.str(std::string());
					strm << basePath << ".adt";

					const auto rootTile = strm.str();

					strm.str(std::string());
					strm << basePath << "_tex0.adt";
					const auto texTile = strm.str();

					strm.str(std::string());
					strm << basePath << "_obj0.adt";
					const auto objTile = strm.str();

					try {
						auto baseReader = mFileProvider->openFile(rootTile);
						auto texReader = mFileProvider->openFile(texTile);
						auto objReader = mFileProvider->openFile(objTile);

						return std::make_shared<bfa::MapTile>(indexX, indexY, baseReader, texReader, objReader, mWdtFlags, mStorageManager);
					} catch (std::exception& e) {
						mLogger.error("Error loading tile ", tile, "_", indexX, "_", indexY, ": ", e.what());
						return nullptr;
					}
                }
            }
        }
    }
}
