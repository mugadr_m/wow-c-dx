#pragma once
#include "bfa/MapTile.h"
#include "io/files/DataLoadCompleteEvent.h"
#include "io/files/IFileProvider.h"
#include "cdi/ApplicationContext.h"
#include "WdtFile.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                class MapTileLoader {
					static utils::Logger<MapTileLoader> mLogger;

					IFileProviderPtr mFileProvider;
					storage::StorageManagerPtr mStorageManager;
					WdtFlags mWdtFlags{};

                public:
					void onLoadComplete(DataLoadCompleteEvent& event);

					void postConstruct(const cdi::ApplicationContextPtr& ctx);

					void onEnterWorld(const std::string& continent);

					bfa::MapTilePtr loadTile(const std::string& tile, uint32_t indexX, uint32_t indexY) const;
                };

				SHARED_PTR(MapTileLoader);
            }
        }
    }
}
