#include "stdafx.h"
#include "WdtFile.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace map {
                WdtFile::WdtFile(BinaryReader& file) {
                    while(file.available() >= 8) {
						const auto signature = file.read<uint32_t>();
						const auto size = file.read<uint32_t>();
                        if(signature == 'MPHD') {
							mFlags = file.read<WdtFlags>();
							break;
                        }

                        file.seekMod(size);
                    }
                }
            }
        }
    }
}
