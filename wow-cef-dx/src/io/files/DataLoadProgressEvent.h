#pragma once
#include "DataLoadInitEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			class DataLoadProgressEvent {
				std::string mMessage;

			public:
				explicit DataLoadProgressEvent(const std::string& message) : mMessage(message) { }

				void toJson(rapidjson::Document& document) const;
				rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
				static DataLoadInitEvent fromJson(const rapidjson::Document& document);
			};
		}
	}
}
