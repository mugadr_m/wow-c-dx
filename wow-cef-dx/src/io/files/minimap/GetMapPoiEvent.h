#pragma once
#include "MapPoi.h"
#include <document.h>

namespace wowc {
    namespace io {
        namespace files {
            namespace minimap {
                class GetMapPoiEvent {
                    std::vector<MapPoi> mMapPois;
					uint32_t mMapId;

                public:
                    explicit GetMapPoiEvent(uint32_t mapId)
                        : mMapId(mapId) {
                    }

                    const std::vector<MapPoi>& getMapPois() const {
                        return mMapPois;
                    }

                    void setMapPois(const std::vector<MapPoi>& mapPois) {
                        mMapPois = mapPois;
                    }

					uint32_t getMapId() const { return mMapId; }

                    void toJson(rapidjson::Document& document) const {
                    }

                    rapidjson::Document getResponseJson() const;
					static GetMapPoiEvent fromJson(const rapidjson::Document& document);
                };
            }
        }
    }
}
