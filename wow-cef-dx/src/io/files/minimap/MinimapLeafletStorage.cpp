#include "stdafx.h"
#include "MinimapLeafletStorage.h"
#include "cdi/ApplicationContext.h"
#include "io/files/storage/MapStorageDao.h"
#include "io/files/storage/StorageManager.h"
#include "io/image/BlpImageLoader.h"
#include "utils/String.h"
#include "ui/events/EventManager.h"
#include "GetMapPoiEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace minimap {
                bool MinimapLeafletStorage::openFromCache(uint64_t cacheKey, png::image<png::rgba_pixel>& outImage) {
                    std::vector<uint8_t> cacheEntry;
                    {
                        std::lock_guard<std::mutex> l(mDataLock);
                        const auto itr = mCache.find(cacheKey);
                        if (itr != mCache.end()) {
                            cacheEntry = itr->second;
                        }
                    }

                    if (!cacheEntry.empty()) {
                        for (auto i = 0u; i < 256; ++i) {
                            memcpy(outImage[i].data(), cacheEntry.data() + i * 256 * 4, 256 * 4);
                        }

                        return true;
                    }

                    return false;
                }

                void MinimapLeafletStorage::addToCache(uint64_t cacheKey, const png::image<png::rgba_pixel>& outImage) {
                    std::vector<uint8_t> dataBuffer(256 * 256 * 4);
                    for (auto i = 0u; i < 256; ++i) {
                        memcpy(dataBuffer.data() + i * 256 * 4, outImage[i].data(), 256 * 4);
                    }
                    {
                        std::lock_guard<std::mutex> l(mDataLock);
                        mCache.emplace(cacheKey, dataBuffer);
                    }
                }

                void MinimapLeafletStorage::switchToMap(uint32_t mapId) {
                    {
                        std::lock_guard<std::mutex> l(mDataLock);
                        mCache.clear();
                    }

                    const auto ctx = cdi::ApplicationContext::instance();
                    const auto mapDao = ctx->produce<storage::StorageManager>()->getMapStroageDao();
                    mMapBasePath = utils::String::toLower(mapDao->getMapDirectory(mapId));
                    if (mFileProvider == nullptr) {
                        mFileProvider = cdi::ApplicationContext::instance()->produce<IFileProvider>();
                    }

                    mActiveMap = mapId;
                }

                BinaryReader MinimapLeafletStorage::openTile(uint32_t tileX, uint32_t tileY) const {
                    std::stringstream strm;
                    strm << "World\\Minimaps\\" << mMapBasePath << "\\Map" << std::setw(2) << std::setfill('0') << tileX
                        << "_" << std::setw(2) << std::setfill('0') << tileY << ".blp";
                    return mFileProvider->openFile(strm.str());
                }

                void MinimapLeafletStorage::onHandleGetMapPois(GetMapPoiEvent& event) {
					const auto ctx = cdi::ApplicationContext::instance();
					const auto mapDao = ctx->produce<storage::StorageManager>()->getMapStroageDao();
					event.setMapPois(mapDao->getPoisForMap(event.getMapId()));
                }

                void MinimapLeafletStorage::readImage(png::image<png::rgba_pixel>& outImage, uint32_t mapId,
                                                      uint32_t zoomLevel,
                                                      int32_t tileX, int32_t tileY) {
                    if (static_cast<int32_t>(mapId) < 0) {
                        throw std::exception("Invalid map id. Cannot be < 0");
                    }

                    if (tileX < 0 || tileY < 0 || tileX >= 64 || tileY >= 64) {
                        throw std::exception("Out of range tile indices");
                    }

                    if (mapId != mActiveMap) {
                        switchToMap(mapId);
                    }

                    zoomLevel = std::min<uint32_t>(zoomLevel, 6);
                    const auto numTiles = 64u >> zoomLevel;
                    const auto perTilePixels = 256u / numTiles;

                    outImage = png::image<png::rgba_pixel>(256, 256);

                    const auto baseTileX = tileX * numTiles;
                    const auto baseTileY = tileY * numTiles;

                    const auto cacheKey = tileX + tileY * 64 + zoomLevel * 4096;
                    if (openFromCache(cacheKey, outImage)) {
                        return;
                    }

                    std::vector<std::shared_future<void>> futures;
                    for (auto y = 0u; y < numTiles; ++y) {
                        for (auto x = 0u; x < numTiles; ++x) {
                            futures.push_back(mLoaderThreadPool.pushWork(
                                [x, y, baseTileX, baseTileY, this, perTilePixels, &outImage] {
                                    try {
                                        png::image<png::rgba_pixel> perTileImage;

                                        auto tile = openTile(baseTileX + x, baseTileY + y);

                                        image::BlpImageLoader::convertToPng(perTileImage, tile, perTilePixels, perTilePixels);

                                        const auto pixelAdvance = perTileImage.get_width() / static_cast<float>(perTilePixels);
                                        const auto rowAdvance = perTileImage.get_height() / static_cast<float>(perTilePixels);

                                        for (auto iy = 0u; iy < perTilePixels; ++iy) {
                                            for (auto ix = 0u; ix < perTilePixels; ++ix) {
                                                const auto curRow = static_cast<uint32_t>(rowAdvance * iy);
                                                const auto curPixel = static_cast<uint32_t>(pixelAdvance * ix);
                                                const auto& buf = perTileImage[curRow];
                                                const auto& color = buf[curPixel];

                                                const auto targetRow = y * perTilePixels + iy;
                                                const auto targetCol = x * perTilePixels + ix;
                                                outImage[targetRow][targetCol] = color;
                                            }
                                        }

                                    } catch (std::exception&) {

                                    }
                                }));
                        }
                    }

                    std::for_each(futures.begin(), futures.end(), [](const auto& future) { future.get(); });
                    addToCache(cacheKey, outImage);
                }

                void MinimapLeafletStorage::preDestroy() {
                    mLoaderThreadPool.shutdown();
                }

                void MinimapLeafletStorage::attachUiEvents(const cdi::ApplicationContextPtr& ctx) {
					ctx->registerEvent<GetMapPoiEvent>([](auto& event) { onHandleGetMapPois(event); });
					ctx->produce<ui::events::EventManager>()->registerEvent<GetMapPoiEvent>();
                }
            }
        }
    }
}
