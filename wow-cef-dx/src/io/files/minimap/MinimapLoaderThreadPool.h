#pragma once
#include <future>

namespace wowc {
	namespace io {
		namespace files {
			namespace minimap {
				class MinimapLoaderThreadPool {
					std::vector<std::thread> mLoaderThreads;
					std::mutex mWorkLock;
					std::condition_variable mWorkEvent;
					std::list<std::shared_ptr<std::packaged_task<void()>>> mWorkItems;
					bool mIsRunning = true;

					void workerThread();

				public:
					MinimapLoaderThreadPool();

					void shutdown();

					std::shared_future<void> pushWork(const std::function<void()>& callback);
				};
			}
		}
	}
}
