#include "stdafx.h"
#include "GetMapPoiEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace minimap {
                rapidjson::Document GetMapPoiEvent::getResponseJson() const {
                    rapidjson::Document document;
                    auto& allocator = document.GetAllocator();

                    auto& root = document.SetObject();
                    rapidjson::Value poiListArray(rapidjson::kArrayType);
                    for (auto& poi : mMapPois) {
                        const auto& locationName = poi.getLocation();
                        const auto x = poi.getPositionX();
                        const auto y = poi.getPositionY();

                        rapidjson::Value locationNameValue(rapidjson::kStringType);
                        locationNameValue.SetString(locationName.c_str(), allocator);

                        rapidjson::Value xValue(rapidjson::kNumberType);
                        xValue.SetFloat(x);

                        rapidjson::Value yValue(rapidjson::kNumberType);
                        yValue.SetFloat(y);

                        rapidjson::Value objectEntry(rapidjson::kObjectType);
                        objectEntry.AddMember("name", locationNameValue, allocator);
                        objectEntry.AddMember("x", xValue, allocator);
                        objectEntry.AddMember("y", yValue, allocator);
                        poiListArray.PushBack(objectEntry, allocator);
                    }

                    root.AddMember("pois", poiListArray, allocator);

                    return document;
                }

                GetMapPoiEvent GetMapPoiEvent::fromJson(const rapidjson::Document& document) {
					const auto mapId = document.FindMember("mapId");
                    if(mapId == document.MemberEnd()) {
						throw std::exception("Missing member 'mapId'");
                    }

					return GetMapPoiEvent(mapId->value.GetInt());
                }
            }
        }
    }
}
