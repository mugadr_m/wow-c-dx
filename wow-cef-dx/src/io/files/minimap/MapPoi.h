#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace minimap {
                class MapPoi {
                    std::string mLocation;
                    float mPositionX = 0.0f;
                    float mPositionY = 0.0f;

                public:
                    MapPoi() = default;

                    MapPoi(std::string location, float positionX, float positionY)
                        : mLocation(std::move(location)),
                          mPositionX(positionX),
                          mPositionY(positionY) {
                    }


                    const std::string& getLocation() const {
                        return mLocation;
                    }

                    void setLocation(const std::string& location) {
                        mLocation = location;
                    }

                    float getPositionX() const {
                        return mPositionX;
                    }

                    void setPositionX(const float positionX) {
                        mPositionX = positionX;
                    }

                    float getPositionY() const {
                        return mPositionY;
                    }

                    void setPositionY(const float positionY) {
                        mPositionY = positionY;
                    }
                };
            }
        }
    }
}
