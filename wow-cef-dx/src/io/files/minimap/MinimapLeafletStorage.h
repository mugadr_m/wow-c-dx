#pragma once
#include "third_party/pngpp/rgba_pixel.hpp"
#include "third_party/pngpp/image.hpp"
#include "io/BinaryReader.h"
#include "io/files/IFileProvider.h"
#include "MinimapLoaderThreadPool.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace minimap {
                class GetMapPoiEvent;

                class MinimapLeafletStorage {
                    uint32_t mActiveMap = static_cast<uint32_t>(-1);
                    std::string mMapBasePath;
                    IFileProviderPtr mFileProvider;
                    std::mutex mDataLock;
                    std::map<uint64_t, std::vector<uint8_t>> mCache;

                    MinimapLoaderThreadPool mLoaderThreadPool;

                    bool openFromCache(uint64_t cacheKey, png::image<png::rgba_pixel>& outImage);
                    void addToCache(uint64_t cacheKey, const png::image<png::rgba_pixel>& outImage);

                    void switchToMap(uint32_t mapId);
                    BinaryReader openTile(uint32_t tileX, uint32_t tileY) const;

                    static void onHandleGetMapPois(GetMapPoiEvent& event);

                public:
                    void readImage(png::image<png::rgba_pixel>& outImage, uint32_t mapId, uint32_t zoomLevel,
                                   int32_t tileX, int32_t tileY);

                    void preDestroy();

                    static void attachUiEvents(const cdi::ApplicationContextPtr& ctx);
                };

                SHARED_PTR(MinimapLeafletStorage);
            }
        }
    }
}
