#include "stdafx.h"
#include "MinimapLoaderThreadPool.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace minimap {

				void MinimapLoaderThreadPool::workerThread() {
					while(mIsRunning) {
						std::shared_ptr<std::packaged_task<void()>> workItem;
						{
							std::unique_lock<std::mutex> l(mWorkLock);
							if(mWorkItems.empty()) {
								mWorkEvent.wait(l);
								continue;
							}

							workItem = mWorkItems.front();
							mWorkItems.pop_front();
						}

						(*workItem)();
					}
				}

				MinimapLoaderThreadPool::MinimapLoaderThreadPool() {
					const auto numThreads = 2 * std::thread::hardware_concurrency();
					for(auto i = 0u; i < numThreads; ++i) {
						mLoaderThreads.emplace_back(std::bind(&MinimapLoaderThreadPool::workerThread, this));
					}
				}

				void MinimapLoaderThreadPool::shutdown() {
					mIsRunning = false;
					mWorkEvent.notify_all();
					for(auto& thread : mLoaderThreads) {
						if(thread.joinable()) {
							thread.join();
						}
					}
				}

				std::shared_future<void> MinimapLoaderThreadPool::pushWork(const std::function<void()>& callback) {
					auto task = std::make_shared<std::packaged_task<void()>>(callback);
					{
						std::unique_lock<std::mutex> l(mWorkLock);
						mWorkItems.emplace_back(task);
					}

					mWorkEvent.notify_one();
					return task->get_future();
				}
			}
		}
	}
}