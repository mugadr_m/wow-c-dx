#include "stdafx.h"
#include "TokenConfig.h"
#include "utils/String.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				void TokenConfig::parse(std::istream& inputStream) {
					auto hasHeader = false;
					std::string currentLine;
					std::vector<std::string> fields;
					std::stringstream tmpStream;
					while(std::getline(inputStream, currentLine)) {
						currentLine = utils::String::trim(currentLine);
						if(currentLine.empty()) {
							continue;
						}

						if(*currentLine.begin() == '#') {
							continue;
						}

						auto tokens = utils::String::split(currentLine, '|');
						if(!hasHeader) {
							hasHeader = true;
							std::for_each(tokens.begin(), tokens.end(), [&fields](const auto& token) {
								auto idx = token.find('!');
								if(idx < 0) {
									fields.push_back(utils::String::trim(token));
								} else {
									fields.push_back(utils::String::trim(token.substr(0, idx)));
								}
							});
						} else {
							auto index = 0u;
							std::map<std::string, std::string> row;
							std::for_each(tokens.begin(), tokens.end(), [&index, &row, &fields](const auto& token) {
								row.emplace(fields[index++], token);
							});
							mValueMap.emplace_back(row);
						}
					}
				}

			    void TokenConfig::parse(const BinaryReader& reader) {
					std::stringstream strm;
					strm.write(reinterpret_cast<const char*>(reader.getData().data()), reader.getSize());
					parse(strm);
			    }
			}
		}
	}
}
