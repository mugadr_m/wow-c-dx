#pragma once
#include "CascKeys.h"
#include <unordered_map>
#include "io/BinaryReader.h"
#include "Structures.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class RootParser {
					std::unordered_multimap<uint64_t, FileKeyMd5>& mRootDataMap;
					std::unordered_multimap<uint32_t, FileKeyMd5>& mFileDataIds;

				public:
					RootParser(std::unordered_multimap<uint64_t, FileKeyMd5>& rootDataMap,
					           std::unordered_multimap<uint32_t, FileKeyMd5>& fileDataIdMap) :
						mRootDataMap(rootDataMap), mFileDataIds(fileDataIdMap) {

					}

					void parse(BinaryReader& reader, const LocaleFlags& localeFlags) const;
				};
			}
		}
	}
}
