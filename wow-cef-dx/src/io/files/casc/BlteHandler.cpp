#include "stdafx.h"
#include "BlteHandler.h"
#include <boost/endian/conversion.hpp>
#include <zlib.h>

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				struct DataBlock {
					uint32_t compressedSize;
					uint32_t decompressedSize;
					uint8_t hash[16];
				};

				void BlteHandler::handleCompressedBlock(uint32_t frameHeaderSize, uint64_t decompressedSize, std::vector<uint8_t>& compressedData) {
					z_stream stream{};
					inflateInit(&stream);
					if (frameHeaderSize != 0) {
						std::vector<uint8_t> decompressedData(decompressedSize);
						
						stream.next_in = compressedData.data() + 1;
						stream.avail_in = static_cast<uInt>(compressedData.size() - 1);
						stream.next_out = decompressedData.data();
						stream.avail_out = static_cast<uInt>(decompressedSize);

						while (stream.avail_in) {
							const auto res = inflate(&stream, Z_FULL_FLUSH);
							if (res < 0) {
								throw std::exception("Error decompressing ZStream");
							}

							if (res == Z_STREAM_END) {
								break;
							}
						}

						mDecompressedData.insert(mDecompressedData.end(), decompressedData.begin(), decompressedData.end());
					} else {
						std::vector<uint8_t> chunk(compressedData.size());
						stream.next_in = compressedData.data() + 1;
						stream.avail_in = static_cast<uInt>(compressedData.size() - 1);
						while(stream.avail_in) {
							stream.next_out = chunk.data();
							stream.avail_out = static_cast<uInt>(chunk.size());
							const auto res = inflate(&stream, Z_FULL_FLUSH);
							if (res < 0) {
								throw std::exception("Error decompressing ZStream");
							}

							const auto numRead = chunk.size() - stream.avail_out;
							mDecompressedData.insert(mDecompressedData.end(), chunk.begin(), chunk.begin() + numRead);

							if (res == Z_STREAM_END) {
								break;
							}
						}
					}

					inflateEnd(&stream);
				}

				void BlteHandler::handleBlock(uint32_t frameHeaderSize, uint32_t decompressedSize, uint32_t compressedSize, BinaryReader& reader) {
					std::vector<uint8_t> blockData(compressedSize);
					reader.read(blockData);
					const auto indicator = blockData[0];
					switch(indicator) {
					case 0x45:
						throw std::exception("Encrypted files not supported");

					case 0x46:
						throw std::exception("Recursive frames not supported");

					case 0x4E: {
						mDecompressedData.insert(mDecompressedData.end(), blockData.begin() + 1, blockData.end());
						break;
					}

					case 0x5A: {
						handleCompressedBlock(frameHeaderSize, decompressedSize, blockData);
						break;
					}

					default: {
						throw std::exception("Unrecognized frame indicator");
					}
					}
				}

				void BlteHandler::parse(BinaryReader& reader) {
					if (reader.getSize() < 8) {
						throw std::exception("Cannot parse BLTE stream: Not enough data");
					}

					const auto magic = reader.read<uint32_t>();
					if (magic != MAGIC) {
						throw std::exception("Cannot parse BLTE stream: Invalid magic");
					}

					const auto headerSize = reader.readIntBE();
					auto blockCount = 1;
					if (headerSize > 0) {
						if (reader.read<uint8_t>() != 0x0F) {
							throw std::exception("Cannot parse BLTE stream: Invalid header");
						}

						blockCount = (reader.read<uint8_t>() << 16) | (reader.read<uint8_t>() << 8) | reader.read<uint8_t>();
						if (blockCount == 0) {
							throw std::exception("Cannot parse BLTE stream: Invalid header");
						}

						const auto frameHeaderSize = 24u * blockCount + 12u;
						if (frameHeaderSize != headerSize) {
							throw std::exception("Cannot parse BLTE stream: Invalid header sizes");
						}
					}

					std::vector<DataBlock> dataBlocks(blockCount);
					if (headerSize != 0) {
						reader.read(dataBlocks);
						for (auto& block : dataBlocks) {
							boost::endian::endian_reverse_inplace(block.compressedSize);
							boost::endian::endian_reverse_inplace(block.decompressedSize);
						}
					} else {
						for (auto& block : dataBlocks) {
							block.compressedSize = static_cast<uint32_t>(reader.getSize() - 8);
							block.decompressedSize = static_cast<uint32_t>(reader.getSize() - 9);
						}
					}

					for (const auto& block : dataBlocks) {
						handleBlock(headerSize, block.decompressedSize, block.compressedSize, reader);
					}
				}

				BlteHandler::BlteHandler(BinaryReader& reader) {
					parse(reader);
				}
			}
		}
	}
}
