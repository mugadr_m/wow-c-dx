#pragma once
#include <document.h>

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                class OpenLocalFolderResultEvent {
					std::string mFolderPath;

                public:
					explicit OpenLocalFolderResultEvent(std::string folderPath);
                    OpenLocalFolderResultEvent() = default;


                    const std::string& getFolderPath() const {
						return mFolderPath;
					}

					void toJson(rapidjson::Document& document) const;
					rapidjson::Document getResponseJson() const;
					static OpenLocalFolderResultEvent fromJson(const rapidjson::Document& document) { return {}; }
                };
            }
        }
    }
}
