#include "stdafx.h"
#include "LocalCascHandler.h"
#include "ui/events/EventManager.h"
#include "utils/Platform.h"
#include "OpenLocalFolderResultEvent.h"
#include <fstream>
#include "TokenConfig.h"
#include "utils/String.h"
#include "OpenLocalBuildEvent.h"
#include <boost/thread/futures/launch.hpp>
#include <boost/thread/future.hpp>
#include "io/files/DataLoadInitEvent.h"
#include "io/files/DataLoadProgressEvent.h"
#include <experimental/filesystem>
#include "CascKeys.h"
#include <boost/algorithm/hex.hpp>
#include "BlteHandler.h"
#include "EncodingFileParser.h"
#include "RootParser.h"
#include "Jenkins96.h"
#include "io/files/DataLoadCompleteEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                utils::Logger<LocalCascHandler> LocalCascHandler::mLogger;
				utils::Logger<DataStream> DataStream::mLogger;

                DataStream::DataStream(const std::string& path, uint32_t index) {
					std::stringstream strm;
					strm << path << "/data/data/data." << std::setw(3) << std::setfill('0') << index;

					mFileHandle = CreateFile(strm.str().c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr,
						OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

                    if(mFileHandle == nullptr) {
						mLogger.error("Error opening file: ", strm.str(), ": ", GetLastError());
						throw std::exception("Error opening file");
                    }

					mFileMapping = CreateFileMapping(mFileHandle, nullptr, PAGE_READONLY, 0, 0, nullptr);
                    if(mFileMapping == nullptr) {
						mLogger.error("Error creating file mapping: ", GetLastError());
						CloseHandle(mFileHandle);
						mFileHandle = nullptr;
						throw std::exception("Error creating file mapping");
                    }
                }

                DataStream::~DataStream() {
                    if(mFileMapping != nullptr) {
						CloseHandle(mFileMapping);
                    }

                    if(mFileHandle != nullptr) {
						CloseHandle(mFileHandle);
                    }
                }

                BinaryReader DataStream::read(uint32_t offset, uint32_t size) const {
					static uint32_t allocationMask = 0;
					static uint32_t offsetMask = 0;
					static std::once_flag flag;
                    std::call_once(flag, []() {
						SYSTEM_INFO info{};
						GetSystemInfo(&info);
						offsetMask = info.dwAllocationGranularity - 1;
						allocationMask = ~offsetMask;
					});

					const auto actualAddress = offset & allocationMask;
					const auto allocationOffset = offset & offsetMask;
					size += allocationOffset;

					const auto view = reinterpret_cast<uint8_t*>(MapViewOfFile(mFileMapping, FILE_MAP_READ, 0, actualAddress, size));
                    if(view == nullptr) {
						mLogger.error("Error mapping range: [", offset, ", ", offset + size, "): ", GetLastError());
						throw std::exception("Error mapping view");
                    }

					std::vector<uint8_t> data(size);
					memcpy(data.data(), view + allocationOffset, size - allocationOffset);
					UnmapViewOfFile(view);
					return BinaryReader(data);
                }

                void LocalCascHandler::onOpenLocalFolderEvent(OpenLocalFolderEvent& event) {
                    utils::Platform::getOrCreate()->asyncBrowseForFolder([](bool success, std::string folder) {
                        if (!success) {
                            return;
                        }

                        const OpenLocalFolderResultEvent result{folder};
                        cdi::ApplicationContext::instance()
                            ->produce<ui::events::EventManager>()->onApplicationEvent(result);
                    });
                }

                void LocalCascHandler::onGetLocalBuildInfo(GetLocalBuildInfoEvent& event) {
                    std::stringstream strm;
                    const auto buildInfo = tryFindBuildInfo(event.getPath());
                    TokenConfig buildConfig;
                    buildConfig.parse(buildInfo);

                    for (auto build : buildConfig) {
                        const auto version = build["Version"];
                        const auto active = build["Active"];
                        const auto tags = build["Tags"];
                        const auto branch = build["Branch"];
                        const auto lastUsed = build["Last Activated"];
                        const auto buildKey = build["Build Key"];

                        const auto parts = utils::String::split(version, '.');
                        std::string buildId{"-1"};
                        if (parts.size() == 4) {
                            buildId = parts[3];
                        }

                        event.addBuildInfo(LocalBuildInfo{
                            version,
                            std::stoi(active) != 0,
                            tags,
                            std::stoi(buildId),
                            lastUsed,
                            branch,
                            buildKey
                        });
                    }
                }

                void LocalCascHandler::onOpenLocalBuildEvent(OpenLocalBuildEvent& event) {
                    const auto buildKey = event.getBuildKey();
                    const auto path = utils::String::replaceAll(event.getPath(), "\\", "/");

					mBuildId = event.getBuildId();

                    async(boost::launch::async, [buildKey, path, this]() {
                        mEventManager->onApplicationEvent(DataLoadInitEvent());

						try {
							publishProgress("Loading build information...");
							loadBuildConfig(path, buildKey);
							loadIndexFiles(path);
							parseIndexFiles(path);
							parseEncodingFile();
							parseRootFile();
							const auto ctx = cdi::ApplicationContext::instance();
							ctx->registerSingleton<IFileProvider>(shared_from_this());
							DataLoadCompleteEvent completeEvent(mBuildId);
							ctx->publishEvent(completeEvent);
						} catch (std::exception& e) {
							mLogger.error(e.what());
						}
                    });
                }

                BinaryReader LocalCascHandler::tryFindBuildInfo(const std::string& path) {
                    std::stringstream strm;
                    strm << path << "\\.build.info";
                    try {
                        return openFileInMemory(strm.str());
                    } catch (std::exception&) {
                        throw;
                    }
                }

                BinaryReader LocalCascHandler::openFileInMemory(const std::string& path) {
                    std::ifstream inStream(path, std::ios::binary);
                    if (!inStream.is_open()) {
                        mLogger.warn("Unable to open file: ", path);
                        throw std::exception("file not found");
                    }

                    inStream.seekg(0, std::ios::end);
                    const auto size = static_cast<std::size_t>(inStream.tellg());
                    inStream.seekg(0, std::ios::beg);
                    std::vector<uint8_t> fileData(size);
                    if (!inStream.read(reinterpret_cast<char*>(fileData.data()), fileData.size())) {
                        mLogger.warn("Cannot fully read file: ", path);
                        throw std::exception("Error reading file");
                    }

                    inStream.close();
                    return BinaryReader{fileData};
                }

                void LocalCascHandler::publishProgress(const std::string& message) const {
                    mEventManager->onApplicationEvent(DataLoadProgressEvent(message));
                }

                void LocalCascHandler::loadBuildConfig(const std::string& basePath, const std::string& buildKey) {
                    std::stringstream strm;
                    strm << "Loading build config from: " << buildKey;
                    publishProgress(strm.str());

                    strm.str(std::string());
                    strm << basePath << "/data/config/" << buildKey.substr(0, 2) << "/" << buildKey.substr(2, 2) << "/"
                        << buildKey;

                    mBuildConfig.parse(openFileInMemory(strm.str()));
                }

                void LocalCascHandler::loadIndexFiles(const std::string& basePath) {
                    namespace fs = std::experimental::filesystem;
                    std::stringstream strm;
                    strm << basePath << "/data/data";
					std::array<std::vector<std::string>, 16> indexLists;

                    for (auto itr = fs::directory_iterator(fs::path(strm.str())); 
						itr != fs::directory_iterator(); ++itr) {
                        if(itr->status().type() != fs::file_type::regular || !itr->path().has_extension()) {
							continue;
                        }

						const auto extension = utils::String::toLower(itr->path().extension().string());
                        if(extension != ".idx") {
							continue;
                        }

						const auto fileName = itr->path().filename().string();
						const auto prefix = std::stoi(fileName.substr(0, 2), nullptr, 16);
                        if(prefix >= 0x10) {
							continue;
                        }

						indexLists[prefix].emplace_back(fileName);
                    }

                    for(auto& list : indexLists) {
						std::sort(list.begin(), list.end());
						mIndexFiles.emplace_back(*list.rbegin());
                    }
                }

                void LocalCascHandler::parseIndexFiles(const std::string& basePath) {
                    for(const auto& indexFile : mIndexFiles) {
						std::stringstream strm;
						strm << "Parsing index file: " << indexFile;
						publishProgress(strm.str());
						strm.str(std::string());

						strm << basePath << "/data/data/" << indexFile;
						auto reader = openFileInMemory(strm.str());
						const auto len = reader.read<uint32_t>();
						reader.seek((8 + len + 0x0F) & 0xFFFFFFF0);
						const auto dataLen = reader.read<uint32_t>();
						reader.seekMod(4);

						const auto numBlocks = dataLen / 18;
                        for(auto i = 0u; i < numBlocks; ++i) {
							FileKeyMd5 key{};
							reader.read(&key, 9);
							const uint32_t indexHigh = reader.read<uint8_t>();
							const auto indexLow = reader.readIntBE();

							const auto size = reader.read<uint32_t>();
							const auto index = (indexHigh << 2) | ((indexLow & 0xC0000000) >> 30);
							const auto offset = indexLow & 0x3FFFFFFF;

                            if(mIndexEntries.find(key) != mIndexEntries.end()) {
								continue;
                            } 

							mIndexEntries.emplace(key, IndexEntry{ index, offset, size });

                            if(mDataStreams.find(index) == mDataStreams.end()) {
								mDataStreams.emplace(index, std::make_shared<DataStream>(basePath, index));
                            }
                        }
                    }
                }

                void LocalCascHandler::parseEncodingFile() {
					const auto encodingKeys = mBuildConfig["encoding"];
                    if(encodingKeys.size() != 2) {
						mLogger.error("Invalid build config, coudl not find 2 encoding keys, got: ", encodingKeys.size());
						throw std::exception("Invalid build config");
                    }

					publishProgress("Loading encoding file...");

					FileKeyMd5 encodingKey;
					boost::algorithm::unhex(encodingKeys[1], encodingKey.begin());
					auto blteEncoding = openFile(getIndexEntry(encodingKey));
					publishProgress("Parsing encoding file...");
					BinaryReader encodingData(BlteHandler(blteEncoding).getDecompressedData());
					EncodingFileParser(mEncodingEntries).parse(encodingData);
					mLogger.info("Found ", mEncodingEntries.size(), " encoding entries");
                }

                void LocalCascHandler::parseRootFile() {
					const auto rootKeys = mBuildConfig["root"];
                    if(rootKeys.empty()) {
						mLogger.error("Invalid build config, no root key found");
						throw std::exception("Invalid build config");
                    }

					FileKeyMd5 key;
					boost::algorithm::unhex(rootKeys[0], key.begin());
					const auto itr = mEncodingEntries.find(key);
                    if(itr == mEncodingEntries.end()) {
						mLogger.error("Root key '", rootKeys[0], "' not found in encoding table");
						throw std::exception("Root key not found");
                    }

					publishProgress("Loading root file...");
					auto blteEncoding = openFile(getIndexEntry(itr->second.key));
					publishProgress("Parsing root file...");
					BinaryReader rootData(BlteHandler(blteEncoding).getDecompressedData());
					RootParser(mRootData, mFileDataMap).parse(rootData, LocaleFlags::ALL);
					mLogger.info("Found ", mRootData.size(), " root entries");
                }

                BinaryReader LocalCascHandler::openFile(const IndexEntry& indexEntry) {
					const auto itr = mDataStreams.find(indexEntry.index);
                    if(itr == mDataStreams.end()) {
						mLogger.error("Unable to find data stream with index: ", indexEntry.index);
						throw std::exception("Unable to open data stream");
                    }

					return itr->second->read(indexEntry.offset + 30, indexEntry.size - 30);
                }

                IndexEntry LocalCascHandler::getIndexEntry(const FileKeyMd5& key) {
					FileKeyMd5 shortKey;
					memcpy(&shortKey, &key, 9);
					return mIndexEntries.at(shortKey);
                }

                bool LocalCascHandler::tryGetIndexEntry(const FileKeyMd5& key, IndexEntry& indexEntry) {
					FileKeyMd5 shortKey;
					memcpy(&shortKey, &key, 9);
					const auto itr = mIndexEntries.find(shortKey);
                    if(itr == mIndexEntries.end()) {
						return false;
                    }

					indexEntry = itr->second;
					return true;
                }

                BinaryReader LocalCascHandler::openFile(const std::string& name) {
					auto upperName = utils::String::toUpper(name);
					upperName = utils::String::replaceAll(upperName, "/", "\\");
					const auto hash = Jenkins96().computeHash(upperName);
					const auto rootEntries = mRootData.equal_range(hash);
                    if(std::distance(rootEntries.first, rootEntries.second) <= 0) {
						throw std::exception("File not found");
                    }

					for (auto rootItr = rootEntries.first; rootItr != rootEntries.second; ++rootItr) {
						const auto encodingItr = mEncodingEntries.find(rootItr->second);
						if (encodingItr == mEncodingEntries.end()) {
							continue;
						}

						IndexEntry indexEntry{};
                        if(!tryGetIndexEntry(encodingItr->second.key, indexEntry)) {
							continue;
                        }

						auto blteReader{ openFile(indexEntry) };
						return BinaryReader(BlteHandler(blteReader).getDecompressedData());
					}

					throw std::exception("File not found");
                }

                BinaryReader LocalCascHandler::openFileByFileId(const uint32_t& fileId) {
					const auto rootEntries = mFileDataMap.equal_range(fileId);
					if (std::distance(rootEntries.first, rootEntries.second) <= 0) {
						throw std::exception("File not found");
					}

					for (auto rootItr = rootEntries.first; rootItr != rootEntries.second; ++rootItr) {
						const auto encodingItr = mEncodingEntries.find(rootItr->second);
						if (encodingItr == mEncodingEntries.end()) {
							continue;
						}

						IndexEntry indexEntry{};
						if (!tryGetIndexEntry(encodingItr->second.key, indexEntry)) {
							continue;
						}

						auto blteReader{ openFile(indexEntry) };
						return BinaryReader(BlteHandler(blteReader).getDecompressedData());
					}

					throw std::exception("File not found");
                }

                bool LocalCascHandler::exists(const std::string& name) {
					auto upperName = utils::String::toUpper(name);
					upperName = utils::String::replaceAll(upperName, "/", "\\");
					const auto hash = Jenkins96().computeHash(upperName);
					const auto rootEntries = mRootData.equal_range(hash);
					if (std::distance(rootEntries.first, rootEntries.second) <= 0) {
						return false;
					}

					for (auto rootItr = rootEntries.first; rootItr != rootEntries.second; ++rootItr) {
						const auto encodingItr = mEncodingEntries.find(rootItr->second);
						if (encodingItr == mEncodingEntries.end()) {
							continue;
						}

						IndexEntry indexEntry{};
						if (!tryGetIndexEntry(encodingItr->second.key, indexEntry)) {
							continue;
						}

						return true;
					}

					return false;
                }

                void LocalCascHandler::postConstruct(const cdi::ApplicationContextPtr& context) {
                    context->registerEvent<OpenLocalFolderEvent>(
                        std::bind(&LocalCascHandler::onOpenLocalFolderEvent, std::placeholders::_1));
                    context->registerEvent<GetLocalBuildInfoEvent>(
                        std::bind(&LocalCascHandler::onGetLocalBuildInfo, this, std::placeholders::_1));
                    context->registerEvent<OpenLocalBuildEvent>(
                        std::bind(&LocalCascHandler::onOpenLocalBuildEvent, this, std::placeholders::_1));
                }

                void LocalCascHandler::preDestroy() {
                }

                void LocalCascHandler::attachUiEvents(const cdi::ApplicationContextPtr& context) {
                    const auto eventManager = context->produce<ui::events::EventManager>();
                    eventManager->registerEvent<OpenLocalFolderEvent>();
                    eventManager->registerEvent<OpenLocalFolderResultEvent>();
                    eventManager->registerEvent<GetLocalBuildInfoEvent>();
                    eventManager->registerEvent<OpenLocalBuildEvent>();

                    mEventManager = context->produce<ui::events::EventManager>();
                }
            }
        }
    }
}
