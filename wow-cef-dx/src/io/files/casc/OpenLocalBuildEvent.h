#pragma once
#include <document.h>

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                class OpenLocalBuildEvent {
					std::string mBuildKey;
					std::string mPath;
					uint32_t mBuildId;

                public:
					explicit OpenLocalBuildEvent(std::string buildKey, std::string path, uint32_t buildId);

					std::string getBuildKey() const { return mBuildKey; }
					std::string getPath() const { return mPath; }
					uint32_t getBuildId() const { return mBuildId; }

					void toJson(rapidjson::Document& document) const {}
					rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
					static OpenLocalBuildEvent fromJson(const rapidjson::Document& document);
                };
            }
        }
    }
}
