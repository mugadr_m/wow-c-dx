#include "stdafx.h"
#include "OpenLocalBuildEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {

                OpenLocalBuildEvent::OpenLocalBuildEvent(std::string buildKey, std::string path, uint32_t buildId)
                    : mBuildKey(std::move(buildKey)),
                      mPath(std::move(path)),
                      mBuildId(buildId) {
                }

                OpenLocalBuildEvent OpenLocalBuildEvent::fromJson(const rapidjson::Document& document) {
                    const auto buildKeyMember = document.FindMember("buildKey");
                    const auto pathMember = document.FindMember("path");
					const auto buildIdMember = document.FindMember("buildId");
                    if (buildKeyMember == document.MemberEnd() || 
						pathMember == document.MemberEnd() ||
						buildIdMember == document.MemberEnd()) {
                        throw std::exception("No buildKey, path or buildId member found in OpenLocalBuildEvent");
                    }

                    const auto ptr = buildKeyMember->value.GetString();
                    const auto pathPtr = pathMember->value.GetString();
                    return OpenLocalBuildEvent{
                        std::string{ptr, ptr + buildKeyMember->value.GetStringLength()},
                        std::string{pathPtr, pathMember->value.GetStringLength()},
                        static_cast<uint32_t>(buildIdMember->value.GetInt())
                    };
                }
            }
        }
    }
}
