#pragma once
#include "OpenLocalFolderEvent.h"
#include "LocalBuildInfo.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                class GetLocalBuildInfoEvent {
					std::string mPath;
					std::vector<LocalBuildInfo> mBuildInfo;

                public:
					explicit GetLocalBuildInfoEvent(std::string path);

                    const std::string& getPath() const {
                        return mPath;
                    }

                    void addBuildInfo(const LocalBuildInfo& buildInfo) {
						mBuildInfo.emplace_back(buildInfo);
                    }

					void toJson(rapidjson::Document& document) const {}
					rapidjson::Document getResponseJson() const;
					static GetLocalBuildInfoEvent fromJson(const rapidjson::Document& document);
                };
            }
        }
    }
}
