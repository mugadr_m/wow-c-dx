#pragma once

#include "io/BinaryReader.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class TokenConfig {
				public:
					typedef std::vector<std::map<std::string, std::string>> TokenMapList;
				private:

					TokenMapList mValueMap;

				public:
					void parse(std::istream& inputStream);
					void parse(const BinaryReader& reader);

					TokenMapList::const_iterator cbegin() const { return mValueMap.cbegin(); }
					TokenMapList::const_iterator cend() const { return mValueMap.cend(); }

					TokenMapList::iterator begin() { return mValueMap.begin(); }
					TokenMapList::iterator end() { return mValueMap.end(); }

					const TokenMapList::value_type& operator [] (const std::size_t& index) const {
						return mValueMap.at(index);
					}
				};
			}
		}
	}
}
