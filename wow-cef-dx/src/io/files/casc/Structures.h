#pragma once

#include "CascKeys.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				struct IndexEntry {
					uint32_t index;
					uint32_t offset;
					uint32_t size;

					IndexEntry() = default;
					IndexEntry(const uint32_t index, const uint32_t offset, const uint32_t size) : index(index), offset(offset), size(size) {
						
					}
				};

				struct EncodingEntry {
					uint32_t size;
					FileKeyMd5 key;

					EncodingEntry() = default;
					EncodingEntry(uint32_t size, FileKeyMd5 key) : size(size), key(std::move(key)) {}
				};

				enum class LocaleFlags : uint32_t {
					ALL = 0xFFFFFFFF,
					NONE = 0,

					EN_US = 0x00000002,
					KO_KR = 0x00000004,
					FR_FR = 0x00000010,
					DE_DE = 0x00000020,
					ZH_CN = 0x00000040,
					ES_ES = 0x00000080,
					ZH_TW = 0x00000100,
					EN_GB = 0x00000200,
					EN_CN = 0x00000400,
					EN_TW = 0x00000800,
					ES_MX = 0x00001000,
					RU_RU = 0x00002000,
					PT_BR = 0x00004000,
					IT_IT = 0x00008000,
					PT_PT = 0x00010000,
					EN_SG = 0x20000000,
					PL_PL = 0x40000000
				};

				struct RootEntry {
					FileKeyMd5 key;
					uint32_t contentFlags;
					LocaleFlags localeFlags;
				};
			}
		}
	}
}