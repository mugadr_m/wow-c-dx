#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                struct LocalBuildInfo {
                    std::string version;
                    bool isActive;
                    std::string tags;
                    int32_t buildId;
                    std::string lastActivated;
                    std::string branch;
                    std::string buildKey;

                    LocalBuildInfo(std::string version, bool isActive, std::string tags, int32_t buildId,
                                   std::string lastActivated, std::string branch, std::string buildKey)
                        : version(std::move(version)),
                          isActive(isActive),
                          tags(std::move(tags)),
                          buildId(buildId),
                          lastActivated(std::move(lastActivated)),
                          branch(std::move(branch)),
                          buildKey(std::move(buildKey)) {
                    }
                };
            }
        }
    }
}
