#pragma once

#include "io/BinaryReader.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class BlteHandler {
					static const unsigned int MAGIC = 0x45544C42;

					std::vector<uint8_t> mDecompressedData;

					void handleCompressedBlock(uint32_t frameHeaderSize, uint64_t decompressedSize, std::vector<uint8_t>& compressedData);
					void handleBlock(uint32_t frameHeaderSize, uint32_t decompressedSize, uint32_t compressedSize, BinaryReader& reader);
					void parse(BinaryReader& reader);
				public:
					explicit BlteHandler(BinaryReader& reader);

					const std::vector<uint8_t>& getDecompressedData() const { return mDecompressedData; }
				};
			}
		}
	}
}
