#include "stdafx.h"
#include "GetLocalBuildInfoEvent.h"
#include <utility>

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                GetLocalBuildInfoEvent::GetLocalBuildInfoEvent(std::string path) : mPath(std::move(path)) {
                }

                rapidjson::Document GetLocalBuildInfoEvent::getResponseJson() const {
					rapidjson::Document response;
					auto& allocator = response.GetAllocator();
					auto& obj = response.SetObject();

					rapidjson::Value arr(rapidjson::kArrayType);

                    for(const auto& buildInfo : mBuildInfo) {
						rapidjson::Value buildObject(rapidjson::kObjectType);
						buildObject.AddMember("version", rapidjson::Value(buildInfo.version.c_str(), allocator), allocator);
						buildObject.AddMember("active", rapidjson::Value(buildInfo.isActive), allocator);
						buildObject.AddMember("tags", rapidjson::Value(buildInfo.tags.c_str(), allocator), allocator);
						buildObject.AddMember("buildId", rapidjson::Value(buildInfo.buildId), allocator);
						buildObject.AddMember("lastUsed", rapidjson::Value(buildInfo.lastActivated.c_str(), allocator), allocator);
						buildObject.AddMember("branch", rapidjson::Value(buildInfo.branch.c_str(), allocator), allocator);
						buildObject.AddMember("buildKey", rapidjson::Value(buildInfo.buildKey.c_str(), allocator), allocator);
						arr.PushBack(buildObject, allocator);
                    }

					obj.AddMember("builds", arr, allocator);
					obj.AddMember("path", rapidjson::Value(mPath.c_str(), allocator), allocator);
					return response;
                }

                GetLocalBuildInfoEvent GetLocalBuildInfoEvent::fromJson(const rapidjson::Document& document) {
					const auto pathItr = document.FindMember("path");
                    if(pathItr == document.MemberEnd()) {
						throw std::exception("Path is required for GetLocalBuildInfoEvent");
                    }

					const auto ptr = pathItr->value.GetString();
					return GetLocalBuildInfoEvent{ { ptr, ptr + pathItr->value.GetStringLength() } };
                }
            }
        }
    }
}
