#pragma once

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class Jenkins96 {
					// ReSharper disable CppInconsistentNaming
					uint32_t a = 0, b = 0, c = 0;
					// ReSharper restore CppInconsistentNaming
					uint64_t mHash = 0;

					static uint32_t rot(uint32_t x, uint32_t k) {
						return (x << k) | (x >> (32 - k));
					}

					void mix();
					void final();
					void calcHash(const char* data, std::size_t size);
				public:
					uint64_t computeHash(const std::string& str);
				};
			}
		}
	}
}