#pragma once
#include "utils/String.h"
#include "io/BinaryReader.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class KeyValueConfig {
				public:
					typedef std::map<std::string, std::vector<std::string>> KeyValueMap;

				private:
					KeyValueMap mValues;

				public:
					void parse(std::istream& stream) {
						std::string curLine;
						while(std::getline(stream, curLine)) {
							curLine = utils::String::trim(curLine);
							if(curLine.empty() || curLine[0] == '#') {
								continue;
							}

							const auto tokens = utils::String::split(curLine, '=');
							if(tokens.size() != std::size_t(2)) {
								throw std::exception("Invalid token amount, expected 2, got ", static_cast<uint32_t>(tokens.size()));
							}

							mValues[utils::String::trim(tokens[0])] = utils::String::split(utils::String::trim(tokens[1]), ' ');
						}
					}

                    void parse(const BinaryReader& reader) {
						std::stringstream strm;
						strm.write(reinterpret_cast<const char*>(reader.getData().data()), reader.getSize());
						parse(strm);
					}

					std::vector<std::string> operator [] (const std::string& key) const {
						return mValues.at(key);
					}

					KeyValueMap::iterator begin() { return mValues.begin(); }
					KeyValueMap::iterator end() { return mValues.end(); }
					KeyValueMap::const_iterator cbegin() const { return mValues.cbegin(); }
					KeyValueMap::const_iterator cend() const { return mValues.cend(); }
				};
			}
		}
	}
}
