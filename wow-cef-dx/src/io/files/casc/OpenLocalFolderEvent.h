#pragma once
#include <document.h>

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                class OpenLocalFolderEvent {
                public:
					void toJson(rapidjson::Document& document) const {}
					rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
					static OpenLocalFolderEvent fromJson(const rapidjson::Document& document) { return OpenLocalFolderEvent(); }
                };
            }
        }
    }
}
