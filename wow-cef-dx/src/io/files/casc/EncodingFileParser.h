#pragma once

#include "Structures.h"
#include "io/BinaryReader.h"
#include <unordered_map>

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class EncodingFileParser {
					std::unordered_map<casc::FileKeyMd5, casc::EncodingEntry, casc::FileKeyMd5::Hash>& mEncodingMap;

				public:
					explicit EncodingFileParser(std::unordered_map<casc::FileKeyMd5, casc::EncodingEntry, casc::FileKeyMd5::Hash>& encodingMap) : mEncodingMap(encodingMap) {
						
					}

					void parse(BinaryReader& reader);
				};
			}
		}
	}
}
