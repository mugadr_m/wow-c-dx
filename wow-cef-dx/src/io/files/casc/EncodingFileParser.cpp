#include "stdafx.h"
#include "EncodingFileParser.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {

				void EncodingFileParser::parse(BinaryReader& reader) {
					reader.seek(9);
					const auto numEntriesA = reader.readIntBE();
					reader.seekMod(5);
					const auto stringBlock = reader.readIntBE();
					reader.seekMod(stringBlock + numEntriesA * 32);
					auto curPos = reader.tell();

					for(auto i = 0u; i < numEntriesA; ++i) {
						auto keyCount = reader.read<uint16_t>();
						while(keyCount != 0) {
							const auto fileSize = reader.readIntBE();
							const auto md5 = reader.read<FileKeyMd5>();

							const auto key = reader.read<FileKeyMd5>();
							mEncodingMap.emplace(md5, EncodingEntry(fileSize, key));
							if(keyCount > 1) {
								reader.seekMod((keyCount - 1) * 16);
							}

							keyCount = reader.read<uint16_t>();
						}

						curPos += 0x1000;
						reader.seek(curPos);
					}
				}
			}
		}
	}
}
