#include "stdafx.h"
#include "OpenLocalFolderResultEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
			    OpenLocalFolderResultEvent::OpenLocalFolderResultEvent(std::string folderPath) : mFolderPath(std::move(folderPath)) {
			    }

			    void OpenLocalFolderResultEvent::toJson(rapidjson::Document& document) const {
					document.SetObject();
					document.AddMember("path", rapidjson::Value(mFolderPath.c_str(), document.GetAllocator()), document.GetAllocator());
			    }

			    rapidjson::Document OpenLocalFolderResultEvent::getResponseJson() const {
					return rapidjson::Document();
			    }
			}
		}
	}
}