#pragma once
#include "io/files/IFileProvider.h"
#include "cdi/ApplicationContext.h"
#include "OpenLocalFolderEvent.h"
#include "GetLocalBuildInfoEvent.h"
#include "ui/events/EventManager.h"
#include "KeyValueConfig.h"
#include "CascKeys.h"
#include <unordered_map>
#include "Structures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace casc {
                class OpenLocalBuildEvent;

                class DataStream {
					static utils::Logger<DataStream> mLogger;

					HANDLE mFileHandle = nullptr;
					HANDLE mFileMapping = nullptr;

                public:
					explicit DataStream(const std::string& path, uint32_t index);
					~DataStream();

					BinaryReader read(uint32_t offset, uint32_t size) const;

                };

				SHARED_PTR(DataStream);

                class LocalCascHandler : public IFileProvider, public std::enable_shared_from_this<LocalCascHandler> {
					static utils::Logger<LocalCascHandler> mLogger;

					ui::events::EventManagerPtr mEventManager;
					std::vector<std::string> mIndexFiles;
					std::unordered_map<FileKeyMd5, IndexEntry, FileKeyMd5::Hash> mIndexEntries;
					std::unordered_map<FileKeyMd5, EncodingEntry, FileKeyMd5::Hash> mEncodingEntries;
					std::unordered_multimap<uint64_t, FileKeyMd5> mRootData;
					std::unordered_multimap<uint32_t, FileKeyMd5> mFileDataMap;

					std::map<uint32_t, DataStreamPtr> mDataStreams;

					KeyValueConfig mBuildConfig;

					uint32_t mBuildId = 0;

                    static void onOpenLocalFolderEvent(OpenLocalFolderEvent& event);
					void onGetLocalBuildInfo(GetLocalBuildInfoEvent& event);

					void onOpenLocalBuildEvent(OpenLocalBuildEvent& event);

					BinaryReader tryFindBuildInfo(const std::string& path);

                    static BinaryReader openFileInMemory(const std::string& path);

					void publishProgress(const std::string& message) const;

					void loadBuildConfig(const std::string& basePath, const std::string& buildKey);
					void loadIndexFiles(const std::string& basePath);
					void parseIndexFiles(const std::string& basePath);
					void parseEncodingFile();
					void parseRootFile();

					BinaryReader openFile(const IndexEntry& indexEntry);

					IndexEntry getIndexEntry(const FileKeyMd5& key);
					bool tryGetIndexEntry(const FileKeyMd5& key, IndexEntry& indexEntry);

                public:
                    ~LocalCascHandler() override = default;
                    BinaryReader openFile(const std::string& name) override;
                    BinaryReader openFileByFileId(const uint32_t& fileId) override;
					bool exists(const std::string& name) override;

					void postConstruct(const cdi::ApplicationContextPtr& context);
					void preDestroy();

                    void attachUiEvents(const cdi::ApplicationContextPtr& context);
                };
            }
        }
    }
}
