#include "stdafx.h"
#include "RootParser.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				void RootParser::parse(BinaryReader& reader, const LocaleFlags& localeFlags) const {
					struct RootElement {
						FileKeyMd5 key{};
						uint64_t hash = 0;
					};

					while(reader.tell() < reader.getSize()) {
						const auto count = reader.read<uint32_t>();
						reader.seekMod(4); // content flags
						const auto locales = reader.read<uint32_t>();
						if((locales & static_cast<uint32_t>(localeFlags)) == 0) {
							reader.seekMod(count * 28);
							continue;
						}

						std::vector<uint32_t> fileDataIdValues(count);
						for(auto i = 0u; i < count; ++i) {
							const auto value = reader.read<uint32_t>();
							if(i == 0) {
								fileDataIdValues[i] = value;
							} else {
								fileDataIdValues[i] = fileDataIdValues[i - 1] + 1 + value;
							}
						}

						std::vector<RootElement> rootElements(count);
						reader.read(rootElements);
						for(auto i = 0u; i < count; ++i) {
							const auto& elem = rootElements[i];
							mRootDataMap.insert(std::make_pair(elem.hash, elem.key));
							mFileDataIds.insert(std::make_pair(fileDataIdValues[i], elem.key));
						}
					}
				}
			}
		}
	}
}
