#pragma once

namespace wowc {
	namespace io {
		namespace files {
			namespace casc {
				class FileKeyMd5 {
					friend class Hash;
				public:
					struct Hash {
						std::size_t operator ()(const FileKeyMd5& value) const {
							const auto vals = reinterpret_cast<const std::size_t*>(value.mKey);
							auto curHash = std::size_t(0);
							for (auto i = 0u; i < sizeof(FileKeyMd5) / sizeof(std::size_t); ++i) {
								curHash = curHash * 31 + vals[i];
							}

							return curHash;
						}
					};

				private:

					static const FileKeyMd5 ZERO_KEY;

					uint8_t mKey[16]{};

				public:
					FileKeyMd5() = default;
					explicit FileKeyMd5(const uint8_t* data) { memcpy(mKey, data, sizeof mKey); }

					bool operator <(const FileKeyMd5& other) const {
						return memcmp(mKey, other.mKey, sizeof mKey) < 0;
					}

					bool operator ==(const FileKeyMd5& other) const {
						return memcmp(mKey, other.mKey, sizeof mKey) == 0;
					}

					bool isNotZero() const {
						return memcmp(mKey, ZERO_KEY.mKey, sizeof mKey) != 0;
					}

					uint8_t* begin() { return mKey; }
					uint8_t* end() { return mKey + sizeof mKey; }

					const uint8_t* cbegin() const { return mKey; }
					const uint8_t* cend() const { return mKey + sizeof mKey; }
				};
			}
		}
	}
}
