#pragma once
#include <document.h>

namespace wowc {
    namespace io {
        namespace files {
            class JsOpenFileEvent {
				bool mIsFileDataId = false;
				uint32_t mFileDataId = 0;
				std::string mFilePath{};

				std::vector<uint8_t> mResponseData;

            public:
                explicit JsOpenFileEvent(const std::string& filePath = "", uint32_t fileDataId = 0);

				void toJson(rapidjson::Document& document) const;
				rapidjson::Document getResponseJson() const;
				static JsOpenFileEvent fromJson(const rapidjson::Document& document);
            };
        }
    }
}
