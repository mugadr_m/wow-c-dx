#pragma once

#include <cef_scheme.h>
#include "io/cef/LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class LoadingScreenHandlerFactory : public CefSchemeHandlerFactory {
			public:
				static const std::string SCHEME_NAME;

			private:

				IMPLEMENT_REFCOUNTING(LoadingScreenHandlerFactory);

				LoadingScreenThreadPoolPtr mThreadPool;
			public:

				CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
					const CefString& scheme_name, CefRefPtr<CefRequest> request) override;

				void postConstruct(const cdi::ApplicationContextPtr& ctx);
			};
		}
	}
}
