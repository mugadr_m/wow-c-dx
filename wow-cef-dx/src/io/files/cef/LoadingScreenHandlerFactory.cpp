#include "stdafx.h"
#include "LoadingScreenHandlerFactory.h"
#include "LoadingScreenResourceHandler.h"

namespace wowc {
	namespace io {
		namespace cef {
			const std::string LoadingScreenHandlerFactory::SCHEME_NAME = "loading-screen";

			CefRefPtr<CefResourceHandler> LoadingScreenHandlerFactory::Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
				const CefString& scheme_name, CefRefPtr<CefRequest> request) {
				return new LoadingScreenResourceHandler(mThreadPool);
			}

			void LoadingScreenHandlerFactory::postConstruct(const cdi::ApplicationContextPtr& ctx) {
				mThreadPool = ctx->produce<LoadingScreenThreadPool>();
			}
		}
	}
}