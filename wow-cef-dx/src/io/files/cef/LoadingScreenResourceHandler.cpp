#include "stdafx.h"
#include "LoadingScreenResourceHandler.h"
#include <winhttp.h>
#include "utils/String.h"
#include "cdi/ApplicationContext.h"
#include "io/files/storage/MapStorageDao.h"
#include "io/files/storage/StorageManager.h"
#include "third_party/pngpp/rgba_pixel.hpp"
#include "third_party/pngpp/image.hpp"
#include "io/image/BlpImageLoader.h"

namespace wowc {
	namespace io {
		namespace cef {
			utils::Logger<LoadingScreenResourceHandler> LoadingScreenResourceHandler::mLogger;

			bool LoadingScreenResourceHandler::getResolution(const std::string& extraInfo, uint32_t& width, uint32_t& height) {
				if (extraInfo.empty()) {
					return false;
				}

				if (extraInfo[0] != '?') {
					return false;
				}

				const auto endIndex = extraInfo.find('#');
				const auto queryPart = extraInfo.substr(1, endIndex == std::string::npos ? std::string::npos : endIndex - 1);

				const auto keyValuePairs = utils::String::split(queryPart, '&');

				std::map<std::string, std::string> keyValueMap;
				std::for_each(keyValuePairs.begin(), keyValuePairs.end(), [&keyValueMap](const auto& pair) {
					const auto subParts = utils::String::split(pair, '=');
					if(subParts.size() >= 2) {
						keyValueMap.emplace(utils::String::toLower(subParts[0]), subParts[1]);
					} else if(subParts.size() == 1) {
						const auto value = subParts[0];
						const auto endsInEquals = value[value.size() - 1] == '=';
						keyValueMap.emplace(utils::String::toLower(endsInEquals ? value.substr(0, value.length() - 1) : value), "");
					}
				});

				const auto widthItr = keyValueMap.find("width");
				const auto heightItr = keyValueMap.find("height");

				if(widthItr == keyValueMap.end() || heightItr == keyValueMap.end()) {
					return false;
				}

				width = std::stoi(widthItr->second);
				height = std::stoi(heightItr->second);
				return true;
			}

			bool LoadingScreenResourceHandler::isImmediateMode(const std::string& path) {
				if(path.empty()) {
					mLogger.error("Empty path is not allowed");
					throw std::exception("Empty path is not allowed");
				}

				auto actualPath = path;
				if(path[0] == '/') {
					actualPath = path.substr(1);
				}

				const auto pathEnd = actualPath.find('/');
				if(pathEnd == std::string::npos) {
					mLogger.error("URL path is too short: ", path, ". Was expecting /<mode>/<data>");
					throw std::exception("Path is too short");
				}

				const auto modeType = actualPath.substr(0, pathEnd);
				if(modeType == "immediate") {
					return true;
				}

				if(modeType == "fileDataId") {
					return false;
				}

				mLogger.error("Received unexpected loading screen mode: ", modeType, ". Expected one of immedate or fileDataId");
				throw std::exception("Invalid mode for loading screen");
			}

			std::string LoadingScreenResourceHandler::getLoadingScreenData(const std::string& path) {
				if (path.empty()) {
					mLogger.error("Empty path is not allowed");
					throw std::exception("Empty path is not allowed");
				}

				auto actualPath = path;
				if (path[0] == '/') {
					actualPath = path.substr(1);
				}

				const auto pathEnd = actualPath.find('/');
				if (pathEnd == std::string::npos) {
					mLogger.error("URL path is too short: ", path, ". Was expecting /<mode>/<data>");
					throw std::exception("Path is too short");
				}

				const auto pathName = actualPath.substr(pathEnd + 1);
				if(pathName.empty()) {
					mLogger.error("URL path does not contain anything after the mode descriptor: ", path);
					throw std::exception("Path is too short");
				}

				return pathName;
			}

			bool LoadingScreenResourceHandler::ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback) {
				std::string url = request->GetURL();
				const auto schemeEnd = url.find(':');
				url = std::string("http") + url.substr(schemeEnd);
				const auto wideUrl = utils::String::toUnicode(url);
				URL_COMPONENTS components{};
				components.dwHostNameLength = -1;
				components.dwExtraInfoLength = -1;
				components.dwPasswordLength = -1;
				components.dwSchemeLength = -1;
				components.dwStructSize = sizeof(URL_COMPONENTS);
				components.dwUrlPathLength = -1;
				components.dwUserNameLength = -1;

				const auto result = WinHttpCrackUrl(wideUrl.c_str(), 0, 0, &components);
				if (!result) {
					mLogger.error("Error decoding URL: ", url);
					return false;
				}

				const std::wstring path(components.lpszUrlPath, components.lpszUrlPath + components.dwUrlPathLength);
				const std::wstring query(components.lpszExtraInfo, components.lpszExtraInfo + components.dwExtraInfoLength);

				uint32_t w = 0, h = 0;
				if (!getResolution(utils::String::toMultibyte(query), w, h)) {
					w = static_cast<uint32_t>(-1);
					h = static_cast<uint32_t>(-1);
				}

				bool immediateMode;
				std::string loadScreenData;
				try {
					const auto multiPath = utils::String::toMultibyte(path);
					immediateMode = isImmediateMode(multiPath);
					loadScreenData = getLoadingScreenData(multiPath);
				} catch (std::exception& ) {
					return false;
				}

				const CefRefPtr<CefResourceHandler> self = this;
				mThreadPool->pushWork([this, self, immediateMode, loadScreenData, callback, w, h]() {
					const auto mapStorageDao = cdi::ApplicationContext::instance()->produce<files::storage::StorageManager>()->getMapStroageDao();
					auto image = mapStorageDao->openLoadingScreen(immediateMode, loadScreenData);
					png::image<png::rgba_pixel> outImage;
					try {
						image::BlpImageLoader::convertToPng(outImage, image, w, h);
						std::stringstream strm;
						outImage.write_stream(strm);
						const auto size = strm.tellp();
						strm.seekg(0, std::ios::beg);
						std::vector<uint8_t> data(static_cast<std::size_t>(size));
						strm.read(reinterpret_cast<char*>(data.data()), size);
						mDataReader = BinaryReader(data);
					} catch(std::exception&) {
						mHasFailed = true;
					}

					callback->Continue();
				});

				return true;
			}

			void LoadingScreenResourceHandler::GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length,
				CefString& redirectUrl) {
				if(mHasFailed) {
					response_length = 0;
					response->SetStatus(500);
					response->SetMimeType("text/html");
					return;
				}

				redirectUrl = "";
				response_length = mDataReader.getSize();
				response->SetStatus(200);
				response->SetMimeType("image/png");
			}

			bool LoadingScreenResourceHandler::ReadResponse(void* data_out, int bytes_to_read, int& bytes_read,
				CefRefPtr<CefCallback> callback) {
				const auto numAvailable = mDataReader.available();
				if(numAvailable <= 0) {
					return false;
				}

				const auto toRead = std::min<std::size_t>(bytes_to_read, numAvailable);
				bytes_read = static_cast<int>(toRead);

				mDataReader.read(data_out, toRead);
				return true;
			}

			bool LoadingScreenResourceHandler::CanGetCookie(const CefCookie& cookie) {
				return false;
			}

			bool LoadingScreenResourceHandler::CanSetCookie(const CefCookie& cookie) {
				return false;
			}

			void LoadingScreenResourceHandler::Cancel() {
				// nothing to do
			}
		}
	}
}
