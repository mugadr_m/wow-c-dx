#pragma once
#include <cef_resource_handler.h>
#include <utility>
#include "utils/Logger.h"
#include "io/BinaryReader.h"
#include "io/cef/LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class LoadingScreenResourceHandler : public CefResourceHandler {
				static utils::Logger<LoadingScreenResourceHandler> mLogger;

			IMPLEMENT_REFCOUNTING(LoadingScreenResourceHandler);

				BinaryReader mDataReader;
				LoadingScreenThreadPoolPtr mThreadPool;
				bool mHasFailed = false;

				static bool getResolution(const std::string& extraInfo, uint32_t& width, uint32_t& height);
				static bool isImmediateMode(const std::string& path);
				static std::string getLoadingScreenData(const std::string& path);
			public:
				explicit LoadingScreenResourceHandler(LoadingScreenThreadPoolPtr threadPool) : mDataReader({}),
				                                                                      mThreadPool(std::move(threadPool)) {
				}

				~LoadingScreenResourceHandler() override = default;

				bool ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback) override;
				void GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length, CefString& redirectUrl) override;
				bool ReadResponse(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefCallback> callback) override;
				bool CanGetCookie(const CefCookie& cookie) override;
				bool CanSetCookie(const CefCookie& cookie) override;
				void Cancel() override;
			};
		}
	}
}
