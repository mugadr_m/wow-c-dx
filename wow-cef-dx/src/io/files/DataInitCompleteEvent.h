#pragma once
#include <document.h>

namespace wowc {
	namespace io {
		namespace files {
			class DataInitCompleteEvent {
			public:
				void toJson(rapidjson::Document& document) const { }
				rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
				static DataInitCompleteEvent fromJson(const rapidjson::Document& document) { return {}; }
			};
		}
	}
}
