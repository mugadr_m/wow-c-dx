#include "stdafx.h"
#include "DataLoadProgressEvent.h"

namespace wowc {
	namespace io {
		namespace files {
			void DataLoadProgressEvent::toJson(rapidjson::Document& document) const {
				document.SetObject();
				rapidjson::Value val(rapidjson::kStringType);
				val.SetString(mMessage.c_str(), document.GetAllocator());
				document.AddMember("message", val, document.GetAllocator());
			}

			DataLoadInitEvent DataLoadProgressEvent::fromJson(const rapidjson::Document& document) {
				throw std::exception("Cannot convert DataLoadProgressEvent from JSON");
			}
		}
	}
}
