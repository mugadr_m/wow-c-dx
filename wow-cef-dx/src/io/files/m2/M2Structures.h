#pragma once

#include "io/BinaryReader.h"
#include "math/Quaternion.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
#pragma pack(push, 1)

                struct C3Vector {
					float x, y, z;
                };

                struct C2Vector {
					float x, y;
                };

                struct M2Bounds {
					C3Vector min;
					C3Vector max;
					float radius;
                };

                struct M2Range {
					uint32_t minimum;
					uint32_t maximum;
                };

                struct M2Sequence {
					uint16_t id;
					uint16_t variationIndex;
					uint32_t duration;
					float movespeed;
					uint32_t flags;
					int16_t frequency;
					uint16_t padding;
					M2Range replay;
					uint16_t blendTimeIn;
					uint16_t blendTimeOut;
					M2Bounds bounds;
					int16_t variationNext;
					uint16_t aliasNext;
				};

                template<typename T = uint8_t>
                struct M2Array {
					uint32_t size;
					uint32_t offset;

                    std::vector<T> get(BinaryReader& reader) const {
						reader.seek(offset);
						const auto numElements = size;
                        if(numElements == 0) {
							return {};
                        }

                        std::vector<T> ret(numElements);
                        reader.read(ret);
                        return ret;
                    }
                };

                template<typename T>
                struct M2Track {
					uint16_t interpolationType;
					int16_t globalSequence;
					M2Array<M2Array<uint32_t>> timestamps;
					M2Array<M2Array<T>> values;
                };

				struct M2CompressedQuaternion {
					int16_t x, y, z, w;

					math::Quaternion toQuaternion() const;
				};

				struct M2Bone {
					int32_t keyBoneId;
					uint32_t flags;
					int16_t parentBone;
					uint16_t subMeshId;
					uint32_t debugData;
					M2Track<C3Vector> translation;
					M2Track<M2CompressedQuaternion> rotation;
					M2Track<C3Vector> scale;
					C3Vector pivotPoint;
				};

                struct M2Vertex {
					C3Vector position;
					uint8_t boneWeights[4];
					uint8_t boneIndices[4];
					C3Vector normal;
					C2Vector textureCoordinates[2];
                };

                struct M2Color {
					M2Track<C3Vector> color;
					M2Track<uint16_t> alpha;
                };

                struct M2TextureAlpha {
					M2Track<uint16_t> alpha;
                };

                struct M2Texture {
					uint32_t type;
					uint32_t flags;
					M2Array<char> fileName;
                };

                struct M2Material {
					uint16_t flags;
					uint32_t blendingMode;
                };

                struct M2Header {
					uint32_t magic;
					uint32_t version;
					M2Array<char> name;
					uint32_t globalFlags;
					M2Array<uint32_t> globalLoops;
					M2Array<M2Sequence> sequences;
					M2Array<int16_t> sequenceLookup;
					M2Array<M2Bone> bones;
					M2Array<uint16_t> keyBoneLookup;
					M2Array<M2Vertex> vertices;
					uint32_t numSkins;
					M2Array<M2Color> colors;
					M2Array<M2Texture> textures;
					M2Array<M2TextureAlpha> textureAlpha;
					M2Array<char> textureTransforms; // TODO: M2TextureTransform
					M2Array<uint16_t> replaceableTextureLookup;
					M2Array<M2Material> materials;
					M2Array<uint16_t> boneLookup;
					M2Array<uint16_t> textureLookup;
					M2Array<uint16_t> textureUnitLookup;
					M2Array<uint16_t> transparencyLookup;
					M2Array<uint16_t> textureTransformLookup;
					C3Vector boundingBoxMin;
					C3Vector boundingBoxMax;
					float boundingSphereRadius;
					C3Vector collisionBoxMin;
					C3Vector collisionBoxMax;
					float collisionSphereRadius;
					M2Array<uint16_t> collisionTriangles;
					M2Array<C3Vector> collisionVertices;
					M2Array<C3Vector> collisionNormals;
					M2Array<char> attachments; // TODO: M2Attachment
					M2Array<uint16_t> attachmentLookup;
					M2Array<char> events; // TODO: M2Event
					M2Array<char> lights; // TODO: M2Light
					M2Array<char> cameras; // TODO: M2Camera
					M2Array<uint16_t> cameraLookup;
					M2Array<char> ribbons; // TODO: M2Ribbon
					M2Array<char> particles; // TODO: M2Particle
                };

				struct M2SkinSection {
					uint16_t skinSectionId;
					uint16_t level;
					uint16_t vertexStart;
					uint16_t vertexCount;
					uint16_t indexStart;
					uint16_t indexCount;
					uint16_t boneCount;
					uint16_t boneComboIndex;
					uint16_t boneInfluences;
					uint16_t centerBoneIndex;
					C3Vector centerPosition;
					C3Vector boundingCenter;
					float boundingRadius;
				};

				struct M2Batch {
					uint8_t flags;
					int8_t priorityPlane;
					uint16_t shaderId;
					uint16_t subMeshId;
					uint16_t geosetIndex;
					int16_t colorIndex;
					uint16_t renderFlags;
					uint16_t texUnitNumber;
					uint16_t textureCount;
					uint16_t textureIndex;
					uint16_t textureUnitIndex;
					uint16_t textureAlphaIndex;
					uint16_t textureTransformIndex;
				};

                struct M2SkinHeader {
					uint32_t magic;
					M2Array<uint16_t> vertices;
					M2Array<uint16_t> indices;
					M2Array<uint32_t> bones;
					M2Array<M2SkinSection> subMeshes;
					M2Array<M2Batch> batches;
					uint32_t maxBouneCount;
                };

#pragma pack(pop)
            }
        }
    }
}