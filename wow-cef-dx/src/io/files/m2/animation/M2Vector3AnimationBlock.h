#pragma once
#include "M2AnimationBlock.h"
#include "math/Vector3.h"
#include "io/BinaryReader.h"
#include "M2VectorConverter.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    class M2Vector3AnimationBlock : public M2AnimationBlock<math::Vector3, math::Vector3, C3Vector, M2Vector3Converter> {
                    public:
						M2Vector3AnimationBlock(const M2Track<C3Vector>& block, BinaryReader& file, const std::vector<uint32_t>& globalSequences, const math::Vector3& defaultValue = math::Vector3{0, 0, 0});
                    };
                }
            }
        }
    }
}
