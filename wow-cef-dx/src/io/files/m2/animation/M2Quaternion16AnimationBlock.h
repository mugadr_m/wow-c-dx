#pragma once
#include "M2AnimationBlock.h"
#include "io/files/m2/M2Structures.h"
#include "math/Quaternion.h"
#include "M2CompressedQuaternionInterpolator.h"
#include "M2IdentityConverter.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    class M2Quaternion16AnimationBlock : public M2AnimationBlock<
                            M2CompressedQuaternion, math::Quaternion, M2CompressedQuaternion, M2IdentityConverter<
                                M2CompressedQuaternion>, M2CompressedQuaternionInterpolator> {
                    public:
						M2Quaternion16AnimationBlock(const M2Track<M2CompressedQuaternion>& block, BinaryReader& file, const std::vector<uint32_t>& globalSequences);

                    };
                }
            }
        }
    }
}
