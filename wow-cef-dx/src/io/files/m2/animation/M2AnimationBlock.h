#pragma once
#include "M2LerpInterpolator.h"
#include "io/files/m2/M2Structures.h"
#include "M2IdentityConverter.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    template <
                        typename TSource,
                        typename TDest = TSource,
                        typename TFile = TSource,
                        typename TConverter = M2IdentityConverter<TSource>,
                        typename TInterpolator = M2LerpInterpolator<TSource, TDest>
                    >
                    class M2AnimationBlock {
                        TInterpolator mInterpolator;
                        TConverter mConverter;

                        TDest mDefaultValue;

                        void loadValues(BinaryReader& reader, const M2Track<TFile>& track) {
                            const auto timeTracks = track.timestamps.get(reader);
                            const auto valueTracks = track.values.get(reader);

                            mTimeStamps.reserve(timeTracks.size());
                            mValues.reserve(valueTracks.size());

                            for (const auto& timeTrack : timeTracks) {
                                mTimeStamps.emplace_back(timeTrack.get(reader));
                            }

                            for (const auto& valueTrack : valueTracks) {
                                const auto values = valueTrack.get(reader);
                                std::vector<TSource> actualValues;
                                actualValues.reserve(values.size());
                                for (const auto& value : values) {
                                    actualValues.emplace_back(mConverter(value));
                                }

                                mValues.emplace_back(actualValues);
                            }
                        }

                        TDest interpolate(uint32_t time, uint32_t timeLine) {
                            const auto& times = mTimeStamps[timeLine];
                            const auto& values = mValues[timeLine];

							const auto maxIndex = static_cast<uint32_t>(std::min(times.size(), values.size()));

							auto iStart = 0u;
							auto iEnd = 0u;
							auto found = false;
                            for(; iStart < maxIndex - 1; ++iStart) {
                                if(times[iStart] <= time && times[iStart + 1] >= time) {
									iEnd = iStart + 1;
									found = true;
									break;
                                }
                            }

                            if(!found) {
                                // in theory this should not happen, edge cases are supposed to
                                // be handled by the calling function, but better safe than sorry
								return mInterpolator(values[0], values[0], 0.0f);
                            }

							auto ts = times[iStart];
							auto te = times[iEnd];

                            if(ts > te) {
								std::swap(ts, te);
                            }

							ts = std::min(ts, time);
							te = std::max(te, time);

                            if(ts == te) {
								return mInterpolator(values[iStart], values[iStart], 0.0f);
                            }

							const auto fac = (time - ts) / static_cast<float>(te - ts);
							return mInterpolator(values[iStart], values[iEnd], fac);
                        }

                    protected:
                        std::vector<std::vector<TSource>> mValues;
                        std::vector<std::vector<uint32_t>> mTimeStamps;

                        uint32_t mGlobalSequence = 0;
                        bool mIsGlobalSequence = false;

                        explicit M2AnimationBlock(BinaryReader& reader, const M2Track<TFile>& track,
                                                  const std::vector<uint32_t>& globalSequences,
                                                  TDest defaultValue = TDest()) :
                            mDefaultValue(defaultValue) {
                            static TInterpolator interpolator;
                            static TConverter converter;

                            if (track.globalSequence >= 0 && track.globalSequence < globalSequences.size()) {
                                mGlobalSequence = globalSequences[track.globalSequence];
                                mIsGlobalSequence = true;
                            }

                            mInterpolator = interpolator;
                            mConverter = converter;

                            loadValues(reader, track);
                        }

                    public:
                        TDest getValue(uint32_t timeLine, uint32_t time, uint32_t animationLength) {
                            if (timeLine >= mTimeStamps.size() || timeLine >= mValues.size()) {
                                return mDefaultValue;
                            }

                            const auto& times = mTimeStamps[timeLine];
                            const auto& values = mValues[timeLine];

                            if (times.empty() || values.empty()) {
                                return mDefaultValue;
                            }

                            if (mIsGlobalSequence && mGlobalSequence > 0) {
                                time %= mGlobalSequence;
                            } else {
                                const auto maxTime = times.back();
                                if (maxTime != 0) {
                                    time %= maxTime;
                                }
                            }

							const auto maxIndex = static_cast<uint32_t>(std::min(times.size(), values.size()));
                            if (maxIndex == 1 || time <= times.front()) {
                                return mInterpolator(values[0], values[0], 0.0f);
                            }

                            if (time >= times.back()) {
                                const auto te = times[0] + animationLength;
                                const auto ts = times.front();
                                const auto fac = (time - ts) / static_cast<float>(te - ts);
                                return mInterpolator(values.front(), values[0], fac);
                            }

                            return interpolate(time, timeLine);
                        }

                        TDest getValueDefaultLength(uint32_t timeLine, uint32_t time) {
                            if (timeLine >= mTimeStamps.size() || timeLine >= mValues.size()) {
                                return mDefaultValue;
                            }

							const auto& times = mTimeStamps[timeLine];
                            return getValue(timeLine, time, times.empty() ? 0 : times.back());
                        }
                    };
                }
            }
        }
    }
}
