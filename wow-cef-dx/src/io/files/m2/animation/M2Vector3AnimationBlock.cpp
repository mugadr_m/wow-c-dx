#include "stdafx.h"
#include "M2Vector3AnimationBlock.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {

                    M2Vector3AnimationBlock::M2Vector3AnimationBlock(
                        const M2Track<C3Vector>& block,
                        BinaryReader& file,
                        const std::vector<uint32_t>& globalSequences,
                        const math::Vector3& defaultValue) :
                        M2AnimationBlock(
                            file,
                            block,
                            globalSequences,
                            defaultValue) {
                    }
                }
            }
        }
    }
}
