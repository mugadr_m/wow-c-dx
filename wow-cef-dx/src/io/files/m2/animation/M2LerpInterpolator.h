#pragma once


namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    template<typename TSource, typename TDest = TSource>
                    class M2LerpInterpolator {
                    public:
                        TDest operator () (const TSource& s1, const TSource& s2, const float factor) {
							return static_cast<TDest>(s2 * factor + s1 * (1.0f - factor));
                        }
                    };
                }
            }
        }
    }
}