#pragma once

#include "math/Quaternion.h"
#include "io/files/m2/M2Structures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    class M2CompressedQuaternionInterpolator {
                    public:
						math::Quaternion operator () (const M2CompressedQuaternion& q1, const M2CompressedQuaternion& q2, const float factor) const;
                    };
                }
            }
        }
    }
}
