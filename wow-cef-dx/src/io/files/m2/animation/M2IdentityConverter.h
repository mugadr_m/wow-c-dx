#pragma once

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    template<typename TSource>
                    class M2IdentityConverter {
                    public:
						TSource operator () (const TSource& source) const {
							return source;
                        }
                    };
                }
            }
        }
    }
}