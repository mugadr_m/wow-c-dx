#include "stdafx.h"
#include "M2AnimationBone.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {

                    M2AnimationBone::M2AnimationBone(const M2Bone& bone, BinaryReader& file, const M2Header& header,
                                                     std::vector<uint32_t> globalSequences) :
                        mBone(bone),
                        mGlobalSequences(std::move(globalSequences)),
                        mTranslation(mBone.translation, file, globalSequences),
                        mRotation(mBone.rotation, file, globalSequences),
                        mScale(mBone.scale, file, globalSequences, math::Vector3{1, 1, 1}) {

                        mIsBillboard = (bone.flags & 0x08) != 0;
                        mIsTransformed = (bone.flags & 0x200) != 0;

                        mPivotMatrix = math::Matrix4::translate(bone.pivotPoint.x,
                                                                bone.pivotPoint.y,
                                                                bone.pivotPoint.z);

                        mInvPivotMatrix = math::Matrix4::translate(-bone.pivotPoint.x,
                                                                   -bone.pivotPoint.y,
                                                                   -bone.pivotPoint.z);
                    }

                    math::Matrix4 M2AnimationBone::getBoneTransform(uint64_t time, uint32_t animationId,
                                                                    uint32_t animationLength,
                                                                    const std::function<const math::Matrix4&(
                                                                        uint32_t, uint64_t)>& boneCallback) {
                        math::Matrix4 boneMatrix;

                        if (mIsTransformed) {
                            const auto position = mTranslation.getValue(animationId, static_cast<uint32_t>(time),
                                                                        animationLength);
                            const auto scaling = mScale.getValue(animationId, static_cast<uint32_t>(time),
                                                                 animationLength);
                            const auto rotation = mRotation.getValue(animationId, static_cast<uint32_t>(time),
                                                                     animationLength);

							boneMatrix =
								math::Matrix4::rotate(rotation) *
								math::Matrix4::scale(scaling) *
                                math::Matrix4::translate(position);
                        }

                        boneMatrix = mInvPivotMatrix * boneMatrix * mPivotMatrix;

                        if (mBone.parentBone >= 0) {
                            boneMatrix = boneMatrix * boneCallback(static_cast<uint32_t>(mBone.parentBone), time);
                        }

                        return boneMatrix;
                    }
                }
            }
        }
    }
}
