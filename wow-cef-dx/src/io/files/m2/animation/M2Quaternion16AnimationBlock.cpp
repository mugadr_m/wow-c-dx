#include "stdafx.h"
#include "M2Quaternion16AnimationBlock.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {

                    M2Quaternion16AnimationBlock::M2Quaternion16AnimationBlock(
                        const M2Track<M2CompressedQuaternion>& block, BinaryReader& file,
                        const std::vector<uint32_t>& globalSequences) : M2AnimationBlock(file, block, globalSequences) {
                    }
                }
            }
        }
    }
}
