#pragma once

#include "io/BinaryReader.h"
#include "io/files/m2/M2Structures.h"
#include "math/Matrix4.h"
#include "M2Vector3AnimationBlock.h"
#include "M2Quaternion16AnimationBlock.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    class M2AnimationBone {
						bool mIsBillboard = false;
						bool mIsTransformed = false;

						M2Bone mBone;
						std::vector<uint32_t> mGlobalSequences;

						math::Matrix4 mPivotMatrix;
						math::Matrix4 mInvPivotMatrix;

						M2Vector3AnimationBlock mTranslation;
						M2Quaternion16AnimationBlock mRotation;
						M2Vector3AnimationBlock mScale;

                    public:
						explicit M2AnimationBone(const M2Bone& bone, BinaryReader& file, const M2Header& header, std::vector<uint32_t> globalSequences);

						math::Matrix4 getBoneTransform(uint64_t time, uint32_t animationId, uint32_t animationLength,
							const std::function<const math::Matrix4&(uint32_t, uint64_t)>& boneCallback);
                    };
                }
            }
        }
    }
}
