#include "stdafx.h"
#include "M2CompressedQuaternionInterpolator.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {

                    math::Quaternion M2CompressedQuaternionInterpolator::operator()(const M2CompressedQuaternion& q1,
                        const M2CompressedQuaternion& q2, const float factor) const {
						return math::Quaternion::slerp(q1.toQuaternion(), q2.toQuaternion(), factor);
                    }
                }
            }
        }
    }
}