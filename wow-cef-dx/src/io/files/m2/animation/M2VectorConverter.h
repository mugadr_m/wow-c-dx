#pragma once

#include "math/Vector3.h"
#include "math/Vector2.h"
#include "io/files/m2/M2Structures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace animation {
                    class M2Vector3Converter {
                    public:
                        math::Vector3 operator () (const C3Vector& cv) const {
							return {cv.x, cv.y, cv.z};
                        }
                    };

                    class M2Vector2Converter {
                    public:
                        math::Vector2 operator () (const C2Vector& cv) const {
							return math::Vector2(cv.x, cv.y);
                        }
                    };
                }
            }
        }
    }
}
