#include "stdafx.h"
#include "M2FileFactory.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                void M2FileFactory::onLoadComplete(DataLoadCompleteEvent& event) {
                    mFileProvider = cdi::ApplicationContext::instance()->produce<IFileProvider>();
                }

                void M2FileFactory::postConstruct(const cdi::ApplicationContextPtr& context) {
                    context->registerEvent<DataLoadCompleteEvent>(
                        std::bind(&M2FileFactory::onLoadComplete, this, std::placeholders::_1));
                }

                chunked::ChunkedM2FilePtr M2FileFactory::loadModel(const std::string& name) const {
                    return std::make_shared<chunked::ChunkedM2File>(
                        name,
                        [this](const std::string& fileName) {
                            return mFileProvider->openFile(fileName);
                        },
                        [this](uint32_t fileDataId) {
                            return mFileProvider->openFileByFileId(
                                fileDataId);
                        }
                    );
                }

                chunked::ChunkedM2FilePtr M2FileFactory::loadModel(uint32_t fileDataId) const {
                    return std::make_shared<chunked::ChunkedM2File>(
                        fileDataId,
                        [this](const std::string& fileName) {
                            return mFileProvider->openFile(fileName);
                        },
                        [this](uint32_t fileDataId) {
                            return mFileProvider->openFileByFileId(
                                fileDataId);
                        }
                    );
                }
            }
        }
    }
}
