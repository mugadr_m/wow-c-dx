#pragma once
#include "chunked/ChunkedM2File.h"
#include "cdi/ApplicationContext.h"
#include "io/files/IFileProvider.h"
#include "io/files/DataLoadCompleteEvent.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                class M2FileFactory {
					IFileProviderPtr mFileProvider;

					void onLoadComplete(DataLoadCompleteEvent& event);
                public:
					void postConstruct(const cdi::ApplicationContextPtr& context);

					chunked::ChunkedM2FilePtr loadModel(const std::string& name) const;
					chunked::ChunkedM2FilePtr loadModel(uint32_t fileDataId) const;
                };

				SHARED_PTR(M2FileFactory);
            }
        }
    }
}
