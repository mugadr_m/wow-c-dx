#include "stdafx.h"
#include "M2SkinFile.h"

namespace wowc {
	namespace io {
		namespace files {
			namespace m2 {

			    M2SkinFile::M2SkinFile(BinaryReader& reader) {
					mHeader = reader.read<M2SkinHeader>();

					mVertices = mHeader.vertices.get(reader);
					mTriangles = mHeader.indices.get(reader);
					mBones = mHeader.bones.get(reader);
					mSubMeshes = mHeader.subMeshes.get(reader);
					mBatches = mHeader.batches.get(reader);

					mIndices.reserve(mTriangles.size());
                    for(auto i = 0u; i < mTriangles.size(); ++i) {
						mIndices.emplace_back(mVertices[mTriangles[i]]);
                    }
			    }
			}
		}
	}
}