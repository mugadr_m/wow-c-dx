#pragma once

#include "io/BinaryReader.h"
#include "M2Structures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                class M2SkinFile {
					M2SkinHeader mHeader{};
					std::vector<uint16_t> mVertices{};
					std::vector<uint16_t> mTriangles{};
					std::vector<uint32_t> mBones{};
					std::vector<M2SkinSection> mSubMeshes{};
					std::vector<M2Batch> mBatches{};

					std::vector<uint16_t> mIndices{};

                public:
					explicit M2SkinFile(BinaryReader& reader);

                    const std::vector<uint16_t>& getVertices() const {
                        return mVertices;
                    }

                    const std::vector<uint16_t>& getTrinagles() const {
                        return mTriangles;
                    }

                    const std::vector<uint32_t>& getBones() const {
                        return mBones;
                    }

                    const std::vector<M2SkinSection>& getSubMeshes() const {
                        return mSubMeshes;
                    }

                    const std::vector<M2Batch>& getBatches() const {
                        return mBatches;
                    }

                    const std::vector<uint16_t>& getIndices() const {
						return mIndices;
                    }
                };

				SHARED_PTR(M2SkinFile);
            }
        }
    }
}