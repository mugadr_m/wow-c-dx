#include "stdafx.h"
#include "ChunkedM2File.h"
#include <boost/endian/conversion.hpp>
#include "utils/String.h"
#include <boost/filesystem.hpp>
#include "io/files/m2/M2SkinFile.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace chunked {
                    utils::Logger<ChunkedM2File> ChunkedM2File::mLogger;

                    void ChunkedM2File::parseChunks(BinaryReader& reader) {
                        const auto magic = reader.read<uint32_t>();
                        reader.seekMod(-4);
                        if (magic == '02DM') {
                            return;
                        }

                        mIsChunked = true;
                        while (reader.available() >= 8) {
                            const auto signature = reader.read<uint32_t>();
                            const auto size = reader.read<uint32_t>();
                            BinaryReader data{{}};
                            if (size > 0) {
                                data = BinaryReader(reader.readBytes(size));
                            }

                            mChunks.emplace(boost::endian::endian_reverse(signature), data);
                        }
                    }

                    void ChunkedM2File::parseTxid() {
                        if (!mIsChunked) {
                            return;
                        }

                        const auto itr = mChunks.find('TXID');
                        if (itr == mChunks.end()) {
                            return;
                        }

                        mTextureFileDataList.resize(itr->second.getSize() / 4);
                        if (mTextureFileDataList.empty()) {
                            return;
                        }

                        itr->second.read(mTextureFileDataList);
                    }

                    void ChunkedM2File::loadLegacyModel(BinaryReader& file) {
                        BinaryReader reader{{}};
                        if (mIsChunked) {
                            const auto itr = mChunks.find('MD21');
                            if (itr == mChunks.end()) {
                                mLogger.error("Chunked M2 model has no subchunk MD21");
                                throw std::exception("Missing MD21 subchunk");
                            }

                            reader = itr->second;
                        } else {
                            reader = file;
                        }

                        mHeader = reader.read<M2Header>();
                        loadName(reader);

                        mVertices = mHeader.vertices.get(reader);
                        const auto fltMax = std::numeric_limits<float>::max();
                        const auto fltMin = -fltMax;
                        math::Vector3 minPos(fltMax, fltMax, fltMax),
                                      maxPos(fltMin, fltMin, fltMin);

                        for(const auto& v : mVertices) {
							const math::Vector3 p{ v.position.x, v.position.y, v.position.z };
							minPos.takeMin(p);
							maxPos.takeMax(p);
                        }

						mBoundingBox.set(minPos, maxPos);

                        loadTextures(reader);
                        loadSkins(reader);
                        loadAnimations(reader);
                    }

                    void ChunkedM2File::loadName(BinaryReader& file) {
                        const auto chars = mHeader.name.get(file);
                        mModelName.assign(chars.begin(), chars.end());
                        mModelName = utils::String::trim(mModelName);
                    }

                    void ChunkedM2File::loadTextures(BinaryReader& file) {
                        const auto textures = mHeader.textures.get(file);
                        auto textureIndex = 0u;

                        for (const auto& texture : textures) {
                            if (texture.type != 0) {
                                mTextures.emplace_back(M2TextureEntry{});
                            } else {
                                if (hasTxid() || texture.fileName.size == 0) {
                                    mTextures.emplace_back(mTextureFileDataList[textureIndex++], std::string(), true,
                                                           true);
                                } else {
                                    auto texChars = texture.fileName.get(file);
                                    if (texChars.back() == '\0') {
                                        texChars.resize(texChars.size() - 1);
                                    }

                                    mTextures.emplace_back(
                                        0,
                                        std::string{texChars.begin(), texChars.end()},
                                        false,
                                        true
                                    );
                                }
                            }
                        }
                    }

                    void ChunkedM2File::loadSkins(BinaryReader& file) {
                        const auto itr = mChunks.find('SFID');
                        const auto hasFileData = itr != mChunks.end();
                        std::vector<uint32_t> fileDataIds;
                        if (hasFileData) {
                            fileDataIds.resize(itr->second.getSize() / 4);
                            itr->second.read(fileDataIds);
                        }

                        BinaryReader reader{{}};
                        if (hasFileData) {
                            reader = mFileDataLoadFunction(fileDataIds.at(0));
                        } else {
                            if (!mHasModelPath) {
                                throw std::exception("Attempted to load an m2 by fileDataId but it has no SFID");
                            }

                            std::stringstream strm;
                            strm << mModelPath << "\\" << mModelName << "00.skin";
                            reader = mFileNameLoadFunction(strm.str());
                        }

                        const auto skin = std::make_shared<M2SkinFile>(reader);

                        for (const auto& subMesh : skin->getSubMeshes()) {
                            mSubMeshes.emplace_back(subMesh.indexCount, subMesh.indexStart + (subMesh.level << 16));
                        }

                        const auto texLookup = mHeader.textureLookup.get(file);
                        const auto renderFlags = mHeader.materials.get(file);

                        for (const auto& batch : skin->getBatches()) {
                            const auto& subMesh = mSubMeshes[batch.subMeshId];
                            std::vector<M2TextureEntry> textures;
                            std::vector<uint32_t> textureIndices;

                            switch (batch.textureCount) {
                            case 2: {
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex]]);
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex + 1]]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex + 1]);
                                break;
                            }

                            case 3: {
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex]]);
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex + 1]]);
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex + 2]]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex + 1]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex + 2]);
                                break;
                            }

                            default: {
                                textures.emplace_back(mTextures[texLookup[batch.textureIndex]]);
                                textureIndices.emplace_back(texLookup[batch.textureIndex]);
                                break;
                            }
                            }

                            const auto renderFlag = renderFlags[batch.renderFlags];
                            mRenderPasses.emplace_back(textures, textureIndices, subMesh.numIndices, subMesh.startIndex,
                                                       batch.texUnitNumber, renderFlag);
                        }

                        mIndices = skin->getIndices();
                    }

                    void ChunkedM2File::loadAnimations(BinaryReader& file) {
                        const auto bones = mHeader.bones.get(file);
                        const auto globalSequences = mHeader.globalLoops.get(file);
                        mAnimationBones.reserve(bones.size());
                        for (const auto& bone : bones) {
                            mAnimationBones.emplace_back(bone, file, mHeader, globalSequences);
                        }

                        mAnimations = mHeader.sequences.get(file);
                        mAnimationLookup = mHeader.sequenceLookup.get(file);

                    }

                    ChunkedM2File::ChunkedM2File(const std::string& fileName,
                                                 std::function<BinaryReader(const std::string&)> fileNameLoadFunction,
                                                 std::function<BinaryReader(uint32_t)> fileDataLoadFunction) :
                        mFileNameLoadFunction(std::move(fileNameLoadFunction)),
                        mFileDataLoadFunction(std::move(fileDataLoadFunction)) {

                        mModelPath = boost::filesystem::path(fileName).branch_path().string();
                        mHasModelPath = true;

                        auto reader = mFileNameLoadFunction(fileName);
                        parseChunks(reader);
                        parseTxid();
                        loadLegacyModel(reader);
                    }

                    ChunkedM2File::ChunkedM2File(uint32_t fileDataId,
                                                 std::function<BinaryReader(const std::string&)> fileNameLoadFunction,
                                                 std::function<BinaryReader(uint32_t)> fileDataLoadFunction) :
                        mFileNameLoadFunction(std::move(fileNameLoadFunction)),
                        mFileDataLoadFunction(std::move(fileDataLoadFunction)) {


                        auto reader = mFileDataLoadFunction(fileDataId);
                        parseChunks(reader);
                        parseTxid();
                        loadLegacyModel(reader);
                    }
                }
            }
        }
    }
}
