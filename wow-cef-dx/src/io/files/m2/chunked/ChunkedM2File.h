#pragma once

#include "io/BinaryReader.h"
#include <unordered_map>
#include "utils/Logger.h"
#include "io/files/m2/M2Structures.h"
#include "io/files/m2/animation/M2AnimationBone.h"
#include "math/BoundingBox.h"


namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                namespace chunked {
                    struct M2TextureEntry {
                        uint32_t fileDataId = 0;
                        std::string textureName{};
                        bool isTxId = false;
                        bool isValid = false;


                        M2TextureEntry() = default;

                        M2TextureEntry(uint32_t fileDataId, std::string textureName, bool isTxId, bool isValid)
                            : fileDataId(fileDataId),
                              textureName(std::move(textureName)),
                              isTxId(isTxId),
                              isValid(isValid) {
                        }
                    };

                    struct M2SubMesh {
                        uint32_t numIndices;
                        uint32_t startIndex;


                        M2SubMesh(uint32_t numIndices, uint32_t startIndex)
                            : numIndices(numIndices),
                              startIndex(startIndex) {
                        }
                    };

                    struct M2RenderPass {
                        std::vector<M2TextureEntry> textures;
                        std::vector<uint32_t> textureIndices;
                        uint32_t indexCount;
                        uint32_t startIndex;
                        uint32_t texUnitNumber;
                        M2Material renderFlags;

                        M2RenderPass(std::vector<M2TextureEntry> m2TextureEntries, std::vector<uint32_t> textureIndices,
                                     uint32_t indexCount, uint32_t startIndex,
                                     uint32_t texUnitNumber, M2Material renderFlags)
                            : textures(std::move(m2TextureEntries)),
                              textureIndices(std::move(textureIndices)),
                              indexCount(indexCount),
                              startIndex(startIndex),
                              texUnitNumber(texUnitNumber),
                              renderFlags(renderFlags) {
                        }
                    };

                    class ChunkedM2File {
                        static utils::Logger<ChunkedM2File> mLogger;

                        std::unordered_map<uint32_t, BinaryReader> mChunks;
                        bool mIsChunked = false;

                        M2Header mHeader{};
                        std::string mModelName;
                        std::string mModelPath;

						bool mHasModelPath = false;

						math::BoundingBox mBoundingBox;

                        std::vector<uint32_t> mTextureFileDataList;

                        std::vector<M2Vertex> mVertices;
                        std::vector<uint16_t> mIndices;

                        std::vector<M2TextureEntry> mTextures;

                        std::vector<M2SubMesh> mSubMeshes;
                        std::vector<M2RenderPass> mRenderPasses;

						std::vector<animation::M2AnimationBone> mAnimationBones;
						std::vector<M2Sequence> mAnimations;
						std::vector<int16_t> mAnimationLookup;

                        std::function<BinaryReader(const std::string&)> mFileNameLoadFunction;
                        std::function<BinaryReader(uint32_t)> mFileDataLoadFunction;

                        bool hasTxid() const {
                            return mIsChunked && (!mTextureFileDataList.empty() || mChunks.find('TXID') != mChunks.end()
                            );
                        }

                        void parseChunks(BinaryReader& reader);
                        void parseTxid();

                        void loadLegacyModel(BinaryReader& file);

                        void loadName(BinaryReader& file);
                        void loadTextures(BinaryReader& file);
                        void loadSkins(BinaryReader& file);
						void loadAnimations(BinaryReader& file);

                    public:
                        explicit ChunkedM2File(
                            const std::string& fileName,
                            std::function<BinaryReader(const std::string&)> fileNameLoadFunction,
                            std::function<BinaryReader(uint32_t)> fileDataLoadFunction
                        );

						explicit ChunkedM2File(
							uint32_t fileDataId,
							std::function<BinaryReader(const std::string&)> fileNameLoadFunction,
							std::function<BinaryReader(uint32_t)> fileDataLoadFunction
						);

                        const std::vector<M2Vertex>& getVertices() const {
                            return mVertices;
                        }

                        const std::vector<uint16_t>& getIndices() const {
                            return mIndices;
                        }

                        const std::vector<M2RenderPass>& getPasses() const {
                            return mRenderPasses;
                        }

                        const std::vector<animation::M2AnimationBone>& getBones() const {
							return mAnimationBones;
                        }

                        const std::vector<M2Sequence>& getAnimations() const {
							return mAnimations;
                        }

                        const std::vector<int16_t>& getAnimationLookup() const {
							return mAnimationLookup;
                        }

                        float getModelRadius() const {
							return mHeader.boundingSphereRadius;
                        }

                        const math::BoundingBox& getBoundingBox() const {
							return mBoundingBox;
                        }
                    };

                    SHARED_PTR(ChunkedM2File);
                }
            }
        }
    }
}
