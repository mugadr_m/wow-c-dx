#include "stdafx.h"
#include "M2Structures.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace m2 {
                math::Quaternion M2CompressedQuaternion::toQuaternion() const {
					return {
					    (x < 0 ? x + 32768 : x - 32767) / 32767.0f,
						(y < 0 ? y + 32768 : y - 32767) / 32767.0f,
                        (z < 0 ? z + 32768 : z - 32767) / 32767.0f,
                        (w < 0 ? w + 32768 : w - 32767) / 32767.0f
					};
                }
            }
        }
    }
}