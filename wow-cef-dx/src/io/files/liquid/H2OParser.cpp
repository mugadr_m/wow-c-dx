#include "stdafx.h"
#include "H2OParser.h"
#include <utility>
#include "utils/Constants.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {
                const std::vector<uint8_t> H2OLiquidBitmap::ALL_TRUE_DATA(8, 0xFF);

                H2OLiquidBitmap::H2OLiquidBitmap(std::vector<uint8_t> bitmapData) :
                    mBitmapData(std::move(bitmapData)) {
                }

                H2OLiquidBitmap::H2OLiquidBitmap() : mBitmapData(ALL_TRUE_DATA) {
                }

                bool H2OLiquidBitmap::isSet(int32_t x, int32_t y, int32_t w, int32_t h) {
                    if (x < 0 || x >= w || y < 0 || y >= h) {
                        return false;
                    }

                    const auto offset = y * w + x;
                    const auto byteOffset = offset / 8;
                    const auto bitOffset = offset % 8;
                    return (mBitmapData[byteOffset] & (1 << bitOffset)) != 0;
                }

                void H2OParser::readHeaders() {
                    mChunkHeaders.resize(256);
                    mReader.read(mChunkHeaders);
                }

                void H2OParser::loadChunks() {
                    for (auto i = 0u; i < mChunkHeaders.size(); ++i) {
                        const auto& header = mChunkHeaders[i];
                        if (header.numLayers <= 0) {
                            continue;
                        }

                        mReader.seek(header.offsetInstances);
                        std::vector<LiquidChunkInstance> chunkInstances(header.numLayers);
                        mReader.read(chunkInstances);

                        const auto cx = i % 16;
                        const auto cy = i / 16;

                        const auto startX = cx * utils::CHUNK_SIZE;
                        const auto startY = cy * utils::CHUNK_SIZE;

                        for (const auto& chunk : chunkInstances) {
							mLiquidManager->handleLiquidInstance(mReader, math::Vector3(startX, startY, 0.0f), chunk);
                        }
                    }
                }

                H2OParser::H2OParser(TileLiquidManagerPtr liquidManager, BinaryReader& reader) :
                    mReader(reader),
                    mLiquidManager(std::move(liquidManager)) {
                    readHeaders();
					loadChunks();
                }
            }
        }
    }
}
