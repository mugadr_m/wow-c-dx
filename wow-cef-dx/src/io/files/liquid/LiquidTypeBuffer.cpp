#include "stdafx.h"
#include "LiquidTypeBuffer.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {

                LiquidTypeBuffer::LiquidTypeBuffer(storage::LiquidType liquidType)
                    : mLiquidType(std::move(liquidType)) {
                    if(mLiquidType.getTextures().empty()) {
						return;
                    }

					const auto pattern = mLiquidType.getTextures().front();
                    for(auto i = 1u; i < 30; ++i) {
						char textureBuffer[2048]{};
						sprintf_s(textureBuffer, pattern.c_str(), i);
						mTextures.emplace_back(textureBuffer);
                    }
                }

                void LiquidTypeBuffer::pushChunk(const std::vector<LiquidVertex>& vertices,
                                                 const std::vector<uint32_t>& indices) {
                    mVertices.insert(mVertices.end(), vertices.begin(), vertices.end());
                    mIndices.insert(mIndices.end(), indices.begin(), indices.end());
                }

                math::BoundingBox LiquidTypeBuffer::getBoundingBox() const {
					const auto fltMax = std::numeric_limits<float>::max();
					const auto fltMin = -fltMax;
					math::Vector3 minPos(fltMax, fltMax, fltMax);
					math::Vector3 maxPos(fltMin, fltMin, fltMin);

                    for(const auto& v : mVertices) {
                        const math::Vector3 pos(v.x, v.y, v.z);
						minPos.takeMin(pos);
						maxPos.takeMax(pos);
                    }

					const auto dz = maxPos.z() - minPos.z();
                    if(dz < 5.0f) {
						maxPos.z(maxPos.z() + 10.0f);
						minPos.z(minPos.z() - 10.0f);
                    }

					return { minPos, maxPos };
                }
            }
        }
    }
}
