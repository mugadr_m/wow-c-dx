#pragma once
#include "TileLiquidManager.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {
                class LiquidTypeBuffer {
                    std::vector<LiquidVertex> mVertices;
                    std::vector<uint32_t> mIndices;

                    std::vector<std::string> mTextures;
                    storage::LiquidType mLiquidType;

                public:
                    explicit LiquidTypeBuffer(storage::LiquidType liquidType);

                    bool isOcean() const {
                        return mLiquidType.getId() == 2;
                    }

                    uint32_t getIndexCount() const {
                        return static_cast<uint32_t>(mIndices.size());
                    }

                    const std::vector<std::string>& getTextureNames() const {
                        return mTextures;
                    }

                    const std::vector<LiquidVertex>& getVertices() const {
                        return mVertices;
                    }

                    const std::vector<uint32_t>& getIndices() const {
                        return mIndices;
                    }

                    uint32_t getBaseIndex() const {
						return static_cast<uint32_t>(mVertices.size());
                    }

                    void pushChunk(const std::vector<LiquidVertex>& vertices, const std::vector<uint32_t>& indices);

					math::BoundingBox getBoundingBox() const;
                };

                SHARED_PTR(LiquidTypeBuffer);
            }
        }
    }
}
