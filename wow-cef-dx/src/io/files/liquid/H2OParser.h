#pragma once

#include "io/BinaryReader.h"
#include "TileLiquidManager.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {
#pragma pack(push, 1)
                struct LiquidChunkHeader {
					uint32_t offsetInstances;
					uint32_t numLayers;
					uint32_t offsetAttributes;
                };

                struct LiquidChunkInstance {
					uint16_t liquidType;
					uint16_t vertexFormat;
					float minHeight;
					float maxHeight;
					uint8_t offsetX;
					uint8_t offsetY;
					uint8_t width;
					uint8_t height;
					uint32_t offsetBitmap;
					uint32_t offsetVertices;
                };
#pragma pack(pop)

                class H2OLiquidBitmap {
                public:
					static const std::vector<uint8_t> ALL_TRUE_DATA;
                private:

					std::vector<uint8_t> mBitmapData;

                public:
					explicit H2OLiquidBitmap(std::vector<uint8_t> bitmapData);
					H2OLiquidBitmap();

					bool isSet(int32_t x, int32_t y, int32_t w, int32_t h);
                };

                class H2OParser {
					BinaryReader& mReader;
					TileLiquidManagerPtr mLiquidManager;

					std::vector<LiquidChunkHeader> mChunkHeaders;

					void readHeaders();
					void loadChunks();

                public:
					explicit H2OParser(TileLiquidManagerPtr liquidManager, BinaryReader& reader);
                };
            }
        }
    }
}
