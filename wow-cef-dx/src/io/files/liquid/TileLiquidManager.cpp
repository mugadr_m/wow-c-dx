#include "stdafx.h"
#include "TileLiquidManager.h"
#include "H2OParser.h"
#include "LiquidTypeBuffer.h"
#include "utils/Constants.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {
                static const storage::LiquidType FALLBACK_LIQUID{
                    0,
                    "fallback_liquid_type",
                    {"XTextures\\river\\lake_a.%d.blp"}
                };


                LiquidTypeBufferPtr TileLiquidManager::getOrCreateLiquidTypeBuffer(uint32_t liquidTypeId) {
                    const auto itr = mLiquidTypeBuffers.find(liquidTypeId);
                    if (itr != mLiquidTypeBuffers.end()) {
                        return itr->second;
                    }

                    storage::LiquidType liquidType;
                    try {
                        liquidType = mLiquidStorageDao->getLiquidTypeById(liquidTypeId);
                    } catch (std::exception&) {
                        liquidType = FALLBACK_LIQUID;
                    }

                    const auto buffer = std::make_shared<LiquidTypeBuffer>(liquidType);
                    mLiquidTypeBuffers.emplace(liquidTypeId, buffer);
                    return buffer;
                }

                void TileLiquidManager::handleLiquidQuad(float baseX, float baseY, uint32_t x, uint32_t y,
                                                         uint32_t rowPitch, uint32_t baseIndex,
                                                         const std::vector<float>& heightMap,
                                                         const std::vector<uint8_t>& depthMap,
                                                         std::vector<LiquidVertex>& vertices,
                                                         std::vector<uint32_t>& indices) {
                    using utils::LIQUID_VERTEX_SIZE;

                    vertices.emplace_back(LiquidVertex{
                        baseX + x * LIQUID_VERTEX_SIZE,
                        baseY + y * LIQUID_VERTEX_SIZE,
                        heightMap[y * rowPitch + x],
                        x + 0.0f, y + 0.0f,
                        depthMap[y * rowPitch + x]
                    });

                    vertices.emplace_back(LiquidVertex{
                        baseX + (x + 1) * LIQUID_VERTEX_SIZE,
                        baseY + y * LIQUID_VERTEX_SIZE,
                        heightMap[y * rowPitch + x + 1],
                        x + 1.0f, y + 0.0f,
                        depthMap[y * rowPitch + x + 1]
                    });

                    vertices.emplace_back(LiquidVertex{
                        baseX + (x + 1) * LIQUID_VERTEX_SIZE,
                        baseY + (y + 1) * LIQUID_VERTEX_SIZE,
                        heightMap[(y + 1) * rowPitch + x + 1],
                        x + 1.0f, y + 1.0f,
                        depthMap[(y + 1) * rowPitch + x + 1]
                    });

                    vertices.emplace_back(LiquidVertex{
                        baseX + x * LIQUID_VERTEX_SIZE,
                        baseY + (y + 1) * LIQUID_VERTEX_SIZE,
                        heightMap[(y + 1) * rowPitch + x],
                        x + 0.0f, y + 1.0f,
                        depthMap[(y + 1) * rowPitch + x]
                    });

                    indices.emplace_back(baseIndex);
                    indices.emplace_back(baseIndex + 1);
                    indices.emplace_back(baseIndex + 2);
                    indices.emplace_back(baseIndex);
                    indices.emplace_back(baseIndex + 2);
                    indices.emplace_back(baseIndex + 3);
                }

                TileLiquidManager::TileLiquidManager(storage::LiquidStorageDaoPtr liquidStorage) :
                    mLiquidStorageDao(std::move(liquidStorage)) {

                }

                void TileLiquidManager::handleLiquidInstance(BinaryReader& reader, const math::Vector3& basePosition,
                                                             const LiquidChunkInstance& instance) {
                    std::vector<LiquidVertex> vertices;
                    std::vector<uint32_t> indices;

                    reader.seek(instance.offsetBitmap);
                    H2OLiquidBitmap bitmap(instance.offsetBitmap == 0
                                               ? H2OLiquidBitmap::ALL_TRUE_DATA
                                               : reader.readBytes((instance.width * instance.height + 7) / 8)
                    );

                    reader.seek(instance.offsetVertices);
                    const auto rowPitch = instance.width + 1;
                    const auto rows = instance.height + 1;

                    std::vector<float> heightMap(rowPitch * rows);
                    std::vector<uint8_t> depthMap(rowPitch * rows);

                    if (instance.liquidType != 2) {
                        reader.read(heightMap);
                    } else {
                        heightMap.assign(heightMap.size(), instance.minHeight);
                    }

                    if (instance.offsetVertices != 0) {
                        reader.read(depthMap);
                    } else {
                        depthMap.assign(depthMap.size(), 255);
                    }

                    const auto buffer = getOrCreateLiquidTypeBuffer(instance.liquidType);
                    auto baseIndex = buffer->getBaseIndex();

                    const auto bx = instance.offsetX * utils::LIQUID_VERTEX_SIZE + basePosition.x();
                    const auto by = instance.offsetY * utils::LIQUID_VERTEX_SIZE + basePosition.y();

                    for (auto x = 0u; x < instance.width; ++x) {
                        for (auto y = 0u; y < instance.height; ++y) {
                            if (!bitmap.isSet(x, y, instance.width, instance.height)) {
                                continue;
                            }

                            handleLiquidQuad(bx, by, x, y, rowPitch, baseIndex, heightMap, depthMap, vertices, indices);
                            baseIndex += 4;
                        }
                    }

                    buffer->pushChunk(vertices, indices);
                }
            }
        }
    }
}
