#pragma once

#include "io/files/storage/LiquidStorageDao.h"
#include "io/BinaryReader.h"
#include "math/Vector3.h"
#include "math/BoundingBox.h"

namespace wowc {
    namespace io {
        namespace files {
            namespace liquid {
#pragma pack(push, 1)
                struct LiquidVertex {
                    float x, y, z;
                    float tx, ty;
                    uint8_t depth;
                };
#pragma pack(pop)

                SHARED_FWD(LiquidTypeBuffer);

                struct LiquidChunkInstance;

                class TileLiquidManager {
                    storage::LiquidStorageDaoPtr mLiquidStorageDao;

                    std::unordered_map<uint32_t, LiquidTypeBufferPtr> mLiquidTypeBuffers;

					math::BoundingBox mBoundingBox;

					LiquidTypeBufferPtr getOrCreateLiquidTypeBuffer(uint32_t liquidTypeId);

                    static void handleLiquidQuad(float baseX, float baseY, uint32_t x, uint32_t y,
                                          uint32_t rowPitch, uint32_t baseIndex, const std::vector<float>& heightMap,
                                          const std::vector<uint8_t>& depthMap,
                                          std::vector<LiquidVertex>& vertices, std::vector<uint32_t>& indices);

                public:
                    explicit TileLiquidManager(storage::LiquidStorageDaoPtr liquidStorage);

                    void handleLiquidInstance(BinaryReader& reader, const math::Vector3& basePosition,
                                              const LiquidChunkInstance& instance);

                    bool hasAnyLiquid() const {
						return !mLiquidTypeBuffers.empty();
                    }

                    const std::unordered_map<uint32_t, LiquidTypeBufferPtr>& getBuffers() const {
						return mLiquidTypeBuffers;
                    }
                };

                SHARED_PTR(TileLiquidManager);
            }
        }
    }
}
