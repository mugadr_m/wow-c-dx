#pragma once
#include <document.h>

namespace wowc {
	namespace io {
		namespace files {
			class DataLoadInitEvent {
			public:
				void toJson(rapidjson::Document& document) const {}
				rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
				static DataLoadInitEvent fromJson(const rapidjson::Document& document) { return DataLoadInitEvent(); }
			};
		}
	}
}
