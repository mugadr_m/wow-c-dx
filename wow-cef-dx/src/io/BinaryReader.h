#include <utility>
#pragma once

namespace wowc {
    namespace io {
        class BinaryReader {
            std::vector<uint8_t> mData;
            std::size_t mPosition = 0;

        public:
            explicit BinaryReader(const std::vector<uint8_t>& data) : mData(data) {

            }

            std::vector<uint8_t> readBytes(std::size_t numBytes) {
                std::vector<uint8_t> ret(numBytes);
                read(ret.data(), numBytes);
                return ret;
            }

            void read(void* data, std::size_t numBytes);

            template <typename T>
            void read(T& value) {
                read(&value, sizeof(T));
            }

            template <typename T>
            T read() {
                T ret{};
                read(&ret, sizeof(T));
                return ret;
            }

            template <typename T, std::size_t Size>
            void read(T (&value)[Size]) {
                read(value, Size * sizeof(T));
            }

            template <typename T>
            void read(std::vector<T>& values) {
                read(values.data(), sizeof(T) * values.size());
            }

            uint32_t readIntBE() {
                const auto valueLE = read<uint32_t>();
                const auto b0 = valueLE & 0xFF;
                const auto b1 = (valueLE >> 8) & 0xFF;
                const auto b2 = (valueLE >> 16) & 0xFF;
                const auto b3 = (valueLE >> 24) & 0xFF;
                return b3 | (b2 << 8) | (b1 << 16) | (b0 << 24);
            }

            void seekMod(const int64_t& modPos) {
                mPosition += modPos;
            }

            void seek(const std::size_t& position) {
                mPosition = position;
            }

            std::size_t tell() const {
                return mPosition;
            }

            std::size_t getSize() const {
                return mData.size();
            }

            const std::vector<uint8_t>& getData() const {
                return mData;
            }

            std::size_t available() const {
                return getSize() - tell();
            }
        };
    }
}
