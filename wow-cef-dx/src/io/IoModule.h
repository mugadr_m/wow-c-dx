#pragma once

namespace wowc {
	namespace io {
		class IoModule {
		public:
			IoModule() = delete;

			static void initialize();
			static void initUiEvents();
		};
	}
}