#include "stdafx.h"
#include "HttpClient.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace io {
		namespace http {
			namespace detail {
				class RequestProxy {
				public:
					static void append(const void* buffer, std::size_t numBytes, HttpClient* client) {
						client->onData(buffer, numBytes);
					}
				};

				size_t curlWriteFunction(const void* buffer, size_t size, size_t nmemb, void* userp) {
					const auto numBytes = static_cast<uint32_t>(size * nmemb);
					const auto request = reinterpret_cast<HttpClient*>(userp);
					RequestProxy::append(buffer, numBytes, request);
					return static_cast<size_t>(numBytes);
				}
			}

			utils::Logger<HttpClient> HttpClient::mLogger;

			void HttpClient::onData(const void* buffer, std::size_t numBytes) {
				const auto ptr = reinterpret_cast<const uint8_t*>(buffer);
				mBuffer.insert(mBuffer.end(), ptr, ptr + numBytes);
			}

			HttpClient::HttpClient() {
				mHandle = curl_easy_init();
				curl_easy_setopt(mHandle, CURLOPT_SSL_VERIFYPEER, 0);
				curl_easy_setopt(mHandle, CURLOPT_SSL_VERIFYHOST, 0);
				curl_easy_setopt(mHandle, CURLOPT_FRESH_CONNECT, true);
				curl_easy_setopt(mHandle, CURLOPT_FORBID_REUSE, true);
				curl_easy_setopt(mHandle, CURLOPT_NOPROGRESS, 1);
				curl_easy_setopt(mHandle, CURLOPT_WRITEDATA, this);
				curl_easy_setopt(mHandle, CURLOPT_WRITEFUNCTION, detail::curlWriteFunction);
				curl_easy_setopt(mHandle, CURLOPT_FOLLOWLOCATION, 1);
			}

			HttpClient::~HttpClient() {
				curl_easy_cleanup(mHandle);
				curl_slist_free_all(mHeaderList);
			}

			void HttpClient::setUrl(const std::string& url) {
				mUrl = url;
				curl_easy_setopt(mHandle, CURLOPT_URL, url.c_str());
			}

			void HttpClient::addHeader(const std::string& name, const std::string& value) {
				std::stringstream strm;
				strm << name << ":" << value;
				mHeaderList = curl_slist_append(mHeaderList, strm.str().c_str());
			}

			void HttpClient::execute() {
				if(mHeaderList != nullptr) {
					curl_easy_setopt(mHandle, CURLOPT_HTTPHEADER, mHeaderList);
				}

				const auto result = curl_easy_perform(mHandle);
				if(result != CURLE_OK) {
					mLogger.error("Request failed: ", curl_easy_strerror(result));
					throw std::exception("Error reading response");
				}

				curl_easy_getinfo(mHandle, CURLINFO_RESPONSE_CODE, &mResponseCode);
			}

			void HttpClient::initCurl() {
				curl_global_init(CURL_GLOBAL_ALL);
			}

			std::string HttpClient::urlEncode(const std::string& string) {
				const auto handle = curl_easy_init();
				auto* encoded = curl_easy_escape(handle, string.c_str(), 0);
				std::string retStr(encoded);
				curl_free(encoded);
				curl_easy_cleanup(handle);
				return retStr;
			}
		}
	}
}
