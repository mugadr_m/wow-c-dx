#pragma once

#include "utils/Logger.h"
extern "C" {
#define CURL_STATICLIB
#include <curl.h>
}

namespace wowc {
	namespace io {
		namespace http {
			namespace detail {
				class RequestProxy;
			}

			class HttpClient {
				friend class detail::RequestProxy;

				static utils::Logger<HttpClient> mLogger;

				std::vector<uint8_t> mBuffer;
				uint32_t mResponseCode = 0;
				std::map<std::string, std::string> mHeaders;
				std::string mUrl;

				CURL* mHandle;
				curl_slist* mHeaderList = nullptr;

				void onData(const void* buffer, std::size_t numBytes);

			public:
				HttpClient();
				~HttpClient();

				void setUrl(const std::string& url);
				void addHeader(const std::string& name, const std::string& value);

				void execute();

				const std::vector<uint8_t>& getResponse() const { return mBuffer; }
				uint32_t getResponseCode() const { return mResponseCode; }

				static void initCurl();

				static std::string urlEncode(const std::string& string);
			};

			SHARED_PTR(HttpClient);
		}
	}
}
