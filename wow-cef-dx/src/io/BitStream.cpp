#include "stdafx.h"
#include "BitStream.h"

namespace wowc {
	namespace io {

		std::vector<uint8_t> BitStream::readBytes(std::size_t numBits, std::size_t outByteSize) {
			outByteSize = outByteSize == 0 ? numBits / 8 : outByteSize;
			std::vector<uint8_t> outData(outByteSize);
			for(auto i = 0u; i < numBits; ++i) {
				const auto byteOffset = i / 8;
				const auto bitOffset = i % 8;
				const auto curBit = readBit();
				outData[byteOffset] |= curBit << bitOffset;
			}

			return outData;
		}
	}
}
