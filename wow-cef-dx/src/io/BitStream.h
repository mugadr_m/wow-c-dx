#pragma once

namespace wowc {
	namespace io {
		class BitStream {
			std::vector<uint8_t> mData;
			std::size_t mBitOffset = 0;

			uint8_t readBit() {
				const auto byteOffset = mBitOffset / 8;
				const auto bitOffset = mBitOffset % 8;
				const auto ret = (mData[byteOffset] >> bitOffset) & 1;
				++mBitOffset;
				return ret;
			}
		public:
			explicit BitStream(std::vector<uint8_t> data) : mData(std::move(data)) { }

			std::vector<uint8_t> readBytes(std::size_t numBits, std::size_t outByteSize = 0);

			void seek(std::size_t bitPosition) {
				mBitOffset = bitPosition;
			}

			std::size_t tell() const {
				return mBitOffset;
			}

			template<typename T>
			T read(const std::size_t numBits = sizeof(T)) {
				if(numBits > sizeof(T) * 8) {
					throw std::exception("Attempted to read more bytes than the target type can hold");
				}

				T ret{};
				const auto res = readBytes(numBits, sizeof(T));
				memcpy(&ret, res.data(), res.size());
				return ret;
			}
		};
	}
}
