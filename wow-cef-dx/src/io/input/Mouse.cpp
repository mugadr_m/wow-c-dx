#include "stdafx.h"
#include "Mouse.h"

namespace wowc {
    namespace io {
        namespace input {

			void Mouse::onUpdate() {
				GetKeyboardState(mButtonState);
				POINT pt{};
				if(GetCursorPos(&pt) == TRUE) {
					mLastPosition.x(pt.x).y(pt.y);
				}
            }
        }
    }
}