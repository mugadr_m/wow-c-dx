#include "stdafx.h"
#include "Keyboard.h"

namespace wowc {
    namespace io {
        namespace input {

            void Keyboard::onUpdate() {
				GetKeyboardState(mState);
            }
        }
    }
}