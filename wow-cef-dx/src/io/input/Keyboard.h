#pragma once

namespace wowc {
    namespace io {
        namespace input {
            class Keyboard {
				uint8_t mState[256]{};

            public:
				void onUpdate();

				bool isKeyDown(const uint8_t key) { return (mState[key] & 0x80) != 0; }
            };

			SHARED_PTR(Keyboard);
        }
    }
}
