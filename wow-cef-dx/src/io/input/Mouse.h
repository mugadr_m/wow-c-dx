#pragma once
#include "math/Vector2.h"

namespace wowc {
    namespace io {
        namespace input {
            class Mouse {
				math::Vector2 mLastPosition;
				uint8_t mButtonState[256]{};

            public:
				void onUpdate();

				const math::Vector2& getLastPosition() const { return mLastPosition; }

				bool isLeftPressed() const { return isButtonPressed(VK_LBUTTON); }
				bool isRightPressed() const { return isButtonPressed(VK_RBUTTON); }
				bool isMiddlePressed() const { return isButtonPressed(VK_MBUTTON); }

				bool isButtonPressed(const uint8_t button) const { return (mButtonState[button] & 0x80) != 0; }
            };

			SHARED_PTR(Mouse);
        }
    }
}
