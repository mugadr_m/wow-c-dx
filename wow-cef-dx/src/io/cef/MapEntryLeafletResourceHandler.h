#pragma once
#include <cef_resource_handler.h>
#include "io/files/minimap/MinimapLeafletStorage.h"
#include "LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class MapEntryLeafletResourceHandler : public CefResourceHandler {
				IMPLEMENT_REFCOUNTING(MapEntryLeafletResourceHandler);

				files::minimap::MinimapLeafletStoragePtr mMinimapLeafletStorage;
				BinaryReader mDataReader;
				LoadingScreenThreadPoolPtr mThreadPool;
				bool mHasFailed = false;

				static std::string getUrlPath(const CefRefPtr<CefRequest>& request);
			public:
				explicit MapEntryLeafletResourceHandler(LoadingScreenThreadPoolPtr threadPool, files::minimap::MinimapLeafletStoragePtr minimapLeafletStorage);
				~MapEntryLeafletResourceHandler() override;

				bool ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback) override;
				void GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length, CefString& redirectUrl) override;
				bool ReadResponse(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefCallback> callback) override;
				bool CanGetCookie(const CefCookie& cookie) override;
				bool CanSetCookie(const CefCookie& cookie) override;
				void Cancel() override;
			};
		}
	}
}
