#pragma once

#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace io {
		namespace cef {
			class LoadingScreenThreadPool {
				std::vector<std::thread> mThreads;
				std::condition_variable mWorkEvent;
				std::mutex mWorkLock;
				std::list<std::function<void()>> mWorkItems;

				bool mIsRunning = true;

				void workerThreadProc();
			public:
				void postConstruct(const cdi::ApplicationContextPtr& context);
				void preDestroy();

				void pushWork(const std::function<void()>& workItem);
			};

			SHARED_PTR(LoadingScreenThreadPool);
		}
	}
}
