#pragma once

#include <cef_scheme.h>
#include "cdi/ApplicationContext.h"
#include "LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class LocalSchemeHandlerFactory : public CefSchemeHandlerFactory {
			public:
				static const std::string SCHEME_NAME;
				static const std::string SCHEME_PREFIX;

			private:
			IMPLEMENT_REFCOUNTING(LocalSchemeHandlerFactory);

				std::string mUiFolder;

			public:
				CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
				                                     const CefString& schemeName, CefRefPtr<CefRequest> request) override;

				void postConstruct(const cdi::ApplicationContextPtr& ctx);
			};
		}
	}
}
