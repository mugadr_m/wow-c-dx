#include "stdafx.h"
#include "IoWorkerThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {

			void IoWorkerThreadPool::workerCallback() {
				while(mIsRunning) {
					std::function<void()> workItem;
					{
						std::unique_lock<std::mutex> l(mWorkLock);
						mWorkEvent.wait(l, [&] { return !mWorkItems.empty() || !mIsRunning;  });
						if(!mIsRunning) {
							return;
						}

						workItem = mWorkItems.front();
						mWorkItems.pop_front();
					}

					if(workItem) {
						workItem();
					}
				}
			}

			void IoWorkerThreadPool::pushWork(const std::function<void()>& workItem) {
				{
					std::lock_guard<std::mutex> l(mWorkLock);
					mWorkItems.emplace_back(workItem);
				}

				mWorkEvent.notify_one();
			}

			void IoWorkerThreadPool::postConstruct(const cdi::ApplicationContextPtr& context) {
				mIsRunning = true;

				const auto numThreads = std::thread::hardware_concurrency();

				for(auto i = 0u; i < numThreads; ++i) {
					mRunnerThreads.emplace_back(std::bind(&IoWorkerThreadPool::workerCallback, this));
				}
			}

			void IoWorkerThreadPool::preDestroy() {
				mIsRunning = false;
				mWorkEvent.notify_all();
				for(auto& thread : mRunnerThreads) {
					if(thread.joinable()) {
						thread.join();
					}
				}
			}

			void IoWorkerThreadPool::shutdown() {
				mIsRunning = false;
				mWorkEvent.notify_all();
				for(auto& thread : mRunnerThreads) {
					if(thread.joinable()) {
						thread.join();
					}
				}
			}
		}
	}
}
