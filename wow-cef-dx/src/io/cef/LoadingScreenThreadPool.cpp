#include "stdafx.h"
#include "LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			void LoadingScreenThreadPool::workerThreadProc() {
				while(mIsRunning) {
					std::function<void()> workItem;
					{
						std::unique_lock<std::mutex> l(mWorkLock);
						if(mWorkItems.empty()) {
							mWorkEvent.wait(l);
							if(!mIsRunning) {
								break;
							}
							continue;
						}

						workItem = mWorkItems.front();
						mWorkItems.pop_front();
					}

					if(workItem) {
						workItem();
					}
				}
			}

			void LoadingScreenThreadPool::postConstruct(const cdi::ApplicationContextPtr&) {
				const auto numThreads = std::thread::hardware_concurrency();
				for (auto i = 0u; i < numThreads; ++i) {
					mThreads.emplace_back(std::bind(&LoadingScreenThreadPool::workerThreadProc, this));
				}
			}

			void LoadingScreenThreadPool::preDestroy() {
				mIsRunning = false;
				mWorkEvent.notify_all();
				for (auto& thread : mThreads) {
					if(thread.joinable()) {
						thread.join();
					}
				}
			}

			void LoadingScreenThreadPool::pushWork(const std::function<void()>& workItem) {
				{
					std::unique_lock<std::mutex> l(mWorkLock);
					mWorkItems.push_back(workItem);
				}

				mWorkEvent.notify_one();
			}
		}
	}
}
