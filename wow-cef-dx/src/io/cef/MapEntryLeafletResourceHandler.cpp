#include "stdafx.h"
#include "MapEntryLeafletResourceHandler.h"
#include "utils/String.h"
#include <winhttp.h>
#include <utility>

namespace wowc {
	namespace io {
		namespace cef {
			std::string MapEntryLeafletResourceHandler::getUrlPath(const CefRefPtr<CefRequest>& request) {
				URL_COMPONENTS components{};
				components.dwUrlPathLength = -1;
				components.dwExtraInfoLength = -1;
				components.dwHostNameLength = -1;
				components.dwPasswordLength = -1;
				components.dwSchemeLength = -1;
				components.dwUserNameLength = -1;
				components.dwStructSize = sizeof(components);

				auto wstringUrl = utils::String::toUnicode(request->GetURL());
				const auto schemeEnd = wstringUrl.find(L':');
				wstringUrl = std::wstring(L"http") + wstringUrl.substr(schemeEnd);
				const auto result = WinHttpCrackUrl(wstringUrl.c_str(), 0, 0, &components);
				if (result == FALSE) {
					throw std::exception("Error parsing URL");
				}

				const std::wstring pathPart(components.lpszUrlPath, components.lpszUrlPath + components.dwUrlPathLength);
				return utils::String::toMultibyte(pathPart);
			}

			MapEntryLeafletResourceHandler::MapEntryLeafletResourceHandler(
				LoadingScreenThreadPoolPtr threadPool,
				files::minimap::MinimapLeafletStoragePtr minimapLeafletStorage) : mMinimapLeafletStorage(std::move(minimapLeafletStorage)),
				                                                                  mDataReader({}),
				                                                                  mThreadPool(std::move(threadPool)) {
			}

			MapEntryLeafletResourceHandler::~MapEntryLeafletResourceHandler() = default;

			bool MapEntryLeafletResourceHandler::ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback) {
				auto urlPath = getUrlPath(request);
				if (urlPath.empty()) {
					throw std::exception("Empty path supplied from leaflet");
				}

				if (urlPath[0] == '/') {
					urlPath = urlPath.substr(1);
				}

				const auto parts = utils::String::split(urlPath, '/');
				if (parts.size() != 4) {
					return false;
				}

				const auto mapId = std::stoi(parts[0]);
				const auto zoomLevel = std::stoi(parts[1]);
				const auto tileX = std::stoi(parts[2]);
				const auto tileY = std::stoi(parts[3]);

				const CefRefPtr<CefResourceHandler> self = this;
				mThreadPool->pushWork([self, this, mapId, zoomLevel, tileX, tileY, callback]() {
					png::image<png::rgba_pixel> image;
					try {
						mMinimapLeafletStorage->readImage(image, mapId, zoomLevel, tileX, tileY);
					} catch (std::exception&) {
						mHasFailed = true;
						callback->Continue();
						return;
					}

					std::stringstream strm;
					image.write_stream(strm);
					const auto size = strm.tellp();
					strm.seekg(0, std::ios::beg);
					std::vector<uint8_t> imageData(size);
					strm.read(reinterpret_cast<char*>(imageData.data()), imageData.size());
					mDataReader = BinaryReader(imageData);
					callback->Continue();
				});

				return true;
			}

			void MapEntryLeafletResourceHandler::GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length,
			                                                        CefString& redirectUrl) {
				redirectUrl = "";
				response->SetMimeType("image/png");

				if(mHasFailed) {
					response->SetStatus(500);
					response_length = 0;
					return;
				}

				response_length = static_cast<int64>(mDataReader.getSize());
				response->SetStatus(200);
			}

			bool MapEntryLeafletResourceHandler::ReadResponse(void* data_out, int bytes_to_read, int& bytes_read,
			                                                  CefRefPtr<CefCallback> callback) {
				const auto numAvailable = mDataReader.available();
				if (numAvailable <= 0) {
					return false;
				}

				const auto toRead = std::min<std::size_t>(bytes_to_read, numAvailable);
				bytes_read = static_cast<int>(toRead);

				mDataReader.read(data_out, toRead);
				return true;
			}

			bool MapEntryLeafletResourceHandler::CanGetCookie(const CefCookie& cookie) {
				return false;
			}

			bool MapEntryLeafletResourceHandler::CanSetCookie(const CefCookie& cookie) {
				return false;
			}

			void MapEntryLeafletResourceHandler::Cancel() {
			}
		}
	}
}
