#pragma once

#include <cef_resource_handler.h>
#include "utils/Logger.h"
#include "IoWorkerThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class LocalSchemeResourceHandler : public CefResourceHandler {
				static utils::Logger<LocalSchemeResourceHandler> mLogger;

			IMPLEMENT_REFCOUNTING(LocalSchemeResourceHandler)

				std::string mUiFolder;
				bool mIsCancelled = false;
				std::unique_ptr<std::ifstream> mInputStream;
				std::string mUrl;
				std::string mFilePath;
				uint64_t mCurrentPosition;
				uint64_t mSize;

			public:
				explicit LocalSchemeResourceHandler(std::string uiFolder);

				bool ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback) override;
				void GetResponseHeaders(CefRefPtr<CefResponse> response, int64& responseLength, CefString& redirectUrl) override;
				bool ReadResponse(void* dataOut, int bytesToRead, int& bytesRead, CefRefPtr<CefCallback> callback) override;
				bool CanGetCookie(const CefCookie& cookie) override;
				bool CanSetCookie(const CefCookie& cookie) override;
				void Cancel() override;
				~LocalSchemeResourceHandler() override = default;
			};
		}
	}
}
