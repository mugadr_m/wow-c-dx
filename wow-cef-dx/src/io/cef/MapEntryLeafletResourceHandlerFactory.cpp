#include "stdafx.h"
#include "MapEntryLeafletResourceHandlerFactory.h"
#include "MapEntryLeafletResourceHandler.h"

namespace wowc {
	namespace io {
		namespace cef {
			const std::string MapEntryLeafletResourceHandlerFactory::SCHEME_NAME = "map-entry";

			CefRefPtr<CefResourceHandler> MapEntryLeafletResourceHandlerFactory::Create(CefRefPtr<CefBrowser> browser,
			                                                                                 CefRefPtr<CefFrame> frame,
			                                                                                 const CefString& scheme_name,
			                                                                                 CefRefPtr<CefRequest> request) {
				return new MapEntryLeafletResourceHandler(mThreadPool, mMinimapLeafletStorage);
			}

			void MapEntryLeafletResourceHandlerFactory::postConstruct(const cdi::ApplicationContextPtr& ctx) {
				mMinimapLeafletStorage = ctx->produce<files::minimap::MinimapLeafletStorage>();
				mThreadPool = ctx->produce<LoadingScreenThreadPool>();
			}
		}
	}
}
