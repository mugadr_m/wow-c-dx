#include "stdafx.h"
#include "LocalSchemeHandlerFactory.h"
#include "LocalSchemeResourceHandler.h"
#include "utils/CommandLine.h"

namespace wowc {
	namespace io {
		namespace cef {
			const std::string LocalSchemeHandlerFactory::SCHEME_NAME = "local";
			const std::string LocalSchemeHandlerFactory::SCHEME_PREFIX = SCHEME_NAME + "://";

			CefRefPtr<CefResourceHandler> LocalSchemeHandlerFactory::Create(CefRefPtr<CefBrowser> browser,
				CefRefPtr<CefFrame> frame, const CefString& schemeName, CefRefPtr<CefRequest> request) {
				return new LocalSchemeResourceHandler(mUiFolder);
			}

			void LocalSchemeHandlerFactory::postConstruct(const cdi::ApplicationContextPtr& ctx) {
				const auto cmdLine = ctx->produce<utils::CommandLine>();
				mUiFolder = cmdLine->getOption("ui-folder", "ui");
			}
		}
	}
}
