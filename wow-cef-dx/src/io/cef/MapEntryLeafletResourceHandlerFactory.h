#pragma once
#include <cef_scheme.h>
#include "io/files/minimap/MinimapLeafletStorage.h"
#include "cdi/ApplicationContext.h"
#include "LoadingScreenThreadPool.h"

namespace wowc {
	namespace io {
		namespace cef {
			class MapEntryLeafletResourceHandlerFactory : public CefSchemeHandlerFactory {
			public:
				static const std::string SCHEME_NAME;
			private:

				IMPLEMENT_REFCOUNTING(MapEntryLeafletResourceHandlerFactory);

				files::minimap::MinimapLeafletStoragePtr mMinimapLeafletStorage;
				LoadingScreenThreadPoolPtr mThreadPool;

			public:

				CefRefPtr<CefResourceHandler> Create(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
					const CefString& scheme_name, CefRefPtr<CefRequest> request) override;
				~MapEntryLeafletResourceHandlerFactory() override = default;

				void postConstruct(const cdi::ApplicationContextPtr& ctx);
			};
		}
	}
}
