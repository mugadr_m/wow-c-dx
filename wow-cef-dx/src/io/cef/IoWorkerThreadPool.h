#pragma once
#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace io {
		namespace cef {
			class IoWorkerThreadPool {
				bool mIsRunning = false;
				std::deque<std::function<void()>> mWorkItems;
				std::condition_variable mWorkEvent;
				std::mutex mWorkLock;

				std::list<std::thread> mRunnerThreads;

				void workerCallback();

			public:
				void postConstruct(const cdi::ApplicationContextPtr& context);
				void preDestroy();

				void pushWork(const std::function<void()>& workItem);
				void shutdown();
			};

			SHARED_PTR(IoWorkerThreadPool);
		}
	}
}
