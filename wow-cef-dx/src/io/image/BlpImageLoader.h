#pragma once

#include "third_party/pngpp/png.hpp"
#include "io/BinaryReader.h"
#include "io/files/blp/BlpHeader.h"

namespace wowc {
	namespace io {
		namespace image {
			class BlpImageLoader {
				struct RgbDataArray {
					RgbDataArray() {
						data.color = 0;
					}

					union data {
						uint32_t color;
						uint8_t buffer[4];
					} data;
				};

				typedef void(*TConvertFunction)(BinaryReader&, std::vector<uint32_t>&, const std::size_t&);

				static TConvertFunction getBcConvertFunction(const files::blp::BlpImageFormat& format);

				static void readBcColors(BinaryReader& stream, RgbDataArray* colors, bool preMultipliedAlpha, bool use4Colors = false);

				static void bc1GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset);
				static void bc2GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset);
				static void bc3GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset);

				static void decompressPaletteArgb8(const uint8_t& alphaDepth, const uint32_t* palette, const std::vector<uint8_t>& indices, png::image<png::rgba_pixel>& outImage);
				static void decompressPaletteFastPath(const uint32_t* palette, const std::vector<uint8_t>& indices, png::image<png::rgba_pixel>& outImage);

				static void parseUncompressed(const std::vector<uint8_t>& imageData, png::image<png::rgba_pixel>& outImage);
				static void parseUncompressedPalette(const std::vector<uint8_t>& imageData, uint32_t* palette, const uint8_t& alphaDepth, png::image<png::rgba_pixel>& outImage);

				static void parseCompressed(const files::blp::BlpImageFormat& format, const std::vector<uint8_t>& imageData, png::image<png::rgba_pixel>& outImage);

				static void loadBlpLayer(png::image<png::rgba_pixel>& outImage, BinaryReader& reader, const files::blp::BlpHeader& header, uint32_t layer);

			public:
				static void convertToPng(png::image<png::rgba_pixel>& outImage, BinaryReader& reader, uint32_t width, uint32_t height);
			};
		}
	}
}
