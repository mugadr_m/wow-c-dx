#include "stdafx.h"
#include "BlpImageLoader.h"
#include "io/files/blp/BlpHeader.h"

namespace wowc {
	namespace io {
		namespace image {
			static const uint32_t ALPHA_LOOKUP1[] = {0x00, 0xFF};
			static const uint32_t ALPHA_LOOKUP4[] = {
				0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
				0xCC, 0xDD, 0xEE, 0xFF
			};

			static void rgb565ToRgb8Array(const uint32_t& input, uint8_t* output) {
				auto r = static_cast<uint32_t>(input & 0x1Fu);
				auto g = static_cast<uint32_t>((input >> 5u) & 0x3Fu);
				auto b = static_cast<uint32_t>((input >> 11u) & 0x1Fu);

				r = (r << 3u) | (r >> 2u);
				g = (g << 2u) | (g >> 4u);
				b = (b << 3u) | (b >> 2u);

				output[0] = static_cast<uint8_t>(b);
				output[1] = static_cast<uint8_t>(g);
				output[2] = static_cast<uint8_t>(r);
			}

			BlpImageLoader::TConvertFunction BlpImageLoader::getBcConvertFunction(const files::blp::BlpImageFormat& format) {
				switch (format) {
				case files::blp::BlpImageFormat::BC1: return &BlpImageLoader::bc1GetBlock;
				case files::blp::BlpImageFormat::BC2: return &BlpImageLoader::bc2GetBlock;
				case files::blp::BlpImageFormat::BC3: return &BlpImageLoader::bc3GetBlock;
				default: throw std::exception("Unrecognized BC format");
				}
			}

			void BlpImageLoader::readBcColors(BinaryReader& stream, RgbDataArray* colors, const bool preMultipliedAlpha,
			                                  const bool use4Colors) {
				const auto color1 = stream.read<uint16_t>();
				const auto color2 = stream.read<uint16_t>();

				rgb565ToRgb8Array(color1, colors[0].data.buffer);
				rgb565ToRgb8Array(color2, colors[1].data.buffer);

				for (auto i = 0u; i < 4; ++i) {
					colors[i].data.buffer[3] = 0xFFu;
				}

				if (use4Colors || color1 > color2) {
					for (auto i = 0u; i < 3u; ++i) {
						colors[3].data.buffer[i] = static_cast<uint8_t>((colors[0].data.buffer[i] + 2u * colors[1].data.buffer[i]) / 3u);
						colors[2].data.buffer[i] = static_cast<uint8_t>((2u * colors[0].data.buffer[i] + colors[1].data.buffer[i]) / 3u);
					}
				} else {
					for (auto i = 0u; i < 3u; ++i) {
						colors[2].data.buffer[i] = static_cast<uint8_t>((colors[0].data.buffer[i] + colors[1].data.buffer[i]) / 2u);
						colors[3].data.buffer[i] = 0;
					}

					if (preMultipliedAlpha) {
						colors[3].data.buffer[3] = 0;
					}
				}
			}

			void BlpImageLoader::bc1GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset) {
				RgbDataArray colors[4];
				readBcColors(stream, colors, true);

				const auto indices = stream.read<uint32_t>();
				for (auto i = 0u; i < 16u; ++i) {
					const auto idx = static_cast<uint8_t>((indices >> (2u * i)) & 3u);
					blockData[blockOffset + i] = colors[idx].data.color;
				}
			}

			void BlpImageLoader::bc2GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset) {
				uint8_t alphaValues[16];
				const auto alpha = stream.read<uint64_t>();
				for (auto i = 0u; i < 16u; ++i) {
					alphaValues[i] = static_cast<uint8_t>(((alpha >> (4u * i)) & 0x0Fu) * 17);
				}

				RgbDataArray colors[4];
				readBcColors(stream, colors, false, true);

				const auto indices = stream.read<uint32_t>();
				for (auto i = 0u; i < 16u; ++i) {
					const auto idx = static_cast<uint8_t>((indices >> (2u * i)) & 3u);
					const auto alphaVal = static_cast<uint32_t>(alphaValues[i]);
					blockData[blockOffset + i] = (colors[idx].data.color & 0x00FFFFFFu) | (alphaVal << 24u);
				}
			}

			void BlpImageLoader::bc3GetBlock(BinaryReader& stream, std::vector<uint32_t>& blockData, const size_t& blockOffset) {
				uint8_t alphaValues[8];
				uint8_t alphaLookup[16];

				const auto alpha1 = static_cast<uint32_t>(stream.read<uint8_t>());
				const auto alpha2 = static_cast<uint32_t>(stream.read<uint8_t>());

				alphaValues[0] = static_cast<uint8_t>(alpha1);
				alphaValues[1] = static_cast<uint8_t>(alpha2);

				if (alpha1 > alpha2) {
					for (auto i = 0u; i < 6u; ++i) {
						alphaValues[i + 2u] = static_cast<uint8_t>(((6u - i) * alpha1 + (1u + i) * alpha2) / 7u);
					}
				} else {
					for (auto i = 0u; i < 4u; ++i) {
						alphaValues[i + 2u] = static_cast<uint8_t>(((4u - i) * alpha1 + (1u + i) * alpha2) / 5u);
					}

					alphaValues[6] = 0;
					alphaValues[7] = 255;
				}

				uint64_t lookupValue = 0;
				stream.read(&lookupValue, 6);

				for (auto i = 0u; i < 16u; ++i) {
					alphaLookup[i] = static_cast<uint8_t>((lookupValue >> (i * 3u)) & 7u);
				}

				RgbDataArray colors[4];
				readBcColors(stream, colors, false);

				const auto indices = stream.read<uint32_t>();
				for (auto i = 0u; i < 16u; ++i) {
					const auto idx = static_cast<uint8_t>((indices >> (2u * i)) & 3u);
					const auto alphaVal = static_cast<uint32_t>(alphaValues[alphaLookup[i]]);
					blockData[blockOffset + i] = (colors[idx].data.color & 0x00FFFFFFu) | (alphaVal << 24u);
				}
			}

			void BlpImageLoader::decompressPaletteArgb8(const uint8_t& alphaDepth, const uint32_t* palette,
			                                            const std::vector<uint8_t>& indices,
			                                            png::image<png::rgba_pixel>& outImage) {
				const auto w = outImage.get_width();
				const auto h = outImage.get_height();

				const auto numEntries = w * h;
				auto& buf = outImage.get_pixbuf();

				std::vector<uint32_t> colorBuffer(numEntries);
				for (auto i = 0u; i < numEntries; ++i) {
					const auto index = indices[i];
					auto color = palette[index];
					color = (color & 0x00FFFFFFu) | 0xFF000000u;
					colorBuffer[i] = color;
				}

				switch (alphaDepth) {
				case 0:
					break;

				case 1: {
					auto colorIndex = 0u;
					for (auto i = 0u; i < (numEntries / 8u); ++i) {
						const auto value = indices[i + numEntries];
						for (auto j = 0u; j < 8; ++j, ++colorIndex) {
							auto& color = colorBuffer[colorIndex];
							color &= 0x00FFFFFF;
							color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
						}
					}

					if ((numEntries % 8) != 0) {
						const auto value = indices[numEntries + numEntries / 8];
						for (auto j = 0u; j < (numEntries % 8); ++j, ++colorIndex) {
							auto& color = colorBuffer[colorIndex];
							color &= 0x00FFFFFF;
							color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
						}
					}

					break;
				}

				case 4: {
					auto colorIndex = 0u;
					for (auto i = 0u; i < (numEntries / 2u); ++i) {
						const auto value = indices[i + numEntries];
						const uint8_t alpha0 = ALPHA_LOOKUP4[value & 0x0Fu];
						const uint8_t alpha1 = ALPHA_LOOKUP4[value >> 4u];
						auto& color1 = colorBuffer[colorIndex++];
						auto& color2 = colorBuffer[colorIndex++];
						color1 = (color1 & 0x00FFFFFFu) | (alpha0 << 24u);
						color2 = (color2 & 0x00FFFFFFu) | (alpha1 << 24u);
					}

					if ((numEntries % 2) != 0) {
						const auto value = indices[numEntries + numEntries / 2];
						const uint8_t alpha = ALPHA_LOOKUP4[value & 0x0Fu];
						auto& color = colorBuffer[colorIndex];
						color = (color & 0x00FFFFFFu) | (alpha << 24u);
					}

					break;
				}

				default: {
					throw std::exception("Invalid alpha depth");
				}
				}

				for (auto i = 0u; i < h; ++i) {
					auto& row = buf.get_row(i);
					memcpy(row.data(), colorBuffer.data() + i * w, w * sizeof(uint32_t));
				}
			}

			void BlpImageLoader::decompressPaletteFastPath(const uint32_t* palette, const std::vector<uint8_t>& indices,
			                                               png::image<png::rgba_pixel>& outImage) {
				const auto w = outImage.get_width();
				const auto h = outImage.get_height();

				auto& buf = outImage.get_pixbuf();
				std::vector<uint32_t> rowBuffer(w);

				const auto numEntries = w * h;
				auto counter = 0u;

				for (auto y = 0u; y < h; ++y) {
					for (auto x = 0u; x < w; ++x) {
						const auto index = indices[counter];
						const auto alpha = indices[numEntries + counter];
						auto color = palette[index];
						color = (color & 0x00FFFFFFu) | (static_cast<uint32_t>(alpha) << 24u);
						rowBuffer[x] = color;
						++counter;
					}

					auto& row = buf.get_row(y);
					memcpy(row.data(), rowBuffer.data(), rowBuffer.size() * sizeof(uint32_t));
				}
			}

			void BlpImageLoader::parseUncompressed(const std::vector<uint8_t>& imageData,
			                                       png::image<png::rgba_pixel>& outImage) {
				const auto rowPitch = outImage.get_width() * 4;
				const auto numRows = outImage.get_height();

				for (auto i = 0u; i < numRows; ++i) {
					auto& row = outImage[i];
					memcpy(row.data(), imageData.data() + i * rowPitch, rowPitch);
				}
			}

			void BlpImageLoader::parseUncompressedPalette(const std::vector<uint8_t>& imageData, uint32_t* palette,
			                                              const uint8_t& alphaDepth,
			                                              png::image<png::rgba_pixel>& outImage) {
				if (alphaDepth == 8) {
					decompressPaletteFastPath(palette, imageData, outImage);
				} else {
					decompressPaletteArgb8(alphaDepth, palette, imageData, outImage);
				}
			}

			void BlpImageLoader::parseCompressed(const files::blp::BlpImageFormat& format, const std::vector<uint8_t>& imageData,
			                                     png::image<png::rgba_pixel>& outImage) {

				const auto w = outImage.get_width();
				const auto h = outImage.get_height();

				auto& buf = outImage.get_pixbuf();

				const auto numBlocks = ((w + 3u) / 4u) * ((h + 3u) / 4u);
				std::vector<uint32_t> blockData(numBlocks * 16u);

				BinaryReader reader(imageData);
				const auto converter = getBcConvertFunction(format);
				for (auto i = 0u; i < numBlocks; ++i) {
					converter(reader, blockData, std::size_t(i * 16));
				}

				std::vector<uint32_t> rowBuffer(w);
				for (auto y = 0u; y < h; ++y) {
					for (auto x = 0u; x < w; ++x) {
						const auto bx = x / 4u;
						const auto by = y / 4u;

						const auto ibx = x % 4u;
						const auto iby = y % 4u;

						const auto blockIndex = by * ((w + 3u) / 4u) + bx;
						const auto innerIndex = iby * 4u + ibx;
						rowBuffer[x] = blockData[blockIndex * 16u + innerIndex];
					}

					auto& row = buf.get_row(y);
					memcpy(row.data(), rowBuffer.data(), rowBuffer.size() * sizeof(uint32_t));
				}
			}

			void BlpImageLoader::loadBlpLayer(png::image<png::rgba_pixel>& outImage, BinaryReader& reader,
			                                  const files::blp::BlpHeader& header, const uint32_t layer) {
				const auto format = header.getFormat();
				if (format == files::blp::BlpImageFormat::UNKNOWN) {
					throw std::exception("Unable to get image format for BLP");
				}

				reader.seek(header.offsets[layer]);
				std::vector<uint8_t> layerData(header.sizes[layer]);
				reader.read(layerData);

				switch (format) {
				case files::blp::BlpImageFormat::RGB: {
					parseUncompressed(layerData, outImage);
					break;
				}

				case files::blp::BlpImageFormat::RGB_PALETTE: {
					uint32_t palette[256]{};
					reader.seek(sizeof(files::blp::BlpHeader));
					reader.read(palette);
					parseUncompressedPalette(layerData, palette, header.alphaDepth, outImage);
					break;
				}

				case files::blp::BlpImageFormat::BC1:
				case files::blp::BlpImageFormat::BC2:
				case files::blp::BlpImageFormat::BC3:
					parseCompressed(format, layerData, outImage);
					break;

				default:
					throw std::exception("Unsupported BLP format");
				}
			}

			void BlpImageLoader::convertToPng(png::image<png::rgba_pixel>& outImage, BinaryReader& reader, uint32_t width,
			                                  uint32_t height) {
				const auto header = reader.read<files::blp::BlpHeader>();

				if (width == static_cast<uint32_t>(-1) || height == static_cast<uint32_t>(-1)) {
					width = header.width;
					height = header.height;
				}

				uint32_t selectedLayer = 0;
				auto curWidth = header.width;
				auto curHeight = header.height;

				for (auto i = 0u; i < 16; ++i, ++selectedLayer) {
					if (header.sizes[i] == 0 || header.offsets[i] == 0) {
						break;
					}

					const auto nextWidth = std::max<uint32_t>(curWidth >> 1, 1);
					const auto nextHeight = std::max<uint32_t>(curHeight >> 1, 1);
					const auto hasNextLayer = i < 15 && header.sizes[i + 1] != 0 && header.offsets[i + 1] != 0;

					if (!hasNextLayer || (curWidth >= width && nextWidth <= width) ||
						(curHeight >= height && nextHeight <= height)) {
						break;
					}

					curWidth = nextWidth;
					curHeight = nextHeight;
				}

				if (header.sizes[selectedLayer] == 0 || header.offsets[selectedLayer] == 0) {
					throw std::exception("Unable to find matching layer");
				}

				outImage = png::image<png::rgba_pixel>(curWidth, curHeight);
				loadBlpLayer(outImage, reader, header, selectedLayer);
			}
		}
	}
}
