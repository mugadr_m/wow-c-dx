#pragma once

#include "utils/Logger.h"
#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace res {
		class ResourceManager {
			static utils::Logger<ResourceManager> mLogger;

			HMODULE mHandle = nullptr;

		public:
			void postConstruct(const cdi::ApplicationContextPtr&);

			std::string getStringResource(int32_t resource) const;
		};
	}
}
