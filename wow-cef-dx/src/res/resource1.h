﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resources.rc
//
#define IDR_CEF_SHADER_VERTEX           101
#define IDR_CEF_SHADER_PIXEL            102

#define IDI_WINDOW_ICON                 103

#define IDR_TERRAIN_SHADER_VERTEX       104
#define IDR_TERRAIN_SHADER_PIXEL        105

#define IDR_SKY_BOX_SHADER_VERTEX       106
#define IDR_SKY_BOX_SHADER_PIXEL        107

#define IDR_LIQUID_SHADER_VERTEX        108
#define IDR_LIQUID_SHADER_PIXEL         109

#define IDR_M2_SHADER_VERTEX            110
#define IDR_M2_SHADER_PIXEL             111

#define IDR_TERRAIN_GRID_SHADER_VERTEX  112
#define IDR_TERRAIN_GRID_SHADER_PIXEL   113

#define TEXTFILE                        256
#define IDI_EXE_ICON                    1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
