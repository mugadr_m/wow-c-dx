#include "stdafx.h"
#include "ResourceManager.h"
#include "resource.h"

namespace wowc {
	namespace res {
		utils::Logger<ResourceManager> ResourceManager::mLogger;

		void ResourceManager::postConstruct(const cdi::ApplicationContextPtr&) {
			mHandle = GetModuleHandle(nullptr);
		}

		std::string ResourceManager::getStringResource(const int32_t resource) const {
			const auto rc = FindResource(mHandle, MAKEINTRESOURCE(resource), MAKEINTRESOURCE(TEXTFILE));
			if(rc == nullptr) {
				mLogger.warn("Resource not found: ", resource);
				throw std::exception("Resource not found");
			}

			const auto rcData = LoadResource(mHandle, rc);
			if(rcData == nullptr) {
				mLogger.warn("Resource not found: ", resource);
				throw std::exception("Resource not found");
			}

			const auto size = SizeofResource(mHandle, rc);
			const auto* ptrStart = reinterpret_cast<const char*>(LockResource(rcData));
			return std::string(ptrStart, ptrStart + size);
		}
	}
}
