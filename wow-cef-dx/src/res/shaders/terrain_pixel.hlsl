struct PixelInput {
	float4 position: SV_Position;
	float2 texCoord : TEXCOORD0;
	float2 texCoordAlpha: TEXCOORD1;
	float3 normal: NORMAL0;
	float4 mccv: COLOR0;
	float4 mclv: COLOR1;
	float3 viewVector : COLOR2;
	float3 worldPosition : COLOR3;
};

struct PixelOutput {
	float4 color : SV_TARGET;
};

float3 sunDirection = float3(1, 1, -1);

struct LightConstantData {
	float3 ambient;
	float3 diffuse;
};

struct SpecularData {
	float4 l0;
	float4 l1;
	float4 l2;
	float4 l3;
};

cbuffer LightBufferData : register(b1) {
	float4 ambientLight;
	float4 diffuseLight;
	float4 oceanNear;
	float4 oceanFar;
	float4 riverNear;
	float4 riverFar;
};

cbuffer RenderSettingsData : register(b2) {
	float specularOverride;
	float specularIntensity;
	float2 padding;
};

cbuffer TextureData : register(b3) {
	float4 texScales;
	float4 heightOffsets;
	float4 heightScales;
	float4 specularFactors;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
	float4 mousePosition;
	float4 brushParams;
}

Texture2D<float4> alphaTexture : register(t0);

Texture2D<float4> layer0Texture : register(t1);
Texture2D<float4> layer1Texture : register(t2);
Texture2D<float4> layer2Texture : register(t3);
Texture2D<float4> layer3Texture : register(t4);

Texture2D<float4> height0Texture : register(t5);
Texture2D<float4> height1Texture : register(t6);
Texture2D<float4> height2Texture : register(t7);
Texture2D<float4> height3Texture : register(t8);

Texture2D<float4> specular0Texture : register(t9);
Texture2D<float4> specular1Texture : register(t10);
Texture2D<float4> specular2Texture : register(t11);
Texture2D<float4> specular3Texture : register(t12);

SamplerState alphaSampler : register(s0);
SamplerState colorSampler : register(s1);

float4 sinusInterpolate(float4 src, float4 dst, float pct) {
	float sinval = sin(pct * 3.1415926 / 2.0f);
	return sinval * dst + (1 - sinval) * src;
}


float4 applyBrush(float4 color, float3 worldPos) {
	float3 dirVec = worldPos - mousePosition.xyz;
	float dsq = dot(dirVec.xy, dirVec.xy);
	float dz = dirVec.z * dirVec.z;

	float innerRadius = brushParams.x * brushParams.x;
	float outerRadius = brushParams.y * brushParams.y;

	float fac = 1.0;
	float brushRotation = 0.0;
	float radius = outerRadius;

	// Is part of the inner circle?
	if (dsq < innerRadius && innerRadius * 0.95 < dsq) {
		brushRotation = 1.0;
		radius = innerRadius;
	}
	// Is part of the outer circle?
	else if (dsq < outerRadius && outerRadius * 0.95 < dsq) {
		brushRotation = -1.0;
		radius = outerRadius;
	}
	// Not part of anything
	else {
		fac = 0.0;
	}

	// Antialiasing for the circle borders
	float antiAliasSize = radius * 0.01;
	fac *= clamp((dsq - radius * 0.95) / antiAliasSize, 0, 1);
	fac *= clamp((radius - dsq) / antiAliasSize, 0, 1);

	float angle = atan2(dirVec.y, dirVec.x) + 3.1415926 * brushRotation;
	float brushTime = brushParams.z * brushRotation * 10;
	angle = fmod(abs(degrees(angle) + brushTime), 36.0f);

	// Antialiasing between the circle segments
	fac *= clamp((18.0 - angle) / 0.4, 0, 1);
	fac *= clamp((angle - 0.4) / 0.4, 0, 1);

	fac *= clamp(1 - dz / 2000, 0, 1);

	float4 brushColor = float4(1, 1, 1, 1);
	brushColor.rgb -= color.rgb;
	return sinusInterpolate(color, brushColor, fac);
}


LightConstantData buildConstantLighting(float3 normal) {
	LightConstantData ret = (LightConstantData)0;
	float3 lightDir = normalize(float3(1, 1, -1));
	normal = normalize(normal);
	float light = dot(normal, -lightDir);
	if (light < 0.0)
		light = 0.0;
	if (light > 0.5)
		light = 0.5 + (light - 0.5) * 0.65;

	ret.diffuse = diffuseLight.bgr * light;
	ret.ambient = ambientLight.bgr;

	return ret;
}

SpecularData getSpecularLighting(float3 normal, float3 viewVector, float2 texCoord) {
	float4 s0 = specular0Texture.Sample(colorSampler, texCoord / texScales.x);
	float4 s1 = specular1Texture.Sample(colorSampler, texCoord / texScales.y);
	float4 s2 = specular2Texture.Sample(colorSampler, texCoord / texScales.z);
	float4 s3 = specular3Texture.Sample(colorSampler, texCoord / texScales.w);

	float3 lightDir = -normalize(float3(1, 1, -1));
	normal = normalize(normal);

	float3 r = normalize(2 * dot(lightDir, normal) * normal - lightDir);
	float sf0 = pow(saturate(dot(r, viewVector)), specularIntensity);
	float sf1 = pow(saturate(dot(r, viewVector)), specularIntensity);
	float sf2 = pow(saturate(dot(r, viewVector)), specularIntensity);
	float sf3 = pow(saturate(dot(r, viewVector)), specularIntensity);

	SpecularData ret = (SpecularData)0;

	ret.l0 = float4(saturate(sf0 * s0.a * s0.rgb * specularFactors.r), 0.0f) * specularOverride;
	ret.l1 = float4(saturate(sf1 * s1.a * s1.rgb * specularFactors.g), 0.0f) * specularOverride;
	ret.l2 = float4(saturate(sf2 * s2.a * s2.rgb * specularFactors.b), 0.0f) * specularOverride;
	ret.l3 = float4(saturate(sf3 * s3.a * s3.rgb * specularFactors.a), 0.0f) * specularOverride;

	return ret;
}

PixelOutput main(PixelInput input) {
	LightConstantData data = buildConstantLighting(input.normal);
	SpecularData specularLight = getSpecularLighting(input.normal, input.viewVector, input.texCoord);

	PixelOutput ret = (PixelOutput)0;
	float h0 = height0Texture.Sample(colorSampler, input.texCoord / texScales.x).a;
	float h1 = height1Texture.Sample(colorSampler, input.texCoord / texScales.y).a;
	float h2 = height2Texture.Sample(colorSampler, input.texCoord / texScales.z).a;
	float h3 = height3Texture.Sample(colorSampler, input.texCoord / texScales.w).a;

	float4 color0 = layer0Texture.Sample(colorSampler, input.texCoord / texScales.x);
	float4 color1 = layer1Texture.Sample(colorSampler, input.texCoord / texScales.y);
	float4 color2 = layer2Texture.Sample(colorSampler, input.texCoord / texScales.z);
	float4 color3 = layer3Texture.Sample(colorSampler, input.texCoord / texScales.w);

	float4 alphaData = alphaTexture.Sample(alphaSampler, input.texCoordAlpha);

	alphaData.z = max(1.0 - alphaData.y - alphaData.x - alphaData.w, 0.0);

	float4 layerPct = float4(
		alphaData.z * (h0 * heightScales.x + heightOffsets.x),
		alphaData.y * (h1 * heightScales.y + heightOffsets.y),
		alphaData.x * (h2 * heightScales.z + heightOffsets.z),
		alphaData.w * (h3 * heightScales.w + heightOffsets.w)
	);

	float layerPctMaxVal = max(max(layerPct.x, layerPct.y), max(layerPct.z, layerPct.w));
	float4 layerPctMax = float4(layerPctMaxVal, layerPctMaxVal, layerPctMaxVal, layerPctMaxVal);
	layerPct = layerPct * (float4(1, 1, 1, 1) - saturate(layerPctMax - layerPct));
	float layerPctSum = layerPct.x + layerPct.y + layerPct.z + layerPct.w;
	layerPct = layerPct / layerPctSum;

	float4 baseColor = saturate(color0 + specularLight.l0) * layerPct.x;
	baseColor += saturate(color1 + specularLight.l1) * layerPct.y;
	baseColor += saturate(color2 + specularLight.l2) * layerPct.z;
	baseColor += saturate(color3 + specularLight.l3) * layerPct.w;

	float4 finalColor = baseColor;
	finalColor.rgb *= 2 * input.mccv.bgr;
	finalColor.rgb += baseColor.rgb * input.mclv.bgr;
	finalColor.rgb *= data.ambient + data.diffuse;

	finalColor = applyBrush(finalColor, input.worldPosition);

	ret.color = saturate(finalColor);
	return ret;
}