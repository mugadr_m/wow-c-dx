struct VertexInput {
	float3 position : POSITION0;
	float2 texCoord : TEXCOORD0;
	float waterDepth : TEXCOORD1;
};

struct VertexOutput {
	float4 position : SV_POSITION;
	float2 texCoord: TEXCOORD0;
	float waterDepth : TEXCOORD2;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
}

cbuffer LiquidParams : register(b3) {
	row_major float4x4 matTransform;
	float4 lightData;
}

VertexOutput main( VertexInput input ) {
	VertexOutput output = (VertexOutput)0;
	float4 position = float4(input.position, 1.0);
	position = mul(position, matTransform);
	position = mul(position, matView);
	position = mul(position, matProjection);

	output.position = position;
	output.texCoord = input.texCoord;
	output.waterDepth = input.waterDepth;

	return output;
}