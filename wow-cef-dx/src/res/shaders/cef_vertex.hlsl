struct VSInput {
	float4 position : POSITION0;
	float2 texCoord: TEXCOORD0;
};

struct VSOutput {
	float4 position : SV_POSITION;
	float2 texCoord: TEXCOORD0;
};

VSOutput main(VSInput input) {
	VSOutput output = (VSOutput)0;
	output.position = input.position;
	output.texCoord = input.texCoord;
	return output;
}