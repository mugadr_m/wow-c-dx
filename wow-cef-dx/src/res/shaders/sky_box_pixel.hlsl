struct PixelInput {
	float4 position: SV_POSITION;
	float2 texCoord: TEXCOORD0;
};

struct PixelOutput {
	float4 color: SV_TARGET;
};

Texture2D<float4> skyTexture : register(t0);
SamplerState skySampler : register(s0);

PixelOutput main(PixelInput input) {
	PixelOutput output = (PixelOutput)0;
	output.color = skyTexture.Sample(skySampler, input.texCoord);

	return output;
}