Texture2D texture0;
SamplerState sampler0;

struct PSInput {
	float4 position : SV_POSITION;
	float2 texCoord : TEXCOORD0;
};

struct PSOutput {
	float4 color : SV_TARGET;
};

PSOutput main(PSInput input) {
	PSOutput output = (PSOutput)0;
	output.color = texture0.Sample(sampler0, float2(input.texCoord.x, 1.0 - input.texCoord.y));
	return output;
}