struct VertexInput {
	float3 position: POSITION0;
	float2 texCoord: TEXCOORD0;
	float2 texCoordAlpha: TEXCOORD1;
	float3 normal: NORMAL0;
	float4 mccv: COLOR0;
	float4 mclv: COLOR1;
};

struct VertexOutput {
	float4 position: SV_Position;
	float2 texCoord: TEXCOORD0;
	float2 texCoordAlpha: TEXCOORD1;
	float3 normal: NORMAL0;
	float4 mccv: COLOR0;
	float4 mclv: COLOR1;
	float3 viewVector : COLOR2;
	float3 worldPosition : COLOR3;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
	float4 mousePosition;
	float4 brushParams;
}

VertexOutput main( VertexInput input ) {
	VertexOutput output = (VertexOutput)0;
	float4 position = float4(input.position, 1.0);
	output.worldPosition = position.xyz / position.w;
	position = mul(position, matView);
	position = mul(position, matProjection);

	output.position = position;
	output.normal = input.normal;
	output.mccv = input.mccv;
	output.mclv = input.mclv;
	output.texCoord = input.texCoord;
	output.texCoordAlpha = input.texCoordAlpha;
	output.viewVector = normalize(cameraPosition.xyz - input.position);
	return output;
}