struct VertexInput {
	float3 position: POSITION0;
	float2 texCoord: TEXCOORD0;
};

struct VertexOutput {
	float4 position: SV_POSITION;
	float2 texCoord: TEXCOORD0;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
}

VertexOutput main(VertexInput input) {
	VertexOutput output = (VertexOutput)0;

	float4 position = float4(input.position + cameraPosition.xyz, 1.0);
	position = mul(position, matView);
	position = mul(position, matProjection);

	output.position = position;
	output.texCoord = input.texCoord;

	return output;
}