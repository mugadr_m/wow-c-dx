struct PixelInput {
	float4 position : SV_POSITION;
	float3 normal : NORMAL0;
};

float3 sunDirection = float3(1, 1, -1);

struct LightConstantData {
	float3 ambient;
	float3 diffuse;
};

cbuffer LightBufferData : register(b1) {
	float4 ambientLight;
	float4 diffuseLight;
	float4 oceanNear;
	float4 oceanFar;
	float4 riverNear;
	float4 riverFar;
};

LightConstantData buildConstantLighting(float3 normal) {
	LightConstantData ret = (LightConstantData)0;
	float3 lightDir = normalize(float3(1, 1, -1));
	normal = normalize(normal);
	float light = dot(normal, -lightDir);
	if (light < 0.0)
		light = 0.0;
	if (light > 0.5)
		light = 0.5 + (light - 0.5) * 0.65;

	ret.diffuse = diffuseLight.bgr * light;
	ret.ambient = ambientLight.bgr;

	return ret;
}

float4 main(PixelInput input) : SV_TARGET {
	float4 baseColor = float4(1.0, 1.0, 1.0, 1.0);
	
	LightConstantData lightData = buildConstantLighting(input.normal);
	baseColor.rgb *= saturate(lightData.ambient + lightData.diffuse);

	return baseColor;
}