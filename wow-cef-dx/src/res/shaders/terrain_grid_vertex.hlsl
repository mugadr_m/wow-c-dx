struct VertexInput {
	float3 position: POSITION0;
	float4 color: COLOR0;
};

struct VertexOutput {
	float4 position : SV_Position;
	float4 color : COLOR0;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
	float4 mousePosition;
	float4 brushParams;
}

VertexOutput main(VertexInput input) {
	VertexOutput output = (VertexOutput)0;

	float4 position = float4(input.position, 1.0f);
	position = mul(position, matView);
	position = mul(position, matProjection);

	output.position = position;
	output.color = input.color;
	return output;
}