struct VertexInput {
	float3 position : POSITION0;
	int4 boneIndices : BLENDINDEX0;
	float4 boneWeights : BLENDWEIGHT0;
	float3 normal : NORMAL0;
	float2 texCoord0 : TEXCOORD0;
	float2 texCoord1 : TEXCOORD1;

	float4 mat0 : TEXCOORD2;
	float4 mat1 : TEXCOORD3;
	float4 mat2 : TEXCOORD4;
	float4 mat3 : TEXCOORD5;
};

struct VertexOutput {
	float4 position : SV_POSITION;
	float3 normal : NORMAL0;
};

cbuffer GlobalParams : register(b0) {
	row_major float4x4 matView;
	row_major float4x4 matProjection;
	float4 cameraPosition;
	float4 mousePosition;
	float4 brushParams;
}

cbuffer BoneData : register(b4) {
	row_major float4x4 boneMatrices[256];
};

VertexOutput main( VertexInput input ) {
	VertexOutput output = (VertexOutput)0;

	float4x4 boneMatrix = boneMatrices[input.boneIndices.x] * input.boneWeights.x;
	boneMatrix += boneMatrices[input.boneIndices.y] * input.boneWeights.y;
	boneMatrix += boneMatrices[input.boneIndices.z] * input.boneWeights.z;
	boneMatrix += boneMatrices[input.boneIndices.w] * input.boneWeights.w;

	float4x4 matTransform = float4x4(
		input.mat0,
		input.mat1,
		input.mat2,
		input.mat3
	);

	float4x4 worldMatrix = mul(boneMatrix, matTransform);

	float4 position = float4(input.position, 1.0);
	position = mul(position, worldMatrix);
	position = mul(position, matView);
	position = mul(position, matProjection);

	output.position = position;
	output.normal = mul(input.normal, (float3x3) worldMatrix);

	return output;
}