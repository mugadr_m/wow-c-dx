struct PixelInput {
	float4 position : SV_POSITION;
	float2 texCoord: TEXCOORD0;
	float waterDepth : TEXCOORD2;
};

cbuffer LightBufferData : register(b1) {
	float4 ambientLight;
	float4 diffuseLight;
	float4 oceanNear;
	float4 oceanFar;
	float4 riverNear;
	float4 riverFar;
	float4 liquidAlpha;
};

cbuffer LiquidParams : register(b3) {
	row_major float4x4 matTransform;
	float4 lightData;
};

Texture2D<float4> waterTexture : register(t0);
SamplerState waterSampler : register(s0);

float4 main(PixelInput input) : SV_TARGET {
	float4 texColor = waterTexture.Sample(waterSampler, input.texCoord);
	float4 oceanColor = lerp(oceanNear, oceanFar, input.waterDepth).bgra;
	float4 riverColor = lerp(riverNear, riverFar, input.waterDepth).bgra;
	float riverAlpha = lerp(liquidAlpha.x, liquidAlpha.y, input.waterDepth);
	float oceanAlpha = lerp(liquidAlpha.z, liquidAlpha.w, input.waterDepth);

	float4 waterColor = oceanColor * lightData.x + riverColor * lightData.y;
	float4 targetColor = saturate(2 * texColor + waterColor);
	targetColor.a = saturate(lightData.x * oceanAlpha + lightData.y * riverAlpha);
	return targetColor;
}