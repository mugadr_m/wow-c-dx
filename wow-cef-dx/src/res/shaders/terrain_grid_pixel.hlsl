struct PixelInput {
	float4 position : SV_Position;
	float4 color : COLOR0;
};

struct PixelOutput {
	float4 color : SV_Target;
};

PixelOutput main(PixelInput input) {
	PixelOutput output = (PixelOutput)0;
	output.color = input.color;
	return output;
}