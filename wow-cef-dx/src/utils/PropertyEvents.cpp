#include "stdafx.h"
#include "PropertyEvents.h"

namespace wowc {
    namespace utils {
		Logger<GetPropertyEvent> GetPropertyEvent::mLogger;
		Logger<SetPropertyEvent> SetPropertyEvent::mLogger;

        rapidjson::Document GetPropertyEvent::getResponseJson() const {
			rapidjson::Document document;
			auto& allocator = document.GetAllocator();

			auto& rootObj = document.SetObject();
			rootObj.AddMember("value", rapidjson::Value(mValue.c_str(), allocator), allocator);
			return document;
        }

        GetPropertyEvent GetPropertyEvent::fromJson(const rapidjson::Document& document) {
			const auto keyItr = document.FindMember("key");

            if(keyItr == document.MemberEnd()) {
				mLogger.error("Event is missing key member");
				throw std::exception("Invalid event, missing key");
            }

			const std::string key(keyItr->value.GetString(), keyItr->value.GetString() + keyItr->value.GetStringLength());
			return GetPropertyEvent{ key };
        }

        SetPropertyEvent SetPropertyEvent::fromJson(const rapidjson::Document& document) {
			const auto keyItr = document.FindMember("key");
			const auto valueItr = document.FindMember("value");

			if (keyItr == document.MemberEnd() || valueItr == document.MemberEnd()) {
				mLogger.error("Event is missing key or value member");
				throw std::exception("Invalid event, missing key or value");
			}

			const std::string key(keyItr->value.GetString(), keyItr->value.GetString() + keyItr->value.GetStringLength());
			const std::string value(valueItr->value.GetString(), valueItr->value.GetString() + valueItr->value.GetStringLength());

			return { key, value };
        }
    }
}