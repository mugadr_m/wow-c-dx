#include "stdafx.h"
#include "String.h"
#include <locale>
#include <codecvt>
#include <boost/algorithm/hex.hpp>
#include <boost/algorithm/string.hpp>

namespace wowc {
	namespace utils {
		namespace detail {
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		}

		std::string String::toMultibyte(const std::wstring& unicode) {
			return detail::converter.to_bytes(unicode);
		}

		std::wstring String::toUnicode(const std::string& multibyte) {
			return detail::converter.from_bytes(multibyte);
		}

		std::vector<std::string> String::split(const std::string& string, char delim) {
			std::vector<std::string> ret;
			std::stringstream strm(string);
			std::string token;
			while(std::getline(strm, token, delim)) {
				ret.push_back(token);
			}

			return ret;
		}

		bool isNotWhitespace(char c) {
			return !isspace(c);
		}

		std::string String::trimRight(const std::string& string) {
			auto idxNonWs = std::find_if(string.rbegin(), string.rend(), isNotWhitespace);
			return std::string(string.begin(), idxNonWs.base());
		}

		std::string String::trim(const std::string& string) {
			return trimRight(trimLeft(string));
		}

		std::string String::toLower(const std::string& string) {
			return boost::algorithm::to_lower_copy(string);
		}

		std::string String::toUpper(const std::string& string) {
			return boost::algorithm::to_upper_copy(string);
		}

		std::string String::replaceAll(const std::string& string, const std::string& old, const std::string& replace) {
			return boost::algorithm::replace_all_copy(string, old, replace);
		}

		std::string String::trimLeft(const std::string& string) {
			const auto idxNonWs = std::find_if(string.begin(), string.end(), isNotWhitespace);
			return std::string(idxNonWs, string.end());
		}
	}
}
