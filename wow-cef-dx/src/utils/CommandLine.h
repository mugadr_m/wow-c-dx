#pragma once

#include <cef_command_line.h>
#include "cdi/ApplicationContext.h"

namespace wowc {
	namespace utils {
		class CommandLine {
			CefRefPtr<CefCommandLine> mCommandLine;

		public:
			void postConstruct(const cdi::ApplicationContextPtr& context);

			std::string getOption(const std::string& name, const std::string& defaultValue = std::string()) const;
		};
	}
}
