#include "stdafx.h"
#include "PropertyStore.h"
#define SECURITY_WIN32
#include "security.h"
#include <regex>
#include <fstream>
#include "String.h"
#include <boost/algorithm/string/join.hpp>

namespace wowc {
    namespace utils {

        AbstractPropertyBinding::
        AbstractPropertyBinding(PropertyStorePtr store, std::size_t key) : mPropertyStore(std::move(store)),
                                                                           mPropertyKey(key),
                                                                           mHasChanged(true) {

        }

        std::string PropertyStore::getStorageFileName() const {
            std::stringstream strm;
            strm << "wow-cef-dx." << mUserNameToken << ".properties";
            return strm.str();
        }

        bool PropertyStore::loadExisting() {
            std::ifstream inputFile(getStorageFileName());
            if (!inputFile.is_open()) {
                mLogger.info("Unable to open property file name, will use default values");
                return false;
            }

            std::string line;
            while (std::getline(inputFile, line)) {
                auto parts = String::split(line, ':');
                if (parts.size() < 3) {
                    mLogger.warn("Unable to parse line in property file, ignoring. Line was: ", line);
                    continue;
                }

                const auto name = boost::lexical_cast<std::size_t>(parts[0]);
                const auto type = boost::lexical_cast<int>(parts[1]);
                parts.erase(parts.begin(), parts.begin() + 2);
                const auto value = boost::algorithm::join(parts, ":");
                switch (type) {
                case typeVal(PropertyType::INT): {
                    defineProperty(name, boost::lexical_cast<int>(value));
                    break;
                }

                case typeVal(PropertyType::FLOAT): {
                    defineProperty(name, boost::lexical_cast<float>(value));
                    break;
                }

                case typeVal(PropertyType::STRING): {
                    defineProperty(name, value);
                    break;
                }

                case typeVal(PropertyType::BOOL): {
                    defineProperty(name, boost::lexical_cast<bool>(value));
                    break;
                }

                default: {
                    mLogger.warn("Skipping unknown property. Type ", type, " is not known");
                    break;
                }
                }
            }

            return true;
        }

        void PropertyStore::loadDefault() {
            defineProperty("terrain.editing.innerRadius", 100.0f);
            defineProperty("terrain.editing.outerRadius", 200.0f);
            defineProperty("terrain.editing.intensity", 5.0f);
            defineProperty("terrain.editing.vertices.show", false);
            save();
        }

        void PropertyStore::loadUserName() {
            char userNameBuffer[2048]{0};
            ULONG userBufferLen = sizeof userNameBuffer;

            if (GetUserNameEx(NameSamCompatible, userNameBuffer, &userBufferLen) == FALSE) {
                auto shortUserBufferLen = static_cast<DWORD>(userBufferLen);
                if (GetUserName(userNameBuffer, &shortUserBufferLen) == FALSE) {
                    strcpy_s(userNameBuffer, "unknown");
                }
            }

            std::string userName = userNameBuffer;
            const std::regex dotRegex{"\\."};
            const std::regex invalidCharRegex{"[^A-Za-z0-9_-]"};

            userName = std::regex_replace(userName, dotRegex, "_");
            userName = std::regex_replace(userName, invalidCharRegex, "");

            mUserNameToken = userName;
        }

        PropertyStore::PropertyType PropertyStore::getPropertyType(const std::type_info* typeInfo) const {
            if (typeInfo == &typeid(int)) {
                return PropertyType::INT;
            }

            if (typeInfo == &typeid(float)) {
                return PropertyType::FLOAT;
            }

            if (typeInfo == &typeid(std::string)) {
                return PropertyType::STRING;
            }

            if (typeInfo == &typeid(bool)) {
                return PropertyType::BOOL;
            }

            mLogger.error("Unable to map the type, ", typeInfo->name(), " to a known property type");
            throw std::exception("Invalid property type");
        }

        std::string PropertyStore::toString(const std::type_info* typeInfo, const boost::any& value) const {
            if (typeInfo == &typeid(int)) {
                return std::to_string(boost::any_cast<int>(value));
            }

            if (typeInfo == &typeid(float)) {
                return std::to_string(boost::any_cast<float>(value));
            }

            if (typeInfo == &typeid(std::string)) {
                return boost::any_cast<std::string>(value);
            }

            if (typeInfo == &typeid(bool)) {
                return boost::lexical_cast<std::string>(boost::any_cast<bool>(value));
            }

            mLogger.error("Unable to map the type, ", typeInfo->name(), " to string");
            throw std::exception("Invalid property type");
        }

        void PropertyStore::propertyChanged(std::size_t key) {
            std::lock_guard<std::recursive_mutex> l(mBindingLock);
            const auto bindingPair = mBindings.equal_range(key);
            if (std::distance(bindingPair.first, bindingPair.second) <= 0) {
                return;
            }

            for (auto itr = bindingPair.first; itr != bindingPair.second; ++itr) {
                itr->second->setHasChanged(true);
            }
        }

        void PropertyStore::handleGetPropertyEvent(GetPropertyEvent& event) {
            const auto key = mHasher(event.getKey());
            const auto& property = mProperties.at(key);
            event.setValue(toString(mTypeInfo.at(key), property));
        }

        void PropertyStore::handleSetPropertyEvent(SetPropertyEvent& event) {
            writeProperty(event.getKey(), event.getValue());
        }

        void PropertyStore::postConstruct(const cdi::ApplicationContextPtr& context) {
            loadUserName();
            if (!loadExisting()) {
                loadDefault();
            }

            context->registerEvent<GetPropertyEvent>(std::bind(&PropertyStore::handleGetPropertyEvent, this,
                                                               std::placeholders::_1));
            context->registerEvent<SetPropertyEvent>(std::bind(&PropertyStore::handleSetPropertyEvent, this,
                                                               std::placeholders::_1));
        }

        void PropertyStore::initUiEvents() {
            const auto eventManager = cdi::ApplicationContext::instance()->produce<ui::events::EventManager>();
            eventManager->registerEvent<GetPropertyEvent>();
            eventManager->registerEvent<SetPropertyEvent>();
        }

        void PropertyStore::save() {
            std::ofstream outputFile(getStorageFileName());
            if (!outputFile.is_open()) {
                mLogger.error("Unable to save properties: Unable to open output file");
            }

            for (const auto& propertyPair : mProperties) {
                const auto metaData = mTypeInfo.at(propertyPair.first);
                outputFile << propertyPair.first << ":" << typeVal(getPropertyType(metaData)) << ":" << toString(
                    metaData, propertyPair.second) << std::endl;
            }
        }
    }
}
