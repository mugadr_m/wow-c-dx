#pragma once

namespace wowc {
	namespace utils {
		template<typename T>
		class Logger {
			std::string mTypeName;

			static void appendTimestamp(std::ostringstream& stream) {
				time_t timestamp = 0;
				time(&timestamp);
				tm timeStruct{};
				localtime_s(&timeStruct, &timestamp);
				stream << std::put_time(&timeStruct, "%FT%TZ");
			}

			template<typename Cur, typename... Rem>
			static void appendArg(std::ostream& strm, const Cur& arg, const Rem&... rem) {
				strm << arg;
				appendArg(strm, rem...);
			}

			template<typename Cur>
			static void appendArg(std::ostream& strm, const Cur& arg) {
				strm << arg;
			}

			static void appendArg(std::ostream& strm) {
				
			}

			template<typename... Args>
			void printMessage(const std::string& header, const Args&... args) const {
				const auto threadId = std::this_thread::get_id();
				std::ostringstream strm;
				appendTimestamp(strm);
				strm << " " << header << " ";
				strm << "[" << std::hex << threadId << "] " << std::dec;
				strm << mTypeName << " ";
				appendArg(strm, args...);

				std::cout << strm.str() << std::endl;
			}

		public:
			Logger() : mTypeName(typeid(T).name()) {
				const auto spaceIndex = mTypeName.find(' ');
				if(spaceIndex != std::string::npos) {
					mTypeName = mTypeName.substr(spaceIndex + 1);
				}
			}

			template<typename... Args>
			void debug(const Args&... args) const {
				printMessage("\033[38;5;69mDEBUG\033[0m", args...);
			}

			template<typename... Args>
			void info(const Args&... args) const {
				printMessage("\033[38;5;118mINFO \033[0m", args...);
			}

			template<typename... Args>
			void warn(const Args&... args) const {
				printMessage("\033[38;5;205mWARN \033[0m", args...);
			}

			template<typename... Args>
			void error(const Args&... args) const {
				printMessage("\033[38;5;197mERROR\033[0m", args...);
			}
		};
	}
}