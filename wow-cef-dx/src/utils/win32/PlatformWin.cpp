#include "stdafx.h"
#include "PlatformWin.h"
#include <vector>
#include <DirectXMath.h>
#include <atlbase.h>
#include <ShObjIdl.h>
#include "cdi/ApplicationContext.h"
#include "gx/Window.h"
#include "utils/String.h"

namespace wowc {
	namespace utils {
		namespace win32 {
			namespace detail {
				std::string getProcessorArchitecture(const SYSTEM_INFO& sysInfo) {
					switch (sysInfo.wProcessorArchitecture) {
					case PROCESSOR_ARCHITECTURE_AMD64:
						return "x64";

					case PROCESSOR_ARCHITECTURE_ARM:
						return "ARM";

					case PROCESSOR_ARCHITECTURE_IA64:
						return "Itanium";

					case PROCESSOR_ARCHITECTURE_INTEL:
						return "x86";

					default:
						return "Unknown";
					}
				}
			}

			Logger<PlatformWin> PlatformWin::mLogger;

			void PlatformWin::initialize() {
				SYSTEM_INFO sysInfo{};
				GetSystemInfo(&sysInfo);

				mLogger.info("Initializing windows platform");
				mLogger.debug("Processor Count: ", sysInfo.dwNumberOfProcessors);
				mLogger.debug("Architecture:    ", detail::getProcessorArchitecture(sysInfo));
				mLogger.debug("Version:         ", getWindowsVersion());
				const auto hResult = CoInitialize(nullptr);
				if (FAILED(hResult)) {
					throw std::exception("Failed to call CoInitialize");
				}

			    const auto xmSupport = DirectX::XMVerifyCPUSupport();
				mLogger.debug("DirectX Math:    ", xmSupport ? "supported" : "not supported");
                if(!xmSupport) {
					throw std::exception("DirectX math must be supported");
                }
			}

		    bool PlatformWin::browseForFolder(std::string& path) {
				CComPtr<IFileOpenDialog> fileOpenDialog;
				auto res = CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_ALL, IID_PPV_ARGS(&fileOpenDialog));
                if(FAILED(res)) {
					mLogger.error("Unable to create instance of CLSID_FileOpenDialog for IFileOpenDialog");
					throw std::exception("Unable to create FileOpenDialog");
                }

				FILEOPENDIALOGOPTIONS options{};
				options |= FOS_FILEMUSTEXIST | FOS_PATHMUSTEXIST | FOS_PICKFOLDERS;
				res = fileOpenDialog->SetOptions(options);
                if(FAILED(res)) {
					mLogger.error("Error setting options: File must exist, path must exit, select folders only");
					throw std::exception("Error settings options");
                }

				const auto window = cdi::ApplicationContext::instance()->produce<gx::Window>();
				res = fileOpenDialog->Show(window != nullptr ? window->getHandle() : nullptr);
                if(FAILED(res)) {
					mLogger.info("User cancelled folder select dialog");
					return false; 
                }

				CComPtr<IShellItemArray> results;
				res = fileOpenDialog->GetResults(&results);
                if(FAILED(res)) {
					mLogger.info("Error getting result from folder select dialog");
					return false;
                }

				CComPtr<IShellItem> item;
				res = results->GetItemAt(0, &item);
                if(FAILED(res)) {
					mLogger.info("Unable to get a result from IFileOpenDialog");
					return false;
                }

				LPWSTR absolutePath;
				res = item->GetDisplayName(SIGDN_FILESYSPATH, &absolutePath);
                if(FAILED(res)) {
					mLogger.info("File cannot be converted to an absolute path");
					return false;
                }

				const auto len = wcslen(absolutePath);
		        const std::wstring stringName(absolutePath, absolutePath + len);
				LocalFree(absolutePath);

				path = String::toMultibyte(stringName);
				return true;
		    }

		    std::string PlatformWin::getWindowsVersion() {
				char moduleName[2048]{};
				const auto kernel = GetModuleHandle("kernel32.dll");
				if (kernel == nullptr) {
					return "Unknown";
				}

				GetModuleFileName(kernel, moduleName, 2048);
				moduleName[2047] = 0;
				const auto size = GetFileVersionInfoSize(moduleName, nullptr);
				if (size == 0) {
					return "Unknown";
				}

				std::vector<uint8_t> buffer(size);
				if (!GetFileVersionInfo(moduleName, 0, size, buffer.data())) {
					return "Unknown";
				}

				WORD* translation = nullptr;
				UINT sizeTranslate = 0;
				if (!VerQueryValue(buffer.data(), TEXT("\\VarFileInfo\\Translation"), reinterpret_cast<LPVOID*>(&translation),
				                   &sizeTranslate) || sizeTranslate < 4) {
					return "Unknown";
				}

				const auto language = translation[0];
				const auto codePage = translation[1];

				std::stringstream strm;
				strm << "\\StringFileInfo\\" << std::hex << std::setw(4) << std::setfill('0') << language
					<< std::hex << std::setw(4) << std::setfill('0') << codePage << "\\ProductVersion";

				TCHAR* messageBuffer = nullptr;
				UINT messageSize = 0;
				if (!VerQueryValue(buffer.data(), strm.str().c_str(), reinterpret_cast<LPVOID*>(&messageBuffer), &messageSize) ||
					messageSize < sizeof(TCHAR)) {
					return "Unknown";
				}

				return std::string(messageBuffer, messageBuffer + messageSize / sizeof(TCHAR));
			}

		}
	}
}
