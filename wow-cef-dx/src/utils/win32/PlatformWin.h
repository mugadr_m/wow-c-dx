#pragma once

#include "utils/Platform.h"
#include "utils/Logger.h"

namespace wowc {
	namespace utils {
		namespace win32 {
			class PlatformWin : public Platform {
				static Logger<PlatformWin> mLogger;

				std::string getWindowsVersion();
			public:
				void initialize() final;
				bool browseForFolder(std::string& path) final;
			};
		}
	}
}
