#include "stdafx.h"
#include "CommandLine.h"

namespace wowc {
	namespace utils {

		void CommandLine::postConstruct(const cdi::ApplicationContextPtr& /*context*/) {
			mCommandLine = CefCommandLine::CreateCommandLine();
			mCommandLine->InitFromString(GetCommandLine());
		}

		std::string CommandLine::getOption(const std::string& name, const std::string& defaultValue) const {
			if(!mCommandLine->HasSwitch(name)) {
				return defaultValue;
			}

			return mCommandLine->GetSwitchValue(name);
		}
	}
}