#pragma once

#include <boost/timer/timer.hpp>

namespace wowc {
	namespace utils {
		class String {

		public:
			static std::string toMultibyte(const std::wstring& unicode);
			static std::wstring toUnicode(const std::string& multibyte);
			static std::vector<std::string> split(const std::string& string, char delim);
			static std::string trimLeft(const std::string& string);
			static std::string trimRight(const std::string& string);
			static std::string trim(const std::string& string);

			static std::string toLower(const std::string& string);
			static std::string toUpper(const std::string& string);

			static std::string replaceAll(const std::string& string, const std::string& old, const std::string& replace);

			String() = delete;
			String(const String&) = delete;
			String(String&&) = delete;

			~String() = delete;

			void operator = (const String&) = delete;
			void operator = (String&&) = delete;
		};
	}
}