#pragma once
#include <document.h>
#include "Logger.h"

namespace wowc {
    namespace utils {
        class GetPropertyEvent {
			static Logger<GetPropertyEvent> mLogger;

			std::string mKey;
			std::string mValue;

        public:
            explicit GetPropertyEvent(std::string key) : mKey(std::move(key)) {
                
            }

            const std::string& getKey() const {
                return mKey;
            }

            const std::string& getValue() const {
                return mValue;
            }

            void setValue(const std::string& value) {
                mValue = value;
            }

			void toJson(rapidjson::Document& document) const {}
			rapidjson::Document getResponseJson() const;
			static GetPropertyEvent fromJson(const rapidjson::Document& document);
        };

        class SetPropertyEvent {
			static Logger<SetPropertyEvent> mLogger;

            std::string mKey;
            std::string mValue;

        public:
            SetPropertyEvent(std::string key, std::string value) :
                mKey(std::move(key)), mValue(std::move(value)) {

            }

            const std::string& getKey() const {
                return mKey;
            }

            const std::string& getValue() const {
                return mValue;
            }

			void toJson(rapidjson::Document& document) const {}
			rapidjson::Document getResponseJson() const { return rapidjson::Document(); }
			static SetPropertyEvent fromJson(const rapidjson::Document& document);
        };
    }
}
