#pragma once

namespace wowc {
	namespace utils {
		SHARED_FWD(Platform);

		class Platform abstract {
		protected:
			Platform() = default;
			Platform(const Platform&) = delete;
			Platform(Platform&&) = delete;

			void operator = (Platform&) = delete;
			void operator = (Platform&&) = delete;

		public:
			virtual ~Platform() = default;

			virtual void initialize() = 0;

			virtual bool browseForFolder(std::string& outPath) = 0;
			virtual void asyncBrowseForFolder(std::function<void(bool, std::string)> callback);

			static PlatformPtr getOrCreate();
		};
	}
}