#pragma once
#include "cdi/ApplicationContext.h"
#include <unordered_map>
#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>
#include "PropertyEvents.h"
#include "ui/events/EventManager.h"

namespace wowc {
    namespace utils {
        SHARED_FWD(PropertyStore);

        class AbstractPropertyBinding {
        protected:
            bool mHasChanged = false;
            std::size_t mPropertyKey = 0;
            PropertyStorePtr mPropertyStore;

        public:
            explicit AbstractPropertyBinding(PropertyStorePtr store, std::size_t key);

            virtual ~AbstractPropertyBinding() {
            }


            bool hasChanged() const {
                return mHasChanged;
            }

            void setHasChanged(const bool hasChanged) {
                mHasChanged = hasChanged;
            }
        };

        template <typename T>
        class PropertyBinding : public AbstractPropertyBinding {
            T mValue{};

        public:
            explicit PropertyBinding(PropertyStorePtr store, std::size_t key) :
                AbstractPropertyBinding(std::move(store), key) {

            }

            T getValue();

            void setValue(const T& value);
        };

        SHARED_PTR(AbstractPropertyBinding);

        template <typename T>
        using PropertyBindingPtr = std::shared_ptr<PropertyBinding<T>>;

        class PropertyStore : public std::enable_shared_from_this<PropertyStore> {
            enum class PropertyType {
                INT = 0,
                FLOAT = 1,
                STRING = 2,
                BOOL = 3
            };

            static constexpr int typeVal(PropertyType type) {
                return static_cast<int>(type);
            }

            Logger<PropertyStore> mLogger;

            std::hash<std::string> mHasher;

            std::unordered_map<std::size_t, boost::any> mProperties;
            std::unordered_map<std::size_t, const std::type_info*> mTypeInfo;

            std::recursive_mutex mBindingLock;
            std::multimap<std::size_t, AbstractPropertyBindingPtr> mBindings;


            std::string mUserNameToken;

            std::string getStorageFileName() const;

            template <typename T>
            void defineProperty(const std::string& name, T value = T{}) {
                const auto key = mHasher(name);
                const auto itr = mProperties.find(key);
                if (itr != mProperties.end()) {
                    mLogger.error("Property already defined: ", name);
                    throw std::exception("Property already defined");
                }

                mProperties.emplace(key, boost::any(value));
                mTypeInfo.emplace(key, &typeid(T));
            }

            template <typename T>
            void defineProperty(std::size_t hash, T value = T{}) {
                const auto itr = mProperties.find(hash);
                if (itr != mProperties.end()) {
                    mLogger.error("Property already defined: ", hash);
                    throw std::exception("Property already defined");
                }

                mProperties.emplace(hash, boost::any(value));
                mTypeInfo.emplace(hash, &typeid(T));
            }

            bool loadExisting();
            void loadDefault();

            void loadUserName();

            PropertyType getPropertyType(const std::type_info* typeInfo) const;
            std::string toString(const std::type_info* typeInfo, const boost::any& value) const;

            void propertyChanged(std::size_t key);

            void handleGetPropertyEvent(GetPropertyEvent& event);
            void handleSetPropertyEvent(SetPropertyEvent& event);

        public:
            void postConstruct(const cdi::ApplicationContextPtr& context);
			static void initUiEvents();

            void save();

            template <typename T>
            T getPropertyAs(const std::string& name) {
                return getPropertyAs<T>(mHasher(name));
            }

            template <typename T>
            T getPropertyAs(std::size_t key) {
                const auto metaData = mTypeInfo.at(key);
                if (metaData != &typeid(T)) {
                    mLogger.error("Property types do not match. Wanted: ", typeid(T).name(), " but was ",
                                  metaData->name());
                    throw std::exception("Property type mismatch");
                }

                return boost::any_cast<T>(mProperties.at(key));
            }

            template <typename T>
            PropertyBindingPtr<T> bind(const std::string& name) {
                const auto key = mHasher(name);
                const auto ret = std::make_shared<PropertyBinding<T>>(shared_from_this(), key);
                {
                    std::lock_guard<std::recursive_mutex> l(mBindingLock);
                    mBindings.emplace(key, ret);
                }
                return ret;
            }

            template<typename T>
            void writeProperty(const std::string& name, const T& value) {
                const auto key = mHasher(name);
                writeProperty(key, value);
            }

            template <typename T>
            void writeProperty(std::size_t key, const T& value) {
                const auto* metaData = mTypeInfo.at(key);
                switch (getPropertyType(metaData)) {
                case PropertyType::BOOL: {
                    mProperties[key] = boost::lexical_cast<bool>(value);
                    break;
                }

                case PropertyType::INT: {
                    mProperties[key] = boost::lexical_cast<int>(value);
                    break;
                }

                case PropertyType::STRING: {
                    mProperties[key] = boost::lexical_cast<std::string>(value);
                    break;
                }

                case PropertyType::FLOAT: {
                    mProperties[key] = boost::lexical_cast<float>(value);
                    break;
                }

                default: {
                    mLogger.error("Cannot set property of unknown type: ", metaData->name());
                    break;
                }
                }

				propertyChanged(key);
            }
        };

        template <typename T>
        T PropertyBinding<T>::getValue() {
            if(mHasChanged) {
                mHasChanged = false;
                mValue = mPropertyStore->getPropertyAs<T>(mPropertyKey);
            }

            return mValue;
        }

        template <typename T>
        void PropertyBinding<T>::setValue(const T& value) {
            mPropertyStore->writeProperty(mPropertyKey, value);
        }

    }
    }
