#include "stdafx.h"
#include "Platform.h"
#include "win32/PlatformWin.h"
#include <boost/thread/future.hpp>

namespace wowc {
	namespace utils {
	    void Platform::asyncBrowseForFolder(std::function<void(bool, std::string)> callback) {
            async(boost::launch::async, [callback, this]() {
				std::string path;
				const auto success = browseForFolder(path);
				callback(success, path);
			});
	    }

		PlatformPtr Platform::getOrCreate() {
			static PlatformPtr gPlatform;

            if(gPlatform == nullptr) {
#ifdef _WIN32
				gPlatform = std::make_shared<win32::PlatformWin>();
#endif
            }

			return gPlatform;
		}
	}
}
