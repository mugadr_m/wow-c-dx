#pragma once

namespace wowc {
    namespace utils {
		const float TILE_SIZE = 533.0f + 1.0f / 3.0f;
		const float CHUNK_SIZE = TILE_SIZE / 16.0f;
		const float UNIT_SIZE = CHUNK_SIZE / 8.0f;

		const float LIQUID_VERTEX_SIZE = CHUNK_SIZE / 8.0f;

		const float MAP_MID_POINT = TILE_SIZE * 32.0f;

		const float PI = 3.1415926f;
    }
}